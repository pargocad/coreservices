SET GLOBAL max_heap_table_size = 1024 * 1024 * 1024 * 4;
SET GLOBAL tmp_table_size = 1024 * 1024 * 1024 * 4;

DROP TABLE `abstractcomponentfunctor`;
DROP TABLE `abstractcomponentfunctorapplication`;
DROP TABLE `abstractcomponentfunctorparameter`;
DROP TABLE `certified`;
DROP TABLE `component`;
DROP TABLE `component_key`;
DROP TABLE `innercomponent`;
DROP TABLE `innercomponentexposed`;
DROP TABLE `interface`;
DROP TABLE `interfaceaction`;
DROP TABLE `interfaceparameters`;
DROP TABLE `resolution_tree`;
DROP TABLE `sessions`;
DROP TABLE `slice`;
DROP TABLE `sliceexposed`;
DROP TABLE `sourcecode`;
DROP TABLE `sourcecode_references`;
DROP TABLE `supplyparameter`;
DROP TABLE `supplyparametercomponent`;
DROP TABLE `supplyparameterparameter`;
DROP TABLE `unit`;

CREATE TABLE `abstractcomponentfunctor` (
  `id_functor_app_supertype` int(11) DEFAULT NULL,
  `library_path` varchar(150) DEFAULT NULL,
  `id_abstract` int(11) NOT NULL,
  `hash_component_UID` varchar(320) DEFAULT NULL,
  `kind` varchar(128) NOT NULL,
  PRIMARY KEY (`id_abstract`),
  UNIQUE KEY `UID` (`hash_component_UID`) USING BTREE,
  UNIQUE KEY `library_path_UNIQUE` (`library_path`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `abstractcomponentfunctorapplication` (
  `id_functor_app` int(11) NOT NULL AUTO_INCREMENT,
  `id_abstract` int(11) NOT NULL,
  `id_functor_app_next` int(11) NOT NULL DEFAULT '-1',
  `facet_index` int(11) NOT NULL DEFAULT '0',
  `quantifier` decimal(38,8) DEFAULT NULL,
  PRIMARY KEY (`id_functor_app`,`facet_index`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1047928 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `abstractcomponentfunctorparameter` (
  `id_abstract` int(11) NOT NULL,
  `id_parameter` varchar(128) NOT NULL,
  `bounds_of` int(11) DEFAULT NULL,
  `variance` varchar(128) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  `class` varchar(128) NOT NULL DEFAULT 'application',
  PRIMARY KEY (`id_abstract`,`id_parameter`) USING BTREE,
  KEY `ORDEM` (`id_abstract`,`id_parameter`,`order`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `certified` (
  `id_concrete` int(11) NOT NULL,
  `id_functor_app` int(11) NOT NULL,
  PRIMARY KEY (`id_concrete`,`id_functor_app`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `component` (
  `library_path` varchar(150) NOT NULL DEFAULT '' COMMENT 'the package where the component is placed.',
  `id_concrete` int(11) NOT NULL,
  `id_concrete_supertype` int(11) NOT NULL,
  `id_functor_app` int(11) NOT NULL,
  `id_abstract` int(11) NOT NULL,
  `hash_component_UID` varchar(320) NOT NULL DEFAULT '',
  `argument_calculator` text,
  PRIMARY KEY (`id_concrete`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `component_key` (
  `id_type` char(1) NOT NULL,
  `id` int(11) NOT NULL,
  `snk_contents` blob,
  `pub_contents` blob,
  PRIMARY KEY (`id_type`,`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `innercomponent` (
  `id_abstract_owner` int(11) NOT NULL,
  `id_inner` varchar(128) NOT NULL,
  `id_functor_app` int(11) NOT NULL,
  `id_abstract_inner` int(11) NOT NULL,
  `parameter_top` varchar(128) DEFAULT NULL,
  `transitive` int(11) DEFAULT '0',
  `public` int(11) DEFAULT '0',
  `multiple` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_abstract_owner`,`id_inner`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `innercomponentexposed` (
  `id_abstract` int(11) NOT NULL AUTO_INCREMENT,
  `id_inner_rename` varchar(128) NOT NULL DEFAULT '"',
  `id_inner_owner` varchar(128) NOT NULL DEFAULT '"',
  `id_inner` varchar(128) DEFAULT '"',
  PRIMARY KEY (`id_abstract`,`id_inner_rename`,`id_inner_owner`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=538 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `interface` (
  `id_abstract` int(11) NOT NULL,
  `id_interface` varchar(128) NOT NULL,
  `id_interface_super` varchar(128) DEFAULT NULL,
  `assembly_string` varchar(511) DEFAULT NULL COMMENT 'The assembly where the class that implements the interface is placed on.',
  `class_name` varchar(256) NOT NULL COMMENT 'The class that implements the interface.',
  `uri_source` varchar(256) NOT NULL DEFAULT '' COMMENT 'where the source code is',
  `class_nargs` int(11) NOT NULL DEFAULT '0',
  `id_interface_super_top` varchar(128) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `actions` text COMMENT '"action_1; action_2; ... action_n". Nomes de ações separados por ponto-e-vírgula para evitar outra tabela.',
  `conditions` text COMMENT '"condition_1; condition_2; ... condition_n". Nomes de condições separados por ponto-e-vírgula para evitar outra tabela.',
  `protocol` text COMMENT 'string xml',
  `is_parallel` int(11) DEFAULT '0',
  `facet` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_abstract`,`id_interface`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `interfaceaction` (
  `id_abstract` int(11) NOT NULL,
  `id_interface` varchar(128) NOT NULL,
  `partition_index` int(11) NOT NULL,
  `id_action` varchar(128) NOT NULL DEFAULT 'main',
  `protocol` text,
  `is_condition` int(11) DEFAULT '0',
  PRIMARY KEY (`id_abstract`,`id_interface`,`partition_index`,`id_action`) USING BTREE,
  KEY `id_abstract` (`id_abstract`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `interfaceparameters` (
  `id_interface` varchar(128) NOT NULL,
  `id_abstract` int(11) NOT NULL,
  `parid` varchar(128) NOT NULL,
  `varid` varchar(128) NOT NULL,
  `id_interface_parameter` varchar(80) NOT NULL,
  `id_unit_parameter` varchar(128) NOT NULL,
  `par_order` int(11) NOT NULL,
  PRIMARY KEY (`id_abstract`,`id_interface`,`parid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/* CREATE TABLE `resolution_tree` (
  `node_id` int NOT NULL AUTO_INCREMENT,
  `super_id` int DEFAULT NULL,
  `id_abstract` int DEFAULT NULL,
  `quantifier` decimal(38,8) DEFAULT NULL,
  `level` int DEFAULT NULL,
  PRIMARY KEY (`node_id`) USING BTREE,
  KEY `QUANTIFIER` (`super_id`,`quantifier`) USING BTREE,
  KEY `QUALIFIER` (`super_id`,`id_abstract`) USING BTREE
) ENGINE=MEMORY AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
*/

CREATE TABLE `resolution_tree` (
  `node_id` int NOT NULL AUTO_INCREMENT,
  `super_id` int DEFAULT NULL,
  `id_abstract` int DEFAULT NULL,
  `quantifier` decimal(38,8) DEFAULT NULL,
  `level` int DEFAULT NULL,
  PRIMARY KEY (`node_id`) USING BTREE,
  KEY `QUANTIFIER` (`super_id`,`quantifier`) USING BTREE,
  KEY `QUALIFIER` (`super_id`,`id_abstract`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sessions` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_concrete` int(11) DEFAULT NULL,
  PRIMARY KEY (`session_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `slice` (
  `id_abstract` int(11) NOT NULL DEFAULT '0',
  `id_inner` varchar(128) NOT NULL DEFAULT '"',
  `id_interface_slice` varchar(128) NOT NULL,
  `id_interface` varchar(128) NOT NULL,
  `property_name` varchar(128) NOT NULL,
  `transitive` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_abstract`,`id_inner`,`id_interface_slice`,`id_interface`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sliceexposed` (
  `id_abstract` int(11) NOT NULL DEFAULT '0',
  `id_inner_owner` varchar(128) NOT NULL,
  `id_interface_slice_owner` varchar(128) NOT NULL,
  `id_inner_original` varchar(128) NOT NULL,
  `id_interface_slice_original` varchar(128) NOT NULL,
  `id_inner` varchar(128) NOT NULL DEFAULT '"',
  `id_interface_slice` varchar(128) NOT NULL,
  `id_interface_host` varchar(128) NOT NULL,
  PRIMARY KEY (`id_abstract`,`id_inner_owner`,`id_interface_slice_owner`,`id_inner_original`,`id_interface_slice_original`,`id_inner`,`id_interface_host`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `sourcecode` (
  `type_owner` char(1) NOT NULL COMMENT '"u" (unit) or "i" (interface)',
  `id_owner` varchar(128) NOT NULL DEFAULT '',
  `contents` longblob NOT NULL,
  `file_type` varchar(30) NOT NULL COMMENT '"dll" or "exe"',
  `file_name` varchar(100) NOT NULL,
  `id_owner_container` int(11) NOT NULL,
  `file_order` int(11) NOT NULL,
  `module_name` varchar(256) NOT NULL,
  `language` varchar(256) NOT NULL DEFAULT 'C# Language',
  `deploy_type` varchar(256) NOT NULL DEFAULT 'C# Wrapper',
  PRIMARY KEY (`type_owner`,`id_owner_container`,`id_owner`,`file_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sourcecode_references` (
  `type_owner` char(1) NOT NULL,
  `id_owner_container` int(11) NOT NULL,
  `id_owner` varchar(128) NOT NULL DEFAULT '"',
  `file_name` varchar(128) NOT NULL,
  `reference` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`type_owner`,`id_owner_container`,`id_owner`,`file_name`,`reference`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `supplyparameter` (
  `id_functor_app` int(11) NOT NULL,
  `id_parameter` varchar(128) NOT NULL,
  `id_abstract` int(11) NOT NULL,
  PRIMARY KEY (`id_functor_app`,`id_parameter`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `supplyparametercomponent` (
  `id_functor_app` int(11) NOT NULL,
  `id_parameter` varchar(128) NOT NULL,
  `id_functor_app_actual` int(11) NOT NULL,
  PRIMARY KEY (`id_functor_app`,`id_parameter`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `supplyparameterparameter` (
  `id_functor_app` int(11) NOT NULL,
  `id_parameter` varchar(128) NOT NULL,
  `id_parameter_actual` varchar(128) NOT NULL,
  `freeVariable` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_functor_app`,`id_parameter`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `unit` (
  `id_concrete` int(11) NOT NULL,
  `id_unit` varchar(128) NOT NULL DEFAULT '"',
  `id_interface` varchar(128) NOT NULL,
  `id_abstract` int(11) NOT NULL DEFAULT '0',
  `id_unit_super` varchar(128) DEFAULT '"',
  `assembly_string` varchar(511) DEFAULT NULL COMMENT 'The name of the assembly where the unit class is contained.',
  `class_name` varchar(256) NOT NULL COMMENT 'The name of the class that implements the unit.',
  `uri_source` varchar(256) NOT NULL DEFAULT '' COMMENT 'where the source code is',
  `class_nargs` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_concrete`,`id_unit`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER DATABASE hashmodel CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

ALTER TABLE abstractcomponentfunctor CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE abstractcomponentfunctorapplication CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE abstractcomponentfunctorparameter CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE certified CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE component CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE component_key CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE innercomponent CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE innercomponentexposed CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE interface CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE interfaceaction CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE interfaceparameters CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE resolution_tree CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE sessions CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE slice CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE sliceexposed CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE sourcecode CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE sourcecode_references CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE supplyparameter CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE supplyparametercomponent CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE supplyparameterparameter CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE unit CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
