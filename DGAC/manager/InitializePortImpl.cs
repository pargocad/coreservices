﻿using System;
using gov.cca.ports;
using gov.cca;
using org.hpcshelf.DGAC;
using System.Collections.Generic;
using System.Threading;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.ports;
using System.Diagnostics;

namespace org.hpcshelf.ports
{
	public class InitializePortImpl : InitializePort
	{
		private ManagerComponentID mcid = null;
		private string session_id_string = null;
		private Port worker_init_port = null;
		
		public InitializePortImpl(ManagerServices services, Port worker_init_port)
		{
		   this.mcid = (ManagerComponentID) services.getComponentID();
		   this.session_id_string = mcid.getInstanceName();
		   this.worker_init_port = worker_init_port;
		}

		#region AutomaticSlicesPort implementation
		public void on_initialize ()
		{			
			((InitializePort)worker_init_port).on_initialize ();
		}
		#endregion

 
		#region AutomaticSlicesPort implementation
		public void after_initialize ()
		{
			((InitializePort)worker_init_port).after_initialize ();
		}
		#endregion

	}
}

