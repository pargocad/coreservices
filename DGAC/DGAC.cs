﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using org.hpcshelf.exception;
using org.hpcshelf.database;
using org.hpcshelf.DGAC.utils;
using gov.cca;
using gov.cca.ports;
using HPE_DGAC_LoadDB;
using Microsoft.CSharp;
using SAFeSWL;
//using XMLComponentHierarchy;

//namespace org.hpcshelf
namespace org.hpcshelf
{
    namespace DGAC
    {
        public class Backend
        {
            public static WorkerObject worker_framework = null;

            public Backend()
            {
                Console.WriteLine("DGAC is up and running.");
            }

            #region FRAMEWORK_INSTANTIATION

            public static AbstractFramework getFrameworkInstance()
            {
                Console.WriteLine("getFrameworkInstance 1 !!!");
                //TcpChannel channel = new TcpChannel(Constants.MANAGER_PORT/*prop, client_provider*/);
                //ChannelServices.RegisterChannel(channel);

                //RemotingConfiguration.ApplicationName = Constants.MANAGER_SERVICE_NAME;
                //ManagerObject obj = ManagerObject.SingleManagerObject;
                ManagerObject obj = new ManagerObject();
                return (gov.cca.AbstractFramework)obj;
            }



            public static AbstractFramework getFrameworkWorkerInstanceLocal()
            {
                WorkerObject wo = null;

                wo = new WorkerObject();
                Console.WriteLine("wo is null ? " + wo == null);
                wo.sayHi();

                return wo;
            }



            #endregion FRAMEWORK_INSTANTIATION


            #region DEPLOY


            private static int registerAbstractComponent(ComponentType ct, byte[] snk_contents, IDbConnection dbcon)
            {
                AbstractComponentFunctor cAbs = null;

                LoaderAbstractComponent abstractloader = new LoaderAbstractComponent();
                HashComponent cAbs_ = null;

                IList<ExternalLibraryType> externalLibrary = null;

                string library_path = ct.header.packagePath + "." + ct.header.name;

                bool component_exists = abstractloader.componentExists(library_path, out cAbs_, dbcon);
                if (!component_exists)
                {
                    cAbs = (AbstractComponentFunctor)abstractloader.loadComponent(ct, snk_contents, ref externalLibrary, dbcon);
                }
                else
                {
                    cAbs = (AbstractComponentFunctor)cAbs_;
                    IList<org.hpcshelf.database.Component> cList = cdao.retrieveThatImplements(cAbs.Id_abstract, dbcon);
                    Console.Error.WriteLine("Abstract component " + ct.header.packagePath + "." + ct.header.name + " is already deployed. Updating sources ...");
                    abstractloader.updateSources(ct, cAbs, dbcon);

                    externalLibrary = new List<ExternalLibraryType>();
                    foreach (Object o in ct.componentInfo)
                    {
                        if (o is ExternalLibraryType)
                        {
                            if (externalLibrary == null) externalLibrary = new List<ExternalLibraryType>();
                            externalLibrary.Add((ExternalLibraryType)o);
                        }
                    }
                }

                return cAbs != null ? cAbs.Id_abstract : -1;
            }


            public static int registerAbstractComponentTransaction(ComponentType ct, byte[] snk_contents, IDbConnection dbcon)
            {
                int res = -1;

                IDbTransaction dbtrans = dbcon.BeginTransaction();

                try
                {
                    res = registerAbstractComponent(ct, snk_contents, dbcon);

                    dbtrans.Commit(); // if it is ok, commit ...

                    Console.Error.WriteLine("commited !");
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(e.StackTrace);
                    dbtrans.Rollback();
                    throw e;
                }

                return res;
            }

            /*           public static string sendToCompile(SourceDeployer deployer, DeployArguments.DeployArgumentsUnitType unitToCompile, byte[] key_file_contents, string userName, string password, string curDir)
                        {
                            //string cuid = unitToCompile.cuid;
                            string library_path = unitToCompile.library_path;
                            string moduleName = unitToCompile.moduleName;
                            DeployArguments.SourceContentsFile[] sourceContents = unitToCompile.sourceContents;
                            string unitName = unitToCompile.unitId;




                            Console.Error.WriteLine("sendToCompile " + moduleName);

                            string publicKey = sendCompileCommandToWorker(library_path,
                                                                          moduleName,
                                                                          deployer,
                                                                          sourceContents,
                                                                          unitToCompile.references,
                                                                          key_file_contents,
                                                                          userName,
                                                                          password,
                                                                          curDir);

                            return publicKey;
                        }
            */
            //       public static void sendToCompile(SourceDeployer deployer, ICollection<LoaderApp.InfoCompile> infoCompile)
            //       {
            //           sendToCompile(deployer, infoCompile, null, null, null);
            //       }

            /*       public static void sendToCompile(SourceDeployer deployer, ICollection<LoaderApp.InfoCompile> infoCompile, string userName, string password, string curDir)
                   {
                       Console.WriteLine("sendToCompile " + infoCompile);

                       foreach (LoaderApp.InfoCompile unitToCompile in infoCompile)
                       {
                           int id = unitToCompile.id;
                           string cuid = unitToCompile.cuid;
                           string library_path = unitToCompile.library_path;
                           string moduleName = unitToCompile.moduleName;
                           Tuple<string, string>[] sourceContents = unitToCompile.sourceContents;
                           string unitName = unitToCompile.unitId;

                           Console.Error.Write(moduleName + ", ");

                           string publicKey = sendCompileCommandToWorker(library_path,
                                                                         moduleName,
                                                                         deployer,
                                                                         sourceContents,
                                                                         unitToCompile.references,
                                                                         userName,
                                                                         password,
                                                                         curDir);


                       }

                   }
       */
            /*            public static void updateConfiguration(int id_abstract, byte[] hcl_data)
                        {
                            try
                            {
                                DBConnector.openConnection();
                                DBConnector.beginTransaction();

                                AbstractComponentFunctor acf = acfdao.retrieve(id_abstract);

                                string filename = acf.Library_path;

                                string path = Constants.PATH_TEMP_WORKER + filename + ".hcl";

                                FileUtil.saveByteArrayIntoFile(hcl_data, path);
                                DBConnector.commitTransaction();

                            }
                            catch (Exception e)
                            {
                                Console.Error.WriteLine("Rolling back transaction ! Exception: " + e.Message);
                                Console.Error.WriteLine(e.StackTrace);
                                DBConnector.rollBackTransaction();
                                throw e;
                            }
                            finally
                            {
                                DBConnector.endTransaction();
                                DBConnector.closeConnection();
                            }
                        }
            */

            static private int registerConcreteComponent(ComponentType ct, byte[] snk_contents, IDbConnection dbcon)
            {
                org.hpcshelf.database.Component cConc = null;

                LoaderConcreteComponent concreteloader = new LoaderConcreteComponent();
                IList<org.hpcshelf.database.Component> cConcList = null;

                IList<ExternalLibraryType> externalLibrary = null;

                string library_path = ct.header.packagePath + "." + ct.header.name;

                bool component_exists = concreteloader.componentExists(library_path, out cConcList, dbcon);
                if (!component_exists)
                {
                    IDictionary<string, SupplyParameter> var_context = new ConcurrentDictionary<string, SupplyParameter>();
                    cConc = (org.hpcshelf.database.Component)concreteloader.loadComponent(ct, snk_contents, ref externalLibrary, var_context, dbcon);

                    concreteloader.componentExists(library_path, out cConcList, dbcon);
                    Resolution.addToResolutionTree(cConc, dbcon);
                }
                else
                {

                    Console.Error.WriteLine("Concrete component " + ct.header.packagePath + "." + ct.header.name + " is already deployed. Updating ...");
                    cConc = cConcList[0];
                    concreteloader.updateSources(ct, cConcList, dbcon);

                    Resolution.addToResolutionTree(cConc, dbcon);

                    externalLibrary = new List<ExternalLibraryType>();
                    foreach (Object o in ct.componentInfo)
                    {
                        if (o is ExternalLibraryType)
                        {
                            if (externalLibrary == null) externalLibrary = new List<ExternalLibraryType>();
                            externalLibrary.Add((ExternalLibraryType)o);
                        }
                    }
                }


                return cConc != null ? cConc.Id_concrete : -1;
            }


            public static int registerConcreteComponentTransaction(ComponentType ct, byte[] snk_contents, IDbConnection dbcon)
            {
                int res = -1;

                IDbTransaction dbtrans = dbcon.BeginTransaction();

                try
                {
                    res = registerConcreteComponent(ct, snk_contents, dbcon);

                    dbtrans.Commit(); // if it is ok, commit ...

                    Console.Error.WriteLine("commited !");
                }
                catch (Exception e)
                {
                    dbtrans.Rollback();
                    throw e;
                }

                return res;
            }

            #endregion DEPLOY


            #region RUN


            internal static IDictionary<string, ISession> open_sessions = new ConcurrentDictionary<string, ISession>();

            public interface ISession
            {
                string SessionID { get; }
                gov.cca.Services Services { get; }
            }

            private class Session : ISession
            {
                private string session_id;
                private gov.cca.Services services;
                public Session(string session_id, gov.cca.Services services)
                {
                    this.session_id = session_id;
                    this.services = services;
                }
                public string SessionID { get { return session_id; } }
                public gov.cca.Services Services { get { return services; } }
            }

            public static ISession startSession(string session_id)
            {
                try
                {
                    TypeMapImpl properties = new TypeMapImpl();

                    AbstractFramework frw = getFrameworkInstance();
                    Services frwServices = frw.getServices(session_id, "Session", properties);

                    // Builder Service Port
                    frwServices.registerUsesPort(Constants.BUILDER_SERVICE_PORT_NAME, Constants.BUILDER_SERVICE_PORT_TYPE, properties);

                    // GoPort Service Port
                    frwServices.registerUsesPort(Constants.GO_PORT_NAME, Constants.GO_PORT_TYPE, properties);

                    open_sessions[session_id] = new Session(session_id, frwServices);

                    return open_sessions[session_id];
                }
                catch (Exception e)
                {
                    Console.WriteLine("## Exception: " + e.Message);
                    Console.WriteLine("## Inner Exception: " + (e.InnerException == null ? "NULL" : e.InnerException.Message));
                    Console.WriteLine("## Trace: " + e.StackTrace);
                    throw e;
                }
            }



            public static string[] getPorts(string session_id, string instance_id)
            {
                Services frwServices = open_sessions[session_id].Services;

                BuilderService bsPort = (BuilderService)frwServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);
                ComponentID sid = frwServices.getComponentID();

                string[] uses_ports = bsPort.getUsedPortNames(sid);

                return uses_ports;
            }


            public static void finishSession(string session_id)
            {
                try
                {
                    gov.cca.Services frwServices = open_sessions[session_id].Services;

                    frwServices.unregisterUsesPort(Constants.BUILDER_SERVICE_PORT_NAME);

                    open_sessions.Remove(session_id);

                    Console.WriteLine("Finishing");
                }
                catch (Exception e)
                {
                    Console.WriteLine("## Exception: " + e.Message);
                    Console.WriteLine("## Inner Exception: " + (e.InnerException == null ? "NULL" : e.InnerException.Message));
                    Console.WriteLine("## Trace: " + e.StackTrace);
                    throw e;
                }
            }

            public static ICollection<string> getSessions()
            {
                return open_sessions.Keys;
            }

            public static int session_id = -1;

            #endregion RUN

            #region DATABASE

            private static AbstractComponentFunctorDAO acfdao_ = null;
            private static AbstractComponentFunctorApplicationDAO acfadao_ = null;
            private static AbstractComponentFunctorParameterDAO acfpdao_ = null;
            private static InterfaceDAO idao_ = null;
            private static UnitDAO udao_ = null;
            private static InterfaceParameterDAO ipdao_ = null;
            private static InterfaceActionDAO iadao_ = null;
            private static SupplyParameterDAO spdao_ = null;
            private static SupplyParameterComponentDAO spcdao_ = null;
            private static SupplyParameterParameterDAO sppdao_ = null;
            private static ComponentDAO cdao_ = null;
            private static CertifiedDAO certified_dao_ = null;
            private static InnerComponentDAO icdao_ = null;
            private static InnerComponentExposedDAO icedao_ = null;
            private static SliceDAO sdao_ = null;
            private static QuantifierDAO qdao_ = null;
            private static SourceCodeDAO scdao_ = null;
            private static SourceCodeReferenceDAO scrdao_ = null;
            private static SliceExposedDAO sedao_ = null;
            private static ComponentKeyDAO ckdao_ = null;
            private static ResolutionTreeDAO rtdao_ = null;

            public static AbstractComponentFunctorDAO acfdao { get { if (acfdao_ == null) acfdao_ = new AbstractComponentFunctorDAO(); return acfdao_; } }
            public static AbstractComponentFunctorApplicationDAO acfadao { get { if (acfadao_ == null) acfadao_ = new AbstractComponentFunctorApplicationDAO(); return acfadao_; } }
            public static AbstractComponentFunctorParameterDAO acfpdao { get { if (acfpdao_ == null) acfpdao_ = new AbstractComponentFunctorParameterDAO(); return acfpdao_; } }
            public static InterfaceDAO idao { get { if (idao_ == null) idao_ = new InterfaceDAO(); return idao_; } }
            public static InterfaceParameterDAO ipdao { get { if (ipdao_ == null) ipdao_ = new InterfaceParameterDAO(); return ipdao_; } }
            public static InterfaceActionDAO iadao { get { if (iadao_ == null) iadao_ = new InterfaceActionDAO(); return iadao_; } }
            public static UnitDAO udao { get { if (udao_ == null) udao_ = new UnitDAO(); return udao_; } }
            public static SupplyParameterDAO spdao { get { if (spdao_ == null) spdao_ = new SupplyParameterDAO(); return spdao_; } }
            public static SupplyParameterComponentDAO spcdao { get { if (spcdao_ == null) spcdao_ = new SupplyParameterComponentDAO(); return spcdao_; } }
            public static SupplyParameterParameterDAO sppdao { get { if (sppdao_ == null) sppdao_ = new SupplyParameterParameterDAO(); return sppdao_; } }
            public static ComponentDAO cdao { get { if (cdao_ == null) cdao_ = new ComponentDAO(); return cdao_; } }
            public static CertifiedDAO certified_dao { get { if (certified_dao_ == null) certified_dao_ = new CertifiedDAO(); return certified_dao_; } }
            public static InnerComponentDAO icdao { get { if (icdao_ == null) icdao_ = new InnerComponentDAO(); return icdao_; } }
            public static InnerComponentExposedDAO icedao { get { if (icedao_ == null) icedao_ = new InnerComponentExposedDAO(); return icedao_; } }
            public static SliceDAO sdao { get { if (sdao_ == null) sdao_ = new SliceDAO(); return sdao_; } }
            public static QuantifierDAO qdao { get { if (qdao_ == null) qdao_ = new QuantifierDAO(); return qdao_; } }
            public static SourceCodeDAO scdao { get { if (scdao_ == null) scdao_ = new SourceCodeDAO(); return scdao_; } }
            public static SourceCodeReferenceDAO scrdao { get { if (scrdao_ == null) scrdao_ = new SourceCodeReferenceDAO(); return scrdao_; } }
            public static SliceExposedDAO sedao { get { if (sedao_ == null) sedao_ = new SliceExposedDAO(); return sedao_; } }
            public static ComponentKeyDAO ckdao { get { if (ckdao_ == null) ckdao_ = new ComponentKeyDAO(); return ckdao_; } }
            public static ResolutionTreeDAO rtdao { get { if (rtdao_ == null) rtdao_ = new ResolutionTreeDAO(); return rtdao_; } }

            private static IDictionary<int, Tuple<char, char>> is_quantifier = null;

            public static Tuple<char, char> isQuantifier(int id_abstract, IDbConnection dbconn)
            {
                if (is_quantifier == null)
                {
                    is_quantifier = new ConcurrentDictionary<int, Tuple<char, char>>();
                    AbstractComponentFunctor acf1 = Backend.acfdao.retrieve_libraryPath("org.hpcshelf.quantifier.DecimalDown", dbconn);
                    is_quantifier[acf1.Id_abstract] = new Tuple<char, char>('d', '-');
                    AbstractComponentFunctor acf2 = Backend.acfdao.retrieve_libraryPath("org.hpcshelf.quantifier.DecimalUp", dbconn);
                    is_quantifier[acf2.Id_abstract] = new Tuple<char, char>('d', '+');
                    AbstractComponentFunctor acf3 = Backend.acfdao.retrieve_libraryPath("org.hpcshelf.quantifier.IntDown", dbconn);
                    is_quantifier[acf3.Id_abstract] = new Tuple<char, char>('i', '-');
                    AbstractComponentFunctor acf4 = Backend.acfdao.retrieve_libraryPath("org.hpcshelf.quantifier.IntUp", dbconn);
                    is_quantifier[acf4.Id_abstract] = new Tuple<char, char>('i', '+');
                }
                return is_quantifier.ContainsKey(id_abstract) ? is_quantifier[id_abstract] : null;
            }

            //    public static Tuple<char, char> isQuantifier(int id_abstract)
            //    {
            //       return isQuantifier(id_abstract,DBConnector.DBcon);
            //    }

            #endregion DATABASE

            #region RESOLUTION

            public static org.hpcshelf.database.Component[] resolveUnit(
                            AbstractComponentFunctorApplication acfaRef,
                            IDictionary<string, int> arguments,
                            IDictionary<string, int> argumentsTop, IDbConnection dbcon
                        )
            {
                return LoaderApp.resolveImpl(acfaRef, arguments, argumentsTop, dbcon);
            }

            public static org.hpcshelf.database.Component[] resolveUnit(AbstractComponentFunctorApplication acfaRef, IDbConnection dbcon)
            {
                return resolveUnit(acfaRef, new ConcurrentDictionary<string, int>(), new ConcurrentDictionary<string, int>(), dbcon);
            }

            public static int createFunctorAppInner(object contract_obj, IDbConnection dbcon)
            {
                return createFunctorAppInner(null, contract_obj, dbcon);
            }

            public static int createFunctorAppInner(int? id_abstract, object contract_obj, IDbConnection dbcon)
            {
                int id_functor_app = -1;

                if (contract_obj is decimal)
                {
                    AbstractComponentFunctorApplicationQuantifier acfa = new AbstractComponentFunctorApplicationQuantifier();
                    acfa.Id_abstract = id_abstract.GetValueOrDefault();
                    acfa.Quantifier = (decimal?)contract_obj;
                    id_functor_app = Backend.acfadao.insert(acfa, dbcon);
                }
                else if (contract_obj is string)
                {
                    AbstractComponentFunctorApplicationQuantifier acfa = new AbstractComponentFunctorApplicationQuantifier();
                    acfa.Id_abstract = id_abstract.GetValueOrDefault();
                    //acfa.QuantifierFunction = (string)contract_obj;
                    // TODO: acfa.QuantifierDependency;
                    id_functor_app = Backend.acfadao.insert(acfa, dbcon);
                }
                else if (contract_obj is IDictionary<string, object>)
                {
                    IDictionary<string, object> contract = (IDictionary<string, object>)contract_obj;
                    IDictionary<string, object> contract_new = new ConcurrentDictionary<string, object>(contract);

                    string library_path = (string)contract["name"];
                    AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath(library_path, dbcon);

                    AbstractComponentFunctorApplication acfa = new AbstractComponentFunctorApplication();
                    acfa.Id_abstract = acf.Id_abstract;
                    id_functor_app = Backend.acfadao.insert(acfa, dbcon);

                    contract_new.Remove("name");
                    foreach (KeyValuePair<string, object> context_argument in contract_new)
                    {
                        SupplyParameter sp = null;
                        if (context_argument.Value is IDictionary<string, object> || context_argument.Value is decimal)
                        {
                            int? id_abstract_parameter = null;
                            if (context_argument.Value is decimal)
                            {
                                AbstractComponentFunctorParameter acfp = acfpdao.retrieve(acf.Id_abstract, context_argument.Key, dbcon);
                                AbstractComponentFunctorApplication acfa_par = acfadao.retrieve(acfp.Bounds_of, dbcon);
                                AbstractComponentFunctor acf_par = acfdao.retrieve(acfa_par.Id_abstract, dbcon);
                                //while (acf_par.Id_functor_app_supertype > 0)
                                //{
                                //    acfa_par = acfadao.retrieve(acf_par.Id_functor_app_supertype);
                                //    acf_par = acfdao.retrieve(acfa_par.Id_abstract);
                                //}
                                id_abstract_parameter = acf_par.Id_abstract;
                            }

                            int id_functor_app_argument = createFunctorAppInner(id_abstract_parameter, context_argument.Value, dbcon);
                            SupplyParameterComponent spc;
                            sp = spc = new SupplyParameterComponent();
                            spc.Id_functor_app = id_functor_app;
                            spc.Id_abstract = acf.Id_abstract;
                            spc.Id_parameter = context_argument.Key;
                            spc.Id_functor_app_actual = id_functor_app_argument;
                        }
                        else if (context_argument.Value is string)
                        {
                            SupplyParameterParameter spp;
                            sp = spp = new SupplyParameterParameter();
                            spp.Id_functor_app = id_functor_app;
                            spp.Id_abstract = acf.Id_abstract;
                            spp.Id_parameter = context_argument.Key;
                            spp.Id_argument = (string)context_argument.Value;
                        }
                        else
                            Console.WriteLine("(createFunctorAppInnner -- UNEXPECTED TYPE !!!");

                        Backend.spdao.insert(sp, dbcon);
                    }
                }

                return id_functor_app;
            }


            public static Unit takeUnit(org.hpcshelf.database.Component c, string id_interface, IDbConnection dbcon)
            {
                string id_unit = null;
                Interface i2 = idao.retrieveSuper(c.GetId_abstract(dbcon), id_interface, dbcon);
                id_unit = i2.Id_interface;
                return org.hpcshelf.DGAC.Backend.udao.retrieve(c.Id_concrete, id_unit, dbcon);
            }

            public static org.hpcshelf.database.Unit takeUnit2(
                org.hpcshelf.database.Component c,
                string id_interface, IDbConnection dbcon)
            {
                string id_unit = null;
                Interface i2 = org.hpcshelf.DGAC.Backend.idao.retrieveSuper(c.GetId_abstract(dbcon), id_interface, dbcon);
                foreach (Interface i in org.hpcshelf.DGAC.Backend.idao.list(c.GetId_abstract(dbcon), dbcon))
                {
                    string[] interface_id_super_top_list_2 = Interface.splitIDs(i2.GetId_interface_super_top(dbcon));
                    string[] interface_id_super_top_list_1 = Interface.splitIDs(i.GetId_interface_super_top(dbcon));
                    bool found = false;
                    foreach (string interface_id_super_top_2 in interface_id_super_top_list_2)
                    {
                        found = false;
                        foreach (string interface_id_super_top_1 in interface_id_super_top_list_1)
                        {
                            if (interface_id_super_top_2.Equals(interface_id_super_top_1))
                                found = true;
                        }
                        if (!found)
                            break;
                    }
                    if (found)
                    {
                        id_unit = i.Id_interface;
                        break;
                    }
                }

                return org.hpcshelf.DGAC.Backend.udao.retrieve(c.Id_concrete, id_unit, dbcon);
            }

            #endregion RESOLUTION

            public static IList<string> fetchCertificationBindingIDs(int id_abstract_system, IDbConnection dbcon)
            {
                IList<string> result = new List<string>();
                //IDbConnection dbcon = DBConnector.DBcon;
                IDbCommand dbcmd = dbcon.CreateCommand();
                string sql = "SELECT DISTINCT ice.id_inner_rename as certification_binding_id FROM innercomponentexposed as ice, innercomponent as ic, abstractcomponentfunctor as acf " +
                             "where ice.id_abstract = ic.id_abstract_owner and ice.id_inner_owner = ic.id_inner and ic.id_abstract_inner = acf.id_abstract and " +
                             "ice.id_abstract=" + id_abstract_system + " and " +
                             "acf.kind like '" + Constants.KIND_CERTIFIER_NAME + "'";
                dbcmd.CommandText = sql;
                IDataReader reader; /* lock (DBConnector.db_lock) */
                { reader = dbcmd.ExecuteReader(); }
                while (reader.Read())
                {
                    string certification_binding_id = (string)reader["certification_binding_id"];
                    result.Add(certification_binding_id);
                }//if

                // clean up
                reader.Close();
                reader = null;
                dbcmd.Dispose();
                dbcmd = null;
                return result;
            }

            #region CALCULATE_CLASS_PARAMETERS

            public static string[] calculateActualClassParameteres2(Interface i, int id_functor_app, IDictionary<string, int> arguments_top, IDictionary<string, int> arguments, IDbConnection dbcon)
            {
                IList<InterfaceParameter> ipars = ipdao.list(i.Id_abstract, i.Id_interface, dbcon);
                string[] result = new string[ipars.Count];

                foreach (InterfaceParameter ipar in ipars)
                {
                    int id_functor_app_parameter;
                    string parid = ipar.ParId;
                    if (arguments.TryGetValue(parid, out id_functor_app_parameter) ||
                        arguments.TryGetValue(parid + "#" + id_functor_app, out id_functor_app_parameter))
                    {
                        AbstractComponentFunctorApplication acfa_parameter = acfadao.retrieve(id_functor_app_parameter, dbcon);
                        AbstractComponentFunctor acf_parameter = acfdao.retrieve(acfa_parameter.Id_abstract, dbcon);
                        Interface iPar = idao.retrieveSuper(acf_parameter.Id_abstract, ipar.Id_unit_parameter, dbcon);

                        IDictionary<string, int> arguments_new;
                        determineArguments(arguments_top, arguments, id_functor_app_parameter, out arguments_new, dbcon);

                        string[] parameters = calculateActualClassParameteres2(iPar, id_functor_app_parameter, arguments_top, arguments_new, dbcon);

                        // Build a type from iPar applied to parType.

                        string assembly_string = iPar.Assembly_string;      // where to found the DLL (retrieve from the component).
                        string class_name = iPar.Class_name;  // the name of the class inside the DLL.
                        int class_nargs = iPar.Class_nargs;

                        string closedT = class_name + (class_nargs > 0 ? "`" + class_nargs + "[" : "");
                        for (int ix = 0; ix < class_nargs; ix++)
                            closedT += parameters[ix] + (ix < class_nargs - 1 ? "," : "]");

                        result[ipar.ParOrder] = "[" + closedT + "," + assembly_string + "]";

                        //                            Console.WriteLine("CALCULATE ACTUAL CLASS PARAMETERS 2 (then): {0} {1} {2} {3}", i.Id_abstract, i.Id_interface, parid, ipar.ParOrder);
                    }
                    // else 
                    //   Console.WriteLine("CALCULATE ACTUAL CLASS PARAMETERS 2 (else): {0} {1} {2}", i.Id_abstract, i.Id_interface, parid);

                }

                IList<string> result_2 = new List<string>();
                for (int j = 0; j < result.Length; j++)
                    if (result[j] != null)
                        result_2.Add(result[j]);
                string[] result_3 = new string[result_2.Count];
                result_2.CopyTo(result_3, 0);

                return result_3;
            }


            public static void calculateActualParams2(
                IDictionary<string, int> arguments_top,
                IDictionary<string, int> arguments,
                AbstractComponentFunctorApplication acfaRef,
                string id_interface,
                out string[] actualParams, IDbConnection dbcon)
            {

                IDictionary<string, int> arguments_new;
                determineArguments(arguments_top, arguments, acfaRef.Id_functor_app, out arguments_new, dbcon);

                IDictionary<string, int> arguments_filter = arguments_new;  // new ConcurrentDictionary<string, int>();
                                                                            //foreach (string par_id in arguments_new.Keys)
                                                                            //{
                                                                            //    if ((par_id.Contains("#") && arguments.ContainsKey(par_id)) || arguments.ContainsKey(par_id))
                                                                            //        arguments_filter[par_id] = arguments_new[par_id];
                                                                            //}

                Interface i = Backend.idao.retrieveSuper(acfaRef.Id_abstract, id_interface, dbcon);
                actualParams = calculateActualClassParameteres2(i, acfaRef.Id_functor_app, arguments_top, arguments_filter, dbcon);
            }

            public static void calculateGenericClassName2(
               Unit u,
               string[] actualParams,
               out string type, IDbConnection dbcon)
            {
                string assembly_string = u.Assembly_string;      // where to found the DLL (retrieve from the component).
                string class_name = u.Class_name;  // the name of the class inside the DLL.
                int class_nargs = u.Class_nargs;

                for (int i = 0; i < actualParams.Length; i++)
                {
                    string ttt = actualParams[i];
                    if (ttt == null)
                        actualParams[i] = fetchTypeConstraint2(u, i, dbcon);
                }

                if (class_nargs > actualParams.Length)
                {
                    Console.WriteLine("class_nargs={0} -- actualParams.Length={1}", class_nargs, actualParams.Length);
                    for (int ix = 0; ix < actualParams.Length; ix++)
                        Console.WriteLine("actualParams[{0}]={1}", ix, actualParams[ix]);
                }

                string closedT = class_name + (class_nargs > 0 ? "`" + class_nargs + "[" : "");
                for (int ix = 0; ix < class_nargs; ix++)
                    closedT += actualParams[ix] + (ix < class_nargs - 1 ? "," : "]");

                type = closedT;
            }


            public static string fetchTypeConstraint2(Unit u, int i, IDbConnection dbcon)
            {
                org.hpcshelf.database.Component c = cdao.retrieve(u.Id_concrete, dbcon);
                AbstractComponentFunctor acf = acfdao.retrieve(c.GetId_abstract(dbcon), dbcon);

                InterfaceParameter ip = ipdao.retrieveByOrder(acf.Id_abstract, u.Id_interface, i, dbcon);

                SupplyParameterComponent spc = (SupplyParameterComponent)spdao.retrieve(ip.ParId, c.Id_functor_app, dbcon);
                AbstractComponentFunctorApplication acfa = acfadao.retrieve(spc.Id_functor_app_actual, dbcon);

                Interface iPar = idao.retrieveSuper(acfa.Id_abstract, ip.Id_unit_parameter, dbcon);

                string assembly_string = iPar.Assembly_string;      // where to found the DLL (retrieve from the component).
                string class_name = iPar.Class_name;  // the name of the class inside the DLL.
                int class_nargs = iPar.Class_nargs;
                string strType = class_name + (class_nargs > 0 ? "`" + class_nargs : "");

                return strType;
            }

            public static void determineArguments(IDictionary<string, int> arguments, int id_functor_app_parameter, out IDictionary<string, int> arguments_new, IDbConnection dbcon)
            {
                determineArguments(new ConcurrentDictionary<string, int>(), arguments, id_functor_app_parameter, out arguments_new, dbcon);
            }

            public static void determineArguments(IDictionary<string, int> argumentsTop, IDictionary<string, int> arguments, int id_functor_app_parameter, out IDictionary<string, int> arguments_new, IDbConnection dbcon)
            {
                determineArguments(0, argumentsTop, arguments, id_functor_app_parameter, out arguments_new, dbcon);
            }

            public static void determineArguments(int id_functor_app_top, IDictionary<string, int> argumentsTop, IDictionary<string, int> arguments, int id_functor_app, out IDictionary<string, int> arguments_new, IDbConnection dbcon)
            {
                //   Console.WriteLine("START determineArguments " + id_functor_app);

                arguments_new = new ConcurrentDictionary<string, int>(); ;

                foreach (KeyValuePair<string, int> parameter in arguments)
                {
                    //       Console.WriteLine("traversing arguments : " + parameter.Key);
                    if (parameter.Key.Contains("#"))
                    {
                        arguments_new.Add(parameter);
                        //           Console.WriteLine("adding parameter key=" + parameter.Key + ", value=" + parameter.Value);
                    }
                }

                AbstractComponentFunctorApplication acfa = acfadao.retrieve(id_functor_app, dbcon);
                IList<AbstractComponentFunctorParameter> acfp_list = acfpdao.list(acfa.Id_abstract, dbcon);
                foreach (AbstractComponentFunctorParameter acfp in acfp_list)
                    arguments_new[acfp.Id_parameter] = acfp.Bounds_of;

                ICollection<SupplyParameter> spcList = spdao.list(id_functor_app, dbcon);

                // TODO: IT IS IGNORING PARAMETERS SUPPLIED IN SUBTYPES ... if a inner component is a subtype, it cannot see the parameters.
                foreach (SupplyParameter sp in spcList)
                {
                    if (sp is SupplyParameterParameter)
                    {
                        SupplyParameterParameter spp = (SupplyParameterParameter)sp;
                        int id_functor_app_actual;
                        bool achou = arguments.TryGetValue(spp.Id_argument, out id_functor_app_actual) ||
                                     argumentsTop.TryGetValue(spp.Id_argument, out id_functor_app_actual) ||
                                     arguments.TryGetValue(spp.Id_parameter + "#" + spp.Id_functor_app, out id_functor_app_actual);
                        //   Console.WriteLine("TESTING " + spp.Id_argument + " : " + achou + " , " + id_functor_app_actual);
                        if (achou)
                        {
                            arguments_new[spp.Id_parameter] = id_functor_app_actual;
                            //     Console.WriteLine("adding parameter (determineArguments) 1 key=" + spp.Id_parameter + "; value=" + id_functor_app_actual);
                        }
                    }
                    else if (sp is SupplyParameterComponent)
                    {
                        SupplyParameterComponent spc = (SupplyParameterComponent)sp;
                        arguments_new[spc.Id_parameter] = spc.Id_functor_app_actual;

                        //   Console.WriteLine("adding parameter (determineArguments) 2 key=" + spc.Id_parameter + "; value=" + spc.Id_functor_app_actual);

                        traverseArguments(id_functor_app_top, spc.Id_functor_app_actual, arguments, arguments_new, dbcon);
                    }
                }

                AbstractComponentFunctor acf = acfdao.retrieve(acfa.Id_abstract, dbcon);
                if (acf.Id_functor_app_supertype > 0)
                    determineArgumentsOfSubtype(arguments_new, acf.Id_functor_app_supertype, dbcon);

                // Console.WriteLine("END determineArguments " + id_functor_app);
                // foreach (KeyValuePair<string, int> parameter in arguments_new)
                // {
                //     Console.WriteLine("END determineArguments arguments_new: key=" + parameter.Key + "; value=" + parameter.Value);
                // }

            }

            private static void determineArgumentsOfSubtype(IDictionary<string, int> arguments_new, int id_functor_app_supertype, IDbConnection dbcon)
            {
                determineArgumentsOfSubtype(arguments_new, id_functor_app_supertype, 0, dbcon);
            }

            private static void determineArgumentsOfSubtype(IDictionary<string, int> arguments_new, int id_functor_app_supertype, int id_functor_app_supersupertype, IDbConnection dbcon)
            {

                AbstractComponentFunctorApplication acfa_super = acfadao.retrieve(id_functor_app_supertype, dbcon);
                IList<AbstractComponentFunctorParameter> acfp_list = acfpdao.list(acfa_super.Id_abstract, dbcon);
                foreach (AbstractComponentFunctorParameter acfp in acfp_list)
                    if (!arguments_new.ContainsKey(acfp.Id_parameter))
                        arguments_new[acfp.Id_parameter] = acfp.Bounds_of;


                ICollection<SupplyParameter> spcList = spdao.list(id_functor_app_supertype, dbcon);
                foreach (SupplyParameter sp in spcList)
                {
                    if (sp is SupplyParameterComponent)
                    {
                        SupplyParameterComponent spc = (SupplyParameterComponent)sp;
                        arguments_new[spc.Id_parameter + "#" + id_functor_app_supertype] = spc.Id_functor_app_actual;
                    }
                    else if (sp is SupplyParameterParameter)
                    {
                        SupplyParameterParameter spp = (SupplyParameterParameter)sp;
                        try
                        {
                            if (id_functor_app_supersupertype == 0 && spp.Id_parameter != spp.Id_argument)
                            {
                                if (spp.Id_argument.Equals("")) // TODO: gambiarra ...
                                    arguments_new[spp.Id_parameter + "#" + id_functor_app_supertype] = arguments_new[id_functor_app_supersupertype == 0 ? spp.Id_parameter : spp.Id_parameter + "#" + id_functor_app_supersupertype];
                                else
                                    arguments_new[spp.Id_parameter + "#" + id_functor_app_supertype] = arguments_new[id_functor_app_supersupertype == 0 ? spp.Id_argument : spp.Id_argument + "#" + id_functor_app_supersupertype];
                            }
                        }
                        catch (KeyNotFoundException e)
                        {
                            throw e;
                        }
                    }

                }
                AbstractComponentFunctorApplication acfa = acfadao.retrieve(id_functor_app_supertype, dbcon);
                AbstractComponentFunctor acf = acfdao.retrieve(acfa.Id_abstract, dbcon);
                if (acf.Id_functor_app_supertype > 0)
                    determineArgumentsOfSubtype(arguments_new, acf.Id_functor_app_supertype, id_functor_app_supertype, dbcon);
            }

            private static void traverseArguments(int id_functor_app_top,
                                                  int id_functor_app,
                                                  IDictionary<string, int> argumentsTop,
                                                  IDictionary<string, int> arguments, IDbConnection dbcon)
            {

                ICollection<SupplyParameter> spcList = spdao.list(id_functor_app, dbcon);

                foreach (SupplyParameter sp in spcList)
                {
                    if (sp is SupplyParameterParameter)
                    {
                        SupplyParameterParameter spp = (SupplyParameterParameter)sp;
                        int id_functor_app_actual;

                        int id_functor_app_supertype = id_functor_app_top;
                        bool achou = argumentsTop.TryGetValue(spp.Id_argument, out id_functor_app_actual) ||
                                     arguments.TryGetValue(spp.Id_parameter + "#" + spp.Id_functor_app, out id_functor_app_actual);
                        while (!achou && id_functor_app_top > 0)
                        {
                            AbstractComponentFunctorApplication acfa = acfadao.retrieve(id_functor_app_supertype, dbcon);
                            AbstractComponentFunctor acf = acfdao.retrieve(acfa.Id_abstract, dbcon);

                            if (acf.Id_functor_app_supertype > 0)
                            {
                                achou = argumentsTop.TryGetValue(spp.Id_argument + "#" + acf.Id_functor_app_supertype, out id_functor_app_actual);
                                id_functor_app_supertype = acf.Id_functor_app_supertype;
                            }
                            else
                                break;

                        }

                        string key = spp.Id_parameter + "#" + id_functor_app;
                        if (id_functor_app_actual > 0)
                            //if (!arguments.ContainsKey(key))
                            //{
                            arguments[key] = id_functor_app_actual;
                        //     Console.WriteLine("adding parameter (traverseParameters) 3 key=" + key + "; value=" + id_functor_app_actual + " -- " + achou + " --- " + spp.Id_argument);
                        // }
                    }
                    else if (sp is SupplyParameterComponent)
                    {
                        SupplyParameterComponent spc = (SupplyParameterComponent)sp;
                        //    Console.WriteLine("traverseArguments - recursive - " + spc.Id_parameter + "," + spc.Id_functor_app + "," + spc.Id_functor_app_actual);
                        string key = spc.Id_parameter + "#" + spc.Id_functor_app;
                        //if (!arguments.ContainsKey(key))
                        //{
                        if (spc.Id_functor_app_actual > 0)
                            arguments[key] = spc.Id_functor_app_actual;
                        //   Console.WriteLine("adding parameter (traverseParameters) 4 key=" + key + "; value=" + spc.Id_functor_app_actual);
                        //}
                        traverseArguments(id_functor_app_top, spc.Id_functor_app_actual, argumentsTop, arguments, dbcon);
                    }
                }


            }

            #endregion CALCULATE_CLASS_PARAMETERS



            public static int generateACFAforContext(int id_abstract, int id_functor_app_component, int id_functor_app_contract, int facet_index, IDbConnection dbcon)
            {
                return generateACFAforContext(id_abstract, id_functor_app_component, id_functor_app_contract, new ConcurrentDictionary<string, int>(), new ConcurrentDictionary<string, int>(), facet_index, dbcon);
            }

            public static int generateACFAforContext(int id_abstract,
                                                      int id_functor_app_component,
                                                      int id_functor_app_contract,
                                                      IDictionary<string, int> arguments,
                                                      IDictionary<string, int> argumentsTop,
                                                      int facet_index, IDbConnection dbcon)
            {
                /* TODO: ESSA FUNÇÃO PRECISA SER MELHORADA PARA CASAR NUMEROS DE ARGUMENTOS DIFERENTES ENTRE  O TIPO DO 
                         COMPONENTE retornado pela resolução e o tipo do componente aninhado, conforme as regras do HTS.
                         DO jeito que está, ambos tem que ter os mesmos argumentos */

                AbstractComponentFunctorApplication acfa = new AbstractComponentFunctorApplication();

                acfa.Id_abstract = id_abstract;
                acfadao.insert(acfa, facet_index, dbcon);

                IDictionary<string, AbstractComponentFunctorParameter> par_dict = new ConcurrentDictionary<string, AbstractComponentFunctorParameter>();
                IList<AbstractComponentFunctorParameter> par_list = acfpdao.list(id_abstract, dbcon);
                foreach (AbstractComponentFunctorParameter par in par_list)
                    par_dict[par.Id_parameter] = par;

                IDictionary<string, SupplyParameter> spDict_contract = spdao.listBy(id_functor_app_contract, dbcon);
                IDictionary<string, SupplyParameter> spDict_component = spdao.listBy(id_functor_app_component, dbcon);

                foreach (KeyValuePair<string, AbstractComponentFunctorParameter> kv in par_dict)
                {
                    SupplyParameter sp = null;
                    if (kv.Value.Variance == VarianceType.contravariant && spDict_contract.ContainsKey(kv.Key))
                        sp = spDict_contract[kv.Key];
                    else if (kv.Value.Variance == VarianceType.contravariant && !spDict_contract.ContainsKey(kv.Key)
                                                                             && spDict_component.ContainsKey(kv.Key)
                                                                             && spDict_component[kv.Key] is SupplyParameterComponent)
                        sp = spDict_component[kv.Key];
                    else if (kv.Value.Variance == VarianceType.covariant && spDict_component.ContainsKey(kv.Key)
                                                                         && spDict_component[kv.Key] is SupplyParameterComponent)
                        sp = spDict_component[kv.Key];
                    else if (kv.Value.Variance == VarianceType.covariant && spDict_contract.ContainsKey(kv.Key)
                                                                         && spDict_component.ContainsKey(kv.Key)
                                                                         && spDict_component[kv.Key] is SupplyParameterParameter)
                        sp = spDict_contract[kv.Key];

                    if (sp != null)
                    {
                        SupplyParameterComponent spc_new = new SupplyParameterComponent();
                        spc_new.Id_functor_app = acfa.Id_functor_app;
                        spc_new.Id_abstract = sp.Id_abstract;
                        spc_new.Id_parameter = sp.Id_parameter;
                        if (sp is SupplyParameterComponent)
                        {
                            SupplyParameterComponent spc = (SupplyParameterComponent)sp;
                            spc_new.Id_functor_app_actual = spc.Id_functor_app_actual;
                        }
                        else // SupplyParameterParameter
                        {
                            SupplyParameterParameter spp = (SupplyParameterParameter)sp;
                            int id_functor_app_actual;
                            if (arguments.TryGetValue(spp.Id_argument, out id_functor_app_actual)
                                || arguments.TryGetValue(spp.Id_parameter + "#" + spp.Id_functor_app, out id_functor_app_actual))
                            {
                                spc_new.Id_functor_app_actual = id_functor_app_actual;
                            }
                            else
                            {
                                // Free parameter. Using the BOUND, by default !
                                AbstractComponentFunctorParameter acfp = Backend.acfpdao.retrieve(id_abstract, sp.Id_parameter, dbcon);
                                spc_new.Id_functor_app_actual = acfp.Bounds_of;
                            }
                        }
                        spdao.insert(spc_new, dbcon);
                    }
                }


                //   Console.WriteLine("generateACFAContext -- id_functor_app_inner=" + id_functor_app + " / spList.Count=" + spList.Count);


                //  Console.WriteLine("generateACFAforContext -- END");
                return acfa.Id_functor_app;
            }


            #region RESOLUTION_AT_MANAGER

            private static void calculate_inner_facet_topology(Instantiator.UnitMappingType[] unit_mapping_inner, ref int[] facet_topology)
            {
                IList<Tuple<int, int>> facet_topology_list = new List<Tuple<int, int>>();

                int max = 0;
                foreach (Instantiator.UnitMappingType um in unit_mapping_inner)
                    if (um.facet_instance >= 0)
                    {
                        facet_topology_list.Add(new Tuple<int, int>(um.facet_instance, um.facet));
                        max = um.facet_instance > max ? um.facet_instance : max;
                    }

                facet_topology = new int[max + 1];
                for (int i = 0; i < facet_topology.Length; i++)
                    facet_topology[i] = -1;

                foreach (Tuple<int, int> p in facet_topology_list)
                    facet_topology[p.Item1] = p.Item2;
            }


            private static IList<string> binding_sequentials = new List<string>();


            private static IDictionary<string, ComponentID> cid_dictionary = new ConcurrentDictionary<string, ComponentID>();

            internal static ComponentID createComponentInstance(ResolveComponentHierarchy ctree,
                                                        int facet_instance,
                                                        string[] channel_id,
                                                        int[] channel_facet_instance,
                                                        string[] channel_facet_address,
                                                        BuilderService bsPort)
            {
                int kind = ctree.Kind;

                Instantiator.UnitMappingType[] unit_mapping = ctree.UnitMapping.Length > 1 ? ctree.UnitMapping[1] : ctree.UnitMapping[0];

                TypeMapImpl properties = new TypeMapImpl();
                properties[Constants.PORT_NAME] = ctree.PortName; //TODO ... ctree.getPortName(***) ????
                properties[Constants.UNIT_MAPPING] = unit_mapping;
                properties[Constants.FACET_INSTANCE] = facet_instance;
                properties[Constants.IGNORE] = ctree.FacetList[facet_instance].Count == 0;
                properties[Constants.COMPONENT_KIND] = kind;
                properties[Constants.CHANNEL_ID] = channel_id.Clone();
                properties[Constants.CHANNEL_FACET_INSTANCE] = channel_facet_instance.Clone();
                properties[Constants.CHANNEL_FACET_ADDRESS] = channel_facet_address.Clone();
                properties[Constants.COMPONENT_KIND] = kind;

                if (kind == Constants.KIND_BINDING)
                {
                    binding_sequentials.Add(ctree.ID);
                    properties[Constants.BINDING_SEQUENTIAL] = binding_sequentials.IndexOf(ctree.ID);
                }

                int[] facet_topology = null;
                calculate_inner_facet_topology(unit_mapping, ref facet_topology);
                properties[Constants.FACET_TOPOLOGY] = facet_topology;

                string component = ctree.ComponentChoice.Length > 1 ? ctree.ComponentChoice[1] : ctree.ComponentChoice[0];

                ctree.CID = (ManagerComponentID)bsPort.createInstance(ctree.ID, component, properties);

                cid_dictionary[ctree.ID] = ctree.CID;

                Console.WriteLine("cid_dictionary[{0}]={1}", ctree.ID, ctree.CID);

                int[] facets_host = ctree.FacetList[0].ToArray();

                foreach (KeyValuePair<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> inner in ctree.InnerComponents)
                {
                    string inner_id = inner.Key;

                    if (ctree.IsPrivate[inner_id] && inner.Value.Count > 0 && !(inner.Value.Count > 0 && (new List<int>() { Constants.KIND_QUALIFIER, Constants.KIND_QUANTIFIER, Constants.KIND_CERTIFIER, Constants.KIND_PLATFORM }.Contains(inner.Value[0].Item1.Kind))))
                    {
                        ResolveComponentHierarchy ctree_inner_0 = inner.Value[0].Item1;
                        ComponentID inner_cid = createComponentInstance(ctree_inner_0, facet_instance, channel_id, channel_facet_instance, channel_facet_address, bsPort);
                        bindComponentInstance(ctree_inner_0, facets_host, bsPort);
                        Console.WriteLine("INSTANTIATED " + inner_cid);
                    }
                }

                if (delayed_host_bindings.ContainsKey(ctree.ID))
                {
                    foreach (Thread t in delayed_host_bindings[ctree.ID])
                    {
                        t.Start();
                        t.Join();
                    }
                }

                return ctree.CID;
            }

            private static IDictionary<string, IList<Thread>> delayed_host_bindings = new ConcurrentDictionary<string, IList<Thread>>();

            internal static void bindComponentInstance(ResolveComponentHierarchy ctree, int[] facets_host, gov.cca.ports.BuilderService bsPort)
            {
                Console.WriteLine("bindComponentInstance 2 --- start ... {0} delay={1} --- {2}", ctree.ID, ctree.InnerComponents.Count, facets_host == null);

                Console.WriteLine("cid_dictionary[{0}] ? {1} {2}", ctree.ID, cid_dictionary.ContainsKey(ctree.ID), ctree.ComponentChoice[0]);

                ComponentID provider_id = cid_dictionary[ctree.ID];
                if (provider_id is ManagerComponentID)
                {
                    IList<Tuple<string, ResolveComponentHierarchy>> hosts = ctree.checkHosts(facets_host);

                    Console.WriteLine("BEFORE ITERATION HOST --- {0}", hosts.Count);
                    foreach (Tuple<string, ResolveComponentHierarchy> ctree_host_pair in hosts)
                    {
                        ResolveComponentHierarchy ctree_host = ctree_host_pair.Item2;
                        Console.WriteLine("ITERATION HOST NEW --- {0} {1}", ctree_host.ID, cid_dictionary.ContainsKey(ctree_host.ID));
                        Thread t_bind_host = new Thread(() =>
                               {
                                   ComponentID user_id = cid_dictionary[ctree_host.ID];
                                   Console.WriteLine("ctree_host.Key = {0}", ctree_host_pair.Item1);
                                   bsPort.connect(user_id, ctree_host_pair.Item1, provider_id, Constants.DEFAULT_PROVIDES_PORT_IMPLEMENTS);
                               });

                        // THE HOST COMPONENT HAS BEEN INSTANTIATED ?
                        if (cid_dictionary.ContainsKey(ctree_host.ID))
                        {
                            // IF YES, BIND !
                            t_bind_host.Start();
                            t_bind_host.Join();
                        }
                        else
                        {
                            // IF NOT, CREATE A SUSPENSION.
                            Console.WriteLine("HOST NOT FOUND {0} -- top level ?", ctree_host.ID);
                            if (!delayed_host_bindings.ContainsKey(ctree_host.ID))
                                delayed_host_bindings[ctree_host.ID] = new List<Thread>();

                            delayed_host_bindings[ctree_host.ID].Add(t_bind_host);
                        }
                    }
                    Console.WriteLine("AFTER ITERATION HOST --- #{0}", ctree.GetHashCode());
                    //if (ctree.IsPrivate[ctree_inner.getPortName(ctree.ID)])
                }
                else
                    Console.WriteLine("UNEXPECTED -- provider_id ({0}) is not ManagerComponentID -- bindComponentInstance2", ctree.ID);
            }



            public static void runSolutionComponent(Services frwServices, string cRef)
            {
                try
                {
                    ComponentID s_cid = cid_dictionary[cRef];
                    string go_port_name = cRef + "_" + Constants.GO_PORT_NAME;

                    // CONNECT THE GO PORT OF THE APPLICATION TO THE SESSION DRIVER.
                    ComponentID host_cid = frwServices.getComponentID();
                    frwServices.registerUsesPort(go_port_name, Constants.GO_PORT_TYPE, new TypeMapImpl());
                    BuilderService bsPort = (BuilderService)frwServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);
                    bsPort.connect(host_cid, go_port_name, s_cid, Constants.GO_PORT_NAME);
                    Console.WriteLine("BEFORE GO PORT " + cRef + " --- " + frwServices.GetHashCode());
                    GoPort go_port = (gov.cca.ports.GoPort)frwServices.getPort(go_port_name);
                    Console.WriteLine("AFTER GO PORT " + cRef + " --- " + frwServices.GetHashCode());

                    (new Thread(() => go_port.go())).Start();

                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("runSolutionComponent (SAFe) -- ERROR WHILE RUNNING {0}", cRef);
                    Console.Error.WriteLine(e.Message);
                    Console.Error.WriteLine(e.StackTrace);
                    if (e.InnerException != null)
                    {
                        Console.Error.WriteLine(e.InnerException.Message);
                        Console.Error.WriteLine(e.InnerException.StackTrace);
                    }
                }
            }

            #endregion RESOLUTION_AT_MANAGER    


            public static void copy_unit_mapping_to_dictionary(Instantiator.UnitMappingType[] unit_mapping,
                                                    out IDictionary<string, IList<Tuple<int, int[], int, string, string, string[]>>> unit_mapping_dict)
            {
                unit_mapping_dict = new ConcurrentDictionary<string, IList<Tuple<int, int[], int, string, string, string[]>>>();
                foreach (Instantiator.UnitMappingType unit_node in unit_mapping)
                {
                    string key = unit_node.unit_id;
                    IList<Tuple<int, int[], int, string, string, string[]>> list = null;
                    if (!unit_mapping_dict.TryGetValue(key, out list))
                    {
                        list = new List<Tuple<int, int[], int, string, string, string[]>>();
                        unit_mapping_dict.Add(key, list);
                    }

                    list.Add(new Tuple<int, int[], int, string, string, string[]>(unit_node.facet_instance, unit_node.node, unit_node.facet, unit_node.assembly_string, unit_node.class_name, unit_node.port_name));

                }
            }

            private void addRootUnitMappingToCertificationBinding()
            {
                Instantiator.UnitMappingType unit_mapping_slice;
                unit_mapping_slice = new Instantiator.UnitMappingType();
                unit_mapping_slice.facet_instance = 0;
                unit_mapping_slice.facet = 1;
                unit_mapping_slice.unit_id = "certifier";
                unit_mapping_slice.unit_id_top = "server";
            }

            public static void build_unit_mapping_of_inner(int id_abstract_host,
                                                            InnerComponent ic,
                                                             Instantiator.UnitMappingType[] unit_mapping_enclosing,
                                                             out Instantiator.UnitMappingType[] unit_mapping_inner, IDbConnection dbcon)
            {
                IDictionary<string, Instantiator.UnitMappingType> unit_mapping_save = new ConcurrentDictionary<string, Instantiator.UnitMappingType>();

                AbstractComponentFunctor acf = acfdao.retrieve(ic.Id_abstract_inner, dbcon);
                if (Constants.kindInt[acf.Kind].Equals(Constants.KIND_BINDING))
                {
                    Interface i = Backend.idao.retrieveSuper(ic.Id_abstract_inner, "root", dbcon);
                    Instantiator.UnitMappingType unit_mapping_root = new Instantiator.UnitMappingType();
                    unit_mapping_root.unit_id = "root";
                    unit_mapping_root.facet_instanceSpecified = true;
                    unit_mapping_root.facet_instance = -1;
                    unit_mapping_root.facetSpecified = true;
                    unit_mapping_root.facet = -1;
                    unit_mapping_root.node = new int[1] { -1 };
                    unit_mapping_save[unit_mapping_root.unit_id] = unit_mapping_root;
                }

                IDictionary<string, IList<Tuple<int, int[], int, string, string, string[]>>> unit_mapping_dict;
                Backend.copy_unit_mapping_to_dictionary(unit_mapping_enclosing, out unit_mapping_dict);

                IList<Slice> slice_list = Backend.sdao.listByInner(ic.Id_abstract_owner, ic.Id_inner, dbcon);

                foreach (Slice slice in slice_list)
                {
                    int[] nodes;
                    int facet_instance;
                    int facet;

                    Interface i_host = Backend.idao.retrieve(ic.Id_abstract_owner, slice.Id_interface, dbcon);
                    //    Console.WriteLine("build_unit_mapping_of_inner 0 - " + i_host.Id_interface);
                    IList<Tuple<int, int[], int, string, string, string[]>> info_list = fetch_info_list(id_abstract_host, i_host, unit_mapping_dict, dbcon);

                    if (info_list != null)
                    {
                        foreach (Tuple<int, int[], int, string, string, string[]> info in info_list)
                        {
                            facet_instance = info.Item1;
                            nodes = info.Item2;
                            facet = info.Item3;
                            Instantiator.UnitMappingType unit_mapping_slice;
                            Interface i = Backend.idao.retrieveSuper(ic.Id_abstract_inner, slice.Id_interface_slice, dbcon);
                            //     Console.WriteLine("build_unit_mapping_of_inner: " + ic.Id_abstract_inner + "," + slice.Id_interface_slice + ", i_host.Facet = " + i_host.Facet + ", i.Facet = " + i.Facet);

                            if (!unit_mapping_save.TryGetValue(i.Id_interface + facet_instance/*i_host.Facet*/, out unit_mapping_slice))
                            {
                                //        Console.WriteLine("build_unit_mapping_of_inner 2 " + (nodes == null));
                                unit_mapping_slice = new Instantiator.UnitMappingType();
                                unit_mapping_slice.facet_instance = facet_instance;
                                unit_mapping_slice.facet = i.Facet;
                                unit_mapping_slice.unit_id = i.Id_interface;
                                unit_mapping_slice.unit_id_top = i.GetId_interface_super_top(dbcon);

                                unit_mapping_save[unit_mapping_slice.unit_id + facet_instance] = unit_mapping_slice;

                                //    Console.WriteLine("build_unit_mapping_of_inner 2: add unit_id=" + unit_mapping_slice.unit_id +
                                //        ", unit_index=" + unit_mapping_slice.unit_index +
                                //                      ", facet = " + i.Facet +
                                //        ", facet_instance = " + facet_instance);
                            }
                        }
                    }
                    else
                    {
                        foreach (string k in unit_mapping_dict.Keys)
                            Console.WriteLine("Wrong configuration ! {0} -- {1}", k, i_host.GetId_interface_super_top(dbcon));

                        //throw new Exception("Wrong configuration !!!");
                    }
                }

                unit_mapping_inner = new Instantiator.UnitMappingType[unit_mapping_save.Count];
                unit_mapping_save.Values.CopyTo(unit_mapping_inner, 0);
            }

            private static IList<Tuple<int, int[], int, string, string, string[]>>
                            fetch_info_list(int id_abstract_host,
                                            Interface i_host,
                                            IDictionary<string, IList<Tuple<int, int[], int, string, string, string[]>>> unit_mapping_dict, IDbConnection dbcon)
            {
                IList<Tuple<int, int[], int, string, string, string[]>> info_list = null;
                IList<string> id_list = i_host.GetId_interface_list(dbcon);
                foreach (string id in id_list)
                    if (unit_mapping_dict.TryGetValue(id, out info_list))
                        break;

                if (info_list == null)
                {
                    string id = i_host.Id_interface;
                    foreach (KeyValuePair<string, IList<Tuple<int, int[], int, string, string, string[]>>> u in unit_mapping_dict)
                    {
                        Interface i_host_owner = idao.retrieve(id_abstract_host, u.Key, dbcon);

                        IList<string> id_owner_list = i_host_owner.GetId_interface_list(dbcon);
                        foreach (string id_owner_item in id_owner_list)
                        {
                            if (id_owner_item.Equals(id))
                                info_list = u.Value;
                        }
                    }
                }
                return info_list;
            }

            private static void set_nodes_in_unit_mapping_of_inner
                                           (int id_abstract_host,
                                            InnerComponent ic,
                                            Instantiator.UnitMappingType[] unit_mapping_enclosing,
                                            Instantiator.UnitMappingType[] unit_mapping_inner, IDbConnection dbcon)
            {

                IDictionary<string, IList<Tuple<int, int[], int, string, string, string[]>>> unit_mapping_dict;
                Backend.copy_unit_mapping_to_dictionary(unit_mapping_enclosing, out unit_mapping_dict);

                IList<Slice> slice_list = Backend.sdao.listByInner(ic.Id_abstract_owner, ic.Id_inner, dbcon);

                IDictionary<string, Instantiator.UnitMappingType> unit_mapping_save = new ConcurrentDictionary<string, Instantiator.UnitMappingType>();

                foreach (Instantiator.UnitMappingType um in unit_mapping_inner)
                    unit_mapping_save[um.unit_id_top + um.facet_instance] = um;

                foreach (Slice slice in slice_list)
                {
                    int[] nodes;
                    int facet_instance;
                    int facet;

                    Interface i_host = Backend.idao.retrieve(ic.Id_abstract_owner, slice.Id_interface, dbcon);
                    //        Console.WriteLine("build_unit_mapping_of_inner 0 - " + i_host.Id_interface_super_top);
                    IList<Tuple<int, int[], int, string, string, string[]>> info_list = fetch_info_list(id_abstract_host, i_host, unit_mapping_dict, dbcon); ;

                    if (info_list != null)
                    {
                        foreach (Tuple<int, int[], int, string, string, string[]> info in info_list)
                        {
                            facet_instance = info.Item1;
                            nodes = info.Item2;
                            facet = info.Item3;
                            Instantiator.UnitMappingType unit_mapping_slice;
                            Interface i = Backend.idao.retrieveSuper(ic.Id_abstract_inner, slice.Id_interface_slice, dbcon);
                            //        Console.WriteLine("build_unit_mapping_of_inner: " + ic.Id_abstract_inner + "," + slice.Id_interface_slice + ", i_host.Facet = " + i_host.Facet + ", i.Facet = " + i.Facet);

                            if (unit_mapping_save.TryGetValue(i.GetId_interface_super_top(dbcon) + facet_instance/*i_host.Facet*/, out unit_mapping_slice))
                            {
                                //            Console.WriteLine("build_unit_mapping_of_inner 1");
                                if (unit_mapping_slice.node == null)
                                    unit_mapping_slice.node = new int[0];
                                int[] nodes_ = new int[unit_mapping_slice.node.Length + nodes.Length];
                                unit_mapping_slice.node.CopyTo(nodes_, 0);
                                nodes.CopyTo(nodes_, unit_mapping_slice.node.Length);
                                unit_mapping_slice.node = nodes_;
                                //         Console.WriteLine("build_unit_mapping_of_inner 1: add unit_id=" + unit_mapping_slice.unit_id
                                //                                                     + ", unit_index=" + unit_mapping_slice.unit_index
                                //                                                    + ", facet=" + i_host.Facet
                                //                                                 + ", total nodes=" + nodes_.Length);
                            }
                        }
                    }
                    else
                    {
                        foreach (string k in unit_mapping_dict.Keys)
                            Console.WriteLine("Wrong configuration ! {0} -- {1}", k, i_host.GetId_interface_super_top(dbcon));
                    }
                }

                // REMOVE REPETITIONS ... (only a single instance of a unit in a node)
                IDictionary<int, int> nodes_list = new ConcurrentDictionary<int, int>();
                foreach (Instantiator.UnitMappingType um in unit_mapping_save.Values)
                {
                    nodes_list.Clear();
                    int[] nodes = um.node;
                    foreach (int n in nodes)
                    {
                        if (!nodes_list.ContainsKey(n))
                            nodes_list[n] = n;
                    }
                    int[] nodes_new = new int[nodes_list.Count];
                    nodes_list.Keys.CopyTo(nodes_new, 0);
                    um.node = nodes_new;
                }

                unit_mapping_inner = new Instantiator.UnitMappingType[unit_mapping_save.Count];
                unit_mapping_save.Values.CopyTo(unit_mapping_inner, 0);
            }


            private static IList<int> build_facet_list(InnerComponent ic, int id_abstract_owner_current, IList<int> enclosing_facet_list, IDbConnection dbcon)
            {
                IList<int> this_facet_list = new List<int>();

                //  Console.WriteLine("build_unit_mapping_of_inner -1 - " + ic.Id_abstract_owner + "," + ic.Id_inner);
                IList<Slice> slice_list = Backend.sdao.listByInner(ic.Id_abstract_owner, ic.Id_inner, dbcon);

                foreach (Slice slice in slice_list)
                {
                    IList<Interface> i_host_list = Backend.idao.retrieve(id_abstract_owner_current, ic.Id_abstract_owner, slice.Id_interface, dbcon);
                    foreach (Interface i_host in i_host_list)
                    {
                        Interface i = Backend.idao.retrieveSuper(ic.Id_abstract_inner, slice.Id_interface_slice, dbcon);

                        // CHECK THIS_FACET
                        if (enclosing_facet_list.Contains(i_host.Facet))
                            this_facet_list.Add(i.Facet);
                    }
                }

                return this_facet_list;
            }


            public interface BaseComponentHierarchy
            {
                string ID { get; }
                IDictionary<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> InnerComponents { get; }

                void accept(BaseComponentHierarchyVisitor visitor, IDbConnection dbcon);
                void addInnerComponent(string inner_id, Tuple<ResolveComponentHierarchy, int[]> inner_component);
                void addInnerComponent(string inner_id, IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component);
            }

            private static SAFeSWL_Architecture DeserializeArchitecture_SAFeSWL(string filename)
            {
                // Declare an object variable of the type to be deserialized.
                SAFeSWL_Architecture i = null;
                FileStream fs = null;
                try
                {
                    // Create an instance of the XmlSerializer specifying type and namespace.
                    XmlSerializer serializer = new XmlSerializer(typeof(SAFeSWL_Architecture));

                    // A FileStream is needed to read the XML document.
                    fs = new FileStream(filename, FileMode.Open);

                    XmlReader reader = new XmlTextReader(fs);

                    // Use the Deserialize method to restore the object's state.
                    i = (SAFeSWL_Architecture)serializer.Deserialize(reader);
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }
                finally
                {
                    if (fs != null)
                        fs.Close();
                }

                return i;

            }



            public static IList<string> fetchPlatforms(int id_abstract_system, IDbConnection dbcon)
            {
                IList<Tuple<int, string>> result = new List<Tuple<int, string>>();


                //IDbConnection dbcon = DBConnector.DBcon;
                IDbCommand dbcmd = dbcon.CreateCommand();
                string sql = "SELECT s.id_inner as platform_id, u.facet as facet " +
                             "FROM slice as s, " +
                                  "interface as u " +
                             "WHERE s.id_abstract=" + id_abstract_system + " AND " +
                                   "s.id_inner like 'platform%' AND " +
                                   "s.id_abstract = u.id_abstract AND " +
                                   "s.id_interface = u.id_interface";

                dbcmd.CommandText = sql;
                Console.WriteLine(sql);
                IDataReader reader; /* lock (DBConnector.db_lock) */
                { reader = dbcmd.ExecuteReader(); }
                while (reader.Read())
                {
                    int facet = (int)reader["facet"];
                    string platform_id = (string)reader["platform_id"];
                    result.Add(new Tuple<int, string>(facet, platform_id));
                }
                reader.Close();
                reader = null;
                dbcmd.Dispose();
                dbcmd = null;

                string[] result_array = new string[result.Count()];
                foreach (Tuple<int, string> t in result)
                    result_array[t.Item1] = t.Item2;

                return result_array.ToList();
            }


            public static IList<Tuple<int, string>> fetchPlacement(int id_abstract_system, string id_inner, string id_platform, IDbConnection dbcon)
            {
                IList<Tuple<int, string>> result = new List<Tuple<int, string>>();

                //  IDbConnection dbcon = DBConnector.DBcon;
                IDbCommand dbcmd = dbcon.CreateCommand();
                string sql = "SELECT u.facet as facet, b.id_inner as platform_id " +
                             "FROM slice AS a, slice AS b, interface AS u, innercomponent AS i " +
                             "WHERE a.id_abstract=" + id_abstract_system + " AND" +
                                  " a.id_inner like '" + id_inner + "' AND" +
                                  " a.id_abstract=b.id_abstract AND" +
                                  " a.id_interface = b.id_interface AND" +
                                  " i.id_abstract_owner = a.id_abstract AND" +
                                  " i.id_inner = a.id_inner AND" +
                                  " u.id_abstract = i.id_abstract_inner AND" +
                                  " u.id_interface = a.id_interface_slice AND" +
                                  " b.id_inner like '%platform%'";

                dbcmd.CommandText = sql;
                Console.WriteLine(sql);
                IDataReader reader; /* lock (DBConnector.db_lock) */
                { reader = dbcmd.ExecuteReader(); }
                while (reader.Read())
                {
                    int facet = (int)reader["facet"];
                    string platform_id = (string)reader["platform_id"];
                    result.Add(new Tuple<int, string>(facet, platform_id));
                }
                reader.Close();
                reader = null;
                dbcmd.Dispose();
                dbcmd = null;

                return result;
            }


            public static SAFeSWL_Architecture readArchitecture(string arch_desc_xml)
            {
                string filename = System.IO.Path.GetTempFileName();
                File.WriteAllText(filename, arch_desc_xml);
                SAFeSWL_Architecture arch_desc = DeserializeArchitecture_SAFeSWL(filename);
                return arch_desc;
            }

            public static Instantiator.UnitMappingType[] build_unit_mapping_of_system(int id_abstract, IDbConnection dbcon)
            {
                return build_unit_mapping_of_system(id_abstract, null, dbcon);
            }

            public static Instantiator.UnitMappingType[] build_unit_mapping_of_system(int id_abstract, int[] nodes, IDbConnection dbcon)
            {
                IList<Instantiator.UnitMappingType> unit_mapping_list = new List<Instantiator.UnitMappingType>();

                foreach (Interface i in Backend.idao.list(id_abstract, dbcon))
                {
                    Instantiator.UnitMappingType unit_mapping_item = buildUnitMappingSystem(i.Id_interface, i.GetId_interface_super_top(dbcon), i.Facet, nodes);
                    unit_mapping_list.Add(unit_mapping_item);
                }

                Instantiator.UnitMappingType[] unit_mapping = new Instantiator.UnitMappingType[unit_mapping_list.Count];
                foreach (Instantiator.UnitMappingType um in unit_mapping_list)
                    unit_mapping[um.facet_instance] = um;

                return unit_mapping;
            }

            public static Instantiator.UnitMappingType buildUnitMappingSystem(string id_interface, string id_interface_super_top, int facet, int[] nodes)
            {
                Instantiator.UnitMappingType unit_mapping_item = new Instantiator.UnitMappingType();

                unit_mapping_item.unit_id = id_interface;
                unit_mapping_item.unit_id_top = id_interface_super_top;
                unit_mapping_item.facet_instanceSpecified = unit_mapping_item.facetSpecified = true;
                unit_mapping_item.facet_instance = unit_mapping_item.facet = facet;
                unit_mapping_item.unit_index = 0;

                int nofnodes = nodes != null ? nodes[unit_mapping_item.facet_instance] : 1;
                unit_mapping_item.node = new int[nofnodes];
                for (int index = 0; index < nofnodes; index++)
                    unit_mapping_item.node[index] = index;

                return unit_mapping_item;
            }

            public interface ResolveComponentHierarchy : BaseComponentHierarchy
            {
                string[] ComponentChoice { get; }
                ComponentID CID { get; set; }
                AbstractComponentFunctorApplication ACFARef { get; set; }
                AbstractComponentFunctorApplication ACFARef0 { get; }
                IDictionary<string, int> Arguments { get; set; }
                IDictionary<string, int> ArgumentsTop { get; set; }
                org.hpcshelf.database.Component[] ComponentDB { get; set; }

                IList<Tuple<string, ResolveComponentHierarchy>> HostComponents { get; }
                IDictionary<string, bool> IsPrivate { get; }
                int Kind { get; set; }
                Instantiator.UnitMappingType[][] UnitMapping { get; set; }
                IList<int>[] FacetList { get; set; }
                string PortName { get; set; }
                bool isInFacet(int facet_instance);

                void addHostComponent(string id_inner_host, ResolveComponentHierarchy host_component);
                string getPortName(string ctree_host_id);

                void addInnerComponent(string inner_id, bool is_private, Tuple<ResolveComponentHierarchy, int[]> inner_component);
                void addInnerComponent(string inner_id, bool is_private, IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component);

                //ResolveComponentHierarchy resolveSolutionComponent(string cRef, IList<Tuple<string,int>> binding_list);
                void setUnitMappingNodes(ResolveComponentHierarchy resolveComponentHierarchyImpl, IDbConnection dbcon);
                void resolveAllComponentHierarchy(IDbConnection dbcon);
                Tuple<org.hpcshelf.database.Component, IDictionary<string, org.hpcshelf.database.Component>> resolveSystem(IDbConnection dbcon);
                IList<Tuple<string, ResolveComponentHierarchy>> checkHosts(int[] facet_list);

                Tuple<ResolveComponentHierarchy, int[]> buildNode(InnerComponent ic, int facet_instance_count, string cstr_inner, IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component_hierarchy_list, IDbConnection dbcon);
                void removeInnerComponent(string id_inner_removed);
                void setSelection(org.hpcshelf.database.Component value);
                void clearSelection();
            }

            public class BaseComponentHierarchyImpl : BaseComponentHierarchy
            {
                protected string id;
                public string ID { get { return this.id; } set { this.id = value; } }

                protected string library_path;
                public string LibraryPath { get { return this.library_path; } set { this.library_path = value; } }

                private IDictionary<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> inner_components = new ConcurrentDictionary<string, IList<Tuple<ResolveComponentHierarchy, int[]>>>();
                public IDictionary<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> InnerComponents => this.inner_components;

                public BaseComponentHierarchyImpl(string id, string library_path)
                {
                    this.ID = id;
                    this.LibraryPath = library_path;
                }

                public BaseComponentHierarchyImpl(string id)
                {
                    this.ID = id;
                }

                public void addInnerComponent(string inner_id, Tuple<ResolveComponentHierarchy, int[]> inner_component)
                {
                    if (!InnerComponents.ContainsKey(inner_id))
                        inner_components[inner_id] = new List<Tuple<ResolveComponentHierarchy, int[]>>();
                    inner_components[inner_id].Add(inner_component);
                }

                public void addInnerComponent(string inner_id, IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component)
                {
                    IList<Tuple<ResolveComponentHierarchy, int[]>> l = new List<Tuple<ResolveComponentHierarchy, int[]>>(inner_component);
                    if (!InnerComponents.ContainsKey(inner_id))
                        inner_components[inner_id] = l;
                    else
                        foreach (Tuple<ResolveComponentHierarchy, int[]> e in l)
                            inner_components[inner_id].Add(e);
                }

                public void accept(BaseComponentHierarchyVisitor visitor, IDbConnection dbcon)
                {
                    visitor.visit(this, dbcon);
                }
            }

            public class ResolveComponentHierarchySystemImpl : ResolveComponentHierarchyImpl
            {
                public ResolveComponentHierarchySystemImpl(string id) : base(id)
                {
                }

                public ResolveComponentHierarchySystemImpl(string id, string library_path) : base(id, library_path)
                {
                }
            }

            public class ResolveComponentHierarchyImpl : BaseComponentHierarchyImpl, ResolveComponentHierarchy
            {
                public ResolveComponentHierarchyImpl(string id, string library_path) : base(id, library_path)
                {
                }

                public ResolveComponentHierarchyImpl(string id) : base(id)
                {
                }

                private string[] component_choice;
                public string[] ComponentChoice { get { return this.component_choice; } set { this.component_choice = value; } }

                public void addInnerComponent(string inner_id, bool is_private, IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component)
                {
                    base.addInnerComponent(inner_id, inner_component);
                    if (!this.IsPrivate.ContainsKey(inner_id))
                        this.is_private[inner_id] = is_private;
                }

                public void addInnerComponent(string inner_id, bool is_private, Tuple<ResolveComponentHierarchy, int[]> inner_component)
                {
                    base.addInnerComponent(id, inner_component);
                    if (!this.IsPrivate.ContainsKey(inner_id))
                        this.is_private[inner_id] = is_private;
                }

                private IDictionary<string, ResolveComponentHierarchy> host_components_by_id = new ConcurrentDictionary<string, ResolveComponentHierarchy>();
                private IList<Tuple<string, ResolveComponentHierarchy>> host_components = new List<Tuple<string, ResolveComponentHierarchy>>();
                public IList<Tuple<string, ResolveComponentHierarchy>> HostComponents => this.host_components;

                public void addHostComponent(string id_inner_host, ResolveComponentHierarchy host_component)
                {
                    if (!host_components_by_id.ContainsKey(host_component.ID))
                    {
                        host_components.Add(new Tuple<string, ResolveComponentHierarchy>(id_inner_host, host_component));
                        host_components_by_id[host_component.ID] = host_component;
                    }
                }

                public string getPortName(string ctree_host_id)
                {
                    Console.WriteLine("getPortName{0}", ctree_host_id);
                    foreach (string k in host_components_by_id.Keys)
                        Console.WriteLine("getPortName -- key={0}", k);

                    ResolveComponentHierarchy ctree_host = host_components_by_id[ctree_host_id];
                    foreach (Tuple<string, ResolveComponentHierarchy> t in host_components)
                        if (t.Item2.ID.Equals(ctree_host.ID))
                            return t.Item1;

                    return null;
                }

                private AbstractComponentFunctorApplication acfaRef;
                public AbstractComponentFunctorApplication ACFARef
                {
                    get
                    {
                        return acfaRef;
                    }
                    set
                    {
                        acfaRef = value;
                        if (acfaRef0 == null)
                            acfaRef0 = acfaRef;
                    }
                }

                private AbstractComponentFunctorApplication acfaRef0 = null;
                public AbstractComponentFunctorApplication ACFARef0 { get { return /*acfaRef0*/ acfaRef0; } } // TODO: retirar ACFARef0, pois parece desnecessário após atualizações ...

                private IDictionary<string, int> arguments;
                public IDictionary<string, int> Arguments { get { return arguments; } set { arguments = value; } }

                private IDictionary<string, int> arguments_top = null;
                public IDictionary<string, int> ArgumentsTop { get { return arguments_top; } set { arguments_top = value; } }

                private IDictionary<string, bool> is_private = new ConcurrentDictionary<string, bool>();
                public IDictionary<string, bool> IsPrivate { get { return is_private; } }

                private bool is_qualifier = false;
                public bool IsQualifier { get { return is_qualifier; } set { is_qualifier = value; } }

                private int kind = -1;
                public int Kind { get { return kind; } set { kind = value; } }

                private Instantiator.UnitMappingType[][] unit_mapping = null;
                public Instantiator.UnitMappingType[][] UnitMapping { get { return unit_mapping; } set { unit_mapping = value; } }

                private IList<int>[] facet_list = null;
                public IList<int>[] FacetList { get { return facet_list; } set { facet_list = value; } }

                private ComponentID cid = null;
                public ComponentID CID { get { return cid; } set { cid = value; } }

                private string port_name = null;
                public string PortName { get { return port_name; } set { port_name = value; } }

                private org.hpcshelf.database.Component[] component_db = null;
                public org.hpcshelf.database.Component[] ComponentDB { get { return component_db; } set { component_db = value; } }

                public void accept(ResolveComponentHierarchyVisitor visitor, IDbConnection dbcon)
                {
                    visitor.visit(this, dbcon);
                }

                public bool isInFacet(int facet_instance)
                {
                    return facet_instance < this.FacetList.Length && this.FacetList[facet_instance].Count > 0;
                }




                public void buildComponentHierarchy(int facet_instance_count, IList<Tuple<string, int>> channel_list, IDictionary<string, Tuple<string, Backend.ResolveComponentHierarchy>> delayed_public_inner_component, IDbConnection dbcon)
                {
                    // Console.WriteLine("ENTER RESOLVE {0} {1}", this.ID, ACFARef.Id_abstract);

                    // if (this.ID.EndsWith("partition_function_action_port"))
                    //    Console.Write(true);

                    int id_abstract = ACFARef.Id_abstract;
                    AbstractComponentFunctor acf = acfdao.retrieve(id_abstract, dbcon);

                    this.Kind = Constants.kindInt[acf.Kind];

                    if (this.Kind == Constants.KIND_BINDING)
                    {
                        int count_facets = 0;
                        foreach (Instantiator.UnitMappingType u in this.UnitMapping[0])
                            if (u.facet_instance >= 0)
                                count_facets++;
                        channel_list.Add(new Tuple<string, int>(this.ID, count_facets));
                    }

                    this.ComponentChoice = new string[1] { acf.Library_path };

                    IList<InnerComponent> icList = Backend.icdao.list(ACFARef.Id_abstract, dbcon);

                    IDictionary<string, ResolveComponentHierarchy> args = new ConcurrentDictionary<string, ResolveComponentHierarchy>();

                    foreach (InnerComponent ic in icList)
                    {
                        if (this.InnerComponents.ContainsKey(ic.Id_inner))
                            continue;

                        string cstr_inner = this.ID + "." + ic.Id_inner;

                        //  if (ic.Id_inner.Equals("partition_function_action_port"))
                        //      Console.Write(true);

                        IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component_hierarchy_list = new List<Tuple<ResolveComponentHierarchy, int[]>>();

                        if (!ic.IsPublic)
                        {
                            Tuple<ResolveComponentHierarchy, int[]> inner_component_hierarchy = null;
                            inner_component_hierarchy = buildNode(ic, facet_instance_count, cstr_inner, inner_component_hierarchy_list, dbcon);
                            if (inner_component_hierarchy == null)
                                continue;
                            args[ic.Id_inner] = inner_component_hierarchy.Item1;
                        }
                        else
                        {
                            IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component_hierarchy_dict_ = new List<Tuple<ResolveComponentHierarchy, int[]>>();

                            this.findPublicInnerComponent(ic.Id_inner, inner_component_hierarchy_dict_, dbcon);
                            foreach (Tuple<ResolveComponentHierarchy, int[]> inner_component_hierarchy in inner_component_hierarchy_dict_)
                            {
                                ((ResolveComponentHierarchyImpl)inner_component_hierarchy.Item1).addHostComponent(ic.Id_inner /*inner_component_hierarchy.Key*/, this);
                                inner_component_hierarchy_list.Add(inner_component_hierarchy);
                            }
                        }

                        this.addInnerComponent(ic.Id_inner, !ic.IsPublic, inner_component_hierarchy_list);

                        if (inner_component_hierarchy_list.Count == 0)
                            delayed_public_inner_component[cstr_inner] = new Tuple<string, ResolveComponentHierarchy>(ic.Id_inner, this);
                    }

                    foreach (ResolveComponentHierarchy a in args.Values)
                        ((ResolveComponentHierarchyImpl)a).buildComponentHierarchy(facet_instance_count, channel_list, delayed_public_inner_component, dbcon);
                }

                public Tuple<ResolveComponentHierarchy, int[]> buildNode(InnerComponent ic, int facet_instance_count, string cstr_inner, IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component_hierarchy_list, IDbConnection dbcon)
                {
                    Tuple<ResolveComponentHierarchy, int[]> inner_component_hierarchy = null;
                    AbstractComponentFunctorApplication acfaRef_inner = null;

                    Instantiator.UnitMappingType[] unit_mapping_inner = null;
                    build_unit_mapping_of_inner(ACFARef.Id_abstract, ic, this.UnitMapping[0], out unit_mapping_inner, dbcon);
                    set_nodes_in_unit_mapping_of_inner(ACFARef.Id_abstract, ic, this.UnitMapping[0], unit_mapping_inner, dbcon);

                    if (unit_mapping_inner.Length == 0)
                        return null;

                    int id_functor_app_inner = ic.Id_functor_app;
                    if (!ic.Parameter_top.Equals(""))
                        id_functor_app_inner = find_parameterized_id_functor_app_inner(ACFARef.Id_abstract, ic, arguments, dbcon);

                    AbstractComponentFunctorApplication acfa_inner = acfadao.retrieve(id_functor_app_inner, dbcon);

                    //if (acfa_inner == null)
                    //    Console.Write(true);

                    id_functor_app_inner = generateACFAforContext(acfa_inner.Id_abstract, id_functor_app_inner, id_functor_app_inner, arguments, ArgumentsTop, 0, dbcon);

                    IDictionary<string, int> arguments_new = null;
                    determineArguments(ACFARef.Id_functor_app, ArgumentsTop, arguments, id_functor_app_inner, out arguments_new, dbcon);

                    acfaRef_inner = Backend.acfadao.retrieve(id_functor_app_inner, dbcon);

                    IList<int>[] inner_facet_list = new IList<int>[facet_instance_count];

                    for (int f = 0; f < facet_instance_count; f++)
                        inner_facet_list[f] = build_facet_list(ic, this.ACFARef.Id_abstract, this.FacetList[f], dbcon);

                    ResolveComponentHierarchy inner_component_hierarchy_item1 = new ResolveComponentHierarchyImpl(cstr_inner);

                    inner_component_hierarchy_item1.PortName = ic.Id_inner;
                    inner_component_hierarchy_item1.UnitMapping = new Instantiator.UnitMappingType[1][] { unit_mapping_inner };
                    inner_component_hierarchy_item1.FacetList = inner_facet_list;
                    inner_component_hierarchy_item1.addHostComponent(ic.Id_inner, this);
                    inner_component_hierarchy_item1.ACFARef = acfaRef_inner;
                    inner_component_hierarchy_item1.Arguments = arguments_new;
                    inner_component_hierarchy_item1.ArgumentsTop = ArgumentsTop;

                    IList<int> facets_list = sdao.findFacets(ic.Id_abstract_owner, ic.Id_inner, dbcon);

                    inner_component_hierarchy = new Tuple<ResolveComponentHierarchy, int[]>(inner_component_hierarchy_item1, facets_list.ToArray());

                    inner_component_hierarchy_list.Add(inner_component_hierarchy);

                    return inner_component_hierarchy;
                }


                string[] ranking_method_list = {
                                                            "MCMD_TOPSISLinear",
                                                            "MCMD_MMOORA",
                                                            "MCMD_TOPSISVector",
                                                            "MCMD_VIKOR",
                                                            "MCMD_WASPAS"/*,
                                                            "MCMD_RIM",
                                                            "MCMD_R_MetaRanking"*/
                                                       };

                static double high = 4.0 / 7.0; //0.5;
                static double medium = 2.0 / 7.0; //0.3;
                static double low = 1.0 / 7.0;   //0.2;

                Tuple<string, IDictionary<string, double>>[] weights_list =
                {
                                new Tuple<string, IDictionary<string,double>>("w1",new Dictionary<string, double>() { { "execution_time", high },
                                                                                                                      { "energy_consumption", medium },
                                                                                                                      { "monetary_cost", low } }),

                                new Tuple<string, IDictionary<string,double>>("w2",new Dictionary<string, double>() { { "execution_time", high },
                                                                                                                      { "monetary_cost", medium },
                                                                                                                      { "energy_consumption", low } }),

                                new Tuple<string, IDictionary<string,double>>("w3",new Dictionary<string, double>() { { "energy_consumption", high },
                                                                                                                      { "execution_time", medium },
                                                                                                                      { "monetary_cost", low } }),

                                new Tuple<string, IDictionary<string,double>>("w4",new Dictionary<string, double>() { { "monetary_cost", high },
                                                                                                                      { "execution_time", medium },
                                                                                                                      { "energy_consumption", low } }),

                                new Tuple<string, IDictionary<string,double>>("w5",new Dictionary<string, double>() { { "monetary_cost", high },
                                                                                                                      { "energy_consumption", medium },
                                                                                                                      { "execution_time", low } }),

                                new Tuple<string, IDictionary<string,double>>("w6",new Dictionary<string, double>() { { "energy_consumption", high },
                                                                                                                      { "monetary_cost", medium },
                                                                                                                      { "execution_time", low } })
                            };

                public Tuple<org.hpcshelf.database.Component, IDictionary<string, org.hpcshelf.database.Component>> resolveSystem(IDbConnection dbcon)
                {
                    long time_resolving_platform = default(long);
                    IDictionary<string, long> time_resolving_system = new ConcurrentDictionary<string, long>();
                    IDictionary<string, long> time_arguments_system = new ConcurrentDictionary<string, long>();
                    IDictionary<string, IDictionary<string, IDictionary<string, long>>> time_ranking_system = new ConcurrentDictionary<string, IDictionary<string, IDictionary<string, long>>>();
                    long time_consolidating = default(long);

                    //  DBConnector.openConnection();

                    IList<Tuple<string, ResolveComponentHierarchy>> hc_list = this.HostComponents;

                    IDictionary<string, ResolveComponentHierarchy> hosted_computation_list = new ConcurrentDictionary<string, ResolveComponentHierarchy>();

                    string system_id = null;
                    foreach (Tuple<string, ResolveComponentHierarchy> hc in hc_list)
                        if (hc.Item2.Kind == Constants.KIND_COMPUTATION)
                        {
                            string[] ids = hc.Item2.ID.Split('.');
                            string id_host = ids[ids.Length - 1];
                            hosted_computation_list[id_host] = hc.Item2;
                        }
                        else if (hc.Item2.Kind == Constants.KIND_APPLICATION)
                            system_id = hc.Item2.ID;

                    if (hosted_computation_list.Count == 0)
                        throw new EmptyPlatformException(this.ID, system_id);

                    var sw1 = new Stopwatch();
                    sw1.Start();
                    this.ACFARef = this.ACFARef0;
                    org.hpcshelf.database.Component[] c_platform = resolveUnit(this.ACFARef, this.Arguments, this.ArgumentsTop, dbcon);
                    sw1.Stop();
                    Console.WriteLine("Elapsed Time (resolving platform {0}): {1} ms", this.ID, sw1.ElapsedMilliseconds);
                    time_resolving_platform = sw1.ElapsedMilliseconds;


                    if (c_platform != null && c_platform.Length == 0)
                        throw new UnresolvedSystemPlatformException(this.ID, system_id);

                    IDictionary<string, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[]> candidate_systems_1 = new ConcurrentDictionary<string, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[]>();

                    foreach (KeyValuePair<string, ResolveComponentHierarchy> t in hosted_computation_list)
                    {
                        ResolveComponentHierarchy hosted_computation = t.Value;
                        hosted_computation.ACFARef = hosted_computation.ACFARef0;
                        AbstractComponentFunctorApplication acfaRef_system = injectPlatformArguments(hosted_computation.ACFARef, this.ACFARef, dbcon);
                        hosted_computation.ACFARef = acfaRef_system;

                        var sw2 = new Stopwatch();
                        sw2.Start();
                        org.hpcshelf.database.Component[] c_computation = resolveUnit(acfaRef_system, hosted_computation.Arguments, hosted_computation.ArgumentsTop, dbcon);
                        sw2.Stop();
                        Console.WriteLine("Elapsed Time (resolving computation {0}): {1} ms", hosted_computation.ID, sw2.ElapsedMilliseconds);
                        time_resolving_system[t.Key] = sw2.ElapsedMilliseconds;

                        if (c_computation != null && c_computation.Length == 0)
                            throw new UnresolvedSystemComputationException(t.Key, this.ID, system_id);

                        var sw3 = new Stopwatch();
                        sw3.Start();
                        Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[] systems = calculateArgumentsOfAllSystems_SEQ(acfaRef_system, c_computation, c_platform, dbcon);
                        sw3.Stop();
                        Console.WriteLine("Elapsed Time (calculating arguments {0}/{1}): {2} ms", this.ID, hosted_computation.ID, sw3.ElapsedMilliseconds);
                        time_arguments_system[t.Key] = sw3.ElapsedMilliseconds;

                        candidate_systems_1[t.Key] = systems;
                    }

                    IDictionary<string, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[]> candidate_systems_2 = null;


                    string ranking_method_default = "MCMD_TOPSISLinear";
                    string weight_default = "w1";
                    if (!this.ID.EndsWith("-benchmark"))
                    {
                        candidate_systems_2 = new ConcurrentDictionary<string, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[]>();

                        string ranking_method = ranking_method_default;

                        IDictionary<string, double> weights = new Dictionary<string, double>() { { "execution_time", high }, { "monetary_cost", medium }, { "energy_consumption", low } };

                        foreach (KeyValuePair<string, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[]> hosted_computation in candidate_systems_1)
                        {
                            var sw3 = new Stopwatch();
                            sw3.Start();
                            Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] classified_systems = Alite.rank(hosted_computation.Value, weights, ranking_method);
                            sw3.Stop();
                            //Console.WriteLine("Elapsed Time (ranking system {0}/{1}/{2}/time={3}/energy={4}/cost={5}): {6} ms", this.ID, hosted_computation.Key, ranking_method, weights["execution_time"], weights["energy_consumption"], weights["monetary_cost"], sw3.ElapsedMilliseconds);
                            if (!time_ranking_system.ContainsKey(hosted_computation.Key))
                                time_ranking_system[hosted_computation.Key] = new ConcurrentDictionary<string, IDictionary<string, long>>();
                            if (!time_ranking_system[hosted_computation.Key].ContainsKey(ranking_method))
                                time_ranking_system[hosted_computation.Key][ranking_method] = new ConcurrentDictionary<string, long>();
                            time_ranking_system[hosted_computation.Key][ranking_method]["w1"] = sw3.ElapsedMilliseconds;

                            candidate_systems_2[hosted_computation.Key] = classified_systems;
                        }
                    }
                    else
                    {
                        IDictionary<string, IDictionary<string, IDictionary<string, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[]>>> candidate_systems_2_alternatives = new ConcurrentDictionary<string, IDictionary<string, IDictionary<string, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[]>>>();

                        Console.WriteLine("FREQUENCY={0}", Stopwatch.Frequency);

                        foreach (KeyValuePair<string, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[]> hosted_computation in candidate_systems_1)
                        {
                            foreach (string ranking_method in ranking_method_list)
                            {
                                foreach (Tuple<string, IDictionary<string, double>> t in weights_list)
                                {
                                    IDictionary<string, double> weights = t.Item2;

                                    //Console.WriteLine("sum={0}", weights["execution_time"] + weights["energy_consumption"] + weights["monetary_cost"]);
                                    var sw3 = new Stopwatch();
                                    sw3.Start();
                                    Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] classified_systems = Alite.rank(hosted_computation.Value, weights, ranking_method);
                                    sw3.Stop();
                                    //Console.WriteLine("Elapsed Time (ranking system {0}/{1}/{2}/time={3}/energy={4}/cost={5}): {6} ms", this.ID, hosted_computation.Key, ranking_method, weights["execution_time"], weights["energy_consumption"], weights["monetary_cost"], sw3.ElapsedMilliseconds);
                                    if (!time_ranking_system.ContainsKey(hosted_computation.Key))
                                        time_ranking_system[hosted_computation.Key] = new ConcurrentDictionary<string, IDictionary<string, long>>();
                                    if (!time_ranking_system[hosted_computation.Key].ContainsKey(ranking_method))
                                        time_ranking_system[hosted_computation.Key][ranking_method] = new ConcurrentDictionary<string, long>();
                                    time_ranking_system[hosted_computation.Key][ranking_method][t.Item1] = sw3.ElapsedTicks;

                                    if (!candidate_systems_2_alternatives.ContainsKey(ranking_method))
                                        candidate_systems_2_alternatives[ranking_method] = new ConcurrentDictionary<string, IDictionary<string, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[]>>();
                                    if (!candidate_systems_2_alternatives[ranking_method].ContainsKey(t.Item1))
                                        candidate_systems_2_alternatives[ranking_method][t.Item1] = new ConcurrentDictionary<string, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[]>();
                                    candidate_systems_2_alternatives[ranking_method][t.Item1][hosted_computation.Key] = classified_systems;

                                    string[] lines = new string[classified_systems.Length];
                                    for (int i = 0; i < classified_systems.Length; i++)
                                    {
                                        Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component> system_candidate = classified_systems[i];
                                        lines[i] = string.Format("{0}", /*i, system_candidate.Item1.Library_path,*/ system_candidate.Item2.Library_path);
                                    }
                                    File.WriteAllLines(string.Format("{0}.{1}.{2}.{3}.txt", this.ID, hosted_computation.Key, ranking_method, t.Item1), lines);
                                }
                            }
                        }

                        candidate_systems_2 = candidate_systems_2_alternatives[ranking_method_default][weight_default];
                    }

                    var sw4 = new Stopwatch();
                    sw4.Start();
                    Tuple<org.hpcshelf.database.Component, IDictionary<string, org.hpcshelf.database.Component>> selection = Alite.finalSelection(candidate_systems_2);
                    sw4.Stop();
                    Console.WriteLine("Elapsed Time (consolidating final system {0}): {1} ms", this.ID, sw4.ElapsedMilliseconds);
                    time_consolidating = sw4.ElapsedMilliseconds;

                    // if (this.ID.EndsWith("-benchmark"))
                    {
                        File.AppendAllLines(string.Format("time.resolving_platform.{0}", this.ID), new string[] { time_resolving_platform.ToString() });
                        foreach (string computation_host_id in candidate_systems_1.Keys)
                        {
                            File.AppendAllLines(string.Format("time.resolving_system.{0}.{1}", this.ID, computation_host_id), new string[] { time_resolving_system[computation_host_id].ToString() });
                            File.AppendAllLines(string.Format("time.arguments_system.{0}.{1}", this.ID, computation_host_id), new string[] { time_arguments_system[computation_host_id].ToString() });
                            foreach (string ranking_method in ranking_method_list)
                                if (time_ranking_system[computation_host_id].ContainsKey(ranking_method))
                                    foreach (Tuple<string, IDictionary<string, double>> t in weights_list)
                                        if (time_ranking_system[computation_host_id][ranking_method].ContainsKey(t.Item1))
                                            File.AppendAllLines(string.Format("time.ranking_system.{0}.{1}.{2}.{3}", this.ID, computation_host_id, ranking_method, t.Item1), new string[] { time_ranking_system[computation_host_id][ranking_method][t.Item1].ToString() });
                        }
                        File.AppendAllLines(string.Format("time.consolidating_ranking_system.{0}", this.ID), new string[] { time_consolidating.ToString() });
                    }

                    return selection;

                }

                private Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[]
                                                            calculateArgumentsOfAllSystems_SEQ(
                                                             AbstractComponentFunctorApplication acfaRef_system,
                                                             org.hpcshelf.database.Component[] c_list_computation,
                                                             org.hpcshelf.database.Component[] c_list_platform, IDbConnection dbcon)
                {

                    int N = c_list_computation.Length * c_list_platform.Length;
                    Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[] systems = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[N];

                    int i = 0;
                    foreach (org.hpcshelf.database.Component c_computation in c_list_computation)
                    {
                        var compilerResult = compileArgumentCalculatorClass(c_computation.ContextArgumentCalculator != null ? c_computation.ContextArgumentCalculator : default_calculator_object);
                        c_computation.ContextArgumentCalculatorObject = createArgumentCalculatorObject(compilerResult);

                        IDictionary<string, SupplyParameter> current_arguments = Backend.spdao.listBy(acfaRef_system.Id_functor_app, dbcon);

                        AbstractComponentFunctorApplication acfaRef_computation;
                        acfaRef_computation = Backend.acfadao.retrieve(c_computation.Id_functor_app, dbcon);
                        IDictionary<string, SupplyParameter> computation_arguments = Backend.spdao.listBy(acfaRef_computation.Id_functor_app, dbcon);

                        IList<AbstractComponentFunctorParameter> acfp_list;
                        acfp_list = Backend.acfpdao.list(acfaRef_computation.Id_abstract, dbcon);

                        foreach (org.hpcshelf.database.Component c_platform in c_list_platform)
                        {
                            IDictionary<string, object> argument_value = calculateArguments(c_computation.ContextArgumentCalculatorObject, current_arguments, computation_arguments, acfp_list, c_platform, dbcon);
                            systems[i] = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>(c_computation, c_platform, argument_value);
                            i++;
                        }
                    }

                    return systems;
                }


                private Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[]
                                                                        calculateArgumentsOfAllSystems(
                                                                             AbstractComponentFunctorApplication acfaRef_system,
                                                                            org.hpcshelf.database.Component[] c_list_computation,
                                                                             org.hpcshelf.database.Component[] c_list_platform, IDbConnection dbcon)
                {

                    int N = 1;
                    Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[] systems = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[c_list_computation.Length * c_list_platform.Length];

                    ManualResetEvent[] doneEvents = new ManualResetEvent[N];

                    //int workerThreads = 0;
                    //int completionPortThreads = 0;
                    //ThreadPool.GetAvailableThreads(out workerThreads, out completionPortThreads);
                    // Console.WriteLine("1 -- workerThreads={0} /completionPortThreads {1}", workerThreads, completionPortThreads);

                    //ThreadPool.SetMaxThreads(8, 8);
                    //ThreadPool.SetMinThreads(4, 4);

                    // ThreadPool.GetAvailableThreads(out workerThreads, out completionPortThreads);
                    // Console.WriteLine("2 -- workerThreads={0} /completionPortThreads {1}", workerThreads, completionPortThreads);

                    int vv = 0;
                    foreach (org.hpcshelf.database.Component c_computation in c_list_computation)
                    {
                        CompilerResults compilerResult = null;
                        Thread t = new Thread(() => compilerResult = compileArgumentCalculatorClass(c_computation.ContextArgumentCalculator != null ? c_computation.ContextArgumentCalculator : default_calculator_object));
                        t.Start();

                        IDictionary<string, SupplyParameter> current_arguments = Backend.spdao.listBy(acfaRef_system.Id_functor_app, dbcon);

                        AbstractComponentFunctorApplication acfaRef_computation;
                        acfaRef_computation = Backend.acfadao.retrieve(c_computation.Id_functor_app, dbcon);
                        IDictionary<string, SupplyParameter> computation_arguments = Backend.spdao.listBy(acfaRef_computation.Id_functor_app, dbcon);

                        IList<AbstractComponentFunctorParameter> acfp_list;
                        acfp_list = Backend.acfpdao.list(acfaRef_computation.Id_abstract, dbcon);

                        org.hpcshelf.database.Component[][] c_platform_chunk = new org.hpcshelf.database.Component[N][];
                        int chunk_s = c_list_platform.Length / N;
                        int chunk_r = c_list_platform.Length % N;
                        int v = 0;

                        t.Join();

                        // var sw = new Stopwatch();
                        // sw.Start();

                        for (int j = 0; j < N; j++)
                        {
                            int r = chunk_r > 0 ? 1 : 0;
                            c_platform_chunk[j] = new org.hpcshelf.database.Component[chunk_s + r];
                            chunk_r--;

                            doneEvents[j] = new ManualResetEvent(false);

                            ThreadPool.QueueUserWorkItem(new WaitCallback(
                                (object state) =>
                                {
                                    //          var sw1 = new Stopwatch();
                                    //          sw1.Start();
                                    var o = (Tuple<Tuple<int, int>,
                                                   IDictionary<string, SupplyParameter>,
                                                   IDictionary<string, SupplyParameter>,
                                                   IList<AbstractComponentFunctorParameter>,
                                                   org.hpcshelf.database.Component,
                                                   org.hpcshelf.database.Component[],
                                                   ManualResetEvent>)state;

                                    var v_min = o.Item1.Item1;
                                    var v_max = o.Item1.Item2;
                                    var current_arguments_ = o.Item2;
                                    var computation_arguments_ = o.Item3;
                                    var acfp_list_ = o.Item4;
                                    var c_computation_ = o.Item5;
                                    var contextArgumentCalculatorObject = createArgumentCalculatorObject(compilerResult); // c_computation_.ContextArgumentCalculatorObject;
                                    var c_list_platform_ = o.Item6;
                                    ManualResetEvent doneEvent = o.Item7;
                                    IDbConnection dbconn_thread = DBConnector.createConnection();
                                    dbconn_thread.Open();
                                    try
                                    {
                                        //  var sw2 = new Stopwatch();
                                        //  sw2.Start();
                                        //  Console.WriteLine("THREAD - v_min={0}, v_max={1}", v_min, v_max);
                                        for (int k = v_min; k < v_max; k++)
                                        {
                                            var c_platform_ = c_list_platform_[k];
                                            IDictionary<string, object> argument_value = calculateArguments(contextArgumentCalculatorObject, current_arguments_, computation_arguments_, acfp_list_, c_platform_, dbconn_thread);
                                            systems[vv + k] = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>(c_computation_, c_platform_, argument_value);
                                        }
                                        //sw2.Stop();
                                        //Console.WriteLine("TIME ================================= {0}ms", sw2.ElapsedMilliseconds);
                                    }
                                    finally
                                    {
                                        dbconn_thread.Close();
                                        doneEvent.Set();
                                    }
                                    //sw1.Stop();
                                    //Console.WriteLine("TIME ++++++++++++++++++++++++++ {0}ms", sw1.ElapsedMilliseconds);
                                }), new Tuple<Tuple<int, int>,
                                              IDictionary<string, SupplyParameter>,
                                              IDictionary<string, SupplyParameter>,
                                              IList<AbstractComponentFunctorParameter>,
                                              org.hpcshelf.database.Component,
                                              org.hpcshelf.database.Component[],
                                              ManualResetEvent>(new Tuple<int, int>(v, v + chunk_s + r), current_arguments, computation_arguments, acfp_list, c_computation, c_list_platform, doneEvents[j]));

                            v += c_platform_chunk[j].Length;
                        }

                        WaitHandle.WaitAll(doneEvents);

                        //sw.Stop();
                        //Console.WriteLine("TIME *** ---------- {0}ms", sw.ElapsedMilliseconds);

                        vv += v;
                    }

                    return systems;
                }

                private string default_calculator_object = "using System;\nusing System.Collections.Generic;\n\npublic class ContextArgumentCalculator\n{\n   public IDictionary<string, object> argument;\n   public IDictionary<string, object> Argument { set { argument = value; } }\n\n    public decimal? energy_consumption\n    {\n        get\n        {\n            return (decimal?)1;\n        }\n    }\n\n    public decimal? monetary_cost\n    {\n        get\n        {\n            return (decimal?)1;\n        }\n    }\n\n    public int? execution_time\n    {\n        get\n        {\n\n\t   return (int?)1;\n        }\n    }\n}\n\n";

                private IDictionary<string, object> calculateArguments
                      (object argument_calculator,
                       IDictionary<string, SupplyParameter> current_arguments,
                       IDictionary<string, SupplyParameter> computation_arguments,
                       IList<AbstractComponentFunctorParameter> acfp_list,
                       org.hpcshelf.database.Component c_platform, IDbConnection dbconn)
                {
                    IDictionary<string, object> argument_final_value = new ConcurrentDictionary<string, object>();

                    PropertyInfo p = argument_calculator.GetType().GetProperty("Argument");
                    p.SetValue(argument_calculator, argument_final_value, null);

                    AbstractComponentFunctorApplication acfaRef_platform = Backend.acfadao.retrieve(c_platform.Id_functor_app, dbconn);
                    IDictionary<string, SupplyParameter> platform_arguments = Backend.spdao.listBy(acfaRef_platform.Id_functor_app, dbconn);

                    foreach (AbstractComponentFunctorParameter acfp in acfp_list)
                    {
                        SupplyParameterComponent argument = current_arguments.ContainsKey(acfp.Id_parameter) && current_arguments[acfp.Id_parameter] is SupplyParameterComponent
                                                                ? (SupplyParameterComponent)current_arguments[acfp.Id_parameter]
                                                                : (SupplyParameterComponent)computation_arguments[acfp.Id_parameter];

                        if (acfp.ParameterClass == ParameterClass.platform)
                        {
                            string id_parameter_platform = acfp.Id_parameter.Replace("platform-", "");
                            if (platform_arguments.ContainsKey(id_parameter_platform) && platform_arguments[id_parameter_platform] is SupplyParameterComponent)
                                argument = (SupplyParameterComponent)platform_arguments[id_parameter_platform];
                        }

                        AbstractComponentFunctorApplication argument_value = Backend.acfadao.retrieve(argument.Id_functor_app_actual, dbconn);

                        if (argument_value is AbstractComponentFunctorApplicationQuantifier)
                        {
                            AbstractComponentFunctorApplicationQuantifier argument_value_quantifier = (AbstractComponentFunctorApplicationQuantifier)argument_value;
                            PropertyInfo prop;
                            if (has_quantifier_calculator(argument_calculator, acfp.Id_parameter, out prop))
                            {
                                object v = calculate_quantifier_value(argument_calculator, prop);
                                argument_final_value[acfp.Id_parameter] = v;
                            }
                            else if (argument_value_quantifier.Quantifier != null)
                            {
                                Tuple<char, char> q = isQuantifier(argument_value_quantifier.Id_abstract, dbconn);
                                if (q.Item1 == 'd')
                                    argument_final_value[acfp.Id_parameter] = argument_value_quantifier.Quantifier;
                                else
                                    argument_final_value[acfp.Id_parameter] = (int)argument_value_quantifier.Quantifier;
                            }
                            else if (argument_value_quantifier.Quantifier == null)
                            {
                                AbstractComponentFunctorApplicationQuantifier bounds_q = (AbstractComponentFunctorApplicationQuantifier)Backend.acfadao.retrieve(acfp.Bounds_of, dbconn);

                                Tuple<char, char> q = isQuantifier(bounds_q.Id_abstract, dbconn);
                                if (q.Item1 == 'd')
                                    argument_final_value[acfp.Id_parameter] = argument_value_quantifier.Quantifier;
                                else
                                    argument_final_value[acfp.Id_parameter] = (int)argument_value_quantifier.Quantifier;
                            }
                        }
                        else
                        {
                            object v = calculate_nonquantifier_value(argument.Id_functor_app_actual, dbconn);
                            argument_final_value[acfp.Id_parameter] = v;
                        }
                    }

                    return argument_final_value;
                }


                private CompilerResults compileArgumentCalculatorClass(string sourceCode)
                {
                    var compParms = new CompilerParameters { GenerateExecutable = false, GenerateInMemory = true };
                    var csProvider = new CSharpCodeProvider();
                    CompilerResults compilerResults = csProvider.CompileAssemblyFromSource(compParms, sourceCode);
                    return compilerResults;
                }

                private object createArgumentCalculatorObject(CompilerResults compilerResults)
                {
                    string class_id = "ContextArgumentCalculator";
                    object argument_calculator = compilerResults.CompiledAssembly.CreateInstance(class_id);
                    return argument_calculator;
                }

                private bool has_quantifier_calculator(object argument_calculator, string parameter_id, out PropertyInfo p)
                {
                    p = argument_calculator.GetType().GetProperty(parameter_id);
                    return p != null;
                }

                private object calculate_quantifier_value(object argument_calculator, PropertyInfo p)
                {
                    object o = p.GetValue(argument_calculator, null);
                    return o;
                }

                private object calculate_nonquantifier_value(int id_functor_app_actual, IDbConnection dbconn)
                {
                    AbstractComponentFunctorApplication acfa = Backend.acfadao.retrieve(id_functor_app_actual, dbconn);
                    AbstractComponentFunctor acf = Backend.acfdao.retrieve(acfa.Id_abstract, dbconn);
                    return acf.Library_path;
                }

                public void resolveAllComponentHierarchy(IDbConnection dbcon)
                {
                    resolveAllComponentHierarchy_(dbcon);
                }


                public static AbstractComponentFunctorApplication injectPlatformArguments
                            (AbstractComponentFunctorApplication acfaRef_computation,
                             AbstractComponentFunctorApplication acfaRef_platform, IDbConnection dbcon)
                {
                    IDictionary<string, SupplyParameter> computation_arguments = Backend.spdao.listBy(acfaRef_computation.Id_functor_app, dbcon);
                    IDictionary<string, int> acfaArguments_platform = new ConcurrentDictionary<string, int>();
                    IDictionary<string, SupplyParameter> platform_arguments = takePlatformArguments(acfaRef_platform, acfaRef_computation, dbcon);

                    /*  IDictionary<string, SupplyParameter> platform_arguments = new ConcurrentDictionary<string, SupplyParameter>();
                      IList<SupplyParameter> sp_list_platform = Backend.spdao.list(acfaRef_platform.Id_functor_app);
                      foreach (SupplyParameter sp in sp_list_platform)
                          platform_arguments[sp.Id_parameter] = sp;
                          */
                    AbstractComponentFunctorApplication aAppNew = new AbstractComponentFunctorApplication();
                    aAppNew.Id_functor_app = DBConnector.nextKey("id_functor_app", "abstractcomponentfunctorapplication", dbcon);
                    aAppNew.Id_abstract = acfaRef_computation.Id_abstract;
                    aAppNew.Id_functor_app_next = acfaRef_computation.Id_functor_app_next;

                    IList<AbstractComponentFunctorParameter> acfp_list = Backend.acfpdao.list(acfaRef_computation.Id_abstract, dbcon);
                    foreach (AbstractComponentFunctorParameter acfp in acfp_list)
                    {
                        SupplyParameter sp_new = null;

                        string id_parameter_computation = acfp.Id_parameter;
                        string id_parameter_platform = acfp.Id_parameter.Replace("platform-", "");

                        if (computation_arguments.ContainsKey(id_parameter_computation) && !platform_arguments.ContainsKey(id_parameter_platform))
                        {
                            sp_new = computation_arguments[id_parameter_computation].getCopy(aAppNew.Id_functor_app, aAppNew.Id_abstract);
                            // Backend.spdao.insert(sp_new);
                        }
                        else if (!computation_arguments.ContainsKey(id_parameter_computation) && platform_arguments.ContainsKey(id_parameter_platform))
                        {
                            sp_new = platform_arguments[id_parameter_platform].getCopy(aAppNew.Id_functor_app, aAppNew.Id_abstract);
                        }
                        else if (computation_arguments.ContainsKey(id_parameter_computation) && platform_arguments.ContainsKey(id_parameter_platform))
                        {
                            // SUBTYPING CHECK

                            SupplyParameter sp_c = computation_arguments[id_parameter_computation];
                            SupplyParameter sp_p = platform_arguments[id_parameter_platform];

                            if (sp_c is SupplyParameterComponent && sp_p is SupplyParameterComponent)
                            {
                                SupplyParameterComponent spp_c = (SupplyParameterComponent)sp_c;
                                SupplyParameterComponent spp_p = (SupplyParameterComponent)sp_p;
                                AbstractComponentFunctorApplication acfa_c = Backend.acfadao.retrieve(spp_c.Id_functor_app_actual, dbcon);
                                AbstractComponentFunctorApplication acfa_p = Backend.acfadao.retrieve(spp_p.Id_functor_app_actual, dbcon);

                                int r = database.Resolution.subType(acfaArguments_platform, acfa_p, acfa_c, dbcon);
                                if (r > 0)
                                    sp_new = platform_arguments[id_parameter_platform].getCopy(aAppNew.Id_functor_app, aAppNew.Id_abstract);
                                else
                                    throw new Exception(String.Format("Typing error: {0} <: {1}", acfa_p.Id_abstract, acfa_c.Id_abstract));
                            }
                            else if (sp_c is SupplyParameterParameter && sp_p is SupplyParameterComponent)
                            {
                                sp_new = platform_arguments[id_parameter_platform].getCopy(aAppNew.Id_functor_app, aAppNew.Id_abstract);
                            }
                            else if (sp_p is SupplyParameterParameter && sp_c is SupplyParameterComponent)
                            {
                                sp_new = computation_arguments[id_parameter_computation].getCopy(aAppNew.Id_functor_app, aAppNew.Id_abstract);
                            }

                        }
                        if (sp_new != null)
                        {
                            sp_new.Id_parameter = acfp.Id_parameter;
                            //if (spdao.retrieve(sp_new.Id_parameter, sp_new.Id_functor_app) == null)
                            Backend.spdao.insert(sp_new, dbcon);

                            if (sp_new is SupplyParameterComponent)
                                acfaArguments_platform[id_parameter_platform] = ((SupplyParameterComponent)sp_new).Id_functor_app_actual;
                            //else
                            //    Console.WriteLine();

                            //else
                            //    Console.WriteLine();
                        }

                    }

                    Backend.acfadao.insert(aAppNew, dbcon);

                    return aAppNew;
                }

                private static IDictionary<string, SupplyParameter> takePlatformArguments(
                         AbstractComponentFunctorApplication acfaRef_platform,
                         AbstractComponentFunctorApplication acfaRef_computation, IDbConnection dbcon)
                {

                    InnerComponent ic_platform = Backend.icdao.retrieve(acfaRef_computation.Id_abstract, "platform", dbcon);

                    AbstractComponentFunctorApplication acfa = acfaRef_platform;
                    IDictionary<string, SupplyParameter> platform_arguments = spdao.listBy(acfa.Id_functor_app, dbcon);
                    AbstractComponentFunctor acf = acfdao.retrieve(acfa.Id_abstract, dbcon);

                    // TODO: it assumes that the name of the parameter of a component is not modified in subtypes ...
                    do
                    {
                        IDictionary<string, SupplyParameter> sp_list_platform_loop = spdao.listBy(acf.Id_functor_app_supertype, dbcon);
                        foreach (KeyValuePair<string, SupplyParameter> sp in sp_list_platform_loop)
                            if (!platform_arguments.ContainsKey(sp.Value.Id_parameter))
                                platform_arguments.Add(sp);

                        acfa = acfadao.retrieve(acf.Id_functor_app_supertype, dbcon);
                        if (acfa != null)
                            acf = acfdao.retrieve(acfa.Id_abstract, dbcon);
                    }
                    while (acfa != null && (acf != null && acf.Id_abstract != ic_platform.Id_abstract_inner));

                    return platform_arguments;
                }

                private string removePlatformPrefix(string s)
                {
                    return s.Replace("platform-", "");
                }

                public void resolveAllComponentHierarchy_(IDbConnection dbcon)
                {
                    //Console.WriteLine("ENTER RESOLVE NODE {0} {1}", this.ID, this.ACFARef.Id_abstract);

                    if (new List<int>() { Constants.KIND_QUALIFIER, Constants.KIND_QUANTIFIER, Constants.KIND_PLATFORM }.Contains(this.Kind))
                        return;

                    if (ComponentDB == null)
                    {
                        org.hpcshelf.database.Component[] c;
                        if (this.Kind == Constants.KIND_BINDING)
                        {
                            string choice_1 = "org.hpcshelf.binding.channel.impl.BindingImpl";
                            string choice_2 = "org.hpcshelf.binding.channel.impl.SAFeBindingImpl";
                            c = new org.hpcshelf.database.Component[2] { cdao.retrieve_libraryPath(choice_1, dbcon), cdao.retrieve_libraryPath(choice_2, dbcon) };
                        }
                        else
                            c = new org.hpcshelf.database.Component[1] { resolveUnit(this.ACFARef, this.Arguments, this.ArgumentsTop, dbcon)[0] };

                        this.ComponentDB = c;
                    }

                    this.resolveNode(this.ComponentDB, dbcon);

                    IDictionary<string, ResolveComponentHierarchy> args = new ConcurrentDictionary<string, ResolveComponentHierarchy>();

                    foreach (KeyValuePair<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> inner in this.InnerComponents)
                    {
                        string cstr_inner = this.ID + "." + inner.Key;
                        InnerComponent ic = icdao.retrieve(this.ACFARef.Id_abstract, inner.Key, dbcon);

                        if (inner.Value.Count == 0)
                            continue;

                        ResolveComponentHierarchy inner_component_hierarchy = inner.Value[0].Item1;

                        IList<ResolveComponentHierarchy> inner_component_hierarchy_list = new List<ResolveComponentHierarchy>();

                        if (this.IsPrivate[inner.Key])
                            args[inner.Key] = inner_component_hierarchy;
                    }

                    foreach (ResolveComponentHierarchy a in args.Values)
                        ((ResolveComponentHierarchyImpl)a).resolveAllComponentHierarchy_(dbcon);
                }

                public void resolveNode(org.hpcshelf.database.Component[] c, IDbConnection dbcon)
                {
                    var t = resolveNode(c, this.UnitMapping, this.ACFARef, this.Arguments, this.ArgumentsTop, dbcon);
                    this.ComponentChoice = t.Item1;
                    this.UnitMapping = t.Item2;
                    this.ACFARef = t.Item3;
                }

                private static Tuple<string[], Instantiator.UnitMappingType[][], AbstractComponentFunctorApplication>
                                                                    resolveNode(org.hpcshelf.database.Component[] c,
                                                                                Instantiator.UnitMappingType[][] unit_mapping,
                                                                                AbstractComponentFunctorApplication acfaRef,
                                                                                IDictionary<string, int> arguments,
                                                                                IDictionary<string, int> arguments_top, IDbConnection dbcon)
                {
                    int id_abstract = c[0].GetId_abstract(dbcon);
                    AbstractComponentFunctor acf = acfdao.retrieve(id_abstract, dbcon);

                    string[] component_choice = new string[c.Length];
                    for (int i = 0; i < c.Length; i++)
                        component_choice[i] = c[i].Library_path;

                    updateUnitIdInUnitMapping(id_abstract, unit_mapping[0], dbcon);

                    AbstractComponentFunctor acf_impl = acfdao.retrieve(id_abstract, dbcon);
                    IDictionary<string, int> argumentsImpl;
                    if (acf_impl.Id_functor_app_supertype > 0)
                    {
                        determineArguments(arguments_top, arguments, acf_impl.Id_functor_app_supertype, out argumentsImpl, dbcon);
                        foreach (KeyValuePair<string, int> t in arguments)
                            if (!argumentsImpl.ContainsKey(t.Key))
                                argumentsImpl.Add(t);
                        arguments = argumentsImpl;
                    }

                    int id_functor_app = generateACFAforContext(id_abstract, c[0].Id_functor_app, acfaRef.Id_functor_app, arguments, arguments_top, 0, dbcon);
                    acfaRef = Backend.acfadao.retrieve(id_functor_app, dbcon);

                    // IT IS ASSUMED THAT THE ALTERNATIVE BINDING CHANNELS HAVE THE SAME UNIT INFORMATION ...
                    IDictionary<string, Tuple<string, string>> class_name_0 = calculateClassName(c[0], arguments_top, arguments, acfaRef, dbcon);
                    IDictionary<string, string[]> port_names_0 = calculatePortNames(c[0], dbcon);
                    foreach (Instantiator.UnitMappingType um in unit_mapping[0])
                    {
                        um.assembly_string = class_name_0[um.unit_id].Item1;
                        um.class_name = class_name_0[um.unit_id].Item2;
                        um.port_name = port_names_0[um.unit_id];
                    }

                    Instantiator.UnitMappingType[][] new_unit_mapping;
                    if (component_choice.Length > 1)
                    {
                        IDictionary<string, Tuple<string, string>> class_name_1 = calculateClassName(c[1], arguments_top, arguments, acfaRef, dbcon);
                        IDictionary<string, string[]> port_names_1 = calculatePortNames(c[1], dbcon);

                        new_unit_mapping = new Instantiator.UnitMappingType[2][];
                        new_unit_mapping[0] = unit_mapping[0];
                        new_unit_mapping[1] = new Instantiator.UnitMappingType[new_unit_mapping[0].Length];
                        for (int i = 0; i < new_unit_mapping[1].Length; i++)
                        {
                            new_unit_mapping[1][i] = new Instantiator.UnitMappingType();
                            new_unit_mapping[1][i].facet = new_unit_mapping[0][i].facet;
                            new_unit_mapping[1][i].facetSpecified = new_unit_mapping[0][i].facetSpecified;
                            new_unit_mapping[1][i].facet_instance = new_unit_mapping[0][i].facet_instance;
                            new_unit_mapping[1][i].facet_instanceSpecified = new_unit_mapping[0][i].facet_instanceSpecified;
                            new_unit_mapping[1][i].unit_id = new_unit_mapping[0][i].unit_id;
                            new_unit_mapping[1][i].unit_id_top = new_unit_mapping[0][i].unit_id_top;
                            new_unit_mapping[1][i].node = new_unit_mapping[0][i].node;
                        }

                        foreach (Instantiator.UnitMappingType um in new_unit_mapping[1])
                        {
                            um.assembly_string = class_name_1[um.unit_id].Item1;
                            um.class_name = class_name_1[um.unit_id].Item2;
                            um.port_name = port_names_1[um.unit_id];
                        }
                    }
                    else
                        new_unit_mapping = unit_mapping;

                    return new Tuple<string[], Instantiator.UnitMappingType[][], AbstractComponentFunctorApplication>(component_choice, new_unit_mapping, acfaRef);


                }

                private static void updateUnitIdInUnitMapping(int id_abstract, Instantiator.UnitMappingType[] unit_mapping, IDbConnection dbcon)
                {
                    IList<Interface> iList = idao.list(id_abstract, dbcon);

                    foreach (Instantiator.UnitMappingType um_item in unit_mapping)
                    {
                        foreach (Interface i in iList)
                        {
                            if (i.GetId_interface_list(dbcon).Contains(um_item.unit_id))
                            {
                                um_item.unit_id = i.GetId_interface_list(dbcon)[0];
                                break;
                            }
                        }
                    }
                }

                private int find_parameterized_id_functor_app_inner(int id_abstract, InnerComponent ic, IDictionary<string, int> arguments, IDbConnection dbcon)
                {
                    Stack<AbstractComponentFunctor> acf_stack = new Stack<AbstractComponentFunctor>();

                    {
                        AbstractComponentFunctor acf;
                        AbstractComponentFunctorApplication acfa;
                        int id_abstract_loop = id_abstract;
                        while (id_abstract_loop != ic.Id_abstract_owner)
                        {
                            acf = acfdao.retrieve(id_abstract_loop, dbcon);
                            acf_stack.Push(acf);

                            acfa = acfadao.retrieve(acf.Id_functor_app_supertype, dbcon);
                            id_abstract_loop = acfa.Id_abstract;
                        }
                    }

                    int id_functor_app_inner;
                    string parameter_top = ic.Parameter_top;
                    while (acf_stack.Count > 0)
                    {
                        AbstractComponentFunctor acf = acf_stack.Pop();

                        ICollection<SupplyParameter> sp_list = spdao.list(acf.Id_functor_app_supertype, dbcon);
                        foreach (SupplyParameter sp in sp_list)
                        {
                            if (sp.Id_parameter.Equals(parameter_top))
                            {
                                if (sp is SupplyParameterParameter)
                                {
                                    SupplyParameterParameter spp = (SupplyParameterParameter)sp;
                                    parameter_top = spp.Id_argument;
                                }
                                else if (sp is SupplyParameterComponent)
                                {
                                    SupplyParameterComponent spc = (SupplyParameterComponent)sp;
                                    return spc.Id_functor_app_actual;
                                }
                            }
                        }
                    }

                    arguments.TryGetValue(parameter_top, out id_functor_app_inner);

                    return id_functor_app_inner;
                }

                public static string[] calculatePortNames(int id_abstract, string id_interface, IDbConnection dbcon)
                {
                    IList<Slice> sList = Backend.sdao.listByInterface(id_abstract, id_interface, dbcon);
                    IDictionary<string, Slice> portNameList = new ConcurrentDictionary<string, Slice>();
                    IList<string> replicated_port_name = new List<string>();

                    foreach (Slice slice in sList)
                    {
                        InnerComponent ic = Backend.icdao.retrieve(slice.Id_abstract, slice.Id_inner, dbcon);
                        if (ic == null)
                            continue;
                        AbstractComponentFunctor acf_inner = Backend.acfdao.retrieve(ic.Id_abstract_inner, dbcon);

                        int slice_kind_inner = Constants.kindInt[acf_inner.Kind];

                        if (!new List<int>() { Constants.KIND_QUALIFIER, Constants.KIND_QUANTIFIER, Constants.KIND_PLATFORM }.Contains(slice_kind_inner))
                        {
                            //  Console.WriteLine("calculatePortNames 1 --- {0}", slice.Id_inner);
                            Slice other_slice;
                            if (!portNameList.TryGetValue(slice.Id_inner, out other_slice))
                            {
                                portNameList[slice.Id_inner /*+ "." + slice.Id_interface_slice*/] = slice;
                            }
                            else
                            {
                                //   Console.WriteLine("calculatePortNames 2 --- multiple slices of the same inner --- {0}.{1} / {2}.{3}", slice.Id_inner, slice.Id_interface_slice, other_slice.Id_inner, other_slice.Id_interface_slice);
                                if (!replicated_port_name.Contains(slice.Id_inner)) replicated_port_name.Add(slice.Id_inner);
                                portNameList[slice.Id_inner + "." + slice.Id_interface_slice] = slice;
                                portNameList[other_slice.Id_inner + "." + other_slice.Id_interface_slice] = other_slice;
                            }
                        }
                    }

                    foreach (string port_name_delete in replicated_port_name)
                        portNameList.Remove(port_name_delete);

                    string[] portNameArray = new string[portNameList.Count];
                    portNameList.Keys.CopyTo(portNameArray, 0);

                    return portNameArray;
                }

                private static IDictionary<string, string[]> calculatePortNames(org.hpcshelf.database.Component c, IDbConnection dbcon)
                {
                    IDictionary<string, string[]> port_names_dict = new ConcurrentDictionary<string, string[]>();

                    foreach (Interface i in idao.list(c.GetId_abstract(dbcon), dbcon))
                    {
                        IList<string> port_name_list = new List<string>();

                        org.hpcshelf.database.Unit u = DGAC.Backend.takeUnit(c, i.Id_interface, dbcon);

                        string[] port_names = calculatePortNames(c.GetId_abstract(dbcon), u.Id_interface, dbcon);

                        port_names_dict[i.Id_interface] = port_names;
                    }

                    return port_names_dict;
                }


                private static IDictionary<string, Tuple<string, string>> calculateClassName(org.hpcshelf.database.Component c, IDictionary<string, int> arguments_top, IDictionary<string, int> arguments, AbstractComponentFunctorApplication acfaRef, IDbConnection dbcon)
                {
                    IDictionary<string, Tuple<string, string>> class_name_list = new ConcurrentDictionary<string, Tuple<string, string>>();

                    foreach (Interface i in idao.list(acfaRef.Id_abstract, dbcon))
                    {
                        string id_unit = i.Id_interface;

                        Unit u = takeUnit(c, id_unit, dbcon);

                        string class_name_worker;

                        string[] arguments_new;
                        calculateActualParams2(arguments_top, arguments, acfaRef, id_unit, out arguments_new, dbcon);

                        calculateGenericClassName2(u, arguments_new, out class_name_worker, dbcon);

                        class_name_list[i.Id_interface] = new Tuple<string, string>(u.Assembly_string, class_name_worker);
                    }

                    return class_name_list;
                }



                public void findPublicInnerComponent(string id_inner, IList<Tuple<ResolveComponentHierarchy, int[]>> inner_component_hierarchy_list, IDbConnection dbcon)
                {
                    foreach (Tuple<string, ResolveComponentHierarchy> host_component in this.HostComponents)
                    {
                        ResolveComponentHierarchyImpl host_component_hierarchy = (ResolveComponentHierarchyImpl)host_component.Item2;

                        int id_abstract;
                        org.hpcshelf.database.Component c = cdao.retrieve_libraryPath(host_component_hierarchy.ComponentChoice[0], dbcon);
                        if (c != null)
                            id_abstract = c.GetId_abstract(dbcon);
                        else
                            id_abstract = acfdao.retrieve_libraryPath(host_component_hierarchy.ComponentChoice[0], dbcon).Id_abstract;
                        IList<InnerComponentExposed> iceList = icedao.retrieveContainerList(id_abstract, this.getPortName(host_component_hierarchy.ID) /*TODO: para cada host, um id_inner diferente, i.e. component_hierarchy.PortName[host_component.ID]*/, id_inner, dbcon);
                        if (iceList != null && iceList.Count > 0)
                        {
                            foreach (InnerComponentExposed ice in iceList)
                            {
                                bool is_private = host_component_hierarchy.IsPrivate[ice.Id_inner_rename];
                                if (is_private)
                                {
                                    IList<int> facets_list = sedao.findFacets(ice.Id_abstract, ice.Id_inner_rename, ice.Id_inner_owner, dbcon);

                                    foreach (Tuple<ResolveComponentHierarchy, int[]> inner_component_hierarchy in host_component_hierarchy.InnerComponents[ice.Id_inner_rename])
                                    {
                                        // TODO: OPTIMIZE !!! VERY INNEFICIENT
                                        bool flag = true;
                                        foreach (Tuple<ResolveComponentHierarchy, int[]> item in inner_component_hierarchy_list)
                                        {
                                            if (item.Item1 == inner_component_hierarchy.Item1)
                                            {
                                                flag = false;
                                                break;
                                            }
                                        }

                                        Tuple<ResolveComponentHierarchy, int[]> a = new Tuple<ResolveComponentHierarchy, int[]>(inner_component_hierarchy.Item1, facets_list.ToArray());
                                        // bool test = inner_component_hierarchy_list.Contains(a);

                                        if (flag)
                                            inner_component_hierarchy_list.Add(a);
                                    }
                                }
                                else
                                    host_component_hierarchy.findPublicInnerComponent(ice.Id_inner_rename, inner_component_hierarchy_list, dbcon);
                            }
                        }
                    }
                }

                public void setUnitMappingNodes(ResolveComponentHierarchy component_hierarchy_host, IDbConnection dbcon)
                {
                    org.hpcshelf.database.Component c = cdao.retrieve_libraryPath(component_hierarchy_host.ComponentChoice[0], dbcon);
                    int id_abstract = c != null ? c.GetId_abstract(dbcon) : acfdao.retrieve_libraryPath(component_hierarchy_host.ComponentChoice[0], dbcon).Id_abstract;

                    InnerComponent ic = icdao.retrieve(id_abstract, this.getPortName(component_hierarchy_host.ID), dbcon);

                    set_nodes_in_unit_mapping_of_inner(id_abstract, ic, component_hierarchy_host.UnitMapping[0], this.UnitMapping[0], dbcon);
                    if (this.UnitMapping.Length > 1)
                        set_nodes_in_unit_mapping_of_inner(id_abstract, ic, component_hierarchy_host.UnitMapping.Length > 1 ? component_hierarchy_host.UnitMapping[1] : component_hierarchy_host.UnitMapping[0], this.UnitMapping[1], dbcon);

                    foreach (KeyValuePair<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> inner_component_hierarchy_list in this.InnerComponents)
                    {
                        if (this.IsPrivate[inner_component_hierarchy_list.Key])
                            foreach (Tuple<ResolveComponentHierarchy, int[]> inner_ctree in inner_component_hierarchy_list.Value)
                                inner_ctree.Item1.setUnitMappingNodes(this, dbcon);
                    }
                }

                public IList<Tuple<string, ResolveComponentHierarchy>> checkHosts(int[] facet_list)
                {
                    IList<Tuple<string, ResolveComponentHierarchy>> result_hosts = new List<Tuple<string, ResolveComponentHierarchy>>();

                    foreach (Tuple<string, ResolveComponentHierarchy> ctree_host in this.HostComponents)
                    {
                        //                        Console.WriteLine("ctree_host.Item1 = {0}; ctree_host.Item2.ID = {1}", ctree_host.Item1, ctree_host.Item2.ID);
                        //                        if (ctree_host.Item2.ID.Equals("teste.workflow"))
                        //                      {
                        //                        foreach (string k in ctree_host.Item2.InnerComponents.Keys)
                        //                          Console.Write("{0}, ", k);
                        //                    Console.WriteLine();
                        //              }

                        IList<Tuple<ResolveComponentHierarchy, int[]>> ctree_inner_host_list = ctree_host.Item2.InnerComponents[ctree_host.Item1];

                        foreach (Tuple<ResolveComponentHierarchy, int[]> ctree_inner_host_ in ctree_inner_host_list)
                        {
                            ResolveComponentHierarchy ctree_inner_host = ctree_inner_host_.Item1;
                            int[] facets = ctree_inner_host_.Item2;

                            if (ctree_inner_host == this && facets.Intersect(facet_list).Count() > 0)
                                result_hosts.Add(new Tuple<string, ResolveComponentHierarchy>(ctree_host.Item1, ctree_host.Item2));
                        }
                    }

                    // Remove "non-leaf" hosts ...

                    /* IList<Tuple<string, ResolveComponentHierarchy>> result_hosts_copy = new List<Tuple<string, ResolveComponentHierarchy>>(result_hosts);

                     bool next = false;
                     foreach (Tuple<string, ResolveComponentHierarchy> host_2 in result_hosts)
                     {
                         next = false;
                         foreach (Tuple<string, ResolveComponentHierarchy> host_1 in result_hosts)
                         {
                             foreach (Tuple<string, ResolveComponentHierarchy> host_host_1 in host_1.Item2.HostComponents)
                             {
                                 if (host_host_1.Item2 == host_2.Item2)
                                 {
                                     result_hosts_copy.Remove(host_2);
                                     next = true;
                                     break;
                                 }
                             }
                         }
                         if (next)
                             break;
                     }*/

                    return result_hosts;
                }

                public void removeInnerComponent(string id_inner_removed)
                {
                    this.IsPrivate.Remove(id_inner_removed);
                    foreach (Tuple<ResolveComponentHierarchy, int[]> t in this.InnerComponents[id_inner_removed])
                    {
                        ResolveComponentHierarchy ctree = t.Item1;
                        foreach (Tuple<string, ResolveComponentHierarchy> tt in ctree.HostComponents)
                        {
                            ResolveComponentHierarchy ctree_host = tt.Item2;
                            ctree_host.InnerComponents.Remove(tt.Item1);
                        }
                    }
                }

                public void setSelection(org.hpcshelf.database.Component c)
                {
                    this.ComponentDB = new org.hpcshelf.database.Component[1] { c };
                }

                public void clearSelection()
                {
                    this.ComponentDB = null;
                }
            }

            public interface BaseComponentHierarchyVisitor
            {
                object visit(BaseComponentHierarchy ctree, IDbConnection dbcon);
            }

            public interface ResolveComponentHierarchyVisitor
            {
                object visit(ResolveComponentHierarchy ctree, IDbConnection dbcon);
            }



            public class DependenciesCalculator : ResolveComponentHierarchyVisitor
            {
                private IDictionary<string, AbstractComponentFunctorApplication> component_contract = null;
                private IDictionary<string, ISet<int>>[] dependency_list = null;
                private IList<string>[] dependency_order = null;
                private IDictionary<string, int>[] dependency_counter = null;

                public IDictionary<string, AbstractComponentFunctorApplication> ComponentContract => component_contract;
                public IDictionary<string, ISet<int>>[] Dependencies => dependency_list;
                public IDictionary<string, int>[] DependenciesCounter => dependency_counter;
                public IList<string>[] DependenciesOrder => dependency_order;

                private int facet_instance_count;

                public DependenciesCalculator(int facet_instance_count)
                {
                    this.facet_instance_count = facet_instance_count;

                    component_contract = new ConcurrentDictionary<string, AbstractComponentFunctorApplication>();
                    dependency_list = new IDictionary<string, ISet<int>>[facet_instance_count];
                    dependency_order = new IList<string>[facet_instance_count];
                    dependency_counter = new IDictionary<string, int>[facet_instance_count];
                    for (int i = 0; i < facet_instance_count; i++)
                    {
                        dependency_list[i] = new ConcurrentDictionary<string, ISet<int>>();
                        dependency_order[i] = new List<string>();
                        dependency_counter[i] = new ConcurrentDictionary<string, int>();
                    }
                }

                public object visit(ResolveComponentHierarchy ctree, IDbConnection dbcon)
                {
                    foreach (KeyValuePair<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> n in ctree.InnerComponents)
                    {
                        foreach (Tuple<ResolveComponentHierarchy, int[]> nn in n.Value)
                            visit(nn.Item1, dbcon);
                    }

                    // if (ctree.ID.Equals("testing_native.application"))
                    //     Console.WriteLine();

                    IDictionary<string, IDictionary<int, ISet<int>>> dependency_list_1 = new ConcurrentDictionary<string, IDictionary<int, ISet<int>>>();
                    IDictionary<string, IDictionary<int, ISet<int>>> dependency_list_2 = null;
                    IList<string> dependency_order_1 = new List<string>();
                    IList<string> dependency_order_2 = null;

                    if (new List<int>() { Constants.KIND_QUALIFIER, Constants.KIND_QUANTIFIER, Constants.KIND_PLATFORM }.Contains(ctree.Kind) || acfdao.retrieve_libraryPath(ctree.ComponentChoice[0], dbcon) != null)
                    {
                        calculateDependenciesAbstract(ctree.ComponentChoice[0], ctree.FacetList, dependency_list_1, dependency_order_1, dbcon);
                        dependency_list_2 = new ConcurrentDictionary<string, IDictionary<int, ISet<int>>>(dependency_list_1);
                        dependency_order_2 = new List<string>(dependency_order_1);
                    }
                    else
                    {
                        foreach (string acfa_ref in ctree.ComponentChoice)
                            ComponentContract[acfa_ref] = ctree.ACFARef;
                        calculateDependenciesConcrete(ctree.ComponentChoice, component_contract, ctree.FacetList, dependency_list_1, out dependency_list_2, dependency_order_1, out dependency_order_2, dbcon);
                    }

                    for (int i = 0; i < facet_instance_count; i++)
                        if (ctree.isInFacet(i))
                            copyToDependencyList(i, i == 0 ? dependency_list_2 : dependency_list_1, i == 0 ? dependency_order_2 : dependency_order_1);

                    return null;
                }

                private void copyToDependencyList(
                      int facet_instance,
                      IDictionary<string, IDictionary<int, ISet<int>>> dependency_list_to_add,
                      IList<string> dependency_order
                    )
                {
                    //foreach (KeyValuePair<string,IDictionary<int,ISet<int>>> v in dependency_list_to_add)
                    foreach (string key in dependency_order)
                    {
                        IDictionary<int, ISet<int>> v = dependency_list_to_add[key];

                        int counter = 0;
                        if (v.ContainsKey(facet_instance))
                        {
                            if (!dependency_counter[facet_instance].TryGetValue(key, out counter))
                            {
                                this.dependency_counter[facet_instance][key] = 0;
                                this.dependency_list[facet_instance][key] = v[facet_instance];
                                this.dependency_order[facet_instance].Add(key);
                            }
                            else
                            {
                                this.dependency_counter[facet_instance][key] = counter + 1;
                                this.dependency_list[facet_instance][key] = new HashSet<int>(this.dependency_list[facet_instance][key].Union(v[facet_instance]));
                            }
                        }
                    }
                }

                private void calculateDependenciesAbstract(
                    string library_path,
                    IList<int>[] facet_list, IDictionary<string, IDictionary<int, ISet<int>>> dependency_list,
                    IList<string> dependency_order,
                    IDbConnection dbcon)
                {
                    int id_abstract_next;
                    AbstractComponentFunctor acf = acfdao.retrieve_libraryPath(library_path, dbcon);

                    IDictionary<int, ISet<int>> local_facet = new ConcurrentDictionary<int, ISet<int>>();
                    for (int facet_instance = 0; facet_instance < facet_instance_count; facet_instance++)
                    {
                        if (facet_instance < facet_list.Length)
                        {
                            foreach (int f in facet_list[facet_instance])
                            {
                                if (!local_facet.ContainsKey(f))
                                    local_facet[f] = new HashSet<int>();
                                local_facet[f].Add(facet_instance);
                            }
                        }
                    }

                    Stack<Tuple<AbstractComponentFunctor, IDictionary<int, ISet<int>>>> m = new Stack<Tuple<AbstractComponentFunctor, IDictionary<int, ISet<int>>>>();

                    IDictionary<string, ISet<int>> interface_list_super = new ConcurrentDictionary<string, ISet<int>>();
                    IList<Interface> interface_list = idao.list(acf.Id_abstract, dbcon);
                    foreach (Interface i in interface_list)
                    {
                        if (local_facet.ContainsKey(i.Facet))
                        {
                            interface_list_super[i.Id_interface] = new HashSet<int>();
                            interface_list_super[i.Id_interface].UnionWith(local_facet[i.Facet]);
                        }
                    }

                    do
                    {
                        IDictionary<int, ISet<int>> fs = new ConcurrentDictionary<int, ISet<int>>();
                        IDictionary<string, ISet<int>> interface_list_super_ = new ConcurrentDictionary<string, ISet<int>>();
                        interface_list = idao.list(acf.Id_abstract, dbcon);
                        foreach (Interface i in interface_list)
                        {
                            if (interface_list_super.ContainsKey(i.Id_interface))
                            {
                                ISet<int> s;
                                ISet<int> facet_instance_list = interface_list_super[i.Id_interface];
                                foreach (int facet_instance in facet_instance_list)
                                {
                                    if (!fs.TryGetValue(facet_instance, out s))
                                    {
                                        s = new HashSet<int>();
                                        fs[facet_instance] = s;
                                    }
                                    s.Add(i.Facet);
                                }

                                string[] ids = i.Id_interface_super.Split(new char[1] { '+' });
                                foreach (string id in ids)
                                {

                                    ISet<int> fsl;
                                    if (!interface_list_super_.TryGetValue(id, out fsl))
                                        interface_list_super_[id] = interface_list_super[i.Id_interface];
                                    else
                                        foreach (int j in interface_list_super[i.Id_interface])
                                            interface_list_super_[id].Add(j);
                                }
                            }
                        }

                        interface_list_super = interface_list_super_;
                        m.Push(new Tuple<AbstractComponentFunctor, IDictionary<int, ISet<int>>>(acf, fs));

                        int id_functor_app = acf.Id_functor_app_supertype;
                        AbstractComponentFunctorApplication acfa = acfadao.retrieve(id_functor_app, dbcon);
                        id_abstract_next = acfa != null ? acfa.Id_abstract : 0;
                        acf = acfdao.retrieve(id_abstract_next, dbcon);

                    }
                    while (id_abstract_next > 0);

                    while (m.Count > 0)
                    {
                        Tuple<AbstractComponentFunctor, IDictionary<int, ISet<int>>> e = m.Pop();

                        AbstractComponentFunctor acf1 = e.Item1;

                        // LOOK AT PARAMETERS
                        IList<AbstractComponentFunctorParameter> acfp_list = acfpdao.list(acf1.Id_abstract, dbcon);
                        foreach (AbstractComponentFunctorParameter acfp_parameter in acfp_list)
                        {
                            AbstractComponentFunctorApplication acfa_argument = acfadao.retrieve(acfp_parameter.Bounds_of, dbcon);
                            AbstractComponentFunctor acf_parameter = acfdao.retrieve(acfa_argument.Id_abstract, dbcon);

                            if (!dependency_list.ContainsKey(acf_parameter.Library_path))
                            {
                                calculateDependenciesAbstract(acf_parameter.Library_path, facet_list, dependency_list, dependency_order, dbcon);
                                //    dependency_list.Add(acf_parameter.Library_path, e.Item2);
                            }
                        }

                        // LOOK AT INNER COMPONENTS
                        IList<InnerComponent> ic_list = icdao.list(acf1.Id_abstract, dbcon);
                        foreach (InnerComponent ic in ic_list)
                        {
                            AbstractComponentFunctorApplication acfa_argument = acfadao.retrieve(ic.Id_functor_app, dbcon);
                            AbstractComponentFunctor acf_inner = acfdao.retrieve(acfa_argument.Id_abstract, dbcon);
                            if (!dependency_list.ContainsKey(acf_inner.Library_path))
                            {
                                calculateDependenciesAbstract(acf_inner.Library_path, facet_list, dependency_list, dependency_order, dbcon);
                                //     dependency_list.Add(acf_inner.Library_path, e.Item2);
                            }

                            // TODO: PERCORRER OS ARGUMENTOS PASSADOS PARA O COMPONENTE ANINHADO !!!
                            foreach (SupplyParameter sp in spdao.list(ic.Id_functor_app, dbcon))
                            {
                                if (sp is SupplyParameterComponent)
                                {
                                    SupplyParameterComponent spc = (SupplyParameterComponent)sp;
                                    AbstractComponentFunctor acf_argument = acfdao.retrieve(acfadao.retrieve(spc.Id_functor_app_actual, dbcon).Id_abstract, dbcon);
                                    if (!dependency_list.ContainsKey(acf_argument.Library_path))
                                        calculateDependenciesAbstract(acf_argument.Library_path, facet_list, dependency_list, dependency_order, dbcon);
                                }
                            }

                        }

                        // LOOK AT THE ARGUMENTS PASSED TO THE SUPERTYPE
                        ICollection<SupplyParameter> sp_list = spdao.list(acf1.Id_functor_app_supertype, dbcon);
                        foreach (SupplyParameter sp in sp_list)
                        {
                            if (sp is SupplyParameterComponent)
                            {
                                SupplyParameterComponent spc = (SupplyParameterComponent)sp;
                                AbstractComponentFunctorApplication acfa_argument = acfadao.retrieve(spc.Id_functor_app_actual, dbcon);
                                AbstractComponentFunctor acf_argument = acfdao.retrieve(acfa_argument.Id_abstract, dbcon);
                                if (!dependency_list.ContainsKey(acf_argument.Library_path))
                                {
                                    calculateDependenciesAbstract(acf_argument.Library_path, facet_list, dependency_list, dependency_order, dbcon);
                                    //     dependency_list.Add(acf_argument.Library_path, e.Item2);
                                }
                            }
                        }

                        if (!dependency_list.ContainsKey(acf1.Library_path))
                        {
                            dependency_list.Add(acf1.Library_path, e.Item2);
                            dependency_order.Add(acf1.Library_path);
                        }
                    }
                }

                private void calculateDependenciesConcrete(
                        IList<string> library_path,
                        IDictionary<string, AbstractComponentFunctorApplication> contract,
                        IList<int>[] facet_list, IDictionary<string, IDictionary<int, ISet<int>>> dependency_list_1,
                        out IDictionary<string, IDictionary<int, ISet<int>>> dependency_list_2,
                        IList<string> dependency_order_1,
                        out IList<string> dependency_order_2,
                        IDbConnection dbcon)
                {
                    org.hpcshelf.database.Component c1 = cdao.retrieve_libraryPath(library_path[0], dbcon);
                    c1.SetComponentContract(component_contract[library_path[0]], dbcon);

                    AbstractComponentFunctorApplication acfa1 = acfadao.retrieve(c1.Id_functor_app, dbcon);
                    AbstractComponentFunctor acf1 = acfdao.retrieve(acfa1.Id_abstract, dbcon);

                    calculateDependenciesAbstract(acf1.Library_path, facet_list, dependency_list_1, dependency_order_1, dbcon);

                    foreach (KeyValuePair<string, AbstractComponentFunctorApplication> par in c1.GetArguments(dbcon))
                    {
                        AbstractComponentFunctorApplication acfa_argument = acfadao.retrieve(par.Value.Id_functor_app, dbcon);
                        AbstractComponentFunctor acf_argument = acfdao.retrieve(acfa_argument.Id_abstract, dbcon);
                        if (!dependency_list_1.ContainsKey(acf_argument.Library_path))
                            calculateDependenciesAbstract(acf_argument.Library_path, facet_list, dependency_list_1, dependency_order_1, dbcon);
                    }

                    {
                        IDictionary<int, ISet<int>> t = new ConcurrentDictionary<int, ISet<int>>();
                        for (int facet_instance = 0; facet_instance < facet_instance_count; facet_instance++)
                            if (facet_instance < facet_list.Length)
                                if (facet_list[facet_instance] == null || facet_list[facet_instance].Count > 0)
                                    t[facet_instance] = new HashSet<int>(facet_list[facet_instance]);
                        dependency_list_1.Add(c1.Library_path, t);
                        dependency_order_1.Add(c1.Library_path);
                    }

                    if (library_path.Count > 1)
                    {
                        dependency_list_2 = new ConcurrentDictionary<string, IDictionary<int, ISet<int>>>();
                        dependency_order_2 = new List<string>();

                        org.hpcshelf.database.Component c2 = cdao.retrieve_libraryPath(library_path[1], dbcon);
                        c1.SetComponentContract(component_contract[library_path[1]], dbcon);

                        AbstractComponentFunctorApplication acfa2 = acfadao.retrieve(c1.Id_functor_app, dbcon);
                        AbstractComponentFunctor acf2 = acfdao.retrieve(acfa1.Id_abstract, dbcon);

                        calculateDependenciesAbstract(acf2.Library_path, facet_list, dependency_list_2, dependency_order_2, dbcon);

                        foreach (KeyValuePair<string, AbstractComponentFunctorApplication> par in c2.GetArguments(dbcon))
                        {
                            AbstractComponentFunctorApplication acfa_argument = acfadao.retrieve(par.Value.Id_functor_app, dbcon);
                            AbstractComponentFunctor acf_argument = acfdao.retrieve(acfa_argument.Id_abstract, dbcon);
                            if (!dependency_list_1.ContainsKey(acf_argument.Library_path))
                                calculateDependenciesAbstract(acf_argument.Library_path, facet_list, dependency_list_1, dependency_order_1, dbcon);
                        }



                        {
                            IDictionary<int, ISet<int>> t = new ConcurrentDictionary<int, ISet<int>>();
                            for (int facet_instance = 0; facet_instance < facet_instance_count; facet_instance++)
                                if (facet_list[facet_instance] == null || facet_list[facet_instance].Count > 0)
                                    t[facet_instance] = new HashSet<int>(facet_list[facet_instance]);
                            dependency_list_2.Add(c2.Library_path, t);
                            dependency_order_2.Add(c2.Library_path);
                        }
                    }
                    else
                    {
                        dependency_list_2 = new ConcurrentDictionary<string, IDictionary<int, ISet<int>>>(dependency_list_1);
                        dependency_order_2 = new List<string>(dependency_order_1);
                    }
                }
            }
        } //BackEnd

        public class PortUsageManager
        {
            private IDictionary<string, int> portFetches = new ConcurrentDictionary<string, int>();
            private IDictionary<string, int> portReleases = new ConcurrentDictionary<string, int>();

            public bool isFetched(string portname)
            {
                int count = 0;
                return portFetches.TryGetValue(portname, out count) && count > 0;
            }

            public bool isReleased(string portname)
            {
                int count = 0;
                return !portFetches.ContainsKey(portname) || portReleases.TryGetValue(portname, out count) && count > 0;
            }

            public void addPortFetch(string portName)
            {
                int count_fetch = 0;
                if (portFetches.TryGetValue(portName, out count_fetch))
                    portFetches.Remove(portName);

                int count_release = 0;
                if (portReleases.TryGetValue(portName, out count_release))
                    portReleases.Remove(portName);

                if (count_release > 0)
                {
                    Console.Error.WriteLine("Port " + portName + " was released (release count=" + count_release + ")");
                    throw new CCAExceptionImpl(CCAExceptionType.PortNotInUse);
                }

                portFetches.Add(portName, count_fetch + 1);

            }

            public void addPortRelease(string portName)
            {
                int count_release = 0;
                if (portReleases.TryGetValue(portName, out count_release))
                    portReleases.Remove(portName);

                if (count_release > 0)
                {
                    Console.Error.WriteLine("Port " + portName + " still in use (release count = " + count_release + ")");
                    throw new CCAExceptionImpl(CCAExceptionType.UsesPortNotReleased);
                }

                portReleases.Add(portName, count_release + 1);
            }

            public void resetPort(string portName)
            {
                portReleases.Remove(portName);
                portFetches.Remove(portName);
            }

        }

        [Serializable]
        public class CertificationFailedException : Exception
        {
            public CertificationFailedException()
            {
            }

            public CertificationFailedException(string message) : base(message)
            {
            }

            public CertificationFailedException(string message, Exception innerException) : base(message, innerException)
            {
            }

            protected CertificationFailedException(SerializationInfo info, StreamingContext context) : base(info, context)
            {
            }
        }


    }//namespace
}




