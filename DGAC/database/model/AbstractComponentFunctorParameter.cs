﻿using System;
using System.Collections.Generic;
using System.IO;

using System.Runtime.Serialization;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class AbstractComponentFunctorParameter
    {
        private string id_parameter;
        private int bounds_of;
        private int id_abstract;
        private VarianceType variance;
        private int order;
        private ParameterClass parameter_class;

        public static IDictionary<string, VarianceType> varianceType = new Dictionary<string, VarianceType>() { { "covariant", VarianceType.covariant }, { "contravariant" , VarianceType.contravariant }, { "invariant", VarianceType.invariant } };
        public static IDictionary<string, ParameterClass> parameterClass = new Dictionary<string, ParameterClass>() { { "application", ParameterClass.application }, { "platform" , ParameterClass.platform }, { "quality", ParameterClass.quality }, { "cost", ParameterClass.cost } };

		public string Id_parameter
        {
            get { return id_parameter; }
            set { id_parameter = value; }
        }

        public ParameterClass ParameterClass
		{
			get { return parameter_class; }
			set { parameter_class = value; }
		}
		
        public int Bounds_of
        {
            get { return bounds_of; }
            set { bounds_of = value; }
        }

        public int Id_abstract
        {
            get { return id_abstract; }
            set { id_abstract = value; }
        }

        public int Order
        {
            get { return order; }
            set { order = value; }
        }

        public VarianceType Variance
        {
            get { return variance; }
            set { variance = value; }
        }

    }//class

}//namespace