﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Globalization;
using System.Data;
using org.hpcshelf.DGAC;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class AbstractComponentFunctorApplicationQuantifier : AbstractComponentFunctorApplication
    {

        private decimal? quantifier = null;
        public decimal? Quantifier { get { return quantifier; } set { quantifier = value; } }

        //      public string quantifier_function = null;
        //		public string QuantifierFunction { get { return quantifier_function; } set { quantifier_function = value; } }

        public string getQuantifierString(IDbConnection dbcon)
        {
            AbstractComponentFunctor acf = Backend.acfdao.retrieve(this.Id_abstract, dbcon);
            if (acf.Library_path.EndsWith("IntUp") || acf.Library_path.EndsWith("IntDown"))
            {
                return Decimal.ToInt64(Quantifier.GetValueOrDefault()).ToString(CultureInfo.InvariantCulture);
            }
            else
                return Quantifier.GetValueOrDefault().ToString(CultureInfo.InvariantCulture);
        }

      /*  private string[] quantifier_dependency;
        public string[] QuantifierDependency
        {
            get { return quantifier_dependency; }
            set { quantifier_dependency = value; }

        }

        public void readQuantifierDependency(string quantifier_dependency)
        {
            QuantifierDependency = quantifier_dependency.Split(';');
        }

		public string readQuantifierDependencyDBString()
		{
            string r = "";

            for (int i=0; i < quantifier_dependency.Length; i ++)
                r += quantifier_dependency[i] + (i < quantifier_dependency.Length - 1 ? ";" : "");

            return r;
		}
	*/
    }//class

}//namespace
