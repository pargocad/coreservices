﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Diagnostics;


namespace org.hpcshelf.database
{
    [Serializable()]
    public class Certified {
 
	private int id_concrete;
	private int id_functor_app;
	  	
  	public int Id_concrete{
        get {return id_concrete;}
        set {id_concrete = value;}
    }
  	    
    public int Id_functor_app{
        get {return id_functor_app;}
        set {id_functor_app = value;}
    }

}//class

}//namespace