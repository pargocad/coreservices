﻿using System;
using System.Data;
using System.Collections.Generic;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class SliceExposedDAO
    {

        public void insert(SliceExposed ac, IDbConnection dbcon)
        {
            String sql =
                "INSERT INTO sliceexposed (id_abstract, id_interface_slice, id_interface_slice_owner, id_inner, id_inner_owner, id_inner_original, id_interface_slice_original, id_interface_host)" +
                " VALUES (" + ac.Id_abstract + ",'" + ac.Id_interface_slice + "','" + ac.Id_interface_slice_owner + "','" + ac.Id_inner + "','" + ac.Id_inner_owner + "','" + ac.Id_inner_original + "','" + ac.Id_interface_slice_original + "','" + ac.Id_interface_host + "')";

		//	Console.WriteLine("SliceExposedDAO.cs: TRY INSERT PUBLIC SLICE : " + sql);
			
            DBConnector.performSQLUpdate(sql, dbcon);
        }

		public void update_id_inner(SliceExposed ac, IDbConnection dbcon)
		{
            String sql = "UPDATE sliceexposed SET id_inner = '" + ac.Id_inner + "'" +
                          " WHERE id_abstract = " + ac.Id_abstract +
                          " AND id_inner_owner like '" + ac.Id_inner_owner + "'" +
                          " AND id_interface_slice_owner like '" + ac.Id_interface_slice_owner + "'" +
                          " AND id_inner_original like '" + ac.Id_inner_original + "'" +
                          " AND id_interface_slice_original like '" + ac.Id_interface_slice_original + "'" +
                          " AND id_inner like '" + ac.Id_inner_original + "'" +
                          " AND id_interface_host like '" + ac.Id_interface_host + "'"; 
            
			// Console.WriteLine("SliceExposedDAO.cs: TRY UPDATE SLICE EXPOSED : " + sql);

			DBConnector.performSQLUpdate(sql, dbcon);
		}

		public IList<SliceExposed> listExposedSlicesByContainer(int id_abstract, string id_inner_owner, string id_interface_slice_owner, IDbConnection dbcon)
        {

            IList<SliceExposed> list = new List<SliceExposed>();
      //     IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_abstract, id_inner, id_inner_owner, id_interface_slice, id_interface_slice_owner, id_inner_original, id_interface_slice_original, id_interface_host " +
                "FROM sliceexposed " +
                "WHERE id_abstract=" + id_abstract + " and " + 
                      "id_inner_owner like '" + id_inner_owner + "' and " +
                      "id_interface_slice_owner like '" + id_interface_slice_owner;
            dbcmd.CommandText = sql;
			//Console.WriteLine (sql);
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                SliceExposed s = new SliceExposed();
                s.Id_abstract = (int)reader["id_abstract"];
                s.Id_inner = (string)reader["id_inner"];
                s.Id_inner_owner = (string)reader["id_inner_owner"];
                s.Id_interface_slice = (string)reader["id_interface_slice"];
                s.Id_interface_slice_owner = (string)reader["id_interface_slice_owner"];
//               s.Slice_replica = (int)reader["slice_replica"];
//                s.Slice_replica_owner = (int)reader["slice_replica_owner"];
                s.Id_inner_original = (string)reader["id_inner_original"];
                s.Id_interface_slice_original = (string)reader["id_interface_slice_original"];
                s.Id_interface_host = (string)reader["id_interface_host"];
                list.Add(s);
            }//while
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return list;

        }

		public IList<SliceExposed> listSlicesOfInnerComponent(int id_abstract, string id_inner_owner, IDbConnection dbcon)
		{

			IList<SliceExposed> list = new List<SliceExposed>();
		// IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_abstract, id_inner, id_inner_owner, id_interface_slice, id_interface_slice_owner, id_inner_original, id_interface_slice_original, id_interface_host " +
                "FROM sliceexposed " +
                "WHERE id_abstract=" + id_abstract + " and " +
                "id_inner_owner like '" + id_inner_owner + "'";
			dbcmd.CommandText = sql;
		//	Console.WriteLine(sql);
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read())
			{
				SliceExposed s = new SliceExposed();
				s.Id_abstract = (int)reader["id_abstract"];
				s.Id_inner = (string)reader["id_inner"];
				s.Id_inner_owner = (string)reader["id_inner_owner"];
				s.Id_interface_slice = (string)reader["id_interface_slice"];
				s.Id_interface_slice_owner = (string)reader["id_interface_slice_owner"];
				//               s.Slice_replica = (int)reader["slice_replica"];
				//                s.Slice_replica_owner = (int)reader["slice_replica_owner"];
				s.Id_inner_original = (string)reader["id_inner_original"];
				s.Id_interface_slice_original = (string)reader["id_interface_slice_original"];
				s.Id_interface_host = (string)reader["id_interface_host"];
				list.Add(s);
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return list;

		}
		


        public string retrieveIdInnerOwner(int id_abstract, string id_inner, string id_unit, IDbConnection dbcon)
        {
            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT DISTINCT id_abstract, id_inner, id_interface_slice, id_inner_owner "  +
                "FROM sliceexposed " +
                "WHERE id_abstract=" + id_abstract + " and " +
                      "id_inner like '" + id_inner + "' and " +
                      "id_interface_slice like '" + id_unit + "'";
            dbcmd.CommandText = sql;
			//Console.WriteLine (sql);
			           IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            string id_inner_owner = null;
            if (reader.Read())            
                id_inner_owner = (string)reader["id_inner_owner"];
             //while
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return id_inner_owner;
        }

        public IList<SliceExposed> listContainers(int id_abstract, string id_inner, string id_interface_slice, IDbConnection dbcon) 
        {
            IList<SliceExposed> ll = new List<SliceExposed>();
          //  IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            SliceExposed se = null;
            string sql =
                "SELECT id_abstract, id_inner, id_interface_slice, id_inner_owner, id_interface_slice_owner, id_inner_original, id_interface_slice_original, id_interface_host " +
                "FROM sliceexposed " +
                "WHERE id_inner like '" + id_inner + "' and " +
                      "id_interface_slice like '" + id_interface_slice + "' and " +
                      "id_abstract = " + id_abstract;
            dbcmd.CommandText = sql;
			// Console.WriteLine (sql);
			           IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                se = new SliceExposed();
                se.Id_inner = (string)reader["id_inner"];
                se.Id_abstract = (int)reader["id_abstract"];
                se.Id_interface_slice = (string)reader["id_interface_slice"];
 //               se.Slice_replica = (int)reader["slice_replica"];
                se.Id_inner_owner = (string)reader["id_inner_owner"];
                se.Id_interface_slice_owner = (string)reader["id_interface_slice_owner"];
//                se.Slice_replica_owner = (int)reader["slice_replica_owner"];
                se.Id_inner_original = (string)reader["id_inner_original"];
                se.Id_interface_slice_original = (string)reader["id_interface_slice_original"];
				se.Id_interface_host = (string)reader["id_interface_host"];
				ll.Add(se);
            }
            //if
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return ll;
        }

        

        public SliceExposed retrieveContainer(string id_inner, string id_interface_slice, int id_abstract, string id_inner_owner, IDbConnection dbcon)
        {
           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            SliceExposed se = null;
            string sql =
                "SELECT id_abstract, id_inner, id_interface_slice, id_inner_owner, id_interface_slice_owner, id_inner_original, id_interface_slice_original, id_interface_host " +
                "FROM sliceexposed " +
                "WHERE id_inner like '" + id_inner + "' and " +
                      "id_interface_slice like '" + id_interface_slice + "' and " +
                      "id_abstract = " + id_abstract + " and " +
                      "id_inner_owner = " + id_inner_owner;
            dbcmd.CommandText = sql;
			// Console.WriteLine (sql);
			           IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                se = new SliceExposed();
                se.Id_inner = (string)reader["id_inner"];
                se.Id_abstract = (int)reader["id_abstract"];
                se.Id_interface_slice = (string)reader["id_interface_slice"];
 //               se.Slice_replica = (int)reader["slice_replica"];
                se.Id_inner_owner = (string)reader["id_inner_owner"];
                se.Id_interface_slice_owner = (string)reader["id_interface_slice_owner"];
//                se.Slice_replica_owner = (int)reader["slice_replica_owner"];
                se.Id_inner_original = (string)reader["id_inner_original"];
                se.Id_interface_slice_original = (string)reader["id_interface_slice_original"];
				se.Id_interface_host = (string)reader["id_interface_host"];
			}
            //if
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return se;
        }

        public SliceExposed retrieveContainerByOriginal(
            string id_inner_original, 
            string id_interface_slice_original, 
            int id_abstract_start, 
            string id_interface_owner,
            string id_inner_owner, IDbConnection dbcon)
        {
			int id_abstract = id_abstract_start;
	        SliceExposed se = null;
			
			while (id_abstract > 0) 
			{
	          //  IDbConnection dbcon = DBConnector.DBcon;
	            IDbCommand dbcmd = dbcon.CreateCommand();
	            string sql =
	                "SELECT id_abstract, id_inner, id_interface_slice, id_inner_owner, id_interface_slice_owner, id_inner_original, id_interface_slice_original, id_interface_host " +
	                "FROM sliceexposed " +
	                "WHERE id_inner_original like '" + id_inner_original + "' and " +
	                      "id_interface_slice_original like '" + id_interface_slice_original + "' and " +
	                      "id_abstract = " + id_abstract + " and " +
	                      "id_interface_slice_owner like '" + id_interface_owner + "' and " +
	                      "id_inner_owner like '" + id_inner_owner + "'";
	            dbcmd.CommandText = sql;
				// Console.WriteLine (sql);
	            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
	            if (reader.Read())
	            {
	                se = new SliceExposed();
	                se.Id_inner = (string)reader["id_inner"];
	                se.Id_abstract = (int)reader["id_abstract"];
	                se.Id_interface_slice = (string)reader["id_interface_slice"];
//	                se.Slice_replica = (int)reader["slice_replica"];
	                se.Id_inner_owner = (string)reader["id_inner_owner"];
	                se.Id_interface_slice_owner = (string)reader["id_interface_slice_owner"];
//	                se.Slice_replica_owner = (int)reader["slice_replica_owner"];
	                se.Id_inner_original = (string)reader["id_inner_original"];
	                se.Id_interface_slice_original = (string)reader["id_interface_slice_original"];
					se.Id_interface_host = (string)reader["id_interface_host"];
				}
	            //if
	            // clean up
	            reader.Close();
	            reader = null;
	            dbcmd.Dispose();
	            dbcmd = null;
				
				if (se == null) 
				{
	                AbstractComponentFunctor acf = org.hpcshelf.DGAC.Backend.acfdao.retrieve(id_abstract, dbcon);
					if (acf.Id_functor_app_supertype > 0)
					{
					   AbstractComponentFunctorApplication acfa = org.hpcshelf.DGAC.Backend.acfadao.retrieve(acf.Id_functor_app_supertype, dbcon);
					   id_abstract = acfa.Id_abstract;
					}
					else 
						id_abstract = -1;
				}
				else 
				   id_abstract = -1;
			}

            return se;
        }

        public SliceExposed retrieve2(string id_inner_original, string id_interface_slice, int id_abstract, string id_interface_container, string id_inner_container, string id_inner, string id_interface_host, IDbConnection dbcon)
        {
           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            SliceExposed se = null;
            string sql =
                "SELECT id_abstract, id_inner, id_interface_slice, id_inner_owner, id_interface_slice_owner, id_inner_original, id_interface_slice_original, id_interface_host " +
                "FROM sliceexposed " +
                "WHERE id_inner_original like '" + id_inner_original + "' and " +
                      "id_interface_slice like '" + id_interface_slice + "' and " +
                      "id_abstract = " + id_abstract + " and " +
                      "id_interface_slice_owner like '" + id_interface_container + "' and " +
					  "id_inner_owner like '" + id_inner_container + "' and " +
                      "id_inner like '" + id_inner + "' and " + 
                      "id_interface_host like '" + id_interface_host + "'";
            dbcmd.CommandText = sql;
			// Console.WriteLine (sql);
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                se = new SliceExposed();
                se.Id_inner = (string)reader["id_inner"];
                se.Id_abstract = (int)reader["id_abstract"];
                se.Id_interface_slice = (string)reader["id_interface_slice"];
 //               se.Slice_replica = (int)reader["slice_replica"];
                se.Id_inner_owner = (string)reader["id_inner_owner"];
                se.Id_interface_slice_owner = (string)reader["id_interface_slice_owner"];
  //              se.Slice_replica_owner = (int)reader["slice_replica_owner"];
                se.Id_inner_original = (string)reader["id_inner_original"];
                se.Id_interface_slice_original = (string)reader["id_interface_slice_original"];
				se.Id_interface_host = (string)reader["id_interface_host"];
			}
            //if
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return se;
        }

        public IList<int> findFacets(int id_abstract, string id_inner, string id_inner_owner, IDbConnection dbcon)
        {
			IList<int> list = new List<int>();
		//	IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT u.facet as facet " +
				"FROM sliceexposed as s, interface as u " +
				"WHERE s.id_abstract=" + id_abstract + " and " +
					  "s.id_inner_owner like '" + id_inner_owner + "' and " +
					  "s.id_inner like '" + id_inner + "' and " +
                      "u.id_abstract = s.id_abstract and " +
                      "u.id_interface = s.id_interface_host";
			dbcmd.CommandText = sql;
			// Console.WriteLine(sql);
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read())
			{
				int s = (int)reader["facet"];
				list.Add(s);
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return list;
		}

        public void delete(int id_abstract, string id_inner_rename, IDbConnection dbcon)
        {
            string sql = "DELETE FROM sliceexposed " +
                         "WHERE id_abstract=" + id_abstract +
                          " AND id_inner like '" + id_inner_rename + "'";

            DBConnector.performSQLUpdate(sql, dbcon);
        }

        public SliceExposed retrieve(int id_abstract, string id_inner_owner, string id_inner, string id_interface_host, IDbConnection dbcon)
        {
			//IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
			SliceExposed se = null;
			string sql =
				"SELECT id_abstract, id_inner, id_interface_slice, id_inner_owner, id_interface_slice_owner, id_inner_original, id_interface_slice_original, id_interface_host " +
				"FROM sliceexposed " +
				"WHERE id_inner_original like '" + id_inner + "' and " +
					  "id_interface_host like '" + id_interface_host + "' and " +
					  "id_abstract = " + id_abstract + " and " +
					  "id_inner_owner like '" + id_inner_owner + "'";
			dbcmd.CommandText = sql;
		// Console.WriteLine(sql);
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			if (reader.Read())
			{
				se = new SliceExposed();
				se.Id_inner = (string)reader["id_inner"];
				se.Id_abstract = (int)reader["id_abstract"];
				se.Id_interface_slice = (string)reader["id_interface_slice"];
				se.Id_inner_owner = (string)reader["id_inner_owner"];
				se.Id_interface_slice_owner = (string)reader["id_interface_slice_owner"];
				se.Id_inner_original = (string)reader["id_inner_original"];
				se.Id_interface_slice_original = (string)reader["id_interface_slice_original"];
				se.Id_interface_host = (string)reader["id_interface_host"];
			}
			//if
			// clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;

			return se;
		}
    }//class

}//namespace
