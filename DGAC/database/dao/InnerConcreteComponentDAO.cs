﻿using System;
using System.IO;
using System.Data;
using MySql.Data.MySqlClient;
using System.Runtime.Serialization;
using System.Collections.Generic; 

namespace org.hpcshelf.database
{
    [Serializable()]
    public class InnerConcreteComponentDAO
    {

        public int getNextFreshIndex(int id_concrete, string id_inner, IDbConnection dbcon) 
        {
            int key = -1;

           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT MAX(id_index) AS fresh_index " +
                 "FROM innerconcrete " +
                 "WHERE id_concrete=" + id_concrete + " AND " +
                 "id_inner like '" + id_inner + "' " +
                 "GROUP BY id_concrete, id_inner";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }

            if (reader.Read()) {
                key = (int)reader["fresh_index"];
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return key+1;
        }

        public void insert(InnerConcreteComponent ac, IDbConnection dbcon)
        {
            String sql =
                "INSERT INTO innerconcrete (id_concrete, id_inner, id_index, id_concrete_actual)" +
                " VALUES (" + ac.Id_concrete + ",'" + ac.Id_inner + "'," + ac.Id_index + "," + ac.Id_concrete_actual + ")";

            DBConnector.performSQLUpdate(sql, dbcon);
        }
		
		public void update(int id_concrete, string id_inner, int id_concrete_actual, IDbConnection dbcon)
        {
            String sql =
                "UPDATE innerconcrete" +
				" SET id_concrete_actual="+id_concrete_actual+
                " WHERE id_concrete=" +id_concrete+ " AND id_inner='"+id_inner+"'";

            DBConnector.performSQLUpdate(sql, dbcon);
        }
		

        public InnerConcreteComponent retrieve(int id_concrete, string id_inner, int id_index, IDbConnection dbcon)
        {
            InnerConcreteComponent c = null;
           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_concrete, id_inner, id_index, id_concrete_actual " +
                 "FROM innerconcrete " +
                 "WHERE id_concrete=" + id_concrete + " AND " + 
                 "id_inner like '" + id_inner + "' AND " + 
                 "id_index=" + id_index;
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                c = new InnerConcreteComponent();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_inner = (string)reader["id_inner"];
                c.Id_index = (int)reader["id_index"];
                c.Id_concrete_actual = (int)reader["id_concrete_actual"];
            }//if
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return c;
        }

        public IList<InnerConcreteComponent> retrieveEnumeration(int id_concrete, string id_inner, IDbConnection dbcon)
        {
            InnerConcreteComponent c = null;
            // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            IList<InnerConcreteComponent> l = new List<InnerConcreteComponent>();
            string sql =
                 "SELECT id_concrete, id_inner, id_index, id_concrete_actual " +
                 "FROM innerconcrete " +
                 "WHERE id_concrete=" + id_concrete + " AND " +
                 "id_inner like '" + id_inner;
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                c = new InnerConcreteComponent();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_inner = (string)reader["id_inner"];
                c.Id_index = (int)reader["id_index"];
                c.Id_concrete_actual = (int) reader["id_concrete_actual"];

                l.Add(c);
            }//while
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return l;
        }
    }
}
