﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using org.hpcshelf.DGAC;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class ResolutionTreeDAO
    {

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int insert(ResolutionTree ac, IDbConnection dbcon)
        {
            ac.NodeId = DBConnector.nextKey("node_id", "resolution_tree", dbcon);

            string sql;
            if (ac.Quantifier == null)
            {
                if (ac.Id_abstract != null)
                    //                    sql = "INSERT INTO resolution_tree (node_id, super_id, bound_node_id, bound_super_id, id_abstract, level)" +
                    sql = "INSERT INTO resolution_tree (node_id, super_id, id_abstract, level)" +
                          " VALUES (" + ac.NodeId + ","
                                      + ac.SuperId + ","
                                    //  + (ac.BoundNodeId.HasValue ? ac.BoundNodeId.ToString() : "null") + ","
                                     // + (ac.BoundSuperId.HasValue ? ac.BoundSuperId.ToString() : "null") + ","
                                      + ac.Id_abstract + ","
                                      + ac.Level + ")";
                else
//                    sql = "INSERT INTO resolution_tree (node_id, super_id, bound_node_id, bound_super_id, level)" +
                    sql = "INSERT INTO resolution_tree (node_id, super_id, level)" +
                          " VALUES (" + ac.NodeId + ","
                                      + ac.SuperId + ","
                                   //   + (ac.BoundNodeId.HasValue ? ac.BoundNodeId.ToString() : "null") + ","
                                   //   + (ac.BoundSuperId.HasValue ? ac.BoundSuperId.ToString() : "null") + ","
                                      + ac.Level + ")";
            }
            else
            {
                //                sql = "INSERT INTO resolution_tree (node_id, super_id, bound_node_id, bound_super_id, id_abstract, quantifier, level)" +
                sql = "INSERT INTO resolution_tree (node_id, super_id, id_abstract, quantifier, level)" +
                      " VALUES (" + ac.NodeId + ","
                                  + ac.SuperId + ","
                              //    + (ac.BoundNodeId.HasValue ? ac.BoundNodeId.ToString() : "null") + ","
                            //      + (ac.BoundSuperId.HasValue ? ac.BoundSuperId.ToString() : "null") + ","
                                  + ac.Id_abstract + ","
                                  + ac.Quantifier.GetValueOrDefault().ToString(CultureInfo.InvariantCulture) + "," +
                                  +ac.Level + ")";
            }

            //Console.WriteLine("ResolutionTree.cs: TRY INSERT: " + sql);

            DBConnector.performSQLUpdate(sql, dbcon);

            if (cache_rt.ContainsKey(ac.NodeId))
            {
                cache_rt.Remove(ac.NodeId);
                cache_rt[ac.NodeId] = ac;
            }

            return ac.NodeId;
        }

        IDictionary<int, ResolutionTree> cache_rt = new Dictionary<int, ResolutionTree>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ResolutionTree retrieve(int node_id, IDbConnection dbcon)
        {
            ResolutionTree rt = null;
            if (cache_rt.TryGetValue(node_id, out rt)) return rt;

          //  IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT node_id, super_id, id_abstract, level, quantifier " +
                 "FROM resolution_tree " +
                 "WHERE node_id=" + node_id;
            //     Console.WriteLine("AbstractComponentFunctorDAO - retrieve: " + sql);
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                rt = new ResolutionTree();
                rt.NodeId = (int)reader["node_id"];
                rt.SuperId = (int)reader["super_id"];
                rt.Id_abstract = (int?)(reader["id_abstract"] == DBNull.Value ? null : reader["id_abstract"]);
                rt.Quantifier = reader["quantifier"] == DBNull.Value ? null : (decimal?)reader["quantifier"];
                rt.Level = (int)reader["level"];
                cache_rt[node_id] = rt;
            }//if
            else
            {
                // Abstract Component Functor NOT FOUND !
            }
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return rt;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ResolutionTree retrieve(int super_id, int id_abstract, int level, IDbConnection dbcon)
        {
            ResolutionTree rt = null;
            //			if (cache_rt.TryGetValue(node_id, out rt)) return rt;

           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT node_id, super_id, id_abstract, level, quantifier " +
                 "FROM resolution_tree " +
                 "WHERE super_id=" + super_id + " AND id_abstract=" + id_abstract + " AND level=" + level;
            //     Console.WriteLine("AbstractComponentFunctorDAO - retrieve: " + sql);
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                rt = new ResolutionTree();
                rt.NodeId = (int)reader["node_id"];
                rt.SuperId = (int)reader["super_id"];
                rt.Id_abstract = (int?)(reader["id_abstract"] == DBNull.Value ? null : reader["id_abstract"]);
                rt.Quantifier = reader["quantifier"] == DBNull.Value ? null : (decimal?)reader["quantifier"];
                rt.Level = (int)reader["level"];
                cache_rt[rt.NodeId] = rt;
            }//if
            else
            {
                // NOT FOUND !
            }
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return rt;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ResolutionTree retrieve(int super_id, int level, IDbConnection dbcon)
        {
            ResolutionTree rt = null;
            //          if (cache_rt.TryGetValue(node_id, out rt)) return rt;

          //  IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT node_id, super_id, id_abstract, level, quantifier " +
                 "FROM resolution_tree " +
                 "WHERE super_id=" + super_id + " AND id_abstract is null AND level=" + level;
            //     Console.WriteLine("AbstractComponentFunctorDAO - retrieve: " + sql);
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                rt = new ResolutionTree();
                rt.NodeId = (int)reader["node_id"];
                rt.SuperId = (int)reader["super_id"];
                rt.Id_abstract = (int?)(reader["id_abstract"] == DBNull.Value ? null : reader["id_abstract"]);
                rt.Quantifier = reader["quantifier"] == DBNull.Value ? null : (decimal?)reader["quantifier"];
                rt.Level = (int)reader["level"];
                cache_rt[rt.NodeId] = rt;
            }//if
            else
            {
                // NOT FOUND !
            }
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return rt;
        }

        IDictionary<int, string> cache_result_2 = new Dictionary<int, string>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public /*IList<int>*/ string retrieve(/*IList<int> super_id*/ string super_id_set, decimal? quantifier/*, int level*/, VarianceType variance, char subtyping_direction/*, bool bounds_flag, out IList<int> bound_node_id*/, IDbConnection dbcon)
        {
        //    bound_node_id = new List<int>();
          //  IList<int> rt_list = new List<int>();

            string comparator = variance == VarianceType.invariant ? "=" : ((variance == VarianceType.covariant && subtyping_direction == '+') || (variance == VarianceType.contravariant && subtyping_direction == '-')) ? ">=" : "<=";

         /*    string super_id_set = "(";
              foreach (int id in super_id)
                  super_id_set += id + ",";
              super_id_set = super_id_set.Substring(0, super_id_set.LastIndexOf(',')) + ")";*/

            string super_id_field = /*bounds_flag ? "bound_super_id" :*/ "super_id";

          // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql = string.Intern(
                 "SELECT node_id, super_id, id_abstract, level, quantifier " +
                 "FROM resolution_tree " +
                             "WHERE (quantifier is null OR quantifier" + comparator + quantifier.GetValueOrDefault().ToString(CultureInfo.InvariantCulture) + ") AND "+ super_id_field +" in" + super_id_set);

            if (cache_result_2.TryGetValue(sql.GetHashCode(), out super_id_set))
                return super_id_set;

            //Console.WriteLine(sql);
            dbcmd.CommandText = sql;
            IDataReader reader = dbcmd.ExecuteReader();

            super_id_set = "(";
            while (reader.Read())
            {
                string id = reader["node_id"].ToString();
                super_id_set += id + ",";
            }

            if (super_id_set.Equals("("))
                Console.WriteLine();
            super_id_set = super_id_set.Substring(0, super_id_set.LastIndexOf(',')) + ")";

            /*
            while (reader.Read())
            {
                rt_list.Add((int)reader["node_id"]);
            //   bound_node_id.Add(((int?)reader["bound_node_id"]).GetValueOrDefault());
            }
            */

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            cache_result_2[sql.GetHashCode()] = super_id_set;

            return /*rt_list*/ super_id_set;
        }

        public /*IList<int>*/ string retrieve2(/*IList<int> super_id*/ string super_id_set /*, int level*/, IDbConnection dbcon)
        {
 //           IList<int> rt_list = new List<int>();

            /*string super_id_set = "(";
            foreach (int id in super_id)
                super_id_set += id + ",";
            super_id_set = super_id_set.Substring(0, super_id_set.LastIndexOf(',')) + ")";*/
                
           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql = string.Format("SELECT node_id FROM resolution_tree WHERE super_id in {0}", super_id_set);
            //"WHERE level=" + level + " AND super_id in" + super_id_set;
            // "WHERE level=" + level + " AND " + super_id_set;

            //Console.WriteLine(sql);
            dbcmd.CommandText = sql;
            IDataReader reader = dbcmd.ExecuteReader();

            super_id_set = "(";
            while (reader.Read())
            {
                string id = reader["node_id"].ToString();
                super_id_set += id + ",";
            }
            super_id_set = super_id_set.Substring(0, super_id_set.LastIndexOf(',')) + ")";

            /*
            while (reader.Read())
            {
                rt_list.Add((int)reader["node_id"]);
            }*/

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return /*rt_list*/ super_id_set;
        }

        IDictionary<int, string> cache_result_1 = new Dictionary<int, string>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public /*IList<int>*/ string retrieve3(/*IList<int> super_id,*/ string super_id_set, IDbCommand dbcmd/*, Stopwatch sw1, Stopwatch sw2, Stopwatch sw3*/)
        {
            string sql = string.Intern(string.Format("SELECT node_id FROM resolution_tree WHERE super_id in {0}", super_id_set));

            if (cache_result_1.TryGetValue(sql.GetHashCode(), out super_id_set))
                return super_id_set;

            dbcmd.CommandText = sql;
            //sw2.Start();
            IDataReader reader = dbcmd.ExecuteReader();
            //sw2.Stop();

            //sw3.Start();
            super_id_set = "(";
            while (reader.Read())
            {
                string id = reader["node_id"].ToString();
                super_id_set += id + ",";
            }
            super_id_set = super_id_set.Substring(0, super_id_set.LastIndexOf(',')) + ")";
            //sw3.Stop();

            reader.Close();

            cache_result_1[sql.GetHashCode()] = super_id_set;
            return super_id_set;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ResolutionTree retrieve(int super_id, decimal? quantifier, int level, IDbConnection dbcon)
        {
            ResolutionTree rt = null;
            //IList<ResolutionTree> rt_list = new List<ResolutionTree>();

            //string comparator = variance == VarianceType.invariant ? "=" : ((variance == VarianceType.covariant && subtyping_direction == '+') || (variance == VarianceType.contravariant && subtyping_direction == '-')) ? ">" : "<";

           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql = null;
            if (quantifier.HasValue)
                sql = "SELECT node_id, super_id, id_abstract, level, quantifier " +
                     "FROM resolution_tree " +
                     "WHERE super_id = " + super_id + " AND quantifier = " + quantifier.GetValueOrDefault().ToString(CultureInfo.InvariantCulture) + " AND level=" + level;
            else
                sql = "SELECT node_id, super_id, id_abstract, level, quantifier " +
                      "FROM resolution_tree " +
                      "WHERE super_id = " + super_id + " AND quantifier is null AND level=" + level;

            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                rt = new ResolutionTree();
                rt.NodeId = (int)reader["node_id"];
                rt.SuperId = (int)reader["super_id"];
                rt.Id_abstract = (int?)(reader["id_abstract"] == DBNull.Value ? null : reader["id_abstract"]);
                rt.Quantifier = reader["quantifier"] == DBNull.Value ? null : (decimal?)reader["quantifier"];
                rt.Level = (int)reader["level"];
                cache_rt[rt.NodeId] = rt;
                //rt_list.Add(rt);
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return rt;
        }


        [MethodImpl(MethodImplOptions.Synchronized)]
        public ResolutionTree retrieve_root(int id_abstract, IDbConnection dbcon)
        {
            ResolutionTree rt = null;

           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT node_id, id_abstract, level FROM resolution_tree " +
                 "WHERE id_abstract=" + id_abstract + " AND level=-2";

            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                rt = new ResolutionTree();
                rt.NodeId = (int)reader["node_id"];
                rt.Id_abstract = (int?)(reader["id_abstract"] == DBNull.Value ? null : reader["id_abstract"]);
                rt.Level = (int)reader["level"];
                cache_rt[rt.NodeId] = rt;
            }//if
            else
            {
                // NOT FOUND !
            }
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return rt;
        }

        internal IList<ResolutionTree> listBySuper(int node_id, IDbConnection dbcon)
        {

            //  Console.WriteLine ("SupplyParameterDAO: list ENTER " + id_functor_app);

            IList<ResolutionTree> list = null;
            list = new List<ResolutionTree>();

        //    IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();

            string sql =
                "SELECT node_id, super_id, id_abstract, level, quantifier " +
                "FROM resolution_tree " +
                "WHERE super_id=" + node_id;
            dbcmd.CommandText = sql;

            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                ResolutionTree rt = new ResolutionTree();
                rt.NodeId = (int)reader["node_id"];
                rt.Id_abstract = (int?)(reader["id_abstract"] == DBNull.Value ? null : reader["id_abstract"]);
                rt.Quantifier = reader["quantifier"] == DBNull.Value ? null : (decimal?)reader["quantifier"];
                rt.Level = (int)reader["level"];
                list.Add(rt);
            }//while

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            //  Console.WriteLine ("SupplyParameterDAO: list EXIT count=" + list.Count);

            return list;
        }

        internal bool componentIsInResolutionTree(int id_concrete, IDbConnection dbcon)
        {
            bool result = false;

        //    IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT node_id, super_id, id_abstract, level, quantifier " +
                 "FROM resolution_tree " +
                 "WHERE id_abstract=" + id_concrete + " AND level=-1";

            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }

            if (reader.Read())
                result = true;

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return result;
        }

        IDictionary<int, string> cache_result_3 = new Dictionary<int, string>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal /*IList<int>*/ string retrieve(/*IList<int> super_id,*/ string super_id_set, int id_abstract/*, int level*/, VarianceType variance/*, bool bounds_flag , out IList<int> bound_node_id*/, IDbConnection dbcon)
        {
            // IList<int> id_list = new List<int>();

            /* string super_id_set = "(";
              foreach (int id in super_id)
                  super_id_set += id + ",";
              super_id_set = super_id_set.Substring(0, super_id_set.LastIndexOf(',')) + ")";*/
                  
            string super_id_field = /*bounds_flag ? "bound_super_id" :*/ "super_id";

            string id_abstract_set = "(";
            switch (variance)
            {
                case VarianceType.covariant:
                    {
                        IList<int> s_list = Backend.acfdao.retrieve_subtypes(id_abstract, dbcon);
                        foreach (int s in s_list) id_abstract_set += s + ",";
                    }
                    break;

                case VarianceType.contravariant:
                    {
                        IList<int> s_list = Backend.acfdao.retrieve_supertypes(id_abstract, dbcon);
                        foreach (int s in s_list) id_abstract_set += s + ",";
                    }
                    break;

                case VarianceType.invariant:
                    {
                        id_abstract_set += id_abstract + ",";
                    }
                    break;
            }

            id_abstract_set = id_abstract_set.Substring(0, id_abstract_set.LastIndexOf(',')) + ")";

           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT node_id " +
                 "FROM resolution_tree " +
                 "WHERE " + super_id_field + " in " + super_id_set + " AND (id_abstract is null OR id_abstract in " + id_abstract_set + ")";

            if (cache_result_3.TryGetValue(sql.GetHashCode(), out super_id_set))
                return super_id_set;

            //Console.WriteLine(sql);
            dbcmd.CommandText = sql;
            IDataReader reader = dbcmd.ExecuteReader(); 

            super_id_set = "(";
            while (reader.Read())
            {
                string id = reader["node_id"].ToString();
                super_id_set += id + ",";
            }
            super_id_set = super_id_set.Substring(0, super_id_set.LastIndexOf(',')) + ")";

            /*
            while (reader.Read())
            {
                id_list.Add((int)reader["node_id"]);
            }
            */

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            cache_result_3[sql.GetHashCode()] = super_id_set;

            return /*id_list*/ super_id_set;
        }


        internal IList<ResolutionTree> listBySuperIds(IList<int> node_id, IDbConnection dbcon)
        {
            IList<ResolutionTree> rt_list = new List<ResolutionTree>();

            string node_id_set = "(";
            foreach (int id in node_id)
                node_id_set += id + ",";
            node_id_set = node_id_set.Substring(0, node_id_set.LastIndexOf(',')) + ")";

          //  IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT node_id, super_id, id_abstract, level, quantifier " +
                 "FROM resolution_tree WHERE super_id in " + node_id_set;

            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                ResolutionTree rt = new ResolutionTree();
                rt.NodeId = (int)reader["node_id"];
                rt.SuperId = (int)reader["super_id"];
                rt.Id_abstract = (int?)(reader["id_abstract"] == DBNull.Value ? null : reader["id_abstract"]);
                rt.Quantifier = reader["quantifier"] == DBNull.Value ? null : (decimal?)reader["quantifier"];
                rt.Level = (int)reader["level"];
                rt_list.Add(rt);
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return rt_list;
        }
        internal Component[] listBySuperIds_(/*IList<int> node_id*/ string node_id_set, IDbConnection dbcon)
        {
            IList<Component> rt_list = new List<Component>();

        /*    string node_id_set = "(";
            foreach (int id in node_id)
                node_id_set += id + ",";
            node_id_set = node_id_set.Substring(0, node_id_set.LastIndexOf(',')) + ")";*/

          //  IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql = "SELECT c.* FROM component as c, resolution_tree as r WHERE c.id_concrete=r.id_abstract AND super_id in " + node_id_set;

            dbcmd.CommandText = sql;
            IDataReader reader = dbcmd.ExecuteReader(); 
            while (reader.Read())
            {
                Component c = new Component();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_concrete_supertype = (int)reader["id_concrete_supertype"];
                c.Library_path = (string)reader["library_path"];
                c.Id_functor_app = (int)reader["id_functor_app"];
                c.SetId_abstract((int)reader["id_abstract"]);
                c.Hash_component_UID = (string)reader["hash_component_UID"];
                c.ContextArgumentCalculator = (string)reader["argument_calculator"];
                rt_list.Add(c);
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            Component[] array = new Component[rt_list.Count];
            rt_list.CopyTo(array, 0);

            return array;
        }
        /*
        internal void updateBoundSuperId(int node_id, int last_bound_id)
        {
            string sql = string.Format("UPDATE resolution_tree SET bound_super_id={0} where node_id={1}", last_bound_id, node_id);
            DBConnector.performSQLUpdate(sql);
        }

        internal void updateBoundNodeId(int node_id, int v)
        {
            string sql = string.Format("UPDATE resolution_tree SET bound_node_id={0} where node_id={1}", v, node_id);
            DBConnector.performSQLUpdate(sql);
        }

        internal IList<int> retrieve_bound_node(IList<int> node_id)
        {
            IList<int> id_list = new List<int>();

            string node_id_set = "(";
            foreach (int id in node_id)
                node_id_set += id + ",";
            node_id_set = node_id_set.Substring(0, node_id_set.LastIndexOf(',')) + ")";

            IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql = "SELECT bound_node_id FROM resolution_tree WHERE node_id in " + node_id_set;

            dbcmd.CommandText = sql;
            IDataReader reader = dbcmd.ExecuteReader();
            while (reader.Read())
            {
                id_list.Add((int)reader["bound_node_id"]);
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return id_list;
        }*/
    }//class



}//namespace