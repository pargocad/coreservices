﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using MySql.Data.MySqlClient;

namespace org.hpcshelf.database
{
    public class SourceCodeDAO
    {
        public void insert(SourceCode ac, IDbConnection dbcon)
        {
            // string contents = ac.Contents.Replace ("'", "\\'");

            byte[] bytecontents = Encoding.UTF8.GetBytes(ac.Contents);

            // string sql =
            //    "INSERT INTO sourcecode (type_owner, id_owner_container, id_owner, contents, file_type, file_name, file_order, module_name, language, deploy_type)" +
            //    " VALUES ('" + ac.Type_owner + "'," + ac.Id_owner_container + ",'" + ac.Id_owner + "','" +  contents + "','" + ac.File_type + "','" + ac.File_name +"'," + ac.Order + ",'" + ac.Module_name + "','" + ac.Language + "','" + ac.DeployType + "')";

            // DBConnector.performSQLUpdate(sql);

            if (ac.File_type == null)
                ac.File_type = "other";

            using (MySqlCommand command = new MySqlCommand())
            {
                command.Connection = (MySqlConnection)dbcon;
                command.CommandText = "INSERT INTO sourcecode (type_owner, id_owner_container, id_owner, contents, file_type, file_name, file_order, module_name, language, deploy_type)" +
                                      " VALUES (?type_owner, ?id_owner_container, ?id_owner, ?contents, ?file_type, ?file_name, ?file_order, ?module_name, ?language, ?deploy_type);";
                MySqlParameter type_owner = new MySqlParameter("?type_owner", MySqlDbType.VarChar, 1);
                MySqlParameter id_owner_container = new MySqlParameter("?id_owner_container", MySqlDbType.Int32, 11);
                MySqlParameter id_owner = new MySqlParameter("?id_owner", MySqlDbType.VarChar, 128);
                MySqlParameter source_contents = new MySqlParameter("?contents", MySqlDbType.Blob, bytecontents.Length);
                MySqlParameter file_type = new MySqlParameter("?file_type", MySqlDbType.VarChar, 30);
                MySqlParameter file_name = new MySqlParameter("?file_name", MySqlDbType.VarChar, 100);
                MySqlParameter file_order = new MySqlParameter("?file_order", MySqlDbType.Int32, 11);
                MySqlParameter module_name = new MySqlParameter("?module_name", MySqlDbType.VarChar, 256);
                MySqlParameter language = new MySqlParameter("?language", MySqlDbType.VarChar, 256);
                MySqlParameter deploy_type = new MySqlParameter("?deploy_type", MySqlDbType.VarChar, 256);

                type_owner.Value = ac.Type_owner;
                id_owner_container.Value = ac.Id_owner_container;
                id_owner.Value = ac.Id_owner;
                source_contents.Value = bytecontents;
                file_type.Value = ac.File_type;
                file_name.Value = ac.File_name;
                file_order.Value = ac.Order;
                module_name.Value = ac.Module_name;
                language.Value = ac.Language;
                deploy_type.Value = ac.DeployType;

                command.Parameters.Add(type_owner);
                command.Parameters.Add(id_owner_container);
                command.Parameters.Add(id_owner);
                command.Parameters.Add(source_contents);
                command.Parameters.Add(file_type);
                command.Parameters.Add(file_name);
                command.Parameters.Add(file_order);
                command.Parameters.Add(module_name);
                command.Parameters.Add(language);
                command.Parameters.Add(deploy_type);

              // command.Connection.Open();

                command.ExecuteNonQuery();

            }
        }

        public IList<SourceCode> list(char type_owner, int id_owner_container, string id_owner, IDbConnection dbcon)
        {
            SourceCodeReferenceDAO scrdao = new SourceCodeReferenceDAO();
            IList<SourceCode> list = new List<SourceCode>();
            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT type_owner, id_owner_container, id_owner, contents, file_type, file_name, file_order, module_name, language, deploy_type " +
                "FROM sourcecode " +
                "WHERE type_owner like '" + type_owner + "' AND id_owner_container=" + id_owner_container + " AND id_owner like '" + id_owner + "' " +
                "ORDER BY deploy_type, file_type, file_order";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                SourceCode sc = new SourceCode();
                sc.Type_owner = ((string)reader["type_owner"])[0];
                sc.Id_owner_container = (int)reader["id_owner_container"];
                sc.Id_owner = (string)reader["id_owner"];
                //sc.Contents = ((string)reader["contents"]).Replace("\'","'");
                sc.File_type = (string)reader["file_type"];
                sc.File_name = (string)reader["file_name"];
                sc.Order = (int)reader["file_order"];
                sc.Module_name = (string)reader["module_name"];
                sc.Language = (string)reader["language"];
				sc.DeployType = (string)reader["deploy_type"];
                sc.Contents = Encoding.UTF8.GetString(((byte[])reader["contents"]));
                list.Add(sc);
            }//while
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return list;

        }//list

		public IList<SourceCode> list(char type_owner, int id_owner_container, IDbConnection dbcon)
		{
			SourceCodeReferenceDAO scrdao = new SourceCodeReferenceDAO();
			IList<SourceCode> list = new List<SourceCode>();
			//IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT type_owner, id_owner_container, id_owner, contents, file_type, file_name, file_order, module_name, language, deploy_type " +
				"FROM sourcecode " +
				"WHERE type_owner like '" + type_owner + "' AND id_owner_container=" + id_owner_container + " " +
				"ORDER BY file_type, file_order";
			dbcmd.CommandText = sql;
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read())
			{
				SourceCode sc = new SourceCode();
				sc.Type_owner = ((string)reader["type_owner"])[0];
				sc.Id_owner_container = (int)reader["id_owner_container"];
				sc.Id_owner = (string)reader["id_owner"];
                // sc.Contents = ((string)reader["contents"]).Replace("\'", "'");
                sc.Contents = Encoding.UTF8.GetString(((byte[])reader["contents"]));
                sc.File_name = (string)reader["file_name"];
				sc.File_type = (string)reader["file_type"];
				sc.Order = (int)reader["file_order"];
				sc.Module_name = (string)reader["module_name"];
				sc.Language = (string)reader["language"];
				sc.DeployType = (string)reader["deploy_type"];

				list.Add(sc);
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return list;

		}//list


        public IList<SourceCode> listByFileType(char type_owner, int id_owner_container, string id_owner, string source_type, IDbConnection dbcon)
		{
			SourceCodeReferenceDAO scrdao = new SourceCodeReferenceDAO();
			IList<SourceCode> list = new List<SourceCode>();
			//IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT type_owner, id_owner_container, id_owner, contents, file_type, file_name, file_order, module_name, language, deploy_type " +
				"FROM sourcecode " +
				"WHERE type_owner like '" + type_owner + "' AND id_owner_container=" + id_owner_container + " AND id_owner like '" + id_owner + "' AND file_type like '" + source_type + "'" +
				"ORDER BY file_type, file_order";
			dbcmd.CommandText = sql;
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read())
			{
				SourceCode sc = new SourceCode();
				sc.Type_owner = ((string)reader["type_owner"])[0];
				sc.Id_owner_container = (int)reader["id_owner_container"];
				sc.Id_owner = (string)reader["id_owner"];
                // sc.Contents = ((string)reader["contents"]).Replace("\'","'");
                sc.Contents = Encoding.UTF8.GetString(((byte[])reader["contents"]));
                sc.File_name = (string)reader["file_name"];
				sc.File_type = (string)reader["file_type"];               
				sc.Order = (int)reader["file_order"];
				sc.Module_name = (string)reader["module_name"];
				sc.Language = (string)reader["language"];
				sc.DeployType = (string)reader["deploy_type"];

				list.Add(sc);
			}//while
			// clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return list;

		}//list

		public IList<SourceCode> listByFileType(char type_owner, int id_owner_container, string source_type, IDbConnection dbcon)
		{
			SourceCodeReferenceDAO scrdao = new SourceCodeReferenceDAO();
			IList<SourceCode> list = new List<SourceCode>();
			//IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT type_owner, id_owner_container, id_owner, contents, file_type, file_name, file_order, module_name, language, deploy_type " +
				"FROM sourcecode " +
				"WHERE type_owner like '" + type_owner + "' AND id_owner_container=" + id_owner_container + " AND file_type like '" + source_type + "'" +
				"ORDER BY file_type, file_order";
			dbcmd.CommandText = sql;
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read())
			{
				SourceCode sc = new SourceCode();
				sc.Type_owner = ((string)reader["type_owner"])[0];
				sc.Id_owner_container = (int)reader["id_owner_container"];
				sc.Id_owner = (string)reader["id_owner"];
				//sc.Contents = ((string)reader["contents"]).Replace("\'", "'");
                sc.Contents = Encoding.UTF8.GetString(((byte[])reader["contents"]));
                sc.File_name = (string)reader["file_name"];
				sc.File_type = (string)reader["file_type"];
				sc.Order = (int)reader["file_order"];
				sc.Module_name = (string)reader["module_name"];
				sc.Language = (string)reader["language"];
				sc.DeployType = (string)reader["deploy_type"];

				list.Add(sc);
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return list;

		}//list

		public IList<SourceCode> listByFileName(char type_owner, int id_owner_container, string id_owner, string file_name, IDbConnection dbcon)
		{
			SourceCodeReferenceDAO scrdao = new SourceCodeReferenceDAO();
			IList<SourceCode> list = new List<SourceCode>();
			//IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT type_owner, id_owner_container, id_owner, contents, file_type, file_name, file_order, module_name, language, deploy_type " +
				"FROM sourcecode " +
                "WHERE type_owner like '" + type_owner + "' AND id_owner_container=" + id_owner_container + " AND id_owner like '" + id_owner + "' AND file_name like '" + file_name + "'" +
				"ORDER BY file_type, file_order";
			dbcmd.CommandText = sql;
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read())
			{
				SourceCode sc = new SourceCode();
				sc.Type_owner = ((string)reader["type_owner"])[0];
				sc.Id_owner_container = (int)reader["id_owner_container"];
				sc.Id_owner = (string)reader["id_owner"];
				//sc.Contents = ((string)reader["contents"]).Replace("\'", "'");
                sc.Contents = Encoding.UTF8.GetString(((byte[])reader["contents"]));
                sc.File_name = (string)reader["file_name"];
				sc.File_type = (string)reader["file_type"];
				sc.Order = (int)reader["file_order"];
				sc.Module_name = (string)reader["module_name"];
				sc.Language = (string)reader["language"];
				sc.DeployType = (string)reader["deploy_type"];

				list.Add(sc);
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return list;

		}//list

		public IList<SourceCode> listByFileName(char type_owner, int id_owner_container, string file_name, IDbConnection dbcon)
		{
			SourceCodeReferenceDAO scrdao = new SourceCodeReferenceDAO();
			IList<SourceCode> list = new List<SourceCode>();
		// IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT type_owner, id_owner_container, id_owner, contents, file_type, file_name, file_order, module_name, language, deploy_type " +
				"FROM sourcecode " +
                "WHERE type_owner like '" + type_owner + "' AND id_owner_container=" + id_owner_container + " AND file_name like '" + file_name + "'" +
				"ORDER BY file_type, file_order";
			dbcmd.CommandText = sql;
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read())
			{
				SourceCode sc = new SourceCode();
				sc.Type_owner = ((string)reader["type_owner"])[0];
				sc.Id_owner_container = (int)reader["id_owner_container"];
				sc.Id_owner = (string)reader["id_owner"];
                //sc.Contents = ((string)reader["contents"]).Replace("\'", "'");
                sc.Contents = Encoding.UTF8.GetString(((byte[])reader["contents"]));
                sc.File_name = (string)reader["file_name"];
				sc.File_type = (string)reader["file_type"];
				sc.Order = (int)reader["file_order"];
				sc.Module_name = (string)reader["module_name"];
				sc.Language = (string)reader["language"];
				sc.DeployType = (string)reader["deploy_type"];

				list.Add(sc);
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return list;

		}//list

        public IList<string> fileTypes(char type_owner, int id_owner_container, IDbConnection dbcon)
		{
			SourceCodeReferenceDAO scrdao = new SourceCodeReferenceDAO();
			IList<string> list = new List<string>();
		//	IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT DISTINCT file_type " +
                "FROM sourcecode " +
                "WHERE type_owner like '" + type_owner + "' AND id_owner_container=" + id_owner_container;
            dbcmd.CommandText = sql;
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read())
			{			
                string file_type = (string)reader["file_type"];
				list.Add(file_type);
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return list;

		}//list

		public IList<string> fileTypes(char type_owner, int id_owner_container, string id_unit, IDbConnection dbcon)
		{
			SourceCodeReferenceDAO scrdao = new SourceCodeReferenceDAO();
			IList<string> list = new List<string>();
		// IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT DISTINCT file_type " +
                "FROM sourcecode " +
                "WHERE type_owner like '" + type_owner + "' AND id_owner_container=" + id_owner_container + " AND id_owner='" + id_unit + "'";
			dbcmd.CommandText = sql;
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read())
			{
				string file_type = (string)reader["file_type"];
				list.Add(file_type);
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return list;

		}//list


        internal void update(SourceCode ss, IDbConnection dbcon)
        {
            string contents = ss.Contents;

          //  if (!ss.File_type.Equals("binary"))
          //  {
          //      contents = contents.Replace("'", "\\'");
          //      Console.WriteLine(contents);
          //  }

       /*     string sql =
                 "UPDATE sourcecode SET contents='" + contents + "' WHERE type_owner like '" + ss.Type_owner + "'"
                                                      + " and id_owner like '" + ss.Id_owner + "'"
                                                      + " and file_name like '" + ss.File_name + "'"
                                                      + " and id_owner_container = " + ss.Id_owner_container;


            DBConnector.performSQLUpdate(sql, dbcon);
            */

            byte[] bytecontents = Encoding.UTF8.GetBytes(contents);

            // string sql =
            //    "INSERT INTO sourcecode (type_owner, id_owner_container, id_owner, contents, file_type, file_name, file_order, module_name, language, deploy_type)" +
            //    " VALUES ('" + ac.Type_owner + "'," + ac.Id_owner_container + ",'" + ac.Id_owner + "','" +  contents + "','" + ac.File_type + "','" + ac.File_name +"'," + ac.Order + ",'" + ac.Module_name + "','" + ac.Language + "','" + ac.DeployType + "')";

            // DBConnector.performSQLUpdate(sql);


            using (MySqlCommand command = new MySqlCommand())
            {
                command.Connection = (MySqlConnection)dbcon;
                command.CommandText = "UPDATE sourcecode SET contents=?contents WHERE type_owner like ?type_owner AND id_owner like ?id_owner AND " +
                                                                                     "file_name like ?file_name AND id_owner_container = ?id_owner_container";
                MySqlParameter type_owner = new MySqlParameter("?type_owner", MySqlDbType.VarChar, 1);
                MySqlParameter id_owner_container = new MySqlParameter("?id_owner_container", MySqlDbType.Int32, 11);
                MySqlParameter id_owner = new MySqlParameter("?id_owner", MySqlDbType.VarChar, 128);
                MySqlParameter source_contents = new MySqlParameter("?contents", MySqlDbType.Blob, bytecontents.Length);
                MySqlParameter file_name = new MySqlParameter("?file_name", MySqlDbType.VarChar, 100);

                type_owner.Value = ss.Type_owner;
                id_owner_container.Value = ss.Id_owner_container;
                id_owner.Value = ss.Id_owner;
                source_contents.Value = bytecontents;
                file_name.Value = ss.File_name;

                command.Parameters.Add(type_owner);
                command.Parameters.Add(id_owner_container);
                command.Parameters.Add(id_owner);
                command.Parameters.Add(source_contents);
                command.Parameters.Add(file_name);

                command.ExecuteNonQuery();
            }
        }

        internal SourceCode retrieve(char type_owner, string id_owner, string file_name, int id_owner_container, IDbConnection dbcon)
        {
            SourceCode s = null;
		//IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT type_owner, " +
					   "id_owner," +
					   "contents," +
					   "file_type," +
					   "file_name," +
					   "id_owner_container," +
					   "file_order, module_name, language, deploy_type " +
				"FROM sourcecode " +
				"WHERE type_owner like '" + type_owner + "' AND " +
				      "id_owner like '" + id_owner + "' AND " +
				      "file_name like '" + file_name + "' AND " +
				      "id_owner_container = " + id_owner_container;
			dbcmd.CommandText = sql;
			Console.WriteLine(sql);
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			if (reader.Read())
			{
                s = new SourceCode();
                s.Type_owner = ((string)reader["type_owner"])[0];
                s.Id_owner = (string)reader["id_owner"];
                //s.Contents = (string)reader["contents"];
                s.Contents = Encoding.UTF8.GetString(((byte[])reader["contents"]));
                s.File_type = (string)reader["file_type"];
                s.File_name = (string)reader["file_name"];
                s.Id_owner_container = (int)reader["id_owner_container"];
                s.Order = (int)reader["file_order"];
				s.Module_name = (string)reader["module_name"];
				s.Language = (string)reader["language"];
				s.DeployType = (string)reader["deploy_type"];
			}

			// clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;

			if (s == null)
			{
                Console.WriteLine("SourceCodeDAO.cs: SourceCode NOT FOUND {0}", s);
			}

			return s;
		}
    }
}
