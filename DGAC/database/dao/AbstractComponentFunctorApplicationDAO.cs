﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using org.hpcshelf.DGAC;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class AbstractComponentFunctorApplicationDAO
    {

        public int insert(AbstractComponentFunctorApplication ac, IDbConnection dbcon)
        {
            return insert(ac, 0, dbcon);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int insert(AbstractComponentFunctorApplication ac, int facet_index, IDbConnection dbcon)
        {
            decimal? quantifier = ac is AbstractComponentFunctorApplicationQuantifier ? ((AbstractComponentFunctorApplicationQuantifier)ac).Quantifier : null;

			//IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();

            String sql;
            if (quantifier.HasValue)
            {
                sql =
                    "INSERT INTO abstractcomponentfunctorapplication (id_abstract, id_functor_app_next, facet_index, quantifier)" +
                    " VALUES (" + ac.Id_abstract + "," + ac.Id_functor_app_next + "," + facet_index + "," + quantifier.GetValueOrDefault().ToString(CultureInfo.InvariantCulture) + "); " +
                    "SELECT max(id_functor_app) as id_functor_app from abstractcomponentfunctorapplication where facet_index=" + facet_index;
            }
            else
                sql =
                    "INSERT INTO abstractcomponentfunctorapplication (id_abstract, id_functor_app_next, facet_index)" +
                    " VALUES (" + ac.Id_abstract + "," + ac.Id_functor_app_next + "," + facet_index + "); " +
                                    "SELECT max(id_functor_app) as id_functor_app from abstractcomponentfunctorapplication where facet_index=" + facet_index;

			//Console.WriteLine("AbstractComponentFunctorApplication.cs: TRY INSERT: " + sql);

            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                ac.Id_functor_app = (int)reader["id_functor_app"];
            }

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            if (cache_acfa.ContainsKey(ac.Id_functor_app))
            {
                cache_acfa.Remove(ac.Id_functor_app);
                cache_acfa[ac.Id_functor_app] = ac;
            }

            return ac.Id_functor_app;
        }


        IDictionary<int, AbstractComponentFunctorApplication> cache_acfa = new Dictionary<int, AbstractComponentFunctorApplication>();


        //public AbstractComponentFunctorApplication retrieve(int id_functor_app)
        //{
        //    return retrieve(id_functor_app, DBConnector.DBcon);
        //}

        [MethodImpl(MethodImplOptions.Synchronized)]
        public AbstractComponentFunctorApplication retrieve(int id_functor_app, IDbConnection dbcon)
        {
            AbstractComponentFunctorApplication acfa = null;
            if (cache_acfa.TryGetValue(id_functor_app, out acfa)) return acfa;

            DGAC.Backend.isQuantifier(0, dbcon);

            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_abstract, id_functor_app, id_functor_app_next, quantifier " +
                 "FROM abstractcomponentfunctorapplication " +
                 "WHERE id_functor_app=" + id_functor_app;
            // Console.WriteLine("AbstractComponentFunctorApplicationDAO - retrieve: " + sql);
            dbcmd.CommandText = sql;
            // IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Tuple<char, char> q = null;

                if ((q = DGAC.Backend.isQuantifier((int)reader["id_abstract"], dbcon)) != null)
                {
                    acfa = new AbstractComponentFunctorApplicationQuantifier();
                    bool null_quantifier = reader["quantifier"] == DBNull.Value;
                    ((AbstractComponentFunctorApplicationQuantifier)acfa).Quantifier = (decimal?) (null_quantifier ? null /*(q.Item2 == '+' ? Decimal.MinValue: Decimal.MaxValue)*/ : reader["quantifier"]);
				}
                else
					acfa = new AbstractComponentFunctorApplication();

                acfa.Id_functor_app = (int)reader["id_functor_app"];
                acfa.Id_abstract = (int)reader["id_abstract"];
                acfa.Id_functor_app_next = (int)reader["id_functor_app_next"];

                cache_acfa[acfa.Id_functor_app] = acfa;
            }

            //while
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            //   if (acfa==null) 
            //   {
            //  	  Console.WriteLine("AbstractComponentFunctorApplicationDAO.cs: ACFA NOT FOUND "+ id_functor_app);
            //   }

            return acfa;
        }

        private IDictionary<int, string> cache_lb = new Dictionary<int, string>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public string retrieveLibraryPath(int id_functor_app, IDbConnection dbcon)
        {
            if (cache_lb.ContainsKey(id_functor_app))
                return cache_lb[id_functor_app];

            string library_path = null;

            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql = string.Format("SELECT library_path FROM abstractcomponentfunctor as a, abstractcomponentfunctorapplication as b WHERE a.id_abstract = b.id_abstract and b.id_functor_app={0}", id_functor_app);

            dbcmd.CommandText = sql;
            IDataReader reader = null;
            try
            {
                reader = dbcmd.ExecuteReader();
                if (reader.Read())
                {
                    library_path = (string)reader["library_path"];
                    cache_lb[id_functor_app] = library_path;
                    return library_path;
                }
            }
            finally
            {
                //while
                // clean up
                reader.Close();
                reader = null;
                dbcmd.Dispose();
                dbcmd = null;
            }

            return library_path;
        }


        public AbstractComponentFunctorApplication retrieve_next(int id_functor_app_next, IDbConnection dbcon)
        {
            AbstractComponentFunctorApplication acfa = null;

            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_abstract, id_functor_app, id_functor_app_next, quantifier " +
                 "FROM abstractcomponentfunctorapplication " +
                 "WHERE id_functor_app_next=" + id_functor_app_next;
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
				if ((Backend.isQuantifier((int)reader["id_abstract"], dbcon)) == null)
                    acfa = new AbstractComponentFunctorApplication();
                else
                {
                    acfa = new AbstractComponentFunctorApplicationQuantifier();
                    ((AbstractComponentFunctorApplicationQuantifier)acfa).Quantifier = (decimal?) (reader["quantifier"] == DBNull.Value ? null : reader["quantifier"]);
				}
                acfa.Id_functor_app = (int)reader["id_functor_app"];
                acfa.Id_abstract = (int)reader["id_abstract"];
                acfa.Id_functor_app_next = (int)reader["id_functor_app_next"];
            }//while

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return acfa;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<AbstractComponentFunctorApplication> list(IDbConnection dbcon)
        {
            IList<AbstractComponentFunctorApplication> list = new List<AbstractComponentFunctorApplication>();
            AbstractComponentFunctorApplication acfa = null;
            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_abstract, id_functor_app, id_functor_app_next, quantifier " +
                 "FROM abstractcomponentfunctorapplication ";

            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
				if (Backend.isQuantifier((int)reader["id_abstract"], dbcon) == null)
					acfa = new AbstractComponentFunctorApplication();
				else
				{
					acfa = new AbstractComponentFunctorApplicationQuantifier();
					((AbstractComponentFunctorApplicationQuantifier)acfa).Quantifier = (decimal?)(reader["quantifier"] == DBNull.Value ? null : reader["quantifier"]);
				}
				acfa.Id_functor_app = (int)reader["id_functor_app"];
                acfa.Id_abstract = (int)reader["id_abstract"];
                acfa.Id_functor_app_next = (int)reader["id_functor_app_next"];
                list.Add(acfa);
                if (!cache_acfa.ContainsKey(acfa.Id_functor_app)) cache_acfa[acfa.Id_functor_app] = acfa;
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return list;
        }

        IDictionary<int, IList<AbstractComponentFunctorApplication>> cache_acfa_list_byabstract = new Dictionary<int, IList<AbstractComponentFunctorApplication>>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<AbstractComponentFunctorApplication> listByIdAbstract(int id_abstract, IDbConnection dbcon)
        {

            IList<AbstractComponentFunctorApplication> list = null;
            if (cache_acfa_list_byabstract.TryGetValue(id_abstract, out list)) return list;

            list = new List<AbstractComponentFunctorApplication>();
            cache_acfa_list_byabstract.Add(id_abstract, list);

            AbstractComponentFunctorApplication acfa = null;
            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT acfa.id_abstract, acfa.id_functor_app, acfa.id_functor_app_next, acfa.quantifier " +
                 "FROM component as c, abstractcomponentfunctorapplication as acfa " +
                 "WHERE c.id_functor_app = acfa.id_functor_app AND acfa.id_abstract = " + id_abstract;
            //  Console.WriteLine(sql);
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
				if (Backend.isQuantifier((int)reader["id_abstract"], dbcon) == null)
					acfa = new AbstractComponentFunctorApplication();
				else
				{
					acfa = new AbstractComponentFunctorApplicationQuantifier();
					((AbstractComponentFunctorApplicationQuantifier)acfa).Quantifier = (decimal)(reader["quantifier"] == DBNull.Value ? null : reader["quantifier"]);
				}
				acfa.Id_functor_app = (int)reader["id_functor_app"];
                acfa.Id_abstract = (int)reader["id_abstract"];
                acfa.Id_functor_app_next = (int)reader["id_functor_app_next"];
                list.Add(acfa);
                if (!cache_acfa.ContainsKey(acfa.Id_functor_app)) cache_acfa.Add(acfa.Id_functor_app, acfa);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return list;
        }

        public void updateIdFunctorAppNext(AbstractComponentFunctorApplication acfa, int id_functor_app_next, IDbConnection dbcon)
        {
            String sql = "UPDATE abstractcomponentfunctorapplication SET id_functor_app_next = " + id_functor_app_next + " " +
                         " WHERE id_functor_app=" + acfa.Id_functor_app + "";

            DBConnector.performSQLUpdate(sql, dbcon);
        }

        internal AbstractComponentFunctorApplication retrieve_last(AbstractComponentFunctorApplication acfa_return, IDbConnection dbcon)
        {
            AbstractComponentFunctorApplication acfa = Backend.acfadao.retrieve_next(acfa_return.Id_functor_app, dbcon);

            while (acfa != null)
            {
                acfa_return = acfa;
                acfa = Backend.acfadao.retrieve_next(acfa.Id_functor_app, dbcon);
            }

            return acfa_return;
        }
    }//class

}//namespace