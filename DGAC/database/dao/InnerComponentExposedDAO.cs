﻿using System;
using System.IO;
using System.Data;
using System.Data.OleDb;
using MySql.Data.MySqlClient;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Diagnostics;


namespace org.hpcshelf.database
{

    [Serializable()]
    public class InnerComponentExposedDAO
    {

        public void insert(InnerComponentExposed ac, IDbConnection dbcon)
        {
            String sql =
                "INSERT INTO innercomponentexposed (id_abstract, id_inner_rename, id_inner_owner, id_inner)" +
                " VALUES (" + ac.Id_abstract + ",'" + ac.Id_inner_rename + "','" + ac.Id_inner_owner + "','" + ac.Id_inner + "')";

		    Console.WriteLine("InnerComponentExposedDAO.cs: TRY INSERT PUBLIC INNER COMPONENT :" + sql);
			
            DBConnector.performSQLUpdate(sql, dbcon);
        }

        public IList<InnerComponentExposed> listExposedInnerOfOwner(int id_abstract, string id_inner_owner, IDbConnection dbcon)
        {

            IList<InnerComponentExposed> list = new List<InnerComponentExposed>();
            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_abstract, id_inner_rename, id_inner_owner, id_inner " +
                "FROM innercomponentexposed " +
                "WHERE id_abstract=" + id_abstract + " AND " +
                "id_inner_owner like '" + id_inner_owner + "'";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                InnerComponentExposed ic = new InnerComponentExposed();
                ic.Id_abstract = (int)reader["id_abstract"];
                ic.Id_inner_rename = (string)reader["id_inner_rename"];
                ic.Id_inner_owner = (string)reader["id_inner_owner"];
                ic.Id_inner = (string)reader["id_inner"];
                list.Add(ic);
            }//while
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return list;

        }//list

		public IList<InnerComponentExposed> list_renamings(int id_abstract, string id_inner_owner, string id_inner, IDbConnection dbcon)
		{
			IList<InnerComponentExposed> list = new List<InnerComponentExposed>();
			//IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT id_abstract, id_inner_rename, id_inner_owner, id_inner " +
				"FROM innercomponentexposed " +
				"WHERE id_abstract=" + id_abstract + " AND " +
				"id_inner_owner like '" + id_inner_owner + "' AND " +
                "id_inner like '" + id_inner + "'";
			dbcmd.CommandText = sql;
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			while (reader.Read())
			{
				InnerComponentExposed ic = new InnerComponentExposed();
				ic.Id_abstract = (int)reader["id_abstract"];
				ic.Id_inner_rename = (string)reader["id_inner_rename"];
				ic.Id_inner_owner = (string)reader["id_inner_owner"];
				ic.Id_inner = (string)reader["id_inner"];
				list.Add(ic);
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return list;
		}//list
		

        public IList<InnerComponentExposed> listOwnerOfExposedInner(int id_abstract, string id_inner_rename, IDbConnection dbcon)
        {

            IList<InnerComponentExposed> list = new List<InnerComponentExposed>();
           // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_abstract, id_inner_rename, id_inner_owner, id_inner " +
                "FROM innercomponentexposed  " +
                "WHERE id_abstract=" + id_abstract + " AND " +
                "id_inner_rename like '" + id_inner_rename + "'";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			Console.WriteLine ("listOwnerOfExposedInner: " + sql);
            while (reader.Read())
            {
                InnerComponentExposed ic = new InnerComponentExposed();
                ic.Id_abstract = (int)reader["id_abstract"];
                ic.Id_inner_rename = (string)reader["id_inner_rename"];
                ic.Id_inner_owner = (string)reader["id_inner_owner"];
                ic.Id_inner = (string)reader["id_inner"];
                list.Add(ic);
            }//while
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return list;

        }//list



        public InnerComponentExposed retrieve(int id_abstract, string id_inner_owner, string id_inner_rename, IDbConnection dbcon)
        {
            InnerComponentExposed ice = null ;
            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_abstract, id_inner_rename, id_inner_owner, id_inner " +
                "FROM innercomponentexposed " +
                "WHERE id_abstract=" + id_abstract + " AND " +
                      "id_inner_rename like '" + id_inner_rename + "' AND " +
                      "id_inner_owner like '" + id_inner_owner + "'";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                ice = new InnerComponentExposed();
                ice.Id_abstract = (int)reader["id_abstract"];
                ice.Id_inner_rename = (string)reader["id_inner_rename"];
                ice.Id_inner_owner = (string)reader["id_inner_owner"];
                ice.Id_inner = (string)reader["id_inner"];
            }//while
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            return ice;
        }

		public InnerComponentExposed retrieve3(int id_abstract, string id_inner_owner, string id_inner, IDbConnection dbcon)
		{
			InnerComponentExposed ice = null;
		//	IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT id_abstract, id_inner_rename, id_inner_owner, id_inner " +
				"FROM innercomponentexposed " +
				"WHERE id_abstract=" + id_abstract + " AND " +
					  "id_inner like '" + id_inner + "' AND " +
					  "id_inner_owner like '" + id_inner_owner + "'";
			dbcmd.CommandText = sql;
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			if (reader.Read())
			{
				ice = new InnerComponentExposed();
				ice.Id_abstract = (int)reader["id_abstract"];
				ice.Id_inner_rename = (string)reader["id_inner_rename"];
				ice.Id_inner_owner = (string)reader["id_inner_owner"];
				ice.Id_inner = (string)reader["id_inner"];
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;

			return ice;
		}

        internal InnerComponentExposed retrieve2(int id_abstract, string id_inner_rename, IDbConnection dbcon)
		{
			InnerComponentExposed ice = null ;
		//	IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();
			string sql =
				"SELECT id_abstract, id_inner_rename, id_inner_owner, id_inner " +
				"FROM innercomponentexposed " +
				"WHERE id_abstract=" + id_abstract + " AND " +
				"id_inner_rename like '" + id_inner_rename;
			dbcmd.CommandText = sql;
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
			if (reader.Read())
			{
				ice = new InnerComponentExposed();
				ice.Id_abstract = (int)reader["id_abstract"];
				ice.Id_inner_rename = (string)reader["id_inner_rename"];
				ice.Id_inner_owner = (string)reader["id_inner_owner"];
				ice.Id_inner = (string)reader["id_inner"];
			}//while
			// clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;

			return ice;
		}

		public IList<InnerComponentExposed> retrieveContainerList(int id_abstract_start, string id_inner_owner, string id_inner, IDbConnection dbcon)
		{
			int id_abstract = id_abstract_start;
			IList<InnerComponentExposed> iceList = new List<InnerComponentExposed>();
			InnerComponentExposed ice = null;

			while (id_abstract > 0)
			{
			//	IDbConnection dbcon = DBConnector.DBcon;
				IDbCommand dbcmd = dbcon.CreateCommand();
				string sql =
					"SELECT id_abstract, id_inner_rename, id_inner_owner, id_inner " +
					"FROM innercomponentexposed " +
					"WHERE id_abstract=" + id_abstract + " AND " +
						  "id_inner like '" + id_inner + "' AND " +
						  "id_inner_owner like '" + id_inner_owner + "'";
				Console.WriteLine("retrieveContainerList: " + sql);
				dbcmd.CommandText = sql;
				IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
				while (reader.Read())
				{
					ice = new InnerComponentExposed();
					ice.Id_abstract = (int)reader["id_abstract"];
					ice.Id_inner_rename = (string)reader["id_inner_rename"];
					ice.Id_inner_owner = (string)reader["id_inner_owner"];
					ice.Id_inner = (string)reader["id_inner"];
					iceList.Add(ice);
				}//while
				 // clean up
				reader.Close();
				reader = null;
				dbcmd.Dispose();
				dbcmd = null;

				if (ice == null)
				{
					AbstractComponentFunctor acf = org.hpcshelf.DGAC.Backend.acfdao.retrieve(id_abstract, dbcon);
					if (acf.Id_functor_app_supertype > 0)
					{
						AbstractComponentFunctorApplication acfa = org.hpcshelf.DGAC.Backend.acfadao.retrieve(acf.Id_functor_app_supertype, dbcon);
						id_abstract = acfa.Id_abstract;
					}
					else
						id_abstract = -1;
				}
				else
					id_abstract = -1;
			}

			return iceList;
		}
		
        internal InnerComponentExposed retrieveContainer(int id_abstract_start, string id_inner_owner, string id_inner, IDbConnection dbcon)
        {
			int id_abstract = id_abstract_start;
	        InnerComponentExposed ice = null;
			
			while (id_abstract > 0) 
			{
	            //IDbConnection dbcon = DBConnector.DBcon;
	            IDbCommand dbcmd = dbcon.CreateCommand();
	            string sql =
	                "SELECT id_abstract, id_inner_rename, id_inner_owner, id_inner " +
	                "FROM innercomponentexposed " +
	                "WHERE id_abstract=" + id_abstract + " AND " +
	                      "id_inner like '" + id_inner + "' AND " +
	                      "id_inner_owner like '" + id_inner_owner + "'";
				Console.WriteLine ("retrieveContainer: " + sql);
	            dbcmd.CommandText = sql;
	            IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
	            if (reader.Read())
	            {
	                ice = new InnerComponentExposed();
	                ice.Id_abstract = (int)reader["id_abstract"];
	                ice.Id_inner_rename = (string)reader["id_inner_rename"];
	                ice.Id_inner_owner = (string)reader["id_inner_owner"];
	                ice.Id_inner = (string)reader["id_inner"];
	            }//while
	            // clean up
	            reader.Close();
	            reader = null;
	            dbcmd.Dispose();
	            dbcmd = null;
				
				if (ice == null) 
				{
	                AbstractComponentFunctor acf = org.hpcshelf.DGAC.Backend.acfdao.retrieve(id_abstract, dbcon);
					if (acf.Id_functor_app_supertype > 0)
					{
					   AbstractComponentFunctorApplication acfa = org.hpcshelf.DGAC.Backend.acfadao.retrieve(acf.Id_functor_app_supertype, dbcon);
					   id_abstract = acfa.Id_abstract;
					}
					else 
						id_abstract = -1;
				}
				else 
				   id_abstract = -1;
			}

            return ice;
        }

        public InnerComponentExposed find_certifier_id(int id_abstract, string id_inner_owner, string id_inner, IDbConnection dbcon)
        {
			// IDbConnection dbcon = DBConnector.DBcon;
			IDbCommand dbcmd = dbcon.CreateCommand();

            string sql = "SELECT A.*, B.id_interface_slice_original, COUNT(C.id_abstract)\n" +
                         "FROM innercomponentexposed as A LEFT JOIN sliceexposed as B ON \n" +
                         "A.id_abstract = B.id_abstract AND A.id_inner_rename = B.id_inner AND A.id_inner_owner = B.id_inner_owner, innercomponentexposed as C\n" +
                         "WHERE A.id_abstract = C.id_abstract AND A.id_inner = C.id_inner AND A.id_inner_rename = C.id_inner_rename AND\n" +
                         "A.id_abstract=" + id_abstract + " AND A.id_inner like '" + id_inner + "' AND C.id_inner_owner like '" + id_inner_owner + "' AND B.id_interface_slice_original like 'certifier'\n" +
                         "GROUP BY A.id_abstract, A.id_inner, A.id_inner_rename, A.id_inner_owner, B.id_interface_slice_original \n";

			dbcmd.CommandText = sql;
			IDataReader reader; /* lock (DBConnector.db_lock) */ { reader = dbcmd.ExecuteReader(); }
            InnerComponentExposed ic=null;
            if (reader.Read())
			{
				ic = new InnerComponentExposed();
				ic.Id_abstract = (int)reader["id_abstract"];
				ic.Id_inner_rename = (string)reader["id_inner_rename"];
				ic.Id_inner_owner = (string)reader["id_inner_owner"];
				ic.Id_inner = (string)reader["id_inner"];
			}//while
			 // clean up
			reader.Close();
			reader = null;
			dbcmd.Dispose();
			dbcmd = null;
			return ic;

        }

        public void delete(int id_abstract, string id_inner_owner, string id_inner_rename, IDbConnection dbcon)
        {
            string sql = "DELETE FROM innercomponentexposed " +
                         "WHERE id_abstract=" + id_abstract + 
                          " AND id_inner_owner like '" + id_inner_owner + "'" +
                          " AND id_inner_rename like '" + id_inner_rename + "'";

			DBConnector.performSQLUpdate(sql, dbcon);
		}

    }//class

}//namespace