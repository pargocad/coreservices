﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace org.hpcshelf.database
{

    [Serializable()]
    public class ComponentDAO
    {
        /* HERON */
        public int insert(Component ac, IDbConnection dbcon)
        {
            int nextKey = DBConnector.nextKey("id_concrete", "component", dbcon);

            String sql =
                "INSERT INTO component (id_concrete, id_concrete_supertype, id_functor_app, id_abstract, library_path, hash_component_UID, argument_calculator)" +
                    " VALUES (" + nextKey + "," + ac.Id_concrete_supertype + "," + ac.Id_functor_app + "," + ac.GetId_abstract(dbcon) + ",'" + ac.Library_path + "','" + ac.Hash_component_UID + "','" + ac.ContextArgumentCalculator + "')";

            // Console.WriteLine("Component.cs: TRY INSERT: " + sql);

            DBConnector.performSQLUpdate(sql, dbcon);

            return nextKey;
        }


        IDictionary<int, Component> cache_c_functor_app = new Dictionary<int, Component>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Component retrieveByFunctorApp(int id_functor_app, IDbConnection dbcon)
        {
            Component c = null;
            if (cache_c_functor_app.TryGetValue(id_functor_app, out c)) return c;

            // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_concrete, id_concrete_supertype, library_path, id_functor_app, id_abstract, hash_component_UID, argument_calculator " +
                 "FROM component " +
                 "WHERE id_functor_app=" + id_functor_app + " AND hash_component_uid not like '%delete%'";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */
            { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                c = new Component();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_concrete_supertype = (int)reader["id_concrete_supertype"];
                c.Library_path = (string)reader["library_path"];
                c.Id_functor_app = (int)reader["id_functor_app"];
                c.SetId_abstract((int)reader["id_abstract"]);
                c.Hash_component_UID = (string)reader["hash_component_UID"];
                c.ContextArgumentCalculator = (string)reader["argument_calculator"];
                cache_c_functor_app.Add(id_functor_app, c);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return c;
        }

        IDictionary<string, Component> cache_c_uid = new Dictionary<string, Component>();

        public Component retrieve_uid(string hash_component_uid, IDbConnection dbcon)
        {

            Component c = null;
            // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_concrete, id_concrete_supertype, library_path, id_functor_app, id_abstract, hash_component_UID, argument_calculator " +
                 "FROM component " +
                 "WHERE hash_component_UID like '" + hash_component_uid + "'";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */
            { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                c = new Component();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_concrete_supertype = (int)reader["id_concrete_supertype"];
                c.Library_path = (string)reader["library_path"];
                c.Id_functor_app = (int)reader["id_functor_app"];
                c.SetId_abstract((int)reader["id_abstract"]);
                c.Hash_component_UID = (string)reader["hash_component_UID"];
                c.ContextArgumentCalculator = (string)reader["argument_calculator"];
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return c;
        }


        IDictionary<string, Component> cache_c_lp = new Dictionary<string, Component>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Component retrieve_libraryPath(string library_path, IDbConnection dbcon)
        {
            Component c = null;

            if (cache_c_lp.TryGetValue(library_path, out c)) return c;

            // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql = "SELECT * FROM component WHERE library_path like '" + library_path + "'";

            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */
            { reader = dbcmd.ExecuteReader(); }
            if (reader.Read())
            {
                c = new Component();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_concrete_supertype = (int)reader["id_concrete_supertype"];
                c.Library_path = (string)reader["library_path"];
                c.Id_functor_app = (int)reader["id_functor_app"];
                c.SetId_abstract((int)reader["id_abstract"]);
                c.Hash_component_UID = (string)reader["hash_component_UID"];
                c.ContextArgumentCalculator = (string)reader["argument_calculator"];
                if (cache_c_lp.ContainsKey(library_path))
                    cache_c_lp.Remove(library_path);
                cache_c_lp.Add(library_path, c);
            }
            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return c;
        }

        IDictionary<int, Component> cache_c_id = new Dictionary<int, Component>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public Component retrieve(int id_concrete, IDbConnection dbcon)
        {

            Component c = null;

            if (cache_c_id.TryGetValue(id_concrete, out c)) return c;

            // IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_concrete, id_concrete_supertype, library_path, id_functor_app, id_abstract, hash_component_UID, argument_calculator " +
                 "FROM component " +
                 "WHERE id_concrete=" + id_concrete;
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */
            { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                c = new Component();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_concrete_supertype = (int)reader["id_concrete_supertype"];
                c.Library_path = (string)reader["library_path"];
                c.Id_functor_app = (int)reader["id_functor_app"];
                c.SetId_abstract((int)reader["id_abstract"]);
                c.Hash_component_UID = (string)reader["hash_component_UID"];
                c.ContextArgumentCalculator = (string)reader["argument_calculator"];
                cache_c_id.Add(id_concrete, c);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return c;
        }

        IDictionary<string, IList<Component>> cache_c_lp_list = new Dictionary<string, IList<Component>>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public IList<Component> retrieve_libraryPathList(string library_path, IDbConnection dbcon)
        {
            IList<Component> cList;
            //if (cache_c_lp_list.TryGetValue(library_path, out cList) && cList.Count > 0) return cList;

            cList = new List<Component>();


            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_concrete, id_concrete_supertype, library_path, id_functor_app, id_abstract, hash_component_UID, argument_calculator " +
                 "FROM component " +
                 "WHERE library_path like '" + library_path + "'";

            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */
            { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Component c = new Component();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_concrete_supertype = (int)reader["id_concrete_supertype"];
                c.Library_path = (string)reader["library_path"];
                c.Id_functor_app = (int)reader["id_functor_app"];
                c.SetId_abstract((int)reader["id_abstract"]);
                c.Hash_component_UID = (string)reader["hash_component_UID"];
                c.ContextArgumentCalculator = (string)reader["argument_calculator"];
                if (cache_c_lp.ContainsKey(library_path))
                    cache_c_lp.Remove(library_path);
                cList.Add(c);
            }//while


            //cache_c_lp_list.Add(library_path, cList);

            // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return cList;
        }


        IDictionary<int, IList<Component>> cache_c_impl = new Dictionary<int, IList<Component>>();

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal IList<Component> retrieveThatImplements(int id_abstract, IDbConnection dbcon)
        {
            IList<Component> cList = null;

            if (cache_c_impl.TryGetValue(id_abstract, out cList)) return cList;

            cList = new List<Component>();

            cache_c_impl.Add(id_abstract, cList);

            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT C.*, A.id_abstract " +
                 "FROM component C, abstractcomponentfunctorapplication A " +
                 "WHERE A.id_abstract=" + id_abstract + " AND " +
                      " C.id_functor_app = A.id_functor_app";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */
            { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Component c = new Component();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_concrete_supertype = (int)reader["id_concrete_supertype"];
                c.Library_path = (string)reader["library_path"];
                c.Id_functor_app = (int)reader["id_functor_app"];
                c.SetId_abstract((int)reader["id_abstract"]);
                c.Hash_component_UID = (string)reader["hash_component_UID"];
                c.ContextArgumentCalculator = (string)reader["argument_calculator"];
                cList.Add(c);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return cList;
        }

        internal IList<Component> list(IDbConnection dbcon)
        {
            IList<Component> cList = new List<Component>();
            //  IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_concrete, id_concrete_supertype, library_path, id_functor_app, hash_component_UID, argument_calculator " +
                 "FROM component";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */
            { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Component c = new Component();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_concrete_supertype = (int)reader["id_concrete_supertype"];
                c.Library_path = (string)reader["library_path"];
                c.Id_functor_app = (int)reader["id_functor_app"];
                c.Hash_component_UID = (string)reader["hash_component_UID"];
                c.ContextArgumentCalculator = (string)reader["argument_calculator"];
                cList.Add(c);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return cList;
        }

        public IList<Component> listByUID(string cuid, IDbConnection dbcon)
        {
            IList<Component> cList = new List<Component>();
            //  IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                 "SELECT id_concrete, id_concrete_supertype, library_path, id_functor_app, hash_component_UID, argument_calculator " +
                 "FROM component WHERE hash_component_UID = '" + cuid + "'";
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */
            { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Component c = new Component();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_concrete_supertype = (int)reader["id_concrete_supertype"];
                c.Library_path = (string)reader["library_path"];
                c.Id_functor_app = (int)reader["id_functor_app"];
                c.Hash_component_UID = (string)reader["hash_component_UID"];
                c.ContextArgumentCalculator = (string)reader["argument_calculator"];
                cList.Add(c);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return cList;
        }

        public IList<Component> listByAbstractComponent(int id_abstract, IDbConnection dbcon)
        {
            IList<Component> cList = new List<Component>();
            //IDbConnection dbcon = DBConnector.DBcon;
            IDbCommand dbcmd = dbcon.CreateCommand();
            string sql =
                "SELECT id_concrete, id_concrete_supertype, library_path, id_functor_app, hash_component_UID, argument_calculator " +
                    "FROM component WHERE id_abstract = " + id_abstract;
            dbcmd.CommandText = sql;
            IDataReader reader; /* lock (DBConnector.db_lock) */
            { reader = dbcmd.ExecuteReader(); }
            while (reader.Read())
            {
                Component c = new Component();
                c.Id_concrete = (int)reader["id_concrete"];
                c.Id_concrete_supertype = (int)reader["id_concrete_supertype"];
                c.Library_path = (string)reader["library_path"];
                c.Id_functor_app = (int)reader["id_functor_app"];
                //c.Id_abstract = (int)reader["id_abstract"];
                c.Hash_component_UID = (string)reader["hash_component_UID"];
                c.ContextArgumentCalculator = (string)reader["argument_calculator"];
                cList.Add(c);
            }//while
             // clean up
            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
            return cList;
        }

    }//class

}//namespace
