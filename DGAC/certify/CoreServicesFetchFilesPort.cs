﻿using System;
using System.Collections.Generic;
using System.Data;
using org.hpcshelf.DGAC;
using org.hpcshelf.database;

namespace DGAC.certify
{

    public interface ICoreServicesFetchFiles
    {
        // <unit name, file_type, file name>
        IList<Tuple<string, string, string>> list(IDbConnection dbcon);

        // the second item (typed as "object") is a reference to be used for fetching files of the abstract component.
        // the list is sorted according to the subtype relation. The first is the direct supertype. 
        IList<Tuple<string, object>> listAbstractComponents(IDbConnection dbcon);

		/* multiple unit version */

		string fetchFileName(object c_id, string unit_id, string file_type, IDbConnection dbcon); // It is supposed to exist a single file in the unit for the 'file_type'
		string fetchContentsByFileType(object c_id, string unit_id, string file_type, IDbConnection dbcon); // It is supposed to exist a single file in the unit for the 'file_type'
		IList<string> fetchFileNameList(object c_id, string unit_id, string file_type, IDbConnection dbcon);
		string fetchContentsByFileName(object c_id, string unit_id, string file_name, IDbConnection dbcon);
		IList<string> fileTypes(object c_id, string unit_id, IDbConnection dbcon);
		bool hasFileType(object c_id, string unit_id, string file_type, IDbConnection dbcon);

        /* multiple unit version */

		string fetchFileName(string unit_id, string file_type, IDbConnection dbconv); // It is supposed to exist a single file in the unit for the 'file_type'
		string fetchContentsByFileType(string unit_id, string file_type, IDbConnection dbcon); // It is supposed to exist a single file in the unit for the 'file_type'
		IList<string> fetchFileNameList(string unit_id, string file_type, IDbConnection dbcon);
		string fetchContentsByFileName(string unit_id, string file_name, IDbConnection dbcon);
		IList<string> fileTypes(string unit_id, IDbConnection dbcon);
        bool hasFileType(string unit_id, string file_type, IDbConnection dbcon);
		
        /* single unit version */

		string fetchFileName(string file_type, IDbConnection dbcon); // It is supposed to exist a single file in the unit for the 'file_type'
		string fetchContentsByFileType(string file_type, IDbConnection dbcon); // It is supposed to exist a single file in the unit for the 'file_type'
		IList<string> fetchFileNameList(string file_type, IDbConnection dbcon);
		string fetchContentsByFileName(string file_name, IDbConnection dbcon);
		IList<string> fileTypes(IDbConnection dbcon);
		bool hasFileType(string file_type, IDbConnection dbcon);
	}

    public class CoreServicesFetchFilesPort : ICoreServicesFetchFiles, gov.cca.Port
    {
        private int id_concrete_certifiable;

        public CoreServicesFetchFilesPort(int id_concrete_certifiable)
        {
            this.id_concrete_certifiable = id_concrete_certifiable;
        }

        public string fetchContentsByFileName(object c_id, string unit_id, string file_name, IDbConnection dbcon)
        {
            IList<string> file_contents_list = new List<string>();

            int id_abstract = abstract_component_list[c_id].Id_abstract;

            IList<SourceCode> sc_list = Backend.scdao.listByFileName('u', id_abstract, unit_id, file_name, dbcon);
            foreach (SourceCode sc in sc_list)
                file_contents_list.Add(sc.Contents);
            return file_contents_list[0];
        }

        public string fetchContentsByFileName(string unit_id, string file_name, IDbConnection dbcon)
        {
            IList<string> file_contents_list = new List<string>();


            IList<SourceCode> sc_list = Backend.scdao.listByFileName('u', this.id_concrete_certifiable, unit_id, file_name, dbcon);
            foreach (SourceCode sc in sc_list)
                file_contents_list.Add(sc.Contents);
            return file_contents_list[0];
        }

        public string fetchContentsByFileName(string file_name, IDbConnection dbcon)
        {
            IList<string> file_contents_list = new List<string>();

            IList<SourceCode> sc_list = Backend.scdao.listByFileName('u', this.id_concrete_certifiable, file_name, dbcon);
            foreach (SourceCode sc in sc_list)
                file_contents_list.Add(sc.Contents);

            return file_contents_list[0];
        }

        public string fetchContentsByFileType(object c_id, string unit_id, string file_type, IDbConnection dbcon)
        {
            IList<string> file_name_list = new List<string>();

            int id_abstract = abstract_component_list[c_id].Id_abstract;

            IList<SourceCode> sc_list = Backend.scdao.listByFileType('u', id_abstract, unit_id, file_type, dbcon);
            foreach (SourceCode sc in sc_list)
                file_name_list.Add(sc.Contents);

            return file_name_list[0];
        }

        public string fetchContentsByFileType(string unit_id, string file_type, IDbConnection dbcon)
        {
            IList<string> file_name_list = new List<string>();

            IList<SourceCode> sc_list = Backend.scdao.listByFileType('u', this.id_concrete_certifiable, unit_id, file_type, dbcon);
            foreach (SourceCode sc in sc_list)
                file_name_list.Add(sc.Contents);

            return file_name_list[0];
        }

        public string fetchContentsByFileType(string file_type, IDbConnection dbcon)
        {
            IList<string> file_name_list = new List<string>();

            IList<SourceCode> sc_list = Backend.scdao.listByFileType('u', this.id_concrete_certifiable, file_type, dbcon);
            foreach (SourceCode sc in sc_list)
                file_name_list.Add(sc.Contents);

            return file_name_list[0];
        }

        public string fetchFileName(object c_id, string unit_id, string file_type, IDbConnection dbcon)
        {
            IList<string> file_name_list = new List<string>();

            int id_abstract = abstract_component_list[c_id].Id_abstract;

            IList<SourceCode> sc_list = Backend.scdao.listByFileType('u', id_abstract, unit_id, file_type, dbcon);
            foreach (SourceCode sc in sc_list)
                file_name_list.Add(sc.File_name);

            return file_name_list[0];
        }

        public string fetchFileName(string unit_id, string file_type, IDbConnection dbcon)
        {
            IList<string> file_name_list = new List<string>();

            IList<SourceCode> sc_list = Backend.scdao.listByFileType('u', this.id_concrete_certifiable, unit_id, file_type, dbcon);
            foreach (SourceCode sc in sc_list)
                file_name_list.Add(sc.File_name);

            return file_name_list[0];
        }

        public string fetchFileName(string file_type, IDbConnection dbcon)
        {
            IList<string> file_name_list = new List<string>();

            IList<SourceCode> sc_list = Backend.scdao.listByFileType('u', this.id_concrete_certifiable, file_type, dbcon);
            foreach (SourceCode sc in sc_list)
                file_name_list.Add(sc.File_name);

            return file_name_list[0];
        }

        public IList<string> fetchFileNameList(object c_id, string unit_id, string file_type, IDbConnection dbcon)
        {
            IList<string> file_name_list = new List<string>();

            int id_abstract = abstract_component_list[c_id].Id_abstract;

            IList<SourceCode> sc_list = Backend.scdao.listByFileType('u', id_abstract, unit_id, file_type, dbcon);
            foreach (SourceCode sc in sc_list)
                file_name_list.Add(sc.File_name);

            return file_name_list;
        }

        public IList<string> fetchFileNameList(string unit_id, string file_type, IDbConnection dbcon)
        {
            IList<string> file_name_list = new List<string>();

            IList<SourceCode> sc_list = Backend.scdao.listByFileType('u', this.id_concrete_certifiable, unit_id, file_type, dbcon);
            foreach (SourceCode sc in sc_list)
                file_name_list.Add(sc.File_name);

            return file_name_list;
        }

        public IList<string> fetchFileNameList(string file_type, IDbConnection dbcon)
        {
            IList<string> file_name_list = new List<string>();

            IList<SourceCode> sc_list = Backend.scdao.listByFileType('u', this.id_concrete_certifiable, file_type, dbcon);
            foreach (SourceCode sc in sc_list)
                file_name_list.Add(sc.File_name);

            return file_name_list;
        }

        public IList<string> fileTypes(object c_id, string unit_id, IDbConnection dbcon)
        {
            int id_abstract = abstract_component_list[c_id].Id_abstract;

            return Backend.scdao.fileTypes('i', id_abstract, unit_id, dbcon);
            //			throw new NotImplementedException();
        }

        public IList<string> fileTypes(string unit_id, IDbConnection dbcon)
        {
            return Backend.scdao.fileTypes('u', this.id_concrete_certifiable, unit_id, dbcon);
            //throw new NotImplementedException();
        }

        public IList<string> fileTypes(IDbConnection dbcon)
        {
            return Backend.scdao.fileTypes('u', this.id_concrete_certifiable, dbcon);
        }

        public bool hasFileType(object c_id, string unit_id, string file_type, IDbConnection dbcon)
        {
            int id_abstract = abstract_component_list[c_id].Id_abstract;

            IList<SourceCode> u_list = Backend.scdao.listByFileType('i', id_abstract, unit_id, file_type, dbcon);

            return u_list.Count > 0;
        }

        public bool hasFileType(string unit_id, string file_type, IDbConnection dbcon)
        {
            IList<SourceCode> u_list = Backend.scdao.listByFileType('u', this.id_concrete_certifiable, unit_id, file_type, dbcon);

            return u_list.Count > 0;
        }

        public bool hasFileType(string file_type, IDbConnection dbcon)
        {
            IList<SourceCode> u_list = Backend.scdao.listByFileType('u', this.id_concrete_certifiable, file_type, dbcon);
            return u_list.Count > 0;
        }

        public IList<Tuple<string, string, string>> list(IDbConnection dbcon)
        {
            IList<Tuple<string, string, string>> l = new List<Tuple<string, string, string>>();

            IList<SourceCode> u_list = Backend.scdao.list('u', this.id_concrete_certifiable, dbcon);

            foreach (SourceCode sc in u_list)
            {
                Tuple<string, string, string> t = new Tuple<string, string, string>(sc.Id_owner, sc.File_type, sc.File_name);
                l.Add(t);
            }

            return l;
        }

        private IDictionary<object, AbstractComponentFunctor> abstract_component_list = new Dictionary<object, AbstractComponentFunctor>();

        public IList<Tuple<string, object>> listAbstractComponents(IDbConnection dbcon)
        {
            IList<Tuple<string, object>> l = new List<Tuple<string, object>>();
            Component c = Backend.cdao.retrieve(this.id_concrete_certifiable, dbcon);

            int id_functor_app = c.Id_functor_app;

            do
            {
                AbstractComponentFunctorApplication acfa = Backend.acfadao.retrieve(id_functor_app, dbcon);
                AbstractComponentFunctor acf = Backend.acfdao.retrieve(acfa.Id_abstract, dbcon);

                object key_obj = new object();
                l.Add(new Tuple<string, object>(acf.Library_path, key_obj));
                abstract_component_list[key_obj] = acf;

                id_functor_app = acf.Id_functor_app_supertype;
            }
            while (id_functor_app > 0);

            return l;
        }
}
}
