﻿

using System;
using System.Collections.Generic;
using RDotNet;

namespace org.hpcshelf
{
    public class Alite
    {

        private static REngine engine = null;
        private static IDictionary<string, string> criteria = new Dictionary<string, string>() 
            { 
                {"execution_time", "min"}, 
                {"energy_consumption","min"}, 
                {"monetary_cost","min"}
            };

		private static void createEngine()
        {
			engine = REngine.GetInstance();
			engine.Initialize();
			engine.Evaluate("require('MCDM')");
		}

        private static void destroyEngine()
        {
            engine.Dispose();
        }

		public static Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] rank(Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[] systems, 
                                                         IDictionary<string, double> weights, string ranking)
		{
            Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] result = null;

            if (engine == null)
                createEngine();
            
            REnvironment renv = engine.CreateEnvironment(engine.GlobalEnvironment);

            double v = 0.5;
            double lambda = 0.5;
            switch (ranking)
            {
				case "MCMD_MMOORA"       : result = rankBy_MMOORA       (engine, renv, systems, weights           ); break;
				case "MCMD_TOPSISLinear" : result = rankBy_TOPSISLinear (engine, renv, systems, weights           ); break;
				case "MCMD_TOPSISVector" : result = rankBy_TOPSISVector (engine, renv, systems, weights           ); break;
				case "MCMD_VIKOR"        : result = rankBy_VIKOR        (engine, renv, systems, weights, v        ); break;
				case "MCMD_WASPAS"       : result = rankBy_WASPAS       (engine, renv, systems, weights,    lambda); break;
				case "MCMD_RIM"          : result = rankBy_RIM          (engine, renv, systems, weights           ); break;
				case "MCMD_R_MetaRanking": result = rankBy_MetaRanking  (engine, renv, systems, weights, v, lambda); break;
			}

			renv.Dispose();

            return result;
		}

        private static Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] rankBy_WASPAS(REngine engine, REnvironment renv, IList<Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>> systems, IDictionary<string, double> weights, double lambda)
        {
			Function f = engine.GetSymbol("WASPAS").AsFunction();

			NumericMatrix d = engine.CreateNumericMatrix(createDecisionMatrix(systems, weights));
			NumericVector w = engine.CreateNumericVector(createWeigthVector(weights));
			CharacterVector cb = engine.CreateCharacterVector(createCriteriaVector(criteria));
            NumericVector lambda_v = engine.CreateNumeric(lambda);

            DataFrame output = f.Invoke(new SymbolicExpression[] { d, w, cb, lambda_v }).AsDataFrame();

			//  Alternatives  WSM WPM Q Ranking
			int[] alternatives = output[0].AsInteger().ToArray();
			int[] ranking = output[4].AsInteger().ToArray();

            Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] result = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[systems.Count];
            for (int i = 0; i < result.Length; i++)
                result[ranking[i] - 1] = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>(systems[i].Item1, systems[i].Item2);

            return result;
		}

		private static Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] rankBy_VIKOR(REngine engine, REnvironment renv, IList<Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>> systems, IDictionary<string, double> weights, double v)
        {
			Function f = engine.GetSymbol("VIKOR").AsFunction();

			NumericMatrix d = engine.CreateNumericMatrix(createDecisionMatrix(systems, weights));
			NumericVector w = engine.CreateNumericVector(createWeigthVector(weights));
			CharacterVector cb = engine.CreateCharacterVector(createCriteriaVector(criteria));
			NumericVector v_v = engine.CreateNumeric(v);

			DataFrame output = f.Invoke(new SymbolicExpression[] { d, w, cb, v_v }).AsDataFrame(); 

			// Alternatives S R Q Ranking
			//int[] alternatives = output[4].AsInteger().ToArray();
			int[] ranking = output[4].AsInteger().ToArray();

			Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] result = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[systems.Count];
            for (int i = 0; i < result.Length; i++)
                result[ranking[i] - 1] = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>(systems[i].Item1, systems[i].Item2);

			return result;
		}

        private static Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] rankBy_TOPSISVector(REngine engine, REnvironment renv, IList<Tuple<org.hpcshelf.database.Component,org.hpcshelf.database.Component, IDictionary<string, object>>> systems, IDictionary<string, double> weights)
        {
			Function f = engine.GetSymbol("TOPSISVector").AsFunction();

			NumericMatrix d = engine.CreateNumericMatrix(createDecisionMatrix(systems, weights));
			NumericVector w = engine.CreateNumericVector(createWeigthVector(weights));
			CharacterVector cb = engine.CreateCharacterVector(createCriteriaVector(criteria));

			DataFrame output = f.Invoke(new SymbolicExpression[] { d, w, cb }).AsDataFrame();

			// Alternatives R Ranking
			int[] alternatives = output[0].AsInteger().ToArray();
			int[] ranking = output[2].AsInteger().ToArray();

			Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] result = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[systems.Count];
			for (int i = 0; i < result.Length; i++)
				result[ranking[i] - 1] = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>(systems[i].Item1, systems[i].Item2);

			return result;
		}

		private static Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] rankBy_MMOORA(REngine engine, REnvironment renv, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[] systems, IDictionary<string, double> weights)
		{
			Function f = engine.GetSymbol("MMOORA").AsFunction();

			NumericMatrix d = engine.CreateNumericMatrix(createDecisionMatrix(systems, weights));
			NumericVector w = engine.CreateNumericVector(createWeigthVector(weights));
			CharacterVector cb = engine.CreateCharacterVector(createCriteriaVector(criteria));

			DataFrame output = f.Invoke(new SymbolicExpression[] { d, w, cb }).AsDataFrame();

			// Alternatives RatioSystem Ranking ReferencePoint Ranking.1 MultiplicativeForm Ranking.2 MultiMooraRanking
			int[] alternatives = output[0].AsInteger().ToArray();
			int[] ranking = output[7].AsInteger().ToArray(); // MultiMooraRanking

			Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] result = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[systems.Length];
            for (int i = 0; i < result.Length; i++)
                result[ranking[i] - 1] = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>(systems[i].Item1, systems[i].Item2);

			return result;
		}
		
        private static Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] rankBy_TOPSISLinear(REngine engine, REnvironment renv, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[] systems, IDictionary<string, double> weights)
        {
			Function f = engine.GetSymbol("TOPSISLinear").AsFunction();

			NumericMatrix d = engine.CreateNumericMatrix(createDecisionMatrix(systems, weights));
			NumericVector w = engine.CreateNumericVector(createWeigthVector(weights));
			CharacterVector cb = engine.CreateCharacterVector(createCriteriaVector(criteria));

			DataFrame output = f.Invoke(new SymbolicExpression[] { d, w, cb }).AsDataFrame();

			// Alternatives R Ranking
			int[] alternatives = output[0].AsInteger().ToArray();
			int[] ranking = output[2].AsInteger().ToArray();

			Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] result = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[systems.Length];
            for (int i = 0; i < result.Length; i++)
                result[ranking[i] - 1] = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>(systems[i].Item1, systems[i].Item2);

			return result;
		}

		private static Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] rankBy_RIM(REngine engine, REnvironment renv, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[] systems, IDictionary<string, double> weights)
		{
			Function f = engine.GetSymbol("RIM").AsFunction();

			NumericMatrix d = engine.CreateNumericMatrix(createDecisionMatrix(systems, weights));
			NumericVector w = engine.CreateNumericVector(createWeigthVector(weights));
			CharacterVector cb = engine.CreateCharacterVector(createCriteriaVector(criteria));
			NumericMatrix AB = engine.CreateNumericMatrix(createABMatrix(systems));
			NumericMatrix CD = engine.CreateNumericMatrix(createCDMatrix(systems));

			DataFrame output = f.Invoke(new SymbolicExpression[] { d, w, cb, AB, CD }).AsDataFrame();

			// Alternatives R Ranking
			int[] alternatives = output[0].AsInteger().ToArray();
			int[] ranking = output[2].AsInteger().ToArray();

			Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] result = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[systems.Length];
			for (int i = 0; i < result.Length; i++)
				result[ranking[i] - 1] = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>(systems[i].Item1, systems[i].Item2);

			return result;
		}

        private static Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] rankBy_MetaRanking(REngine engine, REnvironment renv, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>[] systems, IDictionary<string, double> weights, double v, double lambda)
		{
			Function f = engine.GetSymbol("MetaRanking").AsFunction();

			NumericMatrix d = engine.CreateNumericMatrix(createDecisionMatrix(systems, weights));
			NumericVector w = engine.CreateNumericVector(createWeigthVector(weights));
			CharacterVector cb = engine.CreateCharacterVector(createCriteriaVector(criteria));
			NumericVector lambda_v = engine.CreateNumeric(lambda);
			NumericVector v_v = engine.CreateNumeric(v);
			NumericMatrix AB = engine.CreateNumericMatrix(createABMatrix(systems));
			NumericMatrix CD = engine.CreateNumericMatrix(createCDMatrix(systems));

			DataFrame output = f.Invoke(new SymbolicExpression[] { d, w, cb, v_v, lambda_v, AB, CD }).AsDataFrame();

			// Alternatives MMOORA RIM TOPSISVector TOPSISLinear VIKOR WASPAS MetaRanking_Sum MetaRanking_Aggreg
			int[] alternatives = output[0].AsInteger().ToArray();
			int[] ranking = output[8].AsInteger().ToArray(); // MetaRanking_Aggreg

			Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[] result = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[systems.Length];
			for (int i = 0; i < result.Length; i++)
				result[ranking[i] - 1] = new Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>(systems[i].Item1, systems[i].Item2);

			return result;
		}

        private static double[,] createABMatrix(IList<Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>> systems)
        {
            throw new NotImplementedException();
        }

		private static double[,] createCDMatrix(IList<Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>> systems)
		{
			throw new NotImplementedException();
		}

		private static double[,] createDecisionMatrix(IList<Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>>> systems, IDictionary<string, double> weights)
		{
            double[,] result = new double[systems.Count, weights.Count];

            int i = 0;
            foreach (Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component, IDictionary<string, object>> s in systems)
            {
                int j = 0;
                foreach (string w in weights.Keys)
                {
                    object v = s.Item3[w];
                    if (v is decimal)
                        result[i, j] = decimal.ToDouble((decimal)v);
                    else
                        result[i, j] = (double)((int)v);
                    j++;
                }
                i++;
            }

			return result; // new double[,] { { 1, 4 }, { 3, 5 }, { 2, 3 } }
		}

		private static double[] createWeigthVector(IDictionary<string, double> weights)
		{
            double[] result = new double[weights.Count];

            int i = 0;
            foreach (double v in weights.Values)
                result[i++] = v;

            return result;  // new double[] { 0.5, 0.3, 0.2 };
		}

        private static string[] createCriteriaVector(IDictionary<string, string> criteria)
		{
            string[] result = new string[criteria.Count];

			int i = 0;
			foreach (string v in criteria.Values)
				result[i++] = v;

			return result; // new string[] { "max", "max" };
		}
		
        internal static Tuple<org.hpcshelf.database.Component, IDictionary<string, org.hpcshelf.database.Component>> finalSelection(IDictionary<string, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[]> candidate_systems)
        {
            int n = candidate_systems.Count;

            IDictionary<org.hpcshelf.database.Component, IDictionary<string, org.hpcshelf.database.Component>> a = new Dictionary<org.hpcshelf.database.Component, IDictionary<string, org.hpcshelf.database.Component>>();

            int count = 0;
            while (true)
            {
                foreach (KeyValuePair<string, Tuple<org.hpcshelf.database.Component, org.hpcshelf.database.Component>[]> w in candidate_systems)
                {
                    string cref = w.Key;
                    org.hpcshelf.database.Component c = w.Value[count].Item1;
                    org.hpcshelf.database.Component p = w.Value[count].Item2;

                    if (!a.ContainsKey(p))
                    {
                        a[p] = new Dictionary<string, org.hpcshelf.database.Component>();
                    }

                    a[p][cref] = c;

                    if (a[p].Count == n)
                        return new Tuple<org.hpcshelf.database.Component, IDictionary<string, database.Component>>(p, a[p]);
				}
                count++;
            }

        }
    }
}
