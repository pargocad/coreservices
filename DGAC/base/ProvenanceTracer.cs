﻿using System;
using System.Xml;
using System.Collections.Concurrent;

namespace org.hpcshelf.provenance
{
    public interface IProvenanceTracer
    {
        void save(string port_name, string action_name);
    }

    public class ProvenanceTracer : IProvenanceTracer
    {
        private ConcurrentQueue<Tuple<string,string>> tracking = new ConcurrentQueue<Tuple<string, string>>();

        public void save(string port_name, string action_name)
        {
            tracking.Enqueue(new Tuple<string, string>(port_name, action_name));
        }

        public Tuple<string, string> next()
        {
            Tuple<string, string> action = null;
            tracking.TryDequeue(out action);
            return action;
        }

        public void show()
        {
            foreach (Tuple<string, string> wf in tracking)
            {
                Console.Write(((Tuple<string, string>)wf).Item1);
                Console.Write(":");
                Console.WriteLine(((Tuple<string, string>)wf).Item2);
            }
        }

        /* Persiste o rastreamento */
        // Armazena em arquivo XML
        public bool record(string file_name)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            using (XmlWriter writer = XmlWriter.Create(file_name, settings))
            {
                writer.WriteStartDocument();
                writer.WriteComment("This file is generated automatically.");
                writer.WriteStartElement("Provenance");

                foreach (Tuple<string, string> action in tracking)
                {
                    writer.WriteStartElement("SWLWorkflowInvoke");
                    writer.WriteAttributeString("hash", action.GetHashCode().ToString());
                    writer.WriteElementString("port_name", (action as Tuple<string, string>).Item1);
                    writer.WriteElementString("action_name", (action as Tuple<string, string>).Item2);
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
            return true;
        }

        /* Carrega o rastreamento */
        // TODO: Cria objeto Fila de ações
        public bool load(string orchestration_file_name)
        {
            /*string trace_xml = File.ReadAllText ("provenance.xml");
			List<SWLWorkflowInvoke<T>> action_list = FileUtil.readXML<List<SWLWorkflowInvoke<T>>> (trace_xml);
			action_list.ForEach (x => Console.WriteLine (x));*/
            return true;
        }

    }

    public class ProvenanceTracingNull : IProvenanceTracer
    {
        public void save(string port_name, string action_name) { /* DO NOTHING */ }

    }
}

	