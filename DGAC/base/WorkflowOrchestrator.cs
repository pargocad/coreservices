﻿using System.Collections.Generic;
using System.Threading;
using System;
using System.Diagnostics;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf;
using static org.hpcshelf.kinds.TaskBinding;
using org.hpcshelf.provenance;
using org.hpcshelf.DGAC;
using org.hpcshelf.ports;

namespace org.hpcshelf.kinds
{
    public interface SWLVisitor<T>
    {
        T visit(SWLWorkflowSkip<T> node);
        T visit(SWLWorkflowBreak<T> node);
        T visit(SWLWorkflowContinue<T> node);
        T visit(SWLWorkflowNewSemaphore<T> node);
        T visit(SWLWorkflowPSemaphore<T> node);
        T visit(SWLWorkflowVSemaphore<T> node);
        T visit(SWLWorkflowStart<T> node);
        T visit(SWLWorkflowWait<T> node);
        T visit(SWLWorkflowCancel<T> node);
        T visit(SWLWorkflowInvoke<T> node);
        T visit(SWLWorkflowSequence<T> node);
        T visit(SWLWorkflowParallel<T> node);
        T visit(SWLWorkflowChoice<T> node);
        T visit(SWLWorkflowIterate<T> node);
    }

    public class SWLVisitorOrchestrate : SWLVisitor<bool>
    {
        private gov.cca.Services services;
        int workflow_handle;
        private ICoreServices core_services;
        private IProvenanceTracer prov;

        public SWLVisitorOrchestrate(gov.cca.Services services, int workflow_handle, ICoreServices core_lifecycle_port, ProvenancePort core_provenance_port)
        {
            this.services = services;
            this.workflow_handle = workflow_handle;
            this.core_services = core_lifecycle_port;
            this.prov = core_provenance_port.ProvenanceLevel >= 3 ? (IProvenanceTracer)new ProvenanceTracer() : new ProvenanceTracingNull();
            core_provenance_port.ProvenanceTracer = this.prov;
        }

		public bool visit(SWLWorkflowSkip<bool> node)
        {
            Console.WriteLine("SKIP");

            // DO NOTHING;
            return true;
        }

        IProvenanceTracer ProvenanceTracing { get { return prov; } }

        public bool visit(SWLWorkflowBreak<bool> node)
        {
            Console.WriteLine("BREAK");
            throw new SWLBreakException();
        }

        public bool visit(SWLWorkflowContinue<bool> node)
        {
            Console.WriteLine("CONTINUE");
            throw new SWLContinueException();
        }

        private IDictionary<string, Semaphore> dict_semaphore = new Dictionary<string, Semaphore>();

        public bool visit(SWLWorkflowNewSemaphore<bool> node)
        {
            Console.WriteLine("NEW SEMAPHORE");
            Semaphore s = new Semaphore(node.init, int.MaxValue);
            dict_semaphore[node.sem_id] = s;
            return true;
        }

        public bool visit(SWLWorkflowPSemaphore<bool> node)
        {
            Console.WriteLine("SEMAPHORE P");
            Semaphore s = dict_semaphore[node.sem_id.Trim()];
            s.WaitOne();
            return true;
        }

        public bool visit(SWLWorkflowVSemaphore<bool> node)
        {
            Console.WriteLine("SEMAPHORE V");
            Semaphore s = dict_semaphore[node.sem_id.Trim()];
            s.Release();
            return true;
        }

        private IDictionary<string, IActionFuture> dict_async_invocations = new Dictionary<string, IActionFuture>();

        public bool visit(SWLWorkflowStart<bool> node)
        {
            string port_name = node.port_name;
            string action_name = node.action_name;
            string handle = node.handle;

            Console.WriteLine("START port_name={0} action_name={1} handle={2}", port_name, action_name, handle);

            ITaskBindingKind action_port = port(port_name);

            IActionFuture future_handle;
            action_port.invoke(action_name, () => { prov.save(port_name, action_name); }, out future_handle);

            if (handle != null)
            {
                dict_async_invocations[handle] = future_handle;
                Console.WriteLine("INSERTED {0} into dict_async_invocations -- {1}", handle, dict_async_invocations.ContainsKey(handle));
            }
            else
                Console.WriteLine("HANDLE is NULL");

            return true;
        }

        public bool visit(SWLWorkflowWait<bool> node)
        {
            Console.WriteLine("WAIT handle={0}", node.handle);

            while (!dict_async_invocations.ContainsKey(node.handle))
            {   Console.WriteLine("Waiting {0} !", node.handle);
                Thread.Sleep(1000);
            }
            
            dict_async_invocations[node.handle].wait();
            dict_async_invocations.Remove(node.handle);

            return true;
        }

        public bool visit(SWLWorkflowCancel<bool> node)
        {
            Console.WriteLine("CANCEL handle={0}", node.handle);
            //dict_async_invocations [node.handle].cancel();
            return true;
        }

        public bool visit(SWLWorkflowInvoke<bool> node)
        {
            string port_name = node.port_name;
            string action_name = node.action_name;

            Console.WriteLine("BEGIN INVOKE port_name={0} action_name={1}", port_name, action_name);

            try
            {

                switch (action_name)
                {
                    case Constants.LIFECYCLE_RESOLVE:
                        perform_resolve(port_name);
                        break;
					case Constants.LIFECYCLE_RESOLVE_SYSTEM:
						perform_resolve_system(port_name);
						break;
					case Constants.LIFECYCLE_CERTIFY:
                        try_certify(port_name);
                        break;
                    case Constants.LIFECYCLE_DEPLOY:
                        perform_deploy(port_name);
                        break;
                    case Constants.LIFECYCLE_INSTANTIATE:
                        perform_instantiate(port_name);
                        break;
                    case Constants.LIFECYCLE_RUN:
                        perform_run(port_name);
                        break;
                    case Constants.LIFECYCLE_RELEASE:
                        perform_release(port_name);
                        break;
                    default:
                        ITaskBindingKind action_port = port(port_name);
                        action_port.invoke(action_name);
                        break;
                }

                prov.save(port_name, action_name);
            }
            catch (CertificationFailedException e)
            {
				Console.WriteLine("CERTIFICATION FAILED -- port_name={0}", port_name);
            }
            finally
            {
                Console.WriteLine("END INVOKE port_name={0} action_name={1}", port_name, action_name);
            }

            return true;
        }

        private void perform_resolve(string instance_name)
        {
            Console.WriteLine("resolve {0}", instance_name);
            core_services.resolve(instance_name);
        }

		private void perform_resolve_system(string instance_name)
		{
			Console.WriteLine("resolve {0}", instance_name);
			core_services.resolve_system(instance_name);
		}
		
        private void try_certify(string instance_name)
        {
			// CERTIFICATION PROTOCOL: TODO !!!
            Console.WriteLine("certify {0}", instance_name);
			core_services.certify(instance_name);
		}

        private void perform_deploy(string instance_name)
        {
            Console.WriteLine("deploy {0}", instance_name);
            core_services.deploy(instance_name);
        }

        private void perform_instantiate(string instance_name)
        {
            Console.WriteLine("instantiate {0}", instance_name);
            core_services.instantiate(instance_name);
        }

        private void perform_run(string instance_name)
        {
           Thread t = new Thread(new ThreadStart(delegate
            {
                Console.WriteLine("run {0} !!!", instance_name);
                core_services.run(instance_name);
            }));
            t.Start();
        }

        private void perform_release(string instance_name)
        {
            Thread t = new Thread(new ThreadStart(delegate
            {
                Console.WriteLine("release {0} !!!", instance_name);
                core_services.release(instance_name);

            }));
            t.Start();
        }

        public bool visit(SWLWorkflowSequence<bool> node)
        {
            Console.WriteLine("SEQUENCE {0}", node.action_list.Length);

            bool result = true;

            foreach (SWLWorkflow<bool> action in node.action_list)
                result = result && action.accept(this);

            return true;
        }

        public bool visit(SWLWorkflowParallel<bool> node)
        {
            Console.WriteLine("PARALLEL {0}", node.action_list.Length);

            IList<Thread> t_list = new List<Thread>();

            foreach (SWLWorkflow<bool> action in node.action_list)
                t_list.Add(new Thread(new ThreadStart(delegate () { action.accept(this); })));

            foreach (Thread t in t_list)
                t.Start();

            foreach (Thread t in t_list)
                t.Join();

            return true;
        }

        public bool visit(SWLWorkflowChoice<bool> node)
        {
            IDictionary<string, SWLWorkflow<bool>> dict_action_alt = new Dictionary<string, SWLWorkflow<bool>>();

			string port_name = node.Port;
			ITaskBindingKind action_port = port(port_name);

			for (int i = 0; i < node.action_list.Length; i++)
                dict_action_alt[node.Action[i]] = node.action_list[i];

			Console.WriteLine("CHOICE {0}", dict_action_alt.Count);

            IActionFuture future_handle;
			action_port.invoke(node.Action, out future_handle);

            future_handle.wait();

            prov.save(port_name, (string)future_handle.Action);

            dict_action_alt[(string)future_handle.Action].accept(this);

            return true;
        }



        public bool visit(SWLWorkflowIterate<bool> node)
        {
            Console.WriteLine("ITERATE - BRANCH");

            bool breaking_flag = false;

			IDictionary<Tuple<string, string>, SWLWorkflow<bool>> action_to_run = new Dictionary<Tuple<string, string>, SWLWorkflow<bool>>();
			IDictionary<IActionFuture, string> future_port = new Dictionary<IActionFuture, string>();
			IActionFutureSet fs = new ActionFutureSet();
            IDictionary<string, string[]> action_name_list = new Dictionary<string, string[]>();

			foreach (KeyValuePair<string, IList<Tuple<string, SWLWorkflow<bool>>>> guards in node.Branches)
			{
				string id_port = guards.Key;
				ITaskBindingKind action_port = port(id_port);
				action_name_list[id_port] = new string[guards.Value.Count];
				for (int i = 0; i < guards.Value.Count; i++)
				{
					string action_name = guards.Value[i].Item1;
					action_name_list[id_port][i] = action_name;
					action_to_run[new Tuple<string, string>(id_port, action_name)] = guards.Value[i].Item2;
				}
				IActionFuture future_action;
				action_port.invoke((object[])action_name_list[id_port], out future_action);
				fs.addAction(future_action);
				future_port[future_action] = id_port;
 			}

            while (!breaking_flag && fs.Pending.Length > 0)
            {
                try
                {
                    IActionFuture ready_action = fs.waitAny();
					string id_port = future_port[ready_action];
					prov.save(id_port, (string)ready_action.Action);
					action_to_run[new Tuple<string, string>(id_port, (string)ready_action.Action)].accept(this);

                    // RESTART ACTIVATION
					ITaskBindingKind action_port = port(id_port);
					IActionFuture future_action;
					action_port.invoke(action_name_list[id_port], out future_action);
                    fs.addAction(future_action);
					future_port[future_action] = id_port;
				}
                catch (SWLContinueException e)
                {
                    // DO NOTHING ! Proceed to the next iteration.
                }
                catch (SWLBreakException e)
                {
                    breaking_flag = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    if (e.InnerException != null)
                    {
                        Console.WriteLine(e.InnerException.Message);
                        Console.WriteLine(e.InnerException.StackTrace);
                    }
                    throw e;
                }
            }

            return true;
        }


       // private IDictionary<string, ITaskBindingKind> dict_ports = new Dictionary<string, ITaskBindingKind>();

        private ITaskBindingKind port(string port_name)
        {
            ITaskBindingKind port_return;

            //if (!dict_ports.TryGetValue(port_name, out port_return))
            //    dict_ports[port_name] = 

            port_return = (ITaskBindingKind)services.getPort(port_name);

            Console.WriteLine("***** port ---- {0} {1}", port_name, port_return != null);

            port_return.TraceFlag = true;

            return port_return;
        }

    }

    public class SWLBreakException : Exception
    {
    }


    public class SWLContinueException : Exception
    {
    }


}
