﻿// /home/jefferson/projetos/appexample/appexample/main/TestMain.cs created with MonoDevelop
// User: jefferson at 15:03 12/5/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.unit;
using gov.cca;
using gov.cca.ports;

namespace org.hpcshelf.kinds
{


    public interface IActivateKind : IUnit, GoPort
	{	
		void main();
	}
	
	
	//[Serializable]
	public abstract class Activate : Unit, IActivateKind
	{
        public new void setServices(Services services)
        {
            base.setServices(services);
            GoPort app_port_wrapper = this;
            services.addProvidesPort(app_port_wrapper, Constants.GO_PORT_NAME, Constants.GO_PORT_TYPE, new TypeMapImpl());
        }

		public int go()
		{
			try 
			{
				this.main(); 
			}
			catch(Exception e)
			{
				Console.WriteLine("Error in execution (go): " + e.Message + " *** " + e.StackTrace);
			}
			finally
			{
				
			}
			
			return 0;
		}		
		
		public abstract void main ();

	}	
	
}
