﻿/*=============================================================
(c) all rights reserved
================================================================*/

using gov.cca;

namespace org.hpcshelf.ports
{
    public interface InitializePort : Port
    {
        void on_initialize();

     	void after_initialize();
    }

}
