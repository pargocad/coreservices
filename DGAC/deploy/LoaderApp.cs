﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;

namespace org.hpcshelf.database
{
    public class LoaderApp
    {

		public static ComponentReference.ComponentReference loadComponentReference(string contents)
		{
			// Declare an object variable of the type to be deserialized.
			ComponentReference.ComponentReference i = null;
			FileStream fs = null;
			try
			{
				// Create an instance of the XmlSerializer specifying type and namespace.
				XmlSerializer serializer = new XmlSerializer(typeof(ComponentReference.ComponentReference));

				string filename = Path.GetTempFileName();
				File.WriteAllText(filename, contents);

				// A FileStream is needed to read the XML document.
				fs = new FileStream(filename, FileMode.OpenOrCreate);

				XmlReader reader =  new XmlTextReader(fs);

				// Use the Deserialize method to restore the object's state.
				i = (ComponentReference.ComponentReference)serializer.Deserialize(reader);

			}
			catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
			}
			finally
			{
				if (fs != null)
					fs.Close();
			}

			return i;

		}


        public static ComponentType DeserializeObject(string filename)
        {
            // Declare an object variable of the type to be deserialized.
            ComponentType i = null;
            FileStream fs = null;
            try
            {
                Console.WriteLine("Reading with XmlReader " + filename);

                // Create an instance of the XmlSerializer specifying type and namespace.
                XmlSerializer serializer = new XmlSerializer(typeof(ComponentType));

                // A FileStream is needed to read the XML document.
                fs = new FileStream(filename, FileMode.Open);

                XmlReader reader = new XmlTextReader(fs);

                // Use the Deserialize method to restore the object's state.
                i = (ComponentType)serializer.Deserialize(reader);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }

            return i;

        }

		public static T deserialize<T>(string contents)
		{
			string filename = Path.GetTempFileName ();
            File.WriteAllText(filename, contents);

			// Declare an object variable of the type to be deserialized.
			T i = default(T);
			FileStream fs = null;
			try
			{
				Console.WriteLine("Reading with XmlReader " + filename);

				// Create an instance of the XmlSerializer specifying type and namespace.
				XmlSerializer serializer = new XmlSerializer(typeof(T));

				// A FileStream is needed to read the XML document.
				fs = new FileStream(filename, FileMode.Open);

				XmlReader reader = new XmlTextReader(fs);

				// Use the Deserialize method to restore the object's state.
				i = (T)serializer.Deserialize(reader);

			}
			catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
			}
			finally
			{
				if (fs != null)
					fs.Close();
			}

			return i;

		}

		public static Instantiator.InstanceType DeserializeInstantiator(string filename)
        {
            // Declare an object variable of the type to be deserialized.
            Instantiator.InstanceType i = null;
            FileStream fs = null;
            try
            {
                Console.WriteLine("Reading with XmlReader");

                // Create an instance of the XmlSerializer specifying type and namespace.
                XmlSerializer serializer = new XmlSerializer(typeof(Instantiator.InstanceType));

                // A FileStream is needed to read the XML document.
                fs = new FileStream(filename, FileMode.Open);

                XmlReader reader = new XmlTextReader(fs);

                // Use the Deserialize method to restore the object's state.
                i = (Instantiator.InstanceType)serializer.Deserialize(reader);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }

            return i;

        }

        /*       public static byte[] SerializeEnvironment(string filename, EnvironmentType env)
               {
                   XmlSerializer serializer = new XmlSerializer(typeof(EnvironmentType));

                   FileStream fs = new FileStream(filename, FileMode.Create);

                   XmlWriter writer = new XmlTextWriter(fs, null);

                   serializer.Serialize(writer, env);

                   BinaryReader br = new BinaryReader(fs);
                   int count = (int)fs.Length;
                   fs.Position = 0;

                   byte[] data = br.ReadBytes(count);
                   br.Close();
                   fs.Close();

                   return data;
               }*/

        /*      public static string SerializeCatalog(string filename, Catalog.CatalogType env)
              {
                  XmlSerializer serializer = new XmlSerializer(typeof(Catalog.CatalogType));

                  FileStream fs = new FileStream(filename, FileMode.Create);

                  XmlWriter writer = new XmlTextWriter(fs, null);

                  serializer.Serialize(writer, env);

                  fs.Close();


                  string result = System.IO.File.ReadAllText(filename);

                  return result;
              }*/

        /*       public static string SerializeDeployedComponentInfoType(string filename, DeployedComponentInfoType env)
               {
                   XmlSerializer serializer = new XmlSerializer(typeof(DeployedComponentInfoType));

                   FileStream fs = new FileStream(filename, FileMode.Create);

                   XmlWriter writer = new XmlTextWriter(fs, null);

                   serializer.Serialize(writer, env);

                   fs.Close();

                   return File.ReadAllText(filename);	

               }
       */
        /*		public static string serializeInstantiator(Instantiator.InstanceType instance)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Instantiator.InstanceType));

                    string filename = Path.GetTempFileName ();

                    FileStream fs = new FileStream(filename, FileMode.Open);

                    XmlWriter writer = new XmlTextWriter(fs, null);

                    serializer.Serialize(writer, instance);

                    fs.Close();

                    return File.ReadAllText(filename);	

                }*/

        public static string serialize<T>(T instance)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(T));

			string filename = Path.GetTempFileName ();

			FileStream fs = new FileStream(filename, FileMode.Open);

			XmlWriter writer = new XmlTextWriter(fs, null);

			serializer.Serialize(writer, instance);

			fs.Close();

			return File.ReadAllText(filename);	

		}

/*		public static byte[] serializeInstantiator(string filename, Instantiator.InstanceType inst)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Instantiator.InstanceType));

            FileStream fs = new FileStream(filename, FileMode.Create);

            XmlWriter writer = new XmlTextWriter(fs, null);

            serializer.Serialize(writer, inst);

            BinaryReader br = new BinaryReader(fs);
            int count = (int)fs.Length;
            fs.Position = 0;

            byte[] data = br.ReadBytes(count);
            br.Close();
            fs.Close();

            return data;
        }


/*		public static string serializeContractToString(Instantiator.ComponentFunctorApplicationType inst)
        {
			string filename = Path.GetTempFileName ();

			XmlSerializer serializer = new XmlSerializer(typeof(Instantiator.ComponentFunctorApplicationType));

			FileStream fs = new FileStream(filename, FileMode.OpenOrCreate);

            XmlWriter writer = new XmlTextWriter(fs, null);

            serializer.Serialize(writer, inst);

			fs.Close();     

			string result = File.ReadAllText(filename);                 

            return result;
        }
*/
/*		public static string serialize<T>(T inst)
		{
			string filename = Path.GetTempFileName ();

			XmlSerializer serializer = new XmlSerializer(typeof(T));

			FileStream fs = new FileStream(filename, FileMode.OpenOrCreate);

			XmlWriter writer = new XmlTextWriter(fs, null);

			serializer.Serialize(writer, inst);

			fs.Close();     

			string result = File.ReadAllText(filename);                 

			return result;
		}*/

		//receives id_concrete and id inner wich belongs for that inner
		//returns a impl of the inner
		//return -1 if the impl doesnt exist    

        public static AbstractComponentFunctorApplication loadACFAFromInstantiatorTrans(Instantiator.ComponentFunctorApplicationType instantiator, IDbConnection dbcon)
        {
            AbstractComponentFunctorApplication result = null;

            IDbTransaction dbtrans = dbcon.BeginTransaction();
            try
            {
                result = loadACFAFromInstantiator(instantiator, dbcon);

                dbtrans.Commit(); // if it is ok, commit ...
            }
            catch (Exception e)
			{
                dbtrans.Rollback();
				Console.Error.WriteLine(e.Message);
			}

            return result;
		}

		public static AbstractComponentFunctorApplication loadACFAFromInstantiator(Instantiator.ComponentFunctorApplicationType instantiator, IDbConnection dbcon)
		{
			AbstractComponentFunctorApplication aAppNew = null;

			AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath(instantiator.component_ref, dbcon);

			aAppNew = new AbstractComponentFunctorApplication();
			aAppNew.Id_functor_app = DBConnector.nextKey("id_functor_app", "abstractcomponentfunctorapplication", dbcon);
			aAppNew.Id_abstract = acf.Id_abstract;
            Backend.acfadao.insert(aAppNew, instantiator.GetHashCode(), dbcon);

			if (instantiator.argument != null)
				foreach (Instantiator.ContextArgumentType ctx in instantiator.argument)
				{
					AbstractComponentFunctorApplication acfa_par = loadACFAFromInstantiator((Instantiator.ComponentFunctorApplicationType)ctx.Item, dbcon);

					SupplyParameterComponent par = new SupplyParameterComponent();
					par.Id_abstract = aAppNew.Id_abstract;
					par.Id_functor_app = aAppNew.Id_functor_app;
					par.Id_parameter = ctx.parameter_id;
					par.Id_functor_app_actual = acfa_par.Id_functor_app;

                    Backend.spdao.insert(par, dbcon);
				}


			return aAppNew;
		}

		
        public static Component[] resolveImpl(AbstractComponentFunctorApplication acfaRef, IDictionary<string, int> actualParameters, IDictionary<string, int> actualParametersTop, IDbConnection dbcon)
        {
			//Console.WriteLine("RESOLVE IMPL - id_functor_app=" + acfaRef.Id_functor_app + " - id_abstract=" + acfaRef.Id_abstract);

            // get inner component application
            if (acfaRef != null)
            {
				IDictionary<string,int> actualParameters_new = new ConcurrentDictionary<string,int>();
				IDictionary<string,int> actualParameters_old = new ConcurrentDictionary<string,int>(actualParametersTop);
				foreach (KeyValuePair<string,int> pair in actualParameters)
				{
					if (!actualParameters_old.ContainsKey(pair.Key)) 
                        actualParameters_old.Add(pair);
				}

                Backend.determineArguments(actualParameters_old, acfaRef.Id_functor_app, out actualParameters_new, dbcon);
				
				foreach (KeyValuePair<string,int> pair in actualParameters_new)
				{
					acfaRef.addParameter(pair.Key, pair.Value);
				}				
            }

            // AT THIS POINT, the FUNCTOR OF THE INNER COMPONENT, WITH PARAMETERS SUPPLIED, 
            //    HAVE BEEN DISCOVERED. Now, it is necessary to apply the procedure findHashComponent
            //    descrito no artigo.				

			Console.WriteLine("resolveImpl ENTER findHashComponent ! " + acfaRef.Id_functor_app);

            //Component componentRef = Resolution.findHashComponent(actualParametersTop, acfaRef);

            var sw = new Stopwatch();
            sw.Start();
            Component[] componentRef_list = Resolution.findHashComponent3(acfaRef, dbcon);
			sw.Stop();

            if (componentRef_list.Length == 0)
                Console.WriteLine("Component not found ! id_functor_app = " + acfaRef.Id_functor_app + ", id_abstract = " + acfaRef.Id_abstract);
            else
            {
                Console.WriteLine("Elapsed time resolving {0}: {1} ms (findHashComponent)", componentRef_list[0].Library_path, sw.ElapsedMilliseconds);
                Console.WriteLine("Number of candidates: {0} ", componentRef_list.Length);
            }

            return componentRef_list;
        }//resolution


        //returns an icollection of 
        /*    public static IList<InfoCompile> getReferences_Concrete(int id_concrete)
             {
                 Component component = br.ufc.pargo.hpe.backend.DGAC.Backend.cdao.retrieve(id_concrete);
                 IList<InfoCompile> referencesSet = new List<InfoCompile>();
                 IList<Unit> unitList = br.ufc.pargo.hpe.backend.DGAC.Backend.udao.list(id_concrete);

                 foreach (org.hpcshelf.database.Unit unit in unitList)
                 {
                     Interface interfaceUnit = br.ufc.pargo.hpe.backend.DGAC.Backend.idao.retrieve(unit.Id_abstract, unit.Id_interface);

                     AbstractComponentFunctor acf = br.ufc.pargo.hpe.backend.DGAC.Backend.acfdao.retrieve(interfaceUnit.Id_abstract);

                     IList<string> referencesArrayAll_list = new List<string> ();

                     IDictionary<string, AbstractComponentFunctorApplication> pars = component.Arguments;

                     Console.WriteLine ("BEGIN TAKING REFERENCES OF " + (unit.Id_abstract) + ", " + id_concrete);
                     foreach (string reference in interfaceUnit.fetchReferences(pars))
                     {
                         Console.WriteLine(reference);
                         referencesArrayAll_list.Add(reference);
                     }
                     Console.WriteLine ("END TAKING REFERENCES OF " + (unit.Id_abstract));

                     string file_name_Interface = buildDllName(acf.Library_path, interfaceUnit.Assembly_string);
                     if (!referencesArrayAll_list.Contains(file_name_Interface))
                         referencesArrayAll_list.Add(file_name_Interface);

                  //   string file_name_Interface_base = buildDllNameBase(acf.Library_path, interfaceUnit.Assembly_string);
                 //	if (!referencesArrayAll_list.Contains(file_name_Interface_base))
                     //	referencesArrayAll_list.Add(file_name_Interface_base);

                     IList<SourceCode> scList = br.ufc.pargo.hpe.backend.DGAC.Backend.scdao.list('u', unit.Id_concrete, unit.Id_unit);  

                     InfoCompile info = new InfoCompile();

                     IList<Tuple<string,string>> sourceContents_list = new List<Tuple<string, string>> ();

                     foreach (SourceCode sc in scList)
                     {
                         if (sc.Language.Equals("C# Language")) 
                         {
                             IList<string> sourceRefs = sc.ExternalReferences;
                             foreach (string sourceRef in sourceRefs)
                                 referencesArrayAll_list.Add (sourceRef);
                             sourceContents_list.Add(new Tuple<string,string> (sc.File_name.Split ('.') [0], sc.Contents));
                         } 
                     }

                     info.sourceContents = new Tuple<string,string>[sourceContents_list.Count];
                     sourceContents_list.CopyTo (info.sourceContents, 0);

                     string[] referencesArrayAll = new string[referencesArrayAll_list.Count];
                     referencesArrayAll_list.CopyTo (referencesArrayAll, 0);

                     info.moduleName = unit.Class_name;
                     info.unitId = unit.Id_unit;
                     info.cuid = component.Hash_component_UID;
                     info.library_path = component.Library_path;
                     info.id = component.Id_concrete;
                     info.references = referencesArrayAll;

                     referencesSet.Add(info);

                 }//foreach

                 return referencesSet;
             }
             */

        /*   public static IList<InfoCompile> getReferences_Abstract(int id_abstract)
            {

                IList<InfoCompile> referencesSet = new List<InfoCompile>();
                IList<Interface> iList = Backend.idao.list(id_abstract);

                foreach (Interface i in iList)
                {				
                    IList<string> referencesArrayAll_list = new List<string> ();

                    foreach (string reference in i.References)
                    {
                        Console.WriteLine("REFERENCE: " + id_abstract + " - " + i.Class_name + " -- " + reference);
                        referencesArrayAll_list.Add (reference);
                    }

                    IList<SourceCode> scList = Backend.scdao.list('i', i.Id_abstract, i.Id_interface);

                    InfoCompile info = new InfoCompile();

                    info.sourceContents = new Tuple<string,string>[scList.Count];

                    int source_counter = 0;
                    foreach (SourceCode sc in scList)
                    {
                        IList<string> sourceRefs = sc.ExternalReferences;
                        foreach (string sourceRef in sourceRefs) referencesArrayAll_list.Add (sourceRef);
                        info.sourceContents [source_counter] = new Tuple<string, string> (sc.File_name.Split('.')[0], sc.Contents);
                        source_counter ++;
                    }

                    string[] referencesArrayAll = new string[referencesArrayAll_list.Count];
                    referencesArrayAll_list.CopyTo (referencesArrayAll, 0);

                    AbstractComponentFunctor acf1 = br.ufc.pargo.hpe.backend.DGAC.Backend.acfdao.retrieve(id_abstract);
                    info.moduleName = i.Class_name;
                    info.unitId = i.Id_interface;
                    info.cuid = acf1.Hash_component_UID;
                    info.library_path = acf1.Library_path;
                    info.id = acf1.Id_abstract;
                    info.references = referencesArrayAll;

                    referencesSet.Add(info);


                }//foreach

                return referencesSet;
            }
            */

        // Provides the DLL name from the assembly string but without the extension.
        // Ther assembly string contains a set of fields, separated by commas. The first fielt contains the name of the
        // assembly.
        public static string buildDllName(string library_path, string assemblyString)
        {
            return library_path + Path.DirectorySeparatorChar + (assemblyString.Split(',')[0]) /* + ".dll" */;

        } // get references

        public static string buildDllNameBase(string library_path, string assemblyString)
        {
            int dllpos = assemblyString.IndexOf(",");
            string s = assemblyString.Substring(0, dllpos);
            int i = s.LastIndexOf(".");
            i = i >= 0 ? i : 0;
            s = s.Insert(i+1, "Base");            
            
            return library_path + Path.DirectorySeparatorChar  + s + ".dll" ;

        } // get references

        public static string buildDllName(string assemblyString)
        {
            return assemblyString.Split(',')[0] /* + ".dll" */;

        } // get references




        public struct InfoCompile
        {
			public string moduleName;
            public string[] references;
            public string unitId;
			public Tuple<string,string>[] sourceContents;
            public string cuid;
            public string library_path;
            public int id;
            public byte[] strong_key_file_contents;
        }

        public static Tuple<DeployArguments.DeployArgumentsType, IDictionary<string, byte[]>> buildDeployInfoAbstract(int id_abstract, ISet<int> facet, IDbConnection dbcon)
        { 
            IDictionary<string, byte[]> key_file = new ConcurrentDictionary<string, byte[]>();
            IList<DeployArguments.DeployArgumentsUnitType> deploy_arguments_list = new List<DeployArguments.DeployArgumentsUnitType>();

            //IList<InfoCompile> referencesSet = new List<InfoCompile>();
            IList<Interface> iList = Backend.idao.list(id_abstract, dbcon);

            foreach (Interface i in iList)
                if (facet == null || facet.Contains(i.Facet))
                {
                    IList<SourceCode> scList = Backend.scdao.list('i', i.Id_abstract, i.Id_interface, dbcon);
                    if (scList.Count == 0)
                        continue;

                    IList<string> referencesArrayAll_list = new List<string>();

                    foreach (string reference in i.GetReferences(dbcon))
                    {
                      //  Console.WriteLine("REFERENCE: " + id_abstract + " - " + i.Class_name + " -- " + reference);
                        referencesArrayAll_list.Add(reference);
                    }


                    //InfoCompile info = new InfoCompile();
                    DeployArguments.DeployArgumentsUnitType deploy_unit_arguments = new DeployArguments.DeployArgumentsUnitType();

                    IList<DeployArguments.SourceContentsFile> source_contents_list = new List<DeployArguments.SourceContentsFile>();

					// Obs: module_name => deploy_type
					string deploy_type = scList[0].DeployType;
					string module_name = scList[0].Module_name;

                    int m = 0;
                    while (m < scList.Count)
                    {
						SourceCode sc = scList[m];

						while (sc.Module_name == module_name)
                        {
							IList<string> sourceRefs = sc.GetExternalReferences(dbcon);
                            foreach (string sourceRef in sourceRefs) referencesArrayAll_list.Add(sourceRef);

                            DeployArguments.SourceContentsFile source_contents_list_item = new DeployArguments.SourceContentsFile();
                            source_contents_list_item.fileName = sc.File_name /*.Split('.')[0]*/;
                            source_contents_list_item.contents = sc.Contents;
                            source_contents_list_item.fileType = sc.File_type; // TODO
                            source_contents_list.Add(source_contents_list_item);

                            m++;
							if (m < scList.Count)
								sc = scList[m];
							else
								break;
						}

                        if (m <= scList.Count && source_contents_list.Count > 0)
                        {
                            string[] referencesArrayAll = new string[referencesArrayAll_list.Count];
                            referencesArrayAll_list.CopyTo(referencesArrayAll, 0);

                            AbstractComponentFunctor acf1 = Backend.acfdao.retrieve(id_abstract, dbcon);
                            deploy_unit_arguments.moduleName = module_name;//i.Class_name;
                            deploy_unit_arguments.library_path = acf1.Library_path;
                            deploy_unit_arguments.references = referencesArrayAll;
                            deploy_unit_arguments.deploy_type = deploy_type;

                            // A KEY FILE NAME MUST EXIST IN THE CORE.
                            key_file[deploy_unit_arguments.moduleName] = CommandLineUtil.get_strong_key_file(deploy_unit_arguments.library_path, dbcon);

                            //referencesSet.Add(info);

                            deploy_unit_arguments.sourceContents = new DeployArguments.SourceContentsFile[source_contents_list.Count];
                            source_contents_list.CopyTo(deploy_unit_arguments.sourceContents, 0);
                            source_contents_list.Clear();

                            deploy_arguments_list.Add(deploy_unit_arguments);

                            module_name = sc.Module_name;
							deploy_type = sc.DeployType;
						}
                    }

				}//foreach


            if (key_file.Count == 0)
                return null;
            else
            {
                DeployArguments.DeployArgumentsType deploy_arguments = new DeployArguments.DeployArgumentsType();

                deploy_arguments.unit = new DeployArguments.DeployArgumentsUnitType[deploy_arguments_list.Count];
                deploy_arguments_list.CopyTo(deploy_arguments.unit, 0);

                return new Tuple<DeployArguments.DeployArgumentsType, IDictionary<string, byte[]>>(deploy_arguments, key_file);
            }
        }

		public static Tuple<DeployArguments.DeployArgumentsType, IDictionary<string, byte[]>> buildDeployInfoConcrete (int id_concrete, ISet<int> facet, IDictionary<string, AbstractComponentFunctorApplication> component_contract, IDbConnection dbcon)
		{
			IDictionary<string, byte[]> key_file = new ConcurrentDictionary<string, byte[]>();
			IList<DeployArguments.DeployArgumentsUnitType> deploy_arguments_list = new List<DeployArguments.DeployArgumentsUnitType>();

            Component component = Backend.cdao.retrieve(id_concrete, dbcon);
            component.SetComponentContract(component_contract[component.Library_path], dbcon);

			IList<InfoCompile> referencesSet = new List<InfoCompile>();
			IList<Unit> unitList = Backend.udao.list(id_concrete, dbcon);

            foreach (Unit unit in unitList)
            {
				Interface interfaceUnit = Backend.idao.retrieve(unit.Id_abstract, unit.Id_interface, dbcon);

                if (facet == null || facet.Contains(interfaceUnit.Facet))
                {
                    AbstractComponentFunctor acf = Backend.acfdao.retrieve(interfaceUnit.Id_abstract, dbcon);

                    IList<string> referencesArrayAll_list = new List<string>();

                    IDictionary<string, AbstractComponentFunctorApplication> pars = component.GetArguments0(dbcon);

                    Console.WriteLine("BEGIN TAKING REFERENCES OF " + (unit.Id_abstract) + ", " + id_concrete);
                    foreach (string reference in interfaceUnit.fetchReferences(pars, dbcon))
                    {
                        Console.WriteLine(reference);
                        referencesArrayAll_list.Add(reference);
                    }
                    Console.WriteLine("END TAKING REFERENCES OF " + (unit.Id_abstract));

                    string file_name_Interface = buildDllName(acf.Library_path, interfaceUnit.Assembly_string);
                    if (!referencesArrayAll_list.Contains(file_name_Interface))
                        referencesArrayAll_list.Add(file_name_Interface);

                    IList<SourceCode> scList = Backend.scdao.list('u', unit.Id_concrete, unit.Id_unit, dbcon);

                    // InfoCompile info = new InfoCompile();

                    IList<DeployArguments.SourceContentsFile> source_contents_list = new List<DeployArguments.SourceContentsFile>();

                    // Obs: module_name => deploy_type
                    string module_name = scList[0].Module_name;
                    string deploy_type = scList[0].DeployType;

                    int m = 0;

                    while (m < scList.Count)
                    {
                        SourceCode sc = scList[m];

                        while (sc.Module_name == module_name)
                        {
                            if (!sc.DeployType.Equals("platform.settings"))
                            {
                                IList<string> sourceRefs = sc.GetExternalReferences(dbcon);
                                foreach (string sourceRef in sourceRefs)
                                    referencesArrayAll_list.Add(sourceRef);

                                DeployArguments.SourceContentsFile source_contents_list_item = new DeployArguments.SourceContentsFile();
                                source_contents_list_item.fileName = sc.File_name/*.Split('.')[0]*/;
                                source_contents_list_item.contents = sc.Contents;
                                source_contents_list_item.fileType = sc.File_type; // TODO
                                source_contents_list.Add(source_contents_list_item);
                            }
                            m++;
                            if (m < scList.Count)
                                sc = scList[m];
                            else
                                break;
						}

                        if (m <= scList.Count && source_contents_list.Count > 0)
                        {
                            string[] referencesArrayAll = new string[referencesArrayAll_list.Count];
                            referencesArrayAll_list.CopyTo(referencesArrayAll, 0);

							DeployArguments.DeployArgumentsUnitType deploy_unit_arguments = new DeployArguments.DeployArgumentsUnitType();
							deploy_unit_arguments.moduleName = module_name; //unit.Class_name;
                            deploy_unit_arguments.library_path = component.Library_path;
                            deploy_unit_arguments.references = referencesArrayAll;
                            deploy_unit_arguments.deploy_type = deploy_type;

                            // A KEY FILE NAME MUST EXIST IN THE CORE.
                            key_file[deploy_unit_arguments.moduleName] = CommandLineUtil.get_strong_key_file(deploy_unit_arguments.library_path, dbcon);

                            deploy_unit_arguments.sourceContents = new DeployArguments.SourceContentsFile[source_contents_list.Count];
                            source_contents_list.CopyTo(deploy_unit_arguments.sourceContents, 0);
                            source_contents_list.Clear();

                            deploy_arguments_list.Add(deploy_unit_arguments);

							module_name = sc.Module_name;
                            deploy_type = sc.DeployType;
						}
                    }

                }//foreach
            }

			DeployArguments.DeployArgumentsType deploy_arguments = new DeployArguments.DeployArgumentsType();

			deploy_arguments.unit = new DeployArguments.DeployArgumentsUnitType[deploy_arguments_list.Count];
			deploy_arguments_list.CopyTo(deploy_arguments.unit, 0);

			return new Tuple<DeployArguments.DeployArgumentsType, IDictionary<string, byte[]>>(deploy_arguments, key_file);
		}


	}//class

}//namespace
