﻿using System;
using System.Collections.Generic;

using System.Text;
using org.hpcshelf.database;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using org.hpcshelf.DGAC.utils;
using System.Data;

namespace HPE_DGAC_LoadDB
{
    class LoaderAbstractComponent : LoaderComponent
    {
        /* The parameter fileName points to the location of a concrete component.
         */

        private ComponentType xc;

        public bool componentExists(string library_path, out HashComponent cRef, IDbConnection dbcon)
        {
            AbstractComponentFunctor absC = org.hpcshelf.DGAC.Backend.acfdao.retrieve_libraryPath(library_path, dbcon);
            if (absC == null)
            {
                cRef = null;
                return false;
            }
            else
            {
                cRef = absC;
                return true;
            }

        }

        public new HashComponent loadComponent(ComponentType c, byte[] snk_contents, ref IList<ExternalLibraryType> externalLibrary, IDbConnection dbcon)
        {
            this.xc = c;
            IDictionary<string, SupplyParameter> var_context = new Dictionary<string, SupplyParameter>();
            AbstractComponentFunctor absC = (AbstractComponentFunctor)base.loadComponent(c, snk_contents, ref externalLibrary, var_context, dbcon);
            loadInnerComponents(absC, var_context, dbcon);
            loadInterfaces(absC, dbcon);
            return absC;
        }


        private IList<ParameterRenaming> parameterRenamingSuper = null;

        protected override HashComponent loadComponent_(ComponentType c, byte[] snk_contents, IDictionary<string, SupplyParameter> var_context, IDbConnection dbcon)
        {
            // CREATE Component

            AbstractComponentFunctor c_ = new AbstractComponentFunctor();
            c_.Id_abstract = DBConnector.nextKey("id_abstract", "abstractcomponentfunctor", dbcon);
            c_.Hash_component_UID = c.header.hash_component_UID;
            c_.Library_path = c.header.packagePath + "." + c.header.name;
            c_.Kind = c.header.kind.ToString();

            if (c.header.baseType != null)
            {
                ExtensionTypeType extType = c.header.baseType.extensionType;
                ComponentInUseType baseC = null;

                if (extType.ItemElementName == ItemChoiceType.extends && extType.Item)
                {
                    baseC = c.header.baseType.component;

                    parameterRenamingSuper = baseC.parameter;

                    AbstractComponentFunctorApplication baseCapp = newAbstractComponentFunctorApplication(baseC, var_context, dbcon);

                    // FOLLOW arrow subtype
                    if (baseCapp == null)
                        throw new Exception("DEPLOY ERROR: Unresolved Dependency for base component (extends) : " + baseC.name);

                    c_.Id_functor_app_supertype = baseCapp.Id_functor_app;
                }

                org.hpcshelf.DGAC.Backend.acfdao.insert(c_, dbcon);

                // LOAD EXPOSED INNER COMPONENTS OF THE BASE
                if (baseC.port != null)
                {
                    foreach (InnerComponentType port in baseC.port)
                    {
                        innerAll.Add(port);
                        innerAllByCRef[port.localRef] = port;

                        InnerComponent iNewPort = new InnerComponent();
                        iNewPort.Id_abstract_owner = c_.Id_abstract;
                        string old_port_localRef = port.localRef;
                        port.localRef = lookForRenamingNew(baseC.localRef, port.localRef, port.index_replica);
                        iNewPort.Id_inner = port.localRef;
                        iNewPort.Parameter_top = port.parameter_id;
                        iNewPort.Transitive = true;
                        iNewPort.IsPublic = port.exposed;
                        iNewPort.Multiple = port.multipleSpecified ? port.multiple : false;

                        AbstractComponentFunctorApplication appPort = newAbstractComponentFunctorApplication(port, var_context, dbcon);
                        if (appPort == null)
                        {
                            throw new Exception("DEPLOY ERROR: Unresolved Dependency for base component (public inner component) : " + port.name);
                        }

                        iNewPort.Id_functor_app = appPort.Id_functor_app;
                        iNewPort.Id_abstract_inner = appPort.Id_abstract;

                        if (org.hpcshelf.DGAC.Backend.icdao.retrieve_exact(iNewPort.Id_abstract_owner, iNewPort.Id_inner, dbcon) == null)
                            org.hpcshelf.DGAC.Backend.icdao.insert(iNewPort, dbcon);
                    }
                }

            }
            else
                org.hpcshelf.DGAC.Backend.acfdao.insert(c_, dbcon);

            loadAbstractComponentFunctorParameters(c_, var_context, dbcon);

            insertComponentKey('a', c_.Id_abstract, snk_contents, new byte[0], dbcon);

            return c_;
        }


        private string mkSliceRef(string cRef, string uRef)
        {
            return cRef + "." + uRef;
        }


        private void loadInnerComponents(AbstractComponentFunctor absC, IDictionary<string, SupplyParameter> var_context, IDbConnection dbcon)
        {
            IList<InnerComponentType> includeAsInner = new List<InnerComponentType>();

            IDictionary<string, InnerComponentType> innersByVarName = new Dictionary<string, InnerComponentType>();

            if (parameter != null)
                foreach (ParameterType ir in parameter)
                {
                    InnerComponentType ic = this.lookForInnerComponent(ir.componentRef);
                    if (!innersByVarName.ContainsKey(ir.varName))
                        innersByVarName[ir.varName] = ic;
                    else
                        Console.WriteLine("ALREADY EXISTS - key=" + ir.varName + ", value=" + ic.localRef);
                }

            if (parameterSupply != null)
                foreach (ParameterSupplyType ir in parameterSupply)
                {
                    InnerComponentType ic = this.lookForInnerComponent(ir.cRef);
                    if (!innersByVarName.ContainsKey(ir.varName))
                        innersByVarName[ir.varName] = ic;
                    else
                        Console.WriteLine("ALREADY EXISTS - key=" + ir.varName + ", value=" + ic.localRef);
                }

            //importInnerComponentsOfSuper(absC, includeAsInner);

            if (inner != null)
            {
                foreach (InnerComponentType c in inner)
                {
                    //Console.WriteLine("BEGIN loadInnerComponent : " + c.localRef + " :: " + c.package + "." + c.name);

                    // innerAll.Add(c);
                    if (((isNotInSupply(c) || this.findInSlices(c.localRef)) && (isNotParameter(c) || this.findInSlices(c.localRef))) || includeAsInner.Contains(c))
                    {
                        // CREATE INNER COMPONENT
                        InnerComponent iNew = new InnerComponent();
                        iNew.Id_abstract_owner = absC.Id_abstract;
                        iNew.Id_inner = c.localRef;       // localRef is used to uniquely identify the inner component among the inner components....

                        // FOLLOW arrow has-parameters

                       // if (c.localRef.Equals("node"))
                       //    Console.Write(true);

                        AbstractComponentFunctorApplication app = newAbstractComponentFunctorApplication(c, var_context, dbcon);
                        if (app == null)
                        {
                            throw new Exception("DEPLOY ERROR: Unresolved Dependency for base component (inner component) : " + c.name);
                        }

                        iNew.Id_functor_app = app.Id_functor_app;
                        iNew.Id_abstract_inner = app.Id_abstract;

                        // CHECKS IF IT IS A TOP PARAMETER 
                        ParameterType p = lookForParameterByCRef(c.localRef);
                        iNew.Parameter_top = p != null ? p.formFieldId : null;

                        iNew.Transitive = false;
                        iNew.IsPublic = c.exposed;
                        iNew.Multiple = c.multipleSpecified ? c.multiple : false;

                        // LOAD EXPOSED INNER COMPONENTS
                        if (c.port != null)
                        {
                            foreach (InnerComponentType port in c.port)
                            {
                                Console.WriteLine("loadInnerComponent - BEGIN PORT " + port.localRef);
                                // --------------------------------------------------
                                string varName = null;
                                int id_abstract_port = app.Id_abstract;
                                string id_inner_port = port.localRef;

                                InnerComponent ic_port = org.hpcshelf.DGAC.Backend.icdao.retrieve(id_abstract_port, id_inner_port, dbcon);

                                Console.WriteLine("loadInnerComponent - STEP 1");

                                if (c.parameter != null && ic_port != null)
                                {
                                    foreach (ParameterRenaming par in c.parameter)
                                    {
                                        Console.WriteLine("loadInnerComponent - STEP 2 begin" + par.formFieldId + " - " + par.varName);
                                        if (par.formFieldId.Equals(ic_port.Parameter_top))
                                        {
                                            varName = par.varName;
                                        }
                                        Console.WriteLine("loadInnerComponent - STEP 2 end " + par.formFieldId + " - " + par.varName);
                                    }
                                }

                                InnerComponentType port_replace = port;
                                if (varName != null)
                                {
                                    Console.WriteLine("loadInnerComponent - STEP 3 " + varName);
                                    foreach (KeyValuePair<string, InnerComponentType> iii in innersByVarName)
                                    {
                                        Console.WriteLine("loadInnerComponent x " + iii.Key);
                                        Console.WriteLine("loadInnerComponent y " + (iii.Value == null));
                                        Console.WriteLine("loadInnerComponent z " + c.package);
                                        Console.WriteLine("loadInnerComponent w " + c.name);
                                    }
                                    port_replace = innersByVarName[varName];
                                }
                                else
                                    Console.WriteLine("loadInnerComponent - STEP 3 ");


                                // --------------------------------------------------						

                                if (port.localRef.Equals("output_data"))
                                    Console.WriteLine();

                                string old_port_localRef = port.localRef;
                                port.localRef = lookForRenamingNew(c.localRef, port.localRef, port.index_replica);

                                innerAll.Add(port);
                                innerAllByCRef[port.localRef] = port;

                                InnerComponent iNewPort = new InnerComponent();
                                iNewPort.Id_abstract_owner = absC.Id_abstract;
                                if (old_port_localRef == port.localRef && absC.Id_functor_app_supertype > 0)
                                {
                                    AbstractComponentFunctorApplication acfa_super = org.hpcshelf.DGAC.Backend.acfadao.retrieve(absC.Id_functor_app_supertype, dbcon);
                                    InnerComponentExposed ice_super = org.hpcshelf.DGAC.Backend.icedao.retrieve3(acfa_super.Id_abstract, c.localRef, port.localRef, dbcon);
                                    if (ice_super != null)
                                        port.localRef = ice_super.Id_inner_rename;
                                }

                                iNewPort.Id_inner = port.localRef;
                                iNewPort.Parameter_top = port_replace.parameter_id;
                                iNewPort.Transitive = true;
                                iNewPort.IsPublic = port.exposed;
                                iNewPort.Multiple = port.multipleSpecified ? port.multiple : false;

                                AbstractComponentFunctorApplication appPort = newAbstractComponentFunctorApplication(port_replace, var_context, dbcon);
                                if (appPort == null)
                                {
                                    throw new Exception("DEPLOY ERROR: Unresolved Dependency for base component (public inner component) : " + port.name);
                                }

                                iNewPort.Id_functor_app = appPort.Id_functor_app;
                                iNewPort.Id_abstract_inner = appPort.Id_abstract;

                                InnerComponentExposed ice = new InnerComponentExposed();
                                ice.Id_abstract = absC.Id_abstract;
                                ice.Id_inner_rename = iNewPort.Id_inner;
                                ice.Id_inner_owner = iNew.Id_inner;
                                ice.Id_inner = old_port_localRef;

                               // if (ice.Id_inner_rename.Equals("certification_binding") && ice.Id_abstract == 148)
                               //     Console.WriteLine(true);

                                org.hpcshelf.DGAC.Backend.icedao.insert(ice, dbcon);

                                InnerComponent ic_port_exists = org.hpcshelf.DGAC.Backend.icdao.retrieve_exact(iNewPort.Id_abstract_owner, iNewPort.Id_inner, dbcon);

                                if (ic_port_exists == null && (fusion == null || !fusion.ContainsKey(port.localRef) || (fusion.ContainsKey(port.localRef) && fusion[port.localRef].Equals(c.localRef))))
                                    org.hpcshelf.DGAC.Backend.icdao.insert(iNewPort, dbcon);

                                Console.WriteLine("loadInnerComponent - END PORT " + port.localRef);
                            }
                        }


                        InnerComponent ic = org.hpcshelf.DGAC.Backend.icdao.retrieve(iNew.Id_abstract_owner, iNew.Id_inner, dbcon);

                        if (ic != null && includeAsInner.Contains(c))
                        {
                            Console.WriteLine("loadInnerComponent - BLOCK 2 OPEN");
                            org.hpcshelf.DGAC.Backend.icdao.remove(iNew.Id_abstract_owner, iNew.Id_inner, dbcon);
                            org.hpcshelf.DGAC.Backend.icdao.insert(iNew, dbcon);
                            Console.WriteLine("loadInnerComponent - BLOCK 2 CLOSE");
                        }
                        else if (ic == null)
                        {
                            Console.WriteLine("loadInnerComponent - BLOCK 3 OPEN");
                            org.hpcshelf.DGAC.Backend.icdao.insert(iNew, dbcon);
                            Console.WriteLine("loadInnerComponent - BLOCK 3 CLOSE");
                        }
                    }

                    //Console.WriteLine("END loadInnerComponent : " + c.localRef + " :: " + c.package + "." + c.name);
                }
            }
        }

        private bool findInSlices(string cref)
        {
            foreach (UnitType u in unit)
            {
                if (u.slices != null)
                {
                    foreach (UnitSliceType s in u.slices)
                    {
                        if (s.cRef.Equals(cref))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool isNotParameter(InnerComponentType c)
        {
            return this.parameter == null || !parameterByCRef.ContainsKey(c.localRef);
        }

        private void importInnerComponentsOfSuper(AbstractComponentFunctor absC, IList<InnerComponentType> includeAsInner, IDbConnection dbcon)
        {
            Console.WriteLine("importInnerComponentsOfSupper : " + "START");
            IDictionary<string, SupplyParameter> parsSuper = null;  // new Dictionary<string, SupplyParameter>();

            // Inner components of the supertype.
            if (absC.Id_functor_app_supertype > 0)
            {
                AbstractComponentFunctorApplication acfa = org.hpcshelf.DGAC.Backend.acfadao.retrieve(absC.Id_functor_app_supertype, dbcon);

                // It is a parameter in the subtype. Check if it is supplied in the type.
                parsSuper = org.hpcshelf.DGAC.Backend.spdao.listBy(acfa.Id_functor_app, dbcon);
                //foreach (SupplyParameter sp in spList)
                //{
                //    Console.WriteLine("Adding to parSuper - key=" + sp.Id_parameter + ", value: " + sp.Id_functor_app + "/" + sp.Id_abstract + "/" + sp.Id_parameter);
                //    parsSuper.Add(sp.Id_parameter, sp);
                //}

                IList<InnerComponent> iss = org.hpcshelf.DGAC.Backend.icdao.list(acfa.Id_abstract, dbcon);
                Console.WriteLine("importInnerComponentsOfSuper: " + iss.Count + " - acfa.Id_abstract=" + acfa.Id_abstract);
                foreach (InnerComponent i in iss) //if (!i.IsPublic)
                {
                    Console.WriteLine("[importInnerComponentsOfSupper 1: " + i.Id_inner + " , " + i.Id_functor_app + " , " + (i.Parameter_top));

                    InnerComponent iNew = new InnerComponent();
                    if (i.Parameter_top != null && i.Parameter_top != "")
                    {
                        SupplyParameter sp = null;
                        parsSuper.TryGetValue(i.Parameter_top, out sp);
                        Console.WriteLine("importInnerComponentsOfSupper 2: " + i.Parameter_top + "," + i.Id_inner);

                        if (sp is SupplyParameterComponent)
                        {
                            Console.WriteLine("importInnerComponentsOfSupper 3: " + "sp is SupplyParameterComponent");

                            // 1th CASE: It is not a parameter in the current component.
                            // NOT YET TESTED !!!
                            SupplyParameterComponent spc = (SupplyParameterComponent)sp;

                            AbstractComponentFunctorApplication acfaReplace = org.hpcshelf.DGAC.Backend.acfadao.retrieve(spc.Id_functor_app_actual, dbcon);
                            if (org.hpcshelf.DGAC.Backend.icdao.retrieve(absC.Id_abstract, i.Id_inner, dbcon) == null)
                            {
                                iNew.Id_abstract_inner = acfaReplace.Id_abstract;
                                iNew.Parameter_top = null;
                                iNew.Id_abstract_owner = absC.Id_abstract;
                                iNew.Id_functor_app = liftFunctorApp(acfaReplace.Id_functor_app, parsSuper, dbcon);
                                iNew.Id_inner = i.Id_inner;
                                org.hpcshelf.DGAC.Backend.icdao.insert(iNew, dbcon);
                            }
                        }
                        else if (sp is SupplyParameterParameter)
                        {
                            Console.WriteLine("importInnerComponentsOfSupper 4: " + "sp is SupplyParameterParameter");
                            // 2nd CASE: It continues to be a parameter in the current component.
                            SupplyParameterParameter spp = (SupplyParameterParameter)sp;

                            String varName = null;
                            foreach (ParameterRenaming pr in parameterRenamingSuper)
                            {
                                if (pr.formFieldId.Equals(i.Parameter_top))
                                {
                                    varName = pr.varName;
                                    ParameterType parameter = this.lookForParameterByVarName(varName);
                                    InnerComponentType cReplace = lookForInnerComponent(parameter.componentRef);
                                    //cReplace.localRef = i.Id_inner;
                                    cReplace.exposed = i.IsPublic;
                                    cReplace.multiple = i.Multiple;
                                    includeAsInner.Add(cReplace);
                                }
                            }

                            ParameterSupplyType supply = lookForSupplyForVarName(varName);
                            if (supply != null)
                            {
                                InnerComponentType cReplace = lookForInnerComponent(supply.cRef);
                                if (cReplace != null)
                                    includeAsInner.Add(cReplace);
                            }
                        }

                    }
                    else
                    {
                        // 3rd CASE: 
                        // NOT YET TESTED !!! 
                        iNew.Id_abstract_inner = i.Id_abstract_inner;
                        iNew.Parameter_top = null;
                        iNew.Id_abstract_owner = absC.Id_abstract;
                        iNew.Id_functor_app = liftFunctorApp(i.Id_functor_app, parsSuper, dbcon);
                        iNew.Id_inner = i.Id_inner;
                        iNew.IsPublic = i.IsPublic;
                        org.hpcshelf.DGAC.Backend.icdao.insert(iNew, dbcon);
                    }
                }
            }
            Console.WriteLine("importInnerComponentsOfSuper : FINISH");
        }



        // NOT YET TESTED
        private int liftFunctorApp(int id_functor_app, IDictionary<string, SupplyParameter> parsSuper, IDbConnection dbcon)
        {
            AbstractComponentFunctorApplication acfa = org.hpcshelf.DGAC.Backend.acfadao.retrieve(id_functor_app, dbcon);

            AbstractComponentFunctorApplication acfaNew = new AbstractComponentFunctorApplication();
            acfaNew.Id_abstract = acfa.Id_abstract;
            org.hpcshelf.DGAC.Backend.acfadao.insert(acfaNew, dbcon);

            ICollection<SupplyParameter> supplyList = org.hpcshelf.DGAC.Backend.spdao.list(id_functor_app, dbcon);
            foreach (SupplyParameter sp in supplyList)
            {
                SupplyParameter spNew = null;
                if (sp is SupplyParameterComponent)
                {
                    SupplyParameterComponent spc = (SupplyParameterComponent)sp;
                    SupplyParameterComponent spcNew = new SupplyParameterComponent();
                    spNew = spcNew;
                    spcNew.Id_functor_app = acfaNew.Id_functor_app;
                    spcNew.Id_functor_app_actual = liftFunctorApp(spc.Id_functor_app_actual, parsSuper, dbcon);
                    spcNew.Id_abstract = spc.Id_abstract;
                    spcNew.Id_parameter = spc.Id_parameter;
                }
                else if (sp is SupplyParameterParameter)
                {
                    SupplyParameterParameter spp = (SupplyParameterParameter)sp;
                    SupplyParameter spSuper = null;

                    if (parsSuper.Count > 0)
                    {
                        foreach (KeyValuePair<string, SupplyParameter> p in parsSuper)
                        {
                            Console.WriteLine("Key=" + p.Key + ", Value=" + p.Value.Id_abstract);
                        }
                    }
                    else Console.WriteLine("parSupers EMPTY !!!!!!");

                    if (parsSuper.TryGetValue(spp.Id_argument, out spSuper))
                    {
                        if (spSuper is SupplyParameterComponent)
                        {
                            SupplyParameterComponent spcSuper = (SupplyParameterComponent)spSuper;
                            SupplyParameterComponent spcNew = new SupplyParameterComponent();
                            spNew = spcNew;
                            Console.WriteLine("liftFunctorApp: 1" + (spNew == null));

                            AbstractComponentFunctorApplication acfa_spcSuper = org.hpcshelf.DGAC.Backend.acfadao.retrieve(spcSuper.Id_functor_app_actual, dbcon);

                            spcNew.Id_functor_app = acfaNew.Id_functor_app;
                            spcNew.Id_abstract = acfa_spcSuper.Id_abstract;
                            spcNew.Id_parameter = spp.Id_parameter;
                            spcNew.Id_functor_app_actual = spcSuper.Id_functor_app_actual;
                            Console.WriteLine("liftFunctorApp: 2" + (spcNew == null));
                        }
                        else if (spSuper is SupplyParameterParameter)
                        {
                            SupplyParameterParameter sppSuper = (SupplyParameterParameter)spSuper;
                            SupplyParameterParameter sppNew = new SupplyParameterParameter();
                            spNew = sppNew;
                            Console.WriteLine("liftFunctorApp: 3 " + (spNew == null) + " spp.Id_argument=" + spp.Id_argument + ", spp.Id_parameter=" + spp.Id_parameter);

                            sppNew.Id_functor_app = acfaNew.Id_functor_app;
                            sppNew.Id_abstract = spSuper.Id_abstract;
                            sppNew.Id_parameter = spp.Id_parameter;
                            sppNew.Id_argument = sppSuper.Id_argument;
                            sppNew.FreeVariable = spp.FreeVariable;
                            Console.WriteLine("liftFunctorApp: 4" + (sppNew == null));
                        }
                        else
                        {
                            Console.WriteLine("liftFunctorApp: 5 " + (spp.Id_argument));
                        }
                    }
                    else
                    {
                        Console.WriteLine("liftFunctorApp: 5 ");
                        SupplyParameterParameter sppSuper = (SupplyParameterParameter)spSuper;
                        SupplyParameterParameter sppNew = new SupplyParameterParameter();
                        spNew = sppNew;

                        sppNew.Id_argument = spp.Id_argument;
                        sppNew.FreeVariable = spp.FreeVariable;
                        sppNew.Id_functor_app = acfaNew.Id_functor_app;
                        sppNew.Id_abstract = acfaNew.Id_abstract;
                        sppNew.Id_parameter = spp.Id_parameter;
                        //sppNew.Id_parameter_actual = sppSuper.Id_parameter_actual;
                        //sppNew.FreeVariable = spp.FreeVariable;
                    }
                }
                org.hpcshelf.DGAC.Backend.spdao.insert(spNew, dbcon);
            }
            return acfaNew.Id_functor_app;
        }

        private ParameterType lookForParameterByParameterId(string parId)
        {
            if (parameter != null && parameterByParId.ContainsKey(parId))
                return parameterByParId[parId];
            else
                return null;
        }



        private string lookForRenamingNew(string cRef, string pRef, int index_replica)
        {
            if (innerRenaming != null)
            {
                foreach (InnerRenamingType ir in innerRenaming)
                {
                    if (ir.cRef.Equals(cRef) && ir.cOldName.Equals(pRef) && ir.index_replica == index_replica)
                    {
                        return ir.cNewName;
                    }
                }
            }
            return pRef;
        }

        private string lookForRenamingOld(string cRef, string pRef)
        {
            if (innerRenaming != null)
            {
                foreach (InnerRenamingType ir in innerRenaming)
                {
                    if (ir.cRef.Equals(cRef) && ir.cNewName.Equals(pRef))
                    {
                        return ir.cOldName;
                    }
                }
            }
            return pRef;
        }

        protected IList<AbstractComponentFunctorParameter> loadAbstractComponentFunctorParameters(AbstractComponentFunctor c_, IDictionary<string, SupplyParameter> var_context, IDbConnection dbcon)
        {
            IList<AbstractComponentFunctorParameter> pars = new List<AbstractComponentFunctorParameter>();
            if (parameter != null)
                foreach (ParameterType parameter_ in parameter)
                {
                    Console.WriteLine("loadAbstractComponentFunctorParameters : PARAMETER " + parameter_.componentRef + ":" + parameter_.formFieldId);
                    AbstractComponentFunctorParameter p = new AbstractComponentFunctorParameter();

                    p.Id_abstract = c_.Id_abstract;
                    p.Id_parameter = parameter_.formFieldId;

                    ComponentInUseType cBound = lookForInnerComponent(parameter_.componentRef);
                    AbstractComponentFunctorApplication cApp = newAbstractComponentFunctorApplication(cBound, var_context, dbcon);
                    if (cApp == null)
                    {
                        throw new Exception("DEPLOY ERROR: Unresolved Dependency for base component (context parameter bound) : " + cBound.name);
                    }

                    p.Bounds_of = cApp.Id_functor_app;
                    p.Variance = parameter_.variance;
                    p.Order = parameter_.order;
                    p.ParameterClass = parameter_.@class;

                    if (org.hpcshelf.DGAC.Backend.acfpdao.retrieve(p.Id_abstract, p.Id_parameter, dbcon) == null)
                        org.hpcshelf.DGAC.Backend.acfpdao.insert(p, dbcon);

                    pars.Add(p);
                }
            Console.WriteLine("loadAbstractComponentFunctorParameters : END");
            return pars;
        }

        private void loadInterfaces(AbstractComponentFunctor absC, IDbConnection dbcon)
        {
            if (unit != null)
            {
                int count = 0;
                // for each interface ...
                foreach (UnitType u in unit)
                {
                    string uRef = u.uRef;
                    string iRef = u.iRef;

                    string uRefSuper = u.super == null || u.super.Length == 0 ? "" : u.super[0].uRef;
                    if (u.super != null)
                        for (int j = 1; j < u.super.Length; j++)
                            uRefSuper += "+" + u.super[j].uRef;

                    InterfaceType ui = lookForInterface(iRef);
                    int nargs = ui.nArgsSpecified ? ui.nArgs : 0;

                    Console.Error.WriteLine("STEP 5.3");

                    Interface i = new Interface();
                    i.Id_abstract = absC.Id_abstract;
                    i.Id_interface = uRef;
                    //					i.Unit_replica = unit_replica;
                    i.Facet = u.facet;
                    i.Id_interface_super = uRefSuper;
                    i.Class_name = xc.header.packagePath + "." + xc.header.name + "." + iRef;
                    i.Class_nargs = nargs; // TODO
                    i.Assembly_string = i.Class_name + ", Culture=neutral, Version=0.0.0.0, PublicKey=" + absC.Hash_component_UID /* CommandLineUtil.getPublicKey(absC.Library_path)*/; // In the current implementation, the name of the dll is the name of the class of the unit.
                    i.Order = ++count;
                    i.Is_parallel = u.multiple;

                    Console.Error.WriteLine("STEP 5.4");

                    if (ui.parameter != null)
                    {
                        Console.Error.WriteLine("STEP 5.4 - " + ui.parameter);

                        foreach (InterfaceParameterType ipx in ui.parameter)
                        {
                            Console.Error.WriteLine("STEP 5.4 BEGIN 1- " + ipx.parid);
                            InterfaceParameter ip = new InterfaceParameter();
                            Console.Error.WriteLine("STEP 5.4 BEGIN 2- " + ipx.parid);
                            ip.Id_abstract = i.Id_abstract;
                            ip.Id_interface = i.Id_interface;
                            ip.ParId = ipx.parid;
                            ip.VarId = ipx.varid;
                            ip.Id_interface_parameter = ipx.iname;
                            ip.Id_unit_parameter = ipx.uname;
                            ip.ParOrder = ipx.order;
                            org.hpcshelf.DGAC.Backend.ipdao.insert(ip, dbcon);
                            Console.Error.WriteLine("STEP 5.4 END - " + ipx.parid);
                        }
                    }

                    Console.Error.WriteLine("BEFORE STEP 5.5 " + (ui == null));
                    //Console.Error.WriteLine("STEP 5.5 --- " + (ui.sources[ui.sources.Length - 1].file == null));
                    int order = 0;
                    if (ui.sources != null)
                    {
                        foreach (SourceType st in ui.sources)
                            foreach (SourceFileType sft in st.file)
                            {
                                Console.Error.WriteLine("STEP 5.5.0");
                                SourceCode ss = new SourceCode();
                                ss.Type_owner = 'i';
                                ss.Id_owner_container = i.Id_abstract;
                                ss.Id_owner = i.Id_interface;
                                ss.Contents = sft.contents;
                                ss.File_name = sft.name;
                                ss.File_type = sft.srcType;
                                ss.Order = order++;
                                ss.DeployType = st.deployType;
                                ss.Language = st.sourceType;
                                ss.Module_name = st.moduleName != null ? st.moduleName : i.Class_name;

                                Console.Error.WriteLine("STEP 5.5.1");

                                org.hpcshelf.DGAC.Backend.scdao.insert(ss, dbcon);
                                int size = (sft.externalDependency == null ? 0 : sft.externalDependency.Length) +
                                           (ui.externalReferences == null ? 0 : ui.externalReferences.Length);

                                Console.Error.WriteLine("STEP 5.5.2");
                                if (size > 0)
                                {
                                    string[] allRefs = new string[size];
                                    if (ui.externalReferences != null)
                                        ui.externalReferences.CopyTo(allRefs, 0);

                                    if (sft.externalDependency != null)
                                        sft.externalDependency.CopyTo(allRefs, ui.externalReferences == null ? 0 : ui.externalReferences.Length);

                                    Console.Error.WriteLine("STEP 5.5.3");
                                    foreach (string extRef in allRefs)
                                    {
                                        Console.WriteLine("external reference = " + extRef);
                                        SourceCodeReference ssr = new SourceCodeReference();
                                        ssr.Type_owner = ss.Type_owner;
                                        ssr.Id_owner_container = ss.Id_owner_container;
                                        ssr.Id_owner = ss.Id_owner;
                                        ssr.File_name = ss.File_name;
                                        ssr.Reference = extRef;
                                        if (org.hpcshelf.DGAC.Backend.scrdao.retrieve(ssr, dbcon) == null)
                                        {
                                            org.hpcshelf.DGAC.Backend.scrdao.insert(ssr, dbcon);
                                        }
                                    }
                                    Console.Error.WriteLine("STEP 5.5.4");
                                }
                            }
                    }
                    else
                        Console.WriteLine("NO SOURCES for {0}", ui.iRef);

                    Console.Error.WriteLine("STEP 5.6");
                    org.hpcshelf.DGAC.Backend.idao.insert(i, dbcon);
                    if (u.slices != null)
                    {
                        IList<String> mP = new List<string>();
                        IDictionary<string, UnitSliceType> m = new Dictionary<string, UnitSliceType>();
                        IDictionary<string, UnitSliceType> m_ = new Dictionary<string, UnitSliceType>();


                        // 1st PASS: COLLECT ALL MAPPINGS SLICE/EXPOSED SLICES
                        foreach (UnitSliceType uS in u.slices)
                        {
                            string sliceName = uS.sliceName;
                            if (uS.port != null)
                                foreach (string portName in uS.port)
                                    mP.Add(getSliceName(portName));
                        }

                        // 2nd PASS: 
                        foreach (UnitSliceType uS in u.slices)
                        {
                            string sliceName = uS.sliceName;
                            string sliceUnit = uS.uRef;
                            if (mP.Contains(sliceName) && !m.ContainsKey(sliceName + "." + sliceUnit))
                            {
                                m[sliceName + "." + sliceUnit] = uS;
                                m_[sliceName] = uS;
                            }
                        }


                        Console.Error.WriteLine("STEP 5.7 ---");
                        // 3rd PASS:
                        foreach (UnitSliceType uS in u.slices)
                        {
                            string sname = uS.sliceName;
                            string cRefS = uS.cRef;
                            string uRefS = uS.uRef;

                            //InnerComponentType innerC = lookForInnerComponent(cRefS);
                            Console.Error.WriteLine("STEP 5.8");

                            InnerComponent inner = org.hpcshelf.DGAC.Backend.icdao.retrieve(absC.Id_abstract, cRefS, dbcon);
                            Console.Error.WriteLine("STEP 5.9 ---" + absC.Id_abstract + "," + cRefS);

                            Interface iii = org.hpcshelf.DGAC.Backend.idao.retrieveTop(inner.Id_abstract_inner, uRefS, dbcon);
                            Console.Error.WriteLine("STEP 5.9.5 ---" + (iii == null));

                            Slice s = new Slice();
                            s.Id_abstract = absC.Id_abstract;
                            s.Id_inner = cRefS; //innerC.localRef;
                            s.Id_interface_slice = iii == null ? uRefS : iii.Id_interface;
                            s.Id_interface = uRef;
                            s.Transitive = mP.Contains(sname);
                            Console.Error.WriteLine("STEP 5.10");

                            string property_name = uS.sliceName;
                            string fstletter = property_name.Substring(0, 1);
                            property_name = fstletter.ToUpper() + property_name.Substring(1, property_name.Length - 1);

                            Console.Error.WriteLine("STEP 5.11");

                            s.PortName = property_name;

                           // if (s.Id_abstract == 148 && uS.cRef == "shuffler")
                           //     Console.Write(true);

                            if (!s.Transitive && uS.port != null)
                            {
                                Console.Error.WriteLine("STEP 5.12");
                                foreach (string pname in uS.port)
                                {
                                    Console.Error.WriteLine("STEP 5.12.1 -- " + pname + ", " + (m.Count));

                                    UnitSliceType usPort = null;
                                    m.TryGetValue(pname, out usPort);
                                    if (usPort == null)
                                        m_.TryGetValue(pname, out usPort);
                                    Console.Error.WriteLine("STEP 5.12.2 -- " + pname + ", " + (usPort == null));

                                    Console.Error.WriteLine("STEP 5.12.5 -- " + usPort.cRef);

                                    if (usPort.cRef.Equals("output"))
                                        Console.WriteLine();

                                    InnerComponentType innerCPort = lookForInnerComponent(usPort.cRef);

                                    Console.Error.WriteLine("STEP 5.13");

                                    InnerComponent inner2 = org.hpcshelf.DGAC.Backend.icdao.retrieve(absC.Id_abstract, usPort.cRef, dbcon);
                                    Interface iii2 = org.hpcshelf.DGAC.Backend.idao.retrieveTop(inner2.Id_abstract_inner, usPort.uRef, dbcon);

                                    SliceExposed se = new SliceExposed();
                                    se.Id_abstract = s.Id_abstract;
                                    se.Id_inner = innerCPort.localRef;
                                    se.Id_inner_owner = s.Id_inner;
                                    se.Id_interface_slice_owner = s.GetId_interface_slice_top(dbcon); // mudado de s.Id_interface_slice em 28/06/2011
                                    se.Id_interface_slice = iii2 == null ? usPort.uRef : iii2.Id_interface;
                                    se.Id_interface_host = u.uRef;
                                    // achar innerRenaming para cNewName = usPort.cRef e cRef = cRefS (uS.cRef) -- Id_inner_original = cOldName
                                    string id_inner_original = lookForRenamingOld(cRefS, usPort.cRef);
                                    se.Id_inner_original = id_inner_original != null ? id_inner_original : usPort.cRef;
                                    se.Id_interface_slice_original = usPort.uRef; // DEVE SER O TOP !!!

                                    if (org.hpcshelf.DGAC.Backend.sedao.retrieve2(se.Id_inner_original,
                                                                                              se.Id_interface_slice,
                                                                                              se.Id_abstract,
                                                                                              se.Id_interface_slice_owner,
                                                                                              se.Id_inner_owner,
                                                                                              se.Id_inner,
                                                                                              se.Id_interface_host, dbcon) == null)
                                        org.hpcshelf.DGAC.Backend.sedao.insert(se, dbcon);
                                }
                            }
                            if (org.hpcshelf.DGAC.Backend.sdao.retrieve(s.Id_abstract, s.Id_inner, s.Id_interface_slice, s.Id_interface, dbcon) == null)
                            {
                                org.hpcshelf.DGAC.Backend.sdao.insert(s, dbcon);
                                Console.WriteLine("SLICE REGISTERED " + s.Id_abstract + "/" + s.Id_inner + "/" + s.Id_interface_slice + " --- " + s.Id_interface);
                            }
                            else
                                Console.WriteLine("SLICE NOT REGISTERED " + s.Id_abstract + "/" + s.Id_inner + "/" + s.Id_interface_slice + " --- " + s.Id_interface);
                        }

                    }

                    //if (ui.protocol != null)
                    readProtocol(i, ui, dbcon);
                }
            }
        }

        private static string getSliceName(string slice_name)
        {
            return slice_name.Split(new char[] { '.' })[0];
        }

        private void readProtocol(Interface i, InterfaceType ui, IDbConnection dbcon)
        {
            //ProtocolChoiceType main_protocol = ui.protocol;			
            UnitActionType[] actions = ui.action;
            UnitConditionType[] conditions = ui.condition;

            //readProtocol(i, "main", main_protocol);

            if (actions != null)
                foreach (UnitActionType action in actions)
                {
                    readProtocol(i, action.id, action.protocol, dbcon);
                }

            if (conditions != null)
                foreach (UnitConditionType condition in conditions)
                {
                    readCondition(i, condition, dbcon);
                }

        }

        private void readProtocol(Interface i, string action_id, ProtocolChoiceType protocol, IDbConnection dbcon)
        {
            InterfaceAction action_row = new InterfaceAction();
            action_row.Id_abstract = i.Id_abstract;
            action_row.Id_interface = i.Id_interface;
            //			action_row.PartitionIndex = i.Unit_replica;
            action_row.Id_action = action_id;
            action_row.IsCondition = false;
            action_row.Protocol = "";
            org.hpcshelf.DGAC.Backend.iadao.insert(action_row, dbcon);
        }

        private void readCondition(Interface i, UnitConditionType condition, IDbConnection dbcon)
        {
            InterfaceAction action_row = new InterfaceAction();
            action_row.Id_abstract = i.Id_abstract;
            action_row.Id_interface = i.Id_interface;
            //			action_row.PartitionIndex = i.Unit_replica;
            action_row.Id_action = condition.id;
            action_row.IsCondition = true;
            action_row.Protocol = "";
            org.hpcshelf.DGAC.Backend.iadao.insert(action_row, dbcon);
        }

        internal void updateSources(ComponentType ct, AbstractComponentFunctor c, IDbConnection dbcon)
        {
            LoadBodyItems(ct.componentInfo);

            IDictionary<string, Unit> units = new Dictionary<string, Unit>();

            int id_abstract = c.Id_abstract;

            // for each unit ...
            foreach (UnitType u in unit)
            {
                string uref = u.uRef;
                string iRef = u.iRef;

                Interface i = org.hpcshelf.DGAC.Backend.idao.retrieve(id_abstract, uref, dbcon);
                InterfaceType ui = lookForInterface(iRef);

                if (ui.sources != null)
                    foreach (SourceType st in ui.sources)
                    {
                        foreach (SourceFileType sft in st.file)
                        {
                            SourceCode ss = new SourceCode();
                            ss.Type_owner = 'i';
                            ss.Id_owner_container = c.Id_abstract;
                            ss.Id_owner = i.Id_interface;
                            ss.Contents = sft.contents;
                            ss.File_type = sft.srcType;
                            ss.File_name = sft.name;
                            ss.Module_name = st.moduleName != null ? st.moduleName : i.Class_name;
                            ss.Language = st.sourceType;
                            ss.DeployType = st.deployType;
                            org.hpcshelf.DGAC.Backend.scdao.update(ss, dbcon);

                            if (sft.externalDependency != null)
                            {
                                foreach (string extRef in sft.externalDependency)
                                {
                                    SourceCodeReference ssr = new SourceCodeReference();
                                    ssr.Type_owner = ss.Type_owner;
                                    ssr.Id_owner_container = ss.Id_owner_container;
                                    ssr.Id_owner = ss.Id_owner;
                                    ssr.File_name = ss.File_name;
                                    ssr.Reference = extRef;
                                    if (org.hpcshelf.DGAC.Backend.scrdao.retrieve(ssr, dbcon) != null)
                                    {
                                        // DGAC.BackEnd.scrdao.update(ssr);
                                    }
                                    else
                                    {
                                        org.hpcshelf.DGAC.Backend.scrdao.insert(ssr, dbcon);
                                    }
                                }
                            }
                        }
                    }
            }
        }
    }
}
