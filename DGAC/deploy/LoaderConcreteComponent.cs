﻿using System;
using System.Collections.Generic;
using System.Data;
using org.hpcshelf.database;
using org.hpcshelf.DGAC.utils;

namespace HPE_DGAC_LoadDB
{
    class LoaderConcreteComponent : LoaderComponent
    {

       Component cConc = null;

       ComponentType xc;

       public bool componentExists(string library_path, out IList<Component> cRef, IDbConnection dbcon)
       {   
           IList<Component> concC = org.hpcshelf.DGAC.Backend.cdao.retrieve_libraryPathList(library_path, dbcon);
            if (concC == null || concC.Count == 0)
            {
                cRef = null;
                return false;
            }
           else
           {
               cRef = concC;
               return true;
           }

       }

       
       
       /* The parameter fileName points to the location of a concrete component.
        */
		public new HashComponent loadComponent(ComponentType c, byte[] snk_contents, ref IList<ExternalLibraryType> externalLibrary, IDictionary<string, SupplyParameter> var_context, IDbConnection dbcon)
        {
            this.xc = c;
            cConc = (Component) base.loadComponent(c, snk_contents, ref externalLibrary, var_context, dbcon);
            return cConc;
        }

       private AbstractComponentFunctor lookForAbstractComponentFunctorOfConcreteComponent(Component cConc, IDbConnection dbcon)
       {
           AbstractComponentFunctorApplication acfa = org.hpcshelf.DGAC.Backend.acfadao.retrieve(cConc.Id_functor_app, dbcon);
           
           AbstractComponentFunctor acf = org.hpcshelf.DGAC.Backend.acfdao.retrieve(acfa.Id_abstract, dbcon);
           return acf;
       }

        protected override HashComponent loadComponent_(ComponentType c, byte[] snk_contents, IDictionary<string, SupplyParameter> var_context, IDbConnection dbcon)
        {
            // CREATE Component

            ComponentInUseType baseC = null;
            
            baseC = c.header.baseType.component;
            
            // FOLLOW arrow implements
            IList<AbstractComponentFunctorApplication> cAppList = newAbstractComponentFunctorApplicationForImplements(baseC, var_context, dbcon);
            if (cAppList == null || cAppList.Count == 0)
            {
                throw new Exception("DEPLOY ERROR: Unresolved Dependency for base component (implements) : " + c.header.baseType.component.name);
            }
			
			Component c_first = null;
			foreach (AbstractComponentFunctorApplication cApp in cAppList)
			{   
	            Component c_ = new Component();
				if (c_first==null) c_first = c_;
	            c_.Id_concrete = DBConnector.nextKey("id_concrete", "component", dbcon);
	            c_.Hash_component_UID = c.header.hash_component_UID;
	            c_.Library_path = c.header.packagePath + "." + c.header.name; 
	            c_.Id_functor_app = cApp.Id_functor_app;      
				c_.SetId_abstract(cApp.Id_abstract);
                c_.ContextArgumentCalculator = argument_calculator;
					
	            org.hpcshelf.DGAC.Backend.cdao.insert(c_, dbcon);

                insertComponentKey('c', c_.Id_concrete, snk_contents, new byte[0], dbcon);
				
	            loadUnits(c_, dbcon);
			}

            return c_first;
        }

       private Component lookForHashComponent(string hash_component_UID, IDbConnection dbcon)
       {
           Component c = org.hpcshelf.DGAC.Backend.cdao.retrieve_uid(hash_component_UID, dbcon);
           return c;
       }



       private IDictionary<string, Unit> loadUnits(Component c, IDbConnection dbcon)
        {
            IDictionary<string, Unit> units = new Dictionary<string, Unit>();
			
			AbstractComponentFunctorApplication absCapp_ = org.hpcshelf.DGAC.Backend.acfadao.retrieve(c.Id_functor_app, dbcon);
            AbstractComponentFunctorApplication absCapp = null;
			while (absCapp_ != null) {
			   absCapp = absCapp_;	
			   absCapp_ = org.hpcshelf.DGAC.Backend.acfadao.retrieve_next(absCapp_.Id_functor_app, dbcon);
			}
			
            int id_abstract = absCapp.Id_abstract;

            // for each unit ...
            foreach (UnitType u in unit)
            {
                string uref = u.uRef;
                string iRef = u.iRef;
 				int partition_index = u.replicaSpecified ? u.replica : 0;

				string uRefSuper = u.super.Length == 0 ? "" : u.super[0].uRef;
				for (int j=1; j<u.super.Length;j++) 
					uRefSuper += "+" + u.super[j].uRef;				

                Interface i = org.hpcshelf.DGAC.Backend.idao.retrieve(id_abstract, uref, dbcon);

                InterfaceType ui = lookForInterface(iRef);

                Unit uu = new Unit();
                uu.Id_concrete = c.Id_concrete;
                uu.Id_unit = uref;
                uu.Id_abstract = id_abstract;
                uu.Id_interface = uref;
                uu.Id_unit_super = uRefSuper;
              //  uu.Unit_replica = partition_index;
                uu.Class_name = xc.header.packagePath + "." + xc.header.name + "." + iRef;
                uu.Class_nargs = i.Class_nargs;
                uu.Assembly_string = uu.Class_name + ", Culture=neutral, Version=0.0.0.0, PublicKey=" + c.Hash_component_UID /*CommandLineUtil.getPublicKey(c.Library_path)*/; // In the current implementation, the name of the dll is the name of the class of the unit.
                uu.Order = i.Order;

                units.Add(uu.Id_unit, uu);
				
				//Console.WriteLine("ui.sources={0}",ui.sources);
				//Console.WriteLine("ui.protocol={0}",ui.protocol);
				//Console.WriteLine("{0}={1}", c.Kind, Constants.KIND_COMPUTATION_NAME);
				
				if (ui.sources == null && (c.GetKind(dbcon).Equals(Constants.KIND_COMPUTATION_NAME) || c.GetKind(dbcon).Equals(Constants.KIND_SYNCHRONIZER_NAME)))
				{
					Console.WriteLine("ENTER WRAPPER GENERATOR " + c.Library_path);
					
				}
				else if (ui.sources != null)
				{
                    foreach (SourceType st in ui.sources)
                    {
                        if (st.sourceType.Equals("C# Language") || st.sourceType.Equals("C#"))
                            loadSource_CSharp(uu, ui, st, dbcon);
                        else if (st.sourceType.Equals("C"))
                            loadSource_C(uu, ui, st, dbcon);
                        else
                            loadSource_CSharp(uu, ui, st, dbcon); // throw new Exception("Unknown source language or type.");
                    }
				} 
				else 
				{
					throw new Exception("Unit " + uu.Id_unit + " neither has a source code nor is a connector.");
				}

				UnitDAO udao = new UnitDAO();
                udao.insert(uu, dbcon);
            }

            return units;
        }

        private void loadSource_C(Unit uu, InterfaceType ui, SourceType st, IDbConnection dbcon)
        {
            int order = 0;

            foreach (SourceFileType sft in st.file)
            {
				SourceCode ss = new SourceCode();
				ss.Type_owner = 'u';
				ss.Id_owner_container = uu.Id_concrete;
				ss.Id_owner = uu.Id_unit;
				ss.Contents = sft.contents;
                ss.File_type = sft.srcType; // header or source.
				ss.File_name = sft.name;
				ss.Order = order++;
                ss.Module_name = st.moduleName;
                ss.Language = st.sourceType;
                ss.DeployType = st.deployType;

				org.hpcshelf.DGAC.Backend.scdao.insert(ss,dbcon);
				//Console.WriteLine("loadUnits - C - 2");

				int size = (sft.externalDependency == null ? 0 : sft.externalDependency.Length) +
						   (ui.externalReferences == null ? 0 : ui.externalReferences.Length);

				//Console.WriteLine("loadUnits - C - 3");

				if (size > 0)
				{
				//	Console.WriteLine("loadUnits - C - 4");
					string[] allRefs = new string[size];
					if (ui.externalReferences != null)
						ui.externalReferences.CopyTo(allRefs, 0);

					if (sft.externalDependency != null)
						sft.externalDependency.CopyTo(allRefs, ui.externalReferences == null ? 0 : ui.externalReferences.Length);

				//	Console.WriteLine("loadUnits - C - 5");

					foreach (string extRef in allRefs)
					{
				//		Console.WriteLine("loadUnits - C - 6 - " + extRef);
						SourceCodeReference ssr = new SourceCodeReference();
						ssr.Type_owner = ss.Type_owner;
						ssr.Id_owner_container = ss.Id_owner_container;
						ssr.Id_owner = ss.Id_owner;
						ssr.File_name = ss.File_name;
						ssr.Reference = extRef;
						if (org.hpcshelf.DGAC.Backend.scrdao.retrieve(ssr, dbcon) == null)
						{
							org.hpcshelf.DGAC.Backend.scrdao.insert(ssr, dbcon);
						}
				//		Console.WriteLine("loadUnits - 7 - C - " + extRef);
					}
				}
			}
        }

        private void loadSource_CSharp(Unit uu, InterfaceType ui, SourceType st, IDbConnection dbcon)
        {
            int order = 0;

            foreach (SourceFileType sft in st.file /* ui.sources[ui.sources.Length - 1].file*/)
            {
              //  Console.WriteLine("loadUnits - 1 " + sft.name);

                SourceCode ss = new SourceCode();
                ss.Type_owner = 'u';
                ss.Id_owner_container = uu.Id_concrete;
                ss.Id_owner = uu.Id_unit;
                ss.Contents = sft.contents;
                ss.File_type = sft.srcType;
                ss.File_name = sft.name;
                ss.Order = order++;
                ss.Module_name = st.moduleName != null ? st.moduleName : uu.Class_name;
                ss.Language = st.sourceType;
                ss.DeployType = st.deployType;
                org.hpcshelf.DGAC.Backend.scdao.insert(ss, dbcon);
            //    Console.WriteLine("loadUnits - 2");

                int size = (sft.externalDependency == null ? 0 : sft.externalDependency.Length) +
                           (ui.externalReferences == null ? 0 : ui.externalReferences.Length);

              //  Console.WriteLine("loadUnits - 3");

                if (size > 0)
                {
                //    Console.WriteLine("loadUnits - 4");
                    string[] allRefs = new string[size];
                    if (ui.externalReferences != null)
                        ui.externalReferences.CopyTo(allRefs, 0);

                    if (sft.externalDependency != null)
                        sft.externalDependency.CopyTo(allRefs, ui.externalReferences == null ? 0 : ui.externalReferences.Length);

                  //  Console.WriteLine("loadUnits - 5");

                    foreach (string extRef in allRefs)
                    {
                    //    Console.WriteLine("loadUnits - 6 - " + extRef);
                        SourceCodeReference ssr = new SourceCodeReference();
                        ssr.Type_owner = ss.Type_owner;
                        ssr.Id_owner_container = ss.Id_owner_container;
                        ssr.Id_owner = ss.Id_owner;
                        ssr.File_name = ss.File_name;
                        ssr.Reference = extRef;
                        if (org.hpcshelf.DGAC.Backend.scrdao.retrieve(ssr, dbcon) == null)
                        {
                            org.hpcshelf.DGAC.Backend.scrdao.insert(ssr, dbcon);
                        }
                      //  Console.WriteLine("loadUnits - 7 - " + extRef);
                    }
                }
            }
        }

        internal void updateSources(ComponentType ct, IList<Component> cList, IDbConnection dbcon)
        {
            LoadBodyItems(ct.componentInfo);

            IDictionary<string, Unit> units = new Dictionary<string, Unit>();

            foreach (Component c in cList)
            {
                AbstractComponentFunctorApplication absCapp = org.hpcshelf.DGAC.Backend.acfadao.retrieve(c.Id_functor_app, dbcon);

                int id_abstract = absCapp.Id_abstract;

                // for each unit ...
                foreach (UnitType u in unit)
                {
                    string uref = u.uRef;
                    string iRef = u.iRef;
                    //string urefSuper = u.super == null ? null : u.super.uRef;
                    int partition_index = u.replicaSpecified ? u.replica : 0;

                    Unit uu = org.hpcshelf.DGAC.Backend.udao.retrieve(c.Id_concrete, uref, dbcon);

                    InterfaceType ui = lookForInterface(iRef);

                    if (ui.sources != null)
                    {
                        foreach (SourceType st in ui.sources)
                        {
                            //if (st.sourceType.Equals("C# Language") || st.sourceType.Equals("C#"))
                            updateSource_CSharp(uu, ui, st, dbcon);
                            //else if (st.sourceType.Equals("C"))
                            //	updateSource_C(uu, ui, st);
                            //else
                            //	throw new Exception("Unknown source language or type.");
                        }
                    }
                    else
                        Console.WriteLine("NO SOURCES for {0}...", ui.iRef);
                }
            }
        }

        private void updateSource_C(Unit uu, InterfaceType ui, SourceType st, IDbConnection dbcon)
        {
			foreach (SourceFileType sft in st.file /*ui.sources[ui.sources.Length - 1].file*/)
			{
				SourceCode ss = new SourceCode();
				ss.Type_owner = 'u';
				ss.Id_owner_container = uu.Id_concrete;
                ss.Id_owner = uu.Id_unit;
				ss.Contents = sft.contents;
                ss.File_type = sft.srcType;
				ss.File_name = sft.name;
                ss.Module_name = st.moduleName;
                ss.Language = st.sourceType;
                ss.DeployType = st.deployType;
				if (org.hpcshelf.DGAC.Backend.scdao.retrieve(ss.Type_owner, ss.Id_owner, ss.File_name, ss.Id_owner_container, dbcon) != null)
					org.hpcshelf.DGAC.Backend.scdao.update(ss, dbcon);
				else
					org.hpcshelf.DGAC.Backend.scdao.insert(ss, dbcon);

				if (sft.externalDependency != null)
				{
					foreach (string extRef in sft.externalDependency)
					{
						SourceCodeReference ssr = new SourceCodeReference();
						ssr.Type_owner = ss.Type_owner;
						ssr.Id_owner_container = ss.Id_owner_container;
						ssr.Id_owner = ss.Id_owner;
						ssr.File_name = ss.File_name;
						ssr.Reference = extRef;
						if (org.hpcshelf.DGAC.Backend.scrdao.retrieve(ssr, dbcon) != null)
						{
							// DGAC.BackEnd.scrdao.update(ssr);
						}
						else
						{
							org.hpcshelf.DGAC.Backend.scrdao.insert(ssr, dbcon);
						}
					}
				}
			}
		}

        private void updateSource_CSharp(Unit uu, InterfaceType ui, SourceType st, IDbConnection dbcon)
        {
            foreach (SourceFileType sft in st.file /*ui.sources[ui.sources.Length - 1].file*/)
			{
				SourceCode ss = new SourceCode();
				ss.Type_owner = 'u';
				ss.Id_owner_container = uu.Id_concrete;
                ss.Id_owner = uu.Id_unit;
				ss.Contents = sft.contents;
				ss.File_type = sft.srcType;
				ss.File_name = sft.name;
				ss.Module_name = st.moduleName != null ? st.moduleName : uu.Class_name;
				ss.Language = st.sourceType;
                ss.DeployType = st.deployType;
				if (org.hpcshelf.DGAC.Backend.scdao.retrieve(ss.Type_owner, ss.Id_owner, ss.File_name, ss.Id_owner_container, dbcon) != null)
					org.hpcshelf.DGAC.Backend.scdao.update(ss, dbcon);
				else
					org.hpcshelf.DGAC.Backend.scdao.insert(ss, dbcon);

				if (sft.externalDependency != null)
				{
					foreach (string extRef in sft.externalDependency)
					{
						SourceCodeReference ssr = new SourceCodeReference();
						ssr.Type_owner = ss.Type_owner;
						ssr.Id_owner_container = ss.Id_owner_container;
						ssr.Id_owner = ss.Id_owner;
						ssr.File_name = ss.File_name;
						ssr.Reference = extRef;
						if (org.hpcshelf.DGAC.Backend.scrdao.retrieve(ssr, dbcon) != null)
						{
							// DGAC.BackEnd.scrdao.update(ssr);
						}
						else
						{
							org.hpcshelf.DGAC.Backend.scrdao.insert(ssr, dbcon);
						}
					}
				}
			}
		}
    }
}
