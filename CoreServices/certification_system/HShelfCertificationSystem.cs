﻿using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;
using gov.cca;
using gov.cca.ports;

namespace org.hpcshelf.certify
{
    public class HShelfCertificationSystem
	{
		protected class HShelfFramework : AbstractFramework
        {
			private WorkerObject baseFramework = null;

			private CoreServices core_services;
			//private IPlatformServices platform_services;

			private string certified_id = null;
            private int id_concrete_certifiable = -1;
            readonly Backend.ResolveComponentHierarchy system_component;

            public HShelfFramework (Backend.ResolveComponentHierarchy system_component, string certified_id, int id_concrete_certifiable)
			{
                this.system_component = system_component;

                //this.platform_services = platform_services;
                this.certified_id = certified_id;
                this.id_concrete_certifiable = id_concrete_certifiable;
				init();
			}

			private void init()
			{
                string AppName = system_component.ID + "." + certified_id;

                core_services = new CoreServices(AppName, "127.0.0.1", 8077);

                // TODO: update the certification system after separating Core as a web services.
			//	core_services.openWorkflowSession(system_component, certified_id, id_concrete_certifiable);

				core_services.resolve (Constants.PLATFORM_SAFE_ID);
				core_services.resolve (Constants.CORE_CERTIFICATION_CONTROLLER_COMPONENT_ID);
				core_services.resolve (Constants.CERTIFICATION_WORKFLOW_COMPONENT_ID);

                core_services.deploy (Constants.PLATFORM_SAFE_ID);
				core_services.deploy (Constants.CORE_CERTIFICATION_CONTROLLER_COMPONENT_ID);
                core_services.deploy (Constants.CERTIFICATION_WORKFLOW_COMPONENT_ID);

				core_services.instantiate (Constants.PLATFORM_SAFE_ID);
				core_services.instantiate (Constants.CORE_CERTIFICATION_CONTROLLER_COMPONENT_ID);
				core_services.instantiate (Constants.CERTIFICATION_WORKFLOW_COMPONENT_ID);

				baseFramework = (WorkerObject)ManagerObject.SingleManagerObject.WorkerFramework;

                Services appServices = baseFramework.getServices(AppName + ".shelf_framework", "HShelfFramework", baseFramework.createTypeMap());

				appServices.registerUsesPort(Constants.BUILDER_SERVICE_PORT_NAME, Constants.BUILDER_SERVICE_PORT_TYPE, new TypeMapImpl());
				ComponentID host_cid = appServices.getComponentID();
				BuilderService bsPort = (BuilderService)appServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);

                ComponentID workflow_cid = bsPort.getComponentID(AppName + "." + Constants.CERTIFICATION_WORKFLOW_COMPONENT_ID);
				ComponentID core_cid = core_services.Services.getComponentID();
				bsPort.connect(workflow_cid, Constants.CORE_PROVENANCE_PORT_NAME, core_cid, Constants.CORE_PROVENANCE_PORT_NAME);
				bsPort.connect(workflow_cid, Constants.CORE_LIFECYCLE_PORT_NAME, core_cid, Constants.CORE_LIFECYCLE_PORT_NAME);
            }

			public TypeMap createTypeMap()
			{
				return baseFramework.createTypeMap ();
			}

			public Services getServices(string selfInstanceName, string selfClassName, TypeMap selfProperties)
            {
                // application_unit = baseFramework.UnitOf[this.core_services.AppName + "." +  Constants.CORE_CERTIFICATION_CONTROLLER_COMPONENT_ID];

                return core_services.Services;
			}


			public void releaseServices(Services services)
			{
				baseFramework.releaseServices (services);
			}

			public void shutdownFramework()
			{
				baseFramework.shutdownFramework ();
			}

			public AbstractFramework createEmptyFramework()
			{
                return new HShelfFramework(system_component, "framework", id_concrete_certifiable);
			}
		}

	}
}

