// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.17020
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

// 
//This source code was auto-generated by MonoXSD
//
namespace ComponentXML {
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.example.org/SAFe_driver")]
    [System.Xml.Serialization.XmlRootAttribute("component", Namespace="http://www.example.org/SAFe_driver", IsNullable=false)]
    public partial class IDriverComponent {
        
        private IDriverContextArgument[] context_argumentField;
        
        private IDriverPlacement[] placementField;
        
        private IDriverPort[] portField;
        
        private string nameField;
        
        private IDriverComponentKind kindField;
        
        private bool kindFieldSpecified;
        
        private string library_pathField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("context_argument")]
        public IDriverContextArgument[] context_argument {
            get {
                return this.context_argumentField;
            }
            set {
                this.context_argumentField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("placement")]
        public IDriverPlacement[] placement {
            get {
                return this.placementField;
            }
            set {
                this.placementField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("port")]
        public IDriverPort[] port {
            get {
                return this.portField;
            }
            set {
                this.portField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public IDriverComponentKind kind {
            get {
                return this.kindField;
            }
            set {
                this.kindField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool kindSpecified {
            get {
                return this.kindFieldSpecified;
            }
            set {
                this.kindFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string library_path {
            get {
                return this.library_pathField;
            }
            set {
                this.library_pathField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.example.org/SAFe_driver")]
    public partial class IDriverContextArgument {
        
        private IDriverContextArgument[] restrictionField;
        
        private string parameter_idField;
        
        private string library_pathField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("restriction")]
        public IDriverContextArgument[] restriction {
            get {
                return this.restrictionField;
            }
            set {
                this.restrictionField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string parameter_id {
            get {
                return this.parameter_idField;
            }
            set {
                this.parameter_idField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string library_path {
            get {
                return this.library_pathField;
            }
            set {
                this.library_pathField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.example.org/SAFe_driver")]
    public partial class IDriverPort {
        
        private int indexField;
        
        private bool indexFieldSpecified;
        
        private int facetField;
        
        private bool facetFieldSpecified;
        
        private string bindingField;
        
        private string nameField;
        
        private string componentField;
        
        public IDriverPort() {
            this.bindingField = "null";
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int index {
            get {
                return this.indexField;
            }
            set {
                this.indexField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool indexSpecified {
            get {
                return this.indexFieldSpecified;
            }
            set {
                this.indexFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int facet {
            get {
                return this.facetField;
            }
            set {
                this.facetField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool facetSpecified {
            get {
                return this.facetFieldSpecified;
            }
            set {
                this.facetFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [System.ComponentModel.DefaultValueAttribute("null")]
        public string binding {
            get {
                return this.bindingField;
            }
            set {
                this.bindingField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string component {
            get {
                return this.componentField;
            }
            set {
                this.componentField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.example.org/SAFe_driver")]
    public partial class IDriverPlacement {
        
        private string[] virtual_platformField;
        
        private int facetField;
        
        private bool facetFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("virtual_platform")]
        public string[] virtual_platform {
            get {
                return this.virtual_platformField;
            }
            set {
                this.virtual_platformField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int facet {
            get {
                return this.facetField;
            }
            set {
                this.facetField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool facetSpecified {
            get {
                return this.facetFieldSpecified;
            }
            set {
                this.facetFieldSpecified = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "0.0.0.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.example.org/SAFe_driver")]
    public enum IDriverComponentKind {
        
        /// <remarks/>
        Binding,
        
        /// <remarks/>
        Connector,
        
        /// <remarks/>
        Computation,
        
        /// <remarks/>
        VirtualPlatform,
        
        /// <remarks/>
        DataSource,
    }
}
