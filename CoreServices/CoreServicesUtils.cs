﻿using System;
using System.Collections.Generic;
using SAFeSWL;
using System.Diagnostics;
using System.IO;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.database;
using org.hpcshelf.DGAC;
using System.Xml.Serialization;
using System.Xml;
using org.hpcshelf;
using System.Threading;
using org.hpcshelf.platform;
using System.Data;

namespace org.hpcshelf
{
    public interface IBackendServices 
    {
        string deploy (string platform_config);
    }

    public class CoreServicesUtils
    {

        #region processing SAFeSWL



        public static bool checkIsPlatform (SAFeSWL_Architecture arch_desc, string arch_ref)
        {
            SAFeSWL_Component[] cs = arch_desc.solution;
            foreach (SAFeSWL_Component c in cs) 
            {
                if (c.id_component.Equals (arch_ref) && c is SAFeSWL_Platform)
                    return true;
            }

            return false;
        }

/*        public static IDictionary<string,string> readPlatformAddresses (SAFeSWL_Architecture arch_desc)
        {
            IDictionary<string,string> result = new Dictionary<string, string> ();

            SAFeSWL_Component[] cs = arch_desc.solution;
            foreach (SAFeSWL_Component c in cs) 
            {
                if (c is SAFeSWL_Platform)
                    result.Add(c.id_component,((SAFeSWL_Platform)c).platform_address);
            }

            return result;
        }

        public static string determineSystemComponentRef(SAFeSWL_Architecture arch_desc)
        {
            return arch_desc.application_name;
        }

*/


        #endregion


        #region accessing the catalog



        public static string[] fetchPlatformConfiguration (string component_ref, IDbConnection dbcon)
        {
            /* The concrete platform component has a single unit (homogenous platform), so-called "node", which carries a xml file describing some properties.
             * Besides to provide some information about the platform features that are not decribed in the contract,
             * these properties also help the Back-End on how to instantiate the platform. 
             * NOTE: In this first version, it carries only a string with the address of the Back-End ... 
             */
            //org.hpcshelf.database.DBConnector.openConnection ();

            org.hpcshelf.database.Component c = Backend.cdao.retrieve_libraryPath (component_ref, dbcon);
            IList<SourceCode> sc_list = Backend.scdao.listByFileType ('u', c.Id_concrete, "node", "platform.settings",dbcon);
            SourceCode sc = sc_list[0]; 
            string config = sc.Contents;
            string[] data =  config.Split (new char[1] { '\n' });

            //org.hpcshelf.database.DBConnector.closeConnection ();

            return data;
        }
            
        #endregion

        #region calling services







        public static 
                Tuple<Tuple<ComponentType,ComponentType>,   // application component (abstract + concrete)
                      Tuple<ComponentType,ComponentType>,   // workflow component (abstract + concrete)
                      Tuple<ComponentType,ComponentType>>
                    createSystem(SAFeSWL_Architecture arch_desc, 
                                  IDictionary<string,Instantiator.ComponentFunctorApplicationType> contracts, 
                                 IDictionary<string,Instantiator.UnitMappingType[]> unit_mapping,
                                 ref IList<string> platforms)
        {
            
            string app_name = null;
            string application_component_name = null;
            string workflow_component_name = null;;
            IList<Tuple<string, string, EnvironmentPortType, string>> bindings_application = null; 
            IList<Tuple<string, string, EnvironmentPortType, string>> bindings_workflow = null; 
            IList<Tuple<string, Tuple<string, string,int>[]>> bindings_system = null; 
            IList<Tuple<string, Tuple<string, string,int>[]>> bindings_task = null; 

            LoaderSystem.readArchitecture (arch_desc,
                ref app_name, 
                ref application_component_name, 
                ref workflow_component_name, 
                ref bindings_application, 
                ref bindings_workflow, 
                ref bindings_system, 
                ref bindings_task,
                ref platforms);

            // Take the configuration files of components of the system.
            Tuple<Tuple<ComponentType, ComponentType>,   
                  Tuple<ComponentType, ComponentType>,   
                  Tuple<ComponentType, ComponentType>> system = null; //LoaderSystem.createSystemComponent (app_name, application_component_name, workflow_component_name, contracts, unit_mapping, bindings_application, bindings_workflow, bindings_system, bindings_task, platforms);

            return system;
        }




        public static Tuple<DeployArguments.DeployArgumentsType, IDictionary<string, byte[]>> fetchDeployInfo(string component_ref, ISet<int> facet, IDictionary<string, AbstractComponentFunctorApplication> component_contract, IDbConnection dbcon)
        {
            Tuple<DeployArguments.DeployArgumentsType, IDictionary<string, byte[]>> infoCompile = null;

            Component c = Backend.cdao.retrieve_libraryPath(component_ref, dbcon);
            AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath(component_ref, dbcon);

            if (c == null && acf != null)
                infoCompile = LoaderApp.buildDeployInfoAbstract(acf.Id_abstract, facet, dbcon);

            if (c != null && acf == null)
                infoCompile = LoaderApp.buildDeployInfoConcrete(c.Id_concrete, facet, component_contract, dbcon);

            return infoCompile;
        }

        internal static string fetchBackendAddress(Component c, string default_address, IDbConnection dbcon)
        {
            IList<SourceCode> sc_list = Backend.scdao.listByFileType('u', c.Id_concrete, "platform.settings", dbcon);
            if (sc_list.Count > 0)
            {
                SourceCode sc = sc_list[0];
                Console.WriteLine("sc.Contents = {0}", sc.Contents);
                return sc.Contents;
            }
            else
            {
                Console.WriteLine("default_address = {0}", default_address);
                return default_address;
            }
        }


        #endregion

    }
}

