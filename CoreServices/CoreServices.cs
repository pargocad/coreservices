﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.Services.Protocols;
using ComponentXML;
using DeployArguments;
using DGAC.certify;
using gov.cca;
using gov.cca.ports;
using org.hpcshelf.certify;
using org.hpcshelf.common.utils;
using org.hpcshelf.database;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.exception;
//using org.hpcshelf.certify;
using org.hpcshelf.platform;
using org.hpcshelf.ports;
using Component = org.hpcshelf.database.Component;

namespace org.hpcshelf
{
    public class CoreServices : ICoreServices
    {
        private const string PLATFORM_SAFE_ID = Constants.PLATFORM_SAFE_ID;
        private const string WORKFLOW_COMPONENT_ID = Constants.WORKFLOW_COMPONENT_ID;
        private const string APPLICATION_COMPONENT_ID = Constants.APPLICATION_COMPONENT_ID;
        private const string SOURCE_COMPONENT_ID = Constants.SOURCE_COMPONENT_ID;

        #region Service Methods

        private IDictionary<string, Backend.ResolveComponentHierarchy> workflow_resolution = new ConcurrentDictionary<string, Backend.ResolveComponentHierarchy>();
        private string orchestration_code = null;

        private IList<string> delayed_contracts = new List<string>();
        private Backend.ResolveComponentHierarchy system_component = null;
        //private IDictionary<string, ISet<int>>[] system_dependencies = null;
        private int FacetInstanceCount { get { return platforms.Count; } }
        private Services services;

        private IPlatformServices platform_SAFe;
        public IPlatformServices PlatformSAFe { set { platform_SAFe = value; } }

        private ProvenanceManager provenance_manager = null;
        private LifeCycleManager lifecycle_manager = null;

        internal string SystemRef { get { return system_component.ComponentChoice[0]; } }

        private string platform_SAFe_address;

        private string core_ip = Utils.GetPublicIpAddress();

        private string platform_safe_address_ip = null;

        public void setPlatformSAFeAddress(string platform_safe_address, int platform_safe_port)
        {
            platform_safe_address = platform_safe_address.Equals(core_ip) ? "127.0.0.1" : platform_safe_address;
            this.platform_safe_address_ip = platform_safe_address;
            this.platform_SAFe_address = "http://" + platform_safe_address + ":" + platform_safe_port + "/PlatformSAFe.asmx";

            string errorMessage;
            Utils.checkConnection(this.platform_SAFe_address, 10000, false, out errorMessage);
            if (errorMessage != string.Empty)
            {
                errorMessage = string.Format("The (local) PlatformSAFe services at {0}:{1} is not reachable or available. " +
                    "\n(1) Check whether the IP address of your local machine ({0}) is externally accessible. " +
                    "\n(2) Check for firewall issues (e.g. is port {1} open for external access ?). ", platform_safe_address_ip, platform_safe_port);

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              errorMessage,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.create_system", WebServicesException.FaultCode.Server);
            }
            else
            {
                Console.WriteLine("The PlatformSAFe services is accessible at {0}:{1} !", platform_safe_address_ip, platform_safe_port);
            }

            this.PlatformSAFe = new CoreCachedServices.PlatformServices.PlatformServices(platform_SAFe_address);
        }

        public CoreServices(string app_name, string platform_safe_address, int platform_safe_port)
        {
            setPlatformSAFeAddress(platform_safe_address, platform_safe_port);

            init(app_name);
        }

        ~CoreServices()
        {
            Console.WriteLine("SESSION DESTROYED {0}", app_name);
        }

        private void init(string app_name)
        {
            this.app_name = app_name;
            ((CoreCachedServices.PlatformServices.PlatformServices) platform_SAFe).Timeout = int.MaxValue;

            //this.provenance_manager = new ProvenanceManager(this);
            this.lifecycle_manager = new LifeCycleManager(this);

            AbstractFramework frw = Backend.getFrameworkInstance();

            services = frw.getServices("Core." + this.app_name, "CoreServices", new TypeMapImpl());

            Services.addProvidesPort(ProvenanceManager, Constants.CORE_PROVENANCE_PORT_NAME, Constants.PROVENANCE_PORT_TYPE, new TypeMapImpl());
            Services.addProvidesPort(lifecycle_manager, Constants.CORE_LIFECYCLE_PORT_NAME, Constants.LIFECYCLE_PORT_TYPE, new TypeMapImpl());

            workflow_resolution = new ConcurrentDictionary<string, Backend.ResolveComponentHierarchy>();
            delayed_contracts = new List<string>();
        }

        public CoreServices()
        {
        }

        private string app_name;

        public Services Services { get {return  services;} }
        internal IDictionary<string, Backend.ResolveComponentHierarchy> WorkflowResolution { get { return workflow_resolution; } }
        public ProvenanceManager ProvenanceManager { get { if (provenance_manager == null) provenance_manager = new ProvenanceManager(this); return provenance_manager; } }

        private IList<string> platforms = null;

        IDictionary<string, IList<Tuple<string, int>>> binding_list_dict = new ConcurrentDictionary<string, IList<Tuple<string, int>>>();

        // PARALLEL COMPUTING SYSTEM
        public Backend.ResolveComponentHierarchy    open(string system_contract)
        {
            Backend.ResolveComponentHierarchy ctree;

            IDbConnection dbcon = DBConnector.createConnection();
            dbcon.Open();

            try
            {
                IList<Tuple<string, int>> binding_list = new List<Tuple<string, int>>();
                var result = buildSystem(system_contract, binding_list, dbcon);
                ctree = open(result, binding_list, 1);
            }
            finally
            {
                dbcon.Close();
            }

            return ctree;
        }

        // PARALLEL COMPUTING SYSTEM
        public Backend.ResolveComponentHierarchy open()
        {
            string system_contract = "<?xml version=\"1.0\"?><ComponentFunctorApplicationType xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" component_ref=\"" + library_path_abstract_system + "\"/>";
            return open(system_contract);
        }

        // PARALLEL CERTIFICATION SYSTEM
        public Backend.ResolveComponentHierarchy open(Backend.ResolveComponentHierarchy host_system_component, string certified_id, int id_concrete_certifiable, IDbConnection dbcon)
        {
            IList<Tuple<string, int>> binding_list = new List<Tuple<string, int>>();
            var result = buildSystem(host_system_component, certified_id, id_concrete_certifiable, binding_list, dbcon);
            return open(result, binding_list, 2);
        }

        private Backend.ResolveComponentHierarchy open(Tuple<Backend.ResolveComponentHierarchy, IList<string>, IDictionary<string, Tuple<int, string>[]>> result, IList<Tuple<string, int>> binding_list, int level)
        {
            try
            {
                system_component = result.Item1;
                platform_placement_list = result.Item3;
                platforms = result.Item2;

                groupBindings(platform_placement_list, binding_list, level, ref binding_list_dict);
            //    calculateSystemDependencies(system_component, FacetInstanceCount, ref system_dependencies);
            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "CoreServices.open");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.open", WebServicesException.FaultCode.Server);
            }

            return system_component;
        }

        public string getVirtualPlatformAddress(string platform_ref)
        {
            return deployed_component[platform_ref];
        }

        /*      private static void calculateSystemDependencies(Backend.ResolveComponentHierarchy system_component, int facet_instance_count, ref IDictionary<string, ISet<int>>[] system_dependencies, ref IList<string> system_dependencies_order, IDbConnection dbcon)
              {
                  Backend.DependenciesCalculator dependencies_calculator = new Backend.DependenciesCalculator(facet_instance_count);
                 // DBConnector.openConnection();
                  dependencies_calculator.visit(system_component, dbcon);
                  //DBConnector.closeConnection();

                  system_dependencies = new IDictionary<string, ISet<int>>[dependencies_calculator.Dependencies.Length];
                  system_dependencies_order = new IList<string>[dependencies_calculator.DependenciesOrder.Length];
                  for (int facet_instance = 0; facet_instance < dependencies_calculator.Dependencies.Length; facet_instance++)
                      system_dependencies[facet_instance] = new ConcurrentDictionary<string, ISet<int>>(dependencies_calculator.Dependencies[facet_instance]);
              }
              */

        private static void groupBindings(IDictionary<string, Tuple<int, string>[]> platform_placement_list, IList<Tuple<string, int>> binding_list, int level, ref IDictionary<string, IList<Tuple<string, int>>> binding_list_dict)
        {
            foreach (Tuple<string, int> qid in binding_list)
            {
                string id = qid.Item1.Split(new char[1] { '.' })[level];
                if (platform_placement_list.ContainsKey(id))
                {
                    IList<Tuple<string, int>> l;
                    if (!binding_list_dict.TryGetValue(id, out l))
                        binding_list_dict[id] = l = new List<Tuple<string, int>>();
                    l.Add(qid);
                }
            }
        }

        private AbstractComponentFunctorApplication acfaRef_system = null;
        private org.hpcshelf.database.Component c_system;

        private Tuple<Backend.ResolveComponentHierarchy, IList<string>, IDictionary<string, Tuple<int, string>[]>>
                    buildSystem(string app_contract, IList<Tuple<string, int>> binding_list, IDbConnection dbcon)
        {
            Backend.ResolveComponentHierarchyImpl component_hierarchy = null;
            IList<string> platforms_local = new List<string>();
            IDictionary<string, Tuple<int, string>[]> platform_placement_list = null;

            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {

                Instantiator.ComponentFunctorApplicationType system_contextual_type = LoaderApp.deserialize<Instantiator.ComponentFunctorApplicationType>(app_contract);
                acfaRef_system = LoaderApp.loadACFAFromInstantiator(system_contextual_type, dbcon);

                buildSystem(acfaRef_system, binding_list, placementOfComponents1, out component_hierarchy, out platforms_local, out platform_placement_list, dbcon);

                dbtrans.Commit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                dbtrans.Rollback();
            }

            return new Tuple<Backend.ResolveComponentHierarchy, IList<string>, IDictionary<string, Tuple<int, string>[]>>(component_hierarchy, platforms_local, platform_placement_list);
        }

        private Tuple<Backend.ResolveComponentHierarchy, IList<string>, IDictionary<string, Tuple<int, string>[]>>
                    buildSystem(Backend.ResolveComponentHierarchy host_system_component,
                                             string certified_id,
                                             int id_concrete_certifiable,
                                             IList<Tuple<string, int>> binding_list, IDbConnection dbcon)
        {
            Backend.ResolveComponentHierarchyImpl component_hierarchy = null;
            IList<string> platforms_local = new List<string>();
            IDictionary<string, Tuple<int, string>[]> platform_placement_list = null;

            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                Backend.ResolveComponentHierarchy ctree_certifier = host_system_component.InnerComponents[certified_id][0].Item1;
                int id_functor_app_certifier = Backend.generateACFAforContext(ctree_certifier.ACFARef.Id_abstract, ctree_certifier.ACFARef.Id_functor_app, ctree_certifier.ACFARef.Id_functor_app, ctree_certifier.Arguments, ctree_certifier.ArgumentsTop, 0, dbcon);

                if (isCertified(id_concrete_certifiable, id_functor_app_certifier, dbcon))
                    throw new AlreadyCertifiedException(certified_id, host_system_component.ID);

                acfaRef_system = Backend.acfadao.retrieve(id_functor_app_certifier, dbcon);

                buildSystem(acfaRef_system, binding_list, placementOfComponents2, out component_hierarchy, out platforms_local, out platform_placement_list, dbcon);

                dbtrans.Commit();
            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }

            return new Tuple<Backend.ResolveComponentHierarchy, IList<string>, IDictionary<string, Tuple<int, string>[]>>(component_hierarchy, platforms_local, platform_placement_list);
        }

        private delegate IDictionary<string, Tuple<int, string>[]> Del(int id_abstract_system, IDbConnection dbcon);

        private void buildSystem(AbstractComponentFunctorApplication acfaRef_system,
                                 IList<Tuple<string, int>> binding_list,
                                 Del placementOfComponents,
                                 out Backend.ResolveComponentHierarchyImpl component_hierarchy,
                                 out IList<string> platforms_local,
                                 out IDictionary<string, Tuple<int, string>[]> platform_placement_list, IDbConnection dbcon)
        {
            c_system = Backend.resolveUnit(acfaRef_system, dbcon)[0];

            platforms_local = Backend.fetchPlatforms(c_system.GetId_abstract(dbcon), dbcon);
            foreach (string p in platforms_local)
                deployed_list[p] = new ConcurrentDictionary<string, ISet<int>>();

            platform_placement_list = placementOfComponents(c_system.GetId_abstract(dbcon), dbcon);

            IDictionary<string, int> arguments = new ConcurrentDictionary<string, int>();
            IDictionary<string, int> arguments_top;
            Backend.determineArguments(arguments, arguments, acfaRef_system.Id_functor_app, out arguments_top, dbcon);

            arguments = new ConcurrentDictionary<string, int>(arguments_top);

            AbstractComponentFunctor acf_impl = Backend.acfdao.retrieve(c_system.GetId_abstract(dbcon), dbcon);
            IDictionary<string, int> argumentsImpl;
            if (acf_impl.Id_functor_app_supertype > 0)
            {
                Backend.determineArguments(arguments_top, arguments, acf_impl.Id_functor_app_supertype, out argumentsImpl, dbcon);
                foreach (KeyValuePair<string, int> t in argumentsImpl)
                    if (!arguments.ContainsKey(t.Key))
                        arguments.Add(t);
            }

            AbstractComponentFunctor acf_system = Backend.acfdao.retrieve(c_system.GetId_abstract(dbcon), dbcon);

            component_hierarchy = new Backend.ResolveComponentHierarchySystemImpl(app_name, acf_system.Library_path);

            Instantiator.UnitMappingType[] unit_mapping_system = Backend.build_unit_mapping_of_system(c_system.GetId_abstract(dbcon), dbcon);

            int facet_instance_count = unit_mapping_system.Length;
            IList<int>[] system_facet_list = new IList<int>[facet_instance_count];
            for (int f = 0; f < facet_instance_count; f++)
            {
                system_facet_list[f] = new List<int>();
                system_facet_list[f].Add(f);
            }

            component_hierarchy.FacetList = system_facet_list;
            component_hierarchy.Arguments = arguments;
            component_hierarchy.ArgumentsTop = arguments_top;
            component_hierarchy.ACFARef = acfaRef_system;
            component_hierarchy.UnitMapping = new Instantiator.UnitMappingType[1][] { unit_mapping_system };

            component_hierarchy.buildComponentHierarchy(facet_instance_count, binding_list, delayed_public_inner_component, dbcon);
            component_hierarchy.ComponentDB = new org.hpcshelf.database.Component[1] { c_system };
            component_hierarchy.resolveNode(component_hierarchy.ComponentDB, dbcon);

            ConverterComponentHierachyToXML.addToCTreeCache(component_hierarchy);
        }

        private void updateResolutionTree(Backend.ResolveComponentHierarchy component_hierarchy, IDbConnection dbcon)
        {
            IList<Tuple<string, int>> binding_list = new List<Tuple<string, int>>();

            org.hpcshelf.database.Component[] saved_component_db = component_hierarchy.ComponentDB;

            Backend.ResolveComponentHierarchyImpl component_hierarchy_impl = (Backend.ResolveComponentHierarchyImpl)component_hierarchy;
            component_hierarchy_impl.buildComponentHierarchy(FacetInstanceCount, binding_list, delayed_public_inner_component, dbcon);
            if (saved_component_db != null)
                component_hierarchy_impl.resolveNode(saved_component_db, dbcon);

            // O CHAMADOR DEVE ATUALIZAR o binding_list e as dependências ...
            groupBindings(platform_placement_list, binding_list, 1, ref binding_list_dict);

          //  calculateSystemDependencies(system_component, FacetInstanceCount, ref system_dependencies);
        }


        private void checkDelayedBindings(IDbConnection dbcon)
        {
            // TODO: OPTIMIZE !!! VERY INNEFICIENT

            foreach (Tuple<string, Backend.ResolveComponentHierarchy> t in delayed_public_inner_component.Values)
            {
                string id_inner = t.Item1;
                Backend.ResolveComponentHierarchy ctree = t.Item2;
                IList<Tuple<Backend.ResolveComponentHierarchy, int[]>> ctree_inner_list = ctree.InnerComponents[id_inner];
                ((Backend.ResolveComponentHierarchyImpl)ctree).findPublicInnerComponent(id_inner, ctree_inner_list, dbcon);
                foreach (Tuple<Backend.ResolveComponentHierarchy, int[]> ctree_inner_list_item in ctree_inner_list)
                    ((Backend.ResolveComponentHierarchyImpl)ctree_inner_list_item.Item1).addHostComponent(id_inner, ctree);
            }

        }

        private IDictionary<string, Tuple<string, Backend.ResolveComponentHierarchy>> delayed_public_inner_component = new ConcurrentDictionary<string, Tuple<string, Backend.ResolveComponentHierarchy>>();


        private void removeInnerComponent(Backend.ResolveComponentHierarchy system_component, string id_inner_removed, IDbConnection dbcon)
        {
            IList<Backend.ResolveComponentHierarchy> modified_ctree_list = new List<Backend.ResolveComponentHierarchy>();
            foreach (Tuple<Backend.ResolveComponentHierarchy, int[]> t in system_component.InnerComponents[id_inner_removed])
            {
                Backend.ResolveComponentHierarchy ctree = t.Item1;
                foreach (Tuple<string, Backend.ResolveComponentHierarchy> tt in ctree.HostComponents)
                {
                    Backend.ResolveComponentHierarchy ctree_host = tt.Item2;
                    ctree_host.IsPrivate.Remove(tt.Item1);
                    ctree_host.InnerComponents.Remove(tt.Item1);
                    modified_ctree_list.Add(ctree_host);
                }
            }

            this.updateResolutionTree(system_component, dbcon);
            foreach (Backend.ResolveComponentHierarchy modified_ctree in modified_ctree_list)
                if (modified_ctree != system_component)
                    this.updateResolutionTree(modified_ctree, dbcon);
        }


        public /*string*/ void insertComponent(string solution_component_xml)
        {
            //string result;

            IDbConnection dbcon = DBConnector.createConnection();
            dbcon.Open();

            try
            {
               // IDriverPortInfo driver_port_info = new IDriverPortInfo();
               // driver_port_info.port = new IDriverPortInfoElement[0];
               // result = LoaderApp.serialize<IDriverPortInfo>(driver_port_info);

                IDriverComponent solution_component = LoaderApp.deserialize<IDriverComponent>(solution_component_xml);

                switch (solution_component.kind)
                {
                    case IDriverComponentKind.Binding: insertBindingInCatalog(solution_component, dbcon); break;
                    case IDriverComponentKind.VirtualPlatform:
                        if (solution_component.name.Equals(Constants.PLATFORM_SAFE_ID))
                            insertPlatformSAFeInCatalog(solution_component, dbcon);
                        else
                            insertVirtualPlatformInCatalog(solution_component, dbcon);
                        break;
                    case IDriverComponentKind.Computation: /*result =*/ insertComputationOrDataSourceInCatalog(solution_component, dbcon); break;
                    case IDriverComponentKind.DataSource: /*result = */ insertComputationOrDataSourceInCatalog(solution_component, dbcon); break;
                    case IDriverComponentKind.Connector: /* result = */ insertConnectorInCatalog(solution_component, dbcon); break;
                    case IDriverComponentKind.Workflow: insertAbstractWorkflowInCatalog(dbcon); insertConcreteWorkflowInCatalog(dbcon); break;
                    case IDriverComponentKind.Application: insertAbstractApplicationInCatalog(dbcon); insertConcreteApplicationInCatalog(dbcon); break;
                    case IDriverComponentKind.System:
                        insertAbstractSystemInCatalog(solution_component, dbcon);
                        insertConcreteSystemInCatalog(solution_component, dbcon);
                        break;
                }

                created_components[solution_component.name] = solution_component;

            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "CoreServices.insertComponent");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.insertComponent", WebServicesException.FaultCode.Server);
            }
            finally
            {
                dbcon.Close();
            }

            //return result;
        }

        public string fetchPorts(string library_path, string name)
        {
            string result;

            IDbConnection dbcon = DBConnector.createConnection();
            dbcon.Open();

            try
            {
                IList<Tuple<string, int, int, string>> r = new List<Tuple<string, int, int, string>>();
                fetchServicePorts(library_path, name, ref r, dbcon);
                fetchActionPorts(library_path, name, ref r, dbcon);

                result = createDriverPortInfoOutput(r);
            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "CoreServices.insertComponent");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.insertComponent", WebServicesException.FaultCode.Server);
            }
            finally
            {
                dbcon.Close();
            }

            return result;
        }


        private void insertBindingInCatalog(IDriverComponent solution_component, IDbConnection dbcon)
        {
            IList<string> id_inner_removed_list = new List<string>();

            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                IDictionary<string, object> contract = (IDictionary<string, object>) mkContract(solution_component.library_path, /*new object[1] {*/ solution_component.context_argument /*}*/, dbcon);

                foreach (IDriverPort port in solution_component.port)
                {
                    if (port.component.Equals(Constants.WORKFLOW_COMPONENT_ID))
                    {
                        this.insertWorkflowActionPortInCatalog(solution_component.name, contract, dbcon);
                        break;
                    }
                    if (port.component.Equals(Constants.APPLICATION_COMPONENT_ID))
                    {
                        AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath((string)contract["name"], dbcon);
                        Interface i = Backend.idao.retrieveSuper(acf.Id_abstract, port.role.ToString(), dbcon);

                        this.insertApplicationServicePortInCatalog(solution_component.name, contract, i.Id_interface /*port.role.ToString()*/, dbcon);
                        break;
                    }
                }

                foreach (IDriverPort port in solution_component.port)
                {
                    InnerComponentExposed ice = new InnerComponentExposed();
                    ice.Id_abstract = id_abstract_system;
                    ice.Id_inner_owner = port.component;
                    ice.Id_inner = port.name;
                    ice.Id_inner_rename = solution_component.name;

                    string id_interface_host = null;
                    IDriverComponent cc = created_components[port.component];
                    if (cc.kind == IDriverComponentKind.DataSource || cc.kind == IDriverComponentKind.Computation || cc.kind == IDriverComponentKind.Workflow || cc.kind == IDriverComponentKind.Application)
                    {
                        id_interface_host = "peer_" + platforms.IndexOf(mkPlacement(cc.placement)[0][0].name);
                    }
                    else if (cc.kind == IDriverComponentKind.Connector)
                    {
                        id_interface_host = "peer_" + platforms.IndexOf(mkPlacement(cc.placement)[port.facet_host][port.index].name);
                    }

                    SliceExposed se_ = Backend.sedao.retrieve(ice.Id_abstract, ice.Id_inner_owner, ice.Id_inner, id_interface_host, dbcon);
                    Backend.icedao.delete(ice.Id_abstract, ice.Id_inner_owner, se_.Id_inner, dbcon);
                    Backend.icdao.delete(ice.Id_abstract, se_.Id_inner, dbcon);
                    Backend.sdao.delete(ice.Id_abstract, se_.Id_inner, dbcon);
                    Backend.sedao.delete(ice.Id_abstract, se_.Id_inner, dbcon);
                    Backend.icedao.insert(ice, dbcon);
                    id_inner_removed_list.Add(se_.Id_inner);
                }

                InnerComponent ic = insertInnerComponentIntoCatalog(contract, solution_component.name, true, dbcon);

                IDictionary<string, IList<Tuple<string, Interface, IDriverComponent>>> i_list = new ConcurrentDictionary<string, IList<Tuple<string, Interface, IDriverComponent>>>();

                fillListOfPlaces(mkPlacement(solution_component.placement), ic, i_list, dbcon);

                IDictionary<IDriverPort, bool> marked_port = new ConcurrentDictionary<IDriverPort, bool>();

                foreach (IList<Tuple<string, Interface, IDriverComponent>> ii in i_list.Values)
                {
                    foreach (Tuple<string, Interface, IDriverComponent> iii in ii)
                    {
                        IDriverComponent virtual_platform = iii.Item3;

                        Slice s = insertSliceInCatalog(solution_component.name, ic, iii, true, dbcon);

                        if (s.Transitive)
                        {
                            IDriverPort[] ports = mkPortPlacement(solution_component.port)[virtual_platform]; // solution_binding.PortPlacement[virtual_platform];

                            IDriverPort port = null;
                            for (int k = 0; k < ports.Length; k++)
                            {
                                if (!marked_port.ContainsKey(ports[k]))
                                {
                                    port = ports[k];
                                    marked_port[port] = true;
                                    break;
                                }
                            }

                            int facet = -1;
                            foreach (IDriverPort p in solution_component.port)
                                if (p == port)
                                {
                                    facet = p.facet_binding;
                                    break;
                                }

                            Interface i_inner = Backend.idao.listByFacet(ic.Id_abstract_inner, facet, dbcon)[0];

                            InnerComponent icx = Backend.icdao.retrieve(id_abstract_system, port.component, dbcon);
                            IList<Slice> sx = Backend.sdao.listByInterfaceSlice(icx.Id_abstract_inner, port.name, i_inner.Id_interface, dbcon);

                            SliceExposed se = new SliceExposed();
                            se.Id_abstract = ic.Id_abstract_owner;
                            se.Id_inner = solution_component.name;
                            se.Id_inner_original = port.name;
                            se.Id_inner_owner = port.component;
                            se.Id_interface_host = s.Id_interface;
                            se.Id_interface_slice = i_inner.Id_interface;
                            se.Id_interface_slice_original = i_inner.Id_interface;
                            //if (sx.Count == 0)
                           //    Console.Write(true);
                            se.Id_interface_slice_owner = sx[0].Id_interface; // s.Id_interface_slice;
                            if (Backend.sedao.retrieve2(se.Id_inner_original, se.Id_interface_slice, se.Id_abstract, se.Id_interface_slice_owner, se.Id_inner_owner, se.Id_inner, se.Id_interface_host, dbcon) == null)
                                // BackEnd.sedao.update_id_inner(se);
                                Backend.sedao.insert(se, dbcon);

                        }
                    }
                }

                placementOfComponents1_ic(ic, platform_placement_list, "%platform%", dbcon);


                foreach (string id_inner_removed in id_inner_removed_list)
                    removeInnerComponent(this.system_component, id_inner_removed, dbcon);
                this.updateResolutionTree(this.system_component, dbcon);

                checkDelayedBindings(dbcon);

                dbtrans.Commit();
            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                throw e;
            }
        }

        private IDictionary<IDriverComponent, IDriverPort[]> mkPortPlacement(IDriverPort[] port_placement)
        {
            IList<Tuple<IDriverComponent, IDriverPort>> m = new List<Tuple<IDriverComponent, IDriverPort>>();
            foreach (IDriverPort port in port_placement)
            {
                int port_facet = port.facet_host;
                int index = port.index;
                m.Add(new Tuple<IDriverComponent, IDriverPort>(mkPlacement(created_components[port.component].placement)[port_facet][index], port));
            }

            IDictionary<IDriverComponent, IDriverPort[]> result = new ConcurrentDictionary<IDriverComponent, IDriverPort[]>();

            foreach (Tuple<IDriverComponent, IDriverPort> t in m)
            {
                IDriverPort[] ps;
                if (!result.TryGetValue(t.Item1, out ps))
                    result[t.Item1] = new IDriverPort[1] { t.Item2 };
                else
                {
                    result.Remove(t.Item1);
                    result[t.Item1] = new IDriverPort[ps.Length + 1];
                    ps.CopyTo(result[t.Item1], 0);
                    result[t.Item1][ps.Length] = t.Item2;
                }
            }

            return result;

        }

        private IDictionary<string, IDriverComponent> created_components = new ConcurrentDictionary<string, IDriverComponent>();

        private IDictionary<int, IDriverComponent[]> mkPlacement(IDriverPlacement[] driver_placement)
        {
            IDictionary<int, IDriverComponent[]> placement = new ConcurrentDictionary<int, IDriverComponent[]>();

            foreach (IDriverPlacement p in driver_placement)
            {
                placement[p.facet] = new IDriverComponent[p.virtual_platform.Length];
                for (int i = 0; i < p.virtual_platform.Length; i++)
                    placement[p.facet][i] = created_components[p.virtual_platform[i]];
            }

            return placement;
        }

        private object mkContract(string library_path, object[] items, IDbConnection dbcon)
        {
            if (library_path != null)
            {
                AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath(library_path, dbcon);
                if (acf == null)
                    throw new AbstractComponentNotFoundException(library_path, system_component.ID, system_component.ID);
            }

            if (items == null || items[0] is IDriverContextArgument)
            {
                IDictionary<string, object> contract = new ConcurrentDictionary<string, object>();

                contract["name"] = library_path;

                if (items != null)
                {
                    for (int i = 0; i < items.Length; i++)
                    {
                        IList<IDictionary<string, object>> contract_stack = new List<IDictionary<string, object>>();
                        contract_stack.Add(contract);

                        IDictionary<string, object> contract_par = contract;

                        var driver_argument = (IDriverContextArgument)items[i];
                        string parameter_id_ = driver_argument.parameter_id;
                        string[] parameter_id_parts = parameter_id_.Split('-');
                        string parameter_id = null;

                        string[] parameter_id_stack = new string[parameter_id_parts.Length];

                        int jj = 0;
                        foreach (string s in parameter_id_parts)
                        {
                            if (parameter_id != null)
                            {
                                AbstractComponentFunctor acf_par = Backend.acfdao.retrieve_libraryPath((string)contract_par["name"], dbcon);
                                AbstractComponentFunctorParameter acfp_par = Backend.acfpdao.retrieve(acf_par.Id_abstract, parameter_id, dbcon);
                                if (acfp_par == null)
                                {
                                    //   throw new Exception(String.Format("Parameter {0} not found in {1}", parameter_id, acf_par.Library_path));
                                    parameter_id_stack[0] = parameter_id_;
                                    break;
                                }
                                AbstractComponentFunctorApplication acfa_par = Backend.acfadao.retrieve(acfp_par.Bounds_of, dbcon);
                                string library_path_par = Backend.acfdao.retrieve(acfa_par.Id_abstract, dbcon).Library_path;
                                //contract_par[parameter_id] = new ConcurrentDictionary<string, object>() { { "name", library_path_par } };
                                IDictionary<string,object> new_contract = new Dictionary<string, object>() { { "name", library_path_par } };
                                for (int j=0; j < contract_stack.Count; j++)
                                    contract_stack[j][parameter_id_stack[j]] = new_contract;
                                contract_stack.Add(new_contract);
                                contract_par = new_contract;
                            }


                            if (!contract_par.ContainsKey(s))
                            {
                                parameter_id = s;
                            }
                            else
                            {
                                contract_par = (IDictionary<string, object>)contract_par[s];
                                contract_stack.Add(contract_par);
                            }

                            //parameter_id_x = parameter_id_x.Substring(parameter_id_x.IndexOf('-') + 1);
                            jj++;
                            for (int k = 0; k < jj; k++)
                                parameter_id_stack[k] = parameter_id_stack[k] == null ? s : parameter_id_stack[k] + "-" + s;

                        }

                        string library_path_argument = driver_argument.library_path;

                        object new_contract_2 = mkContract(library_path_argument, driver_argument.Items, dbcon);

                        int n = contract_stack.Count;
                        for (int j = 0; j < n; j++)
                            contract_stack[j][parameter_id_stack[j]] = new_contract_2;
                    }
                }
                return contract;
            }
            else
            {
                decimal quantifier = (decimal)items[0];
                return quantifier;
            }

        }

        private Slice insertSliceInCatalog(string name, InnerComponent ic, Tuple<string, Interface, IDriverComponent> iii, bool transitive, IDbConnection dbcon)
        {
            Slice s = new Slice();
            s.Id_abstract = ic.Id_abstract_owner;
            s.Id_inner = name;
            s.Id_interface = iii.Item2.Id_interface;
            s.Id_interface_slice = iii.Item1;
            s.Transitive = transitive;
            s.PortName = ic.Id_inner.Substring(0, 1).ToUpper() + ic.Id_inner.Substring(1);
            if (Backend.sdao.retrieve(s.Id_abstract, s.Id_inner, s.Id_interface_slice, s.Id_interface, dbcon) == null)
                Backend.sdao.insert(s, dbcon);

            return s;
        }

        private void fillListOfPlaces(IDictionary<int, IDriverComponent[]> placement, InnerComponent ic, IDictionary<string, IList<Tuple<string, Interface, IDriverComponent>>> i_list, IDbConnection dbcon)
        {
            foreach (KeyValuePair<int, IDriverComponent[]> f in placement)
            {
                int facet = f.Key;
                for (int index = 0; index < f.Value.Length; index++)
                {
                    IDriverComponent place = created_components[f.Value[index].name];
                    int facet_instance = platforms.IndexOf(place.name);
                    Interface i = Backend.idao.retrieve(ic.Id_abstract_owner, "peer_" + facet_instance, dbcon);

                    string id_interface_inner;
                    IList<Interface> i_list_inner = Backend.idao.listByFacet(ic.Id_abstract_inner, facet, dbcon);
                    id_interface_inner = i_list_inner[0].Id_interface;

                    IList<Tuple<string, Interface, IDriverComponent>> l;
                    if (!i_list.TryGetValue(i.Id_interface, out l))
                        i_list[i.Id_interface] = l = new List<Tuple<string, Interface, IDriverComponent>>();
                    l.Add(new Tuple<string, Interface, IDriverComponent>(id_interface_inner, i, place));
                }
            }
        }

        private InnerComponent insertInnerComponentIntoCatalog(IDictionary<string, object> contract, string name, bool transitive, IDbConnection dbcon)
        {
            string library_path_inner = (string)contract["name"];

            AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath(library_path_inner,dbcon);

            if (acf == null)
                throw new AbstractComponentNotFoundException(library_path_inner, name, system_component.ID);

            int id_functor_app_inner = Backend.createFunctorAppInner(contract, dbcon); ;

            InnerComponent ic = new InnerComponent();
            ic.Id_abstract_inner = acf.Id_abstract;
            ic.Id_abstract_owner = id_abstract_system;
            ic.Id_functor_app = id_functor_app_inner;
            ic.Id_inner = name;
            ic.IsPublic = false;
            ic.Multiple = false;
            ic.Parameter_top = "";
            ic.Transitive = transitive; // solution_component is IBinding || solution_component is IVirtualPlatform
            Backend.icdao.insert(ic, dbcon);

            return ic;
        }

        private void insertVirtualPlatformInCatalog(IDriverComponent solution_component, IDbConnection dbcon)
        {
            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                updatePlatforms(solution_component.name);

                InnerComponent ic = insertInnerComponentIntoCatalog((IDictionary<string, object>)mkContract(solution_component.library_path, /*new object[1] {*/ solution_component.context_argument /*}*/, dbcon), solution_component.name, true, dbcon);

                AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath((string)solution_component.library_path, dbcon);

                Interface i = insertNewSystemUnitInCatalog(solution_component.name, ic, acf.Hash_component_UID, dbcon);

                IDictionary<string, IList<Tuple<string, Interface, IDriverComponent>>> i_list = new ConcurrentDictionary<string, IList<Tuple<string, Interface, IDriverComponent>>>();
                IList<Tuple<string, Interface, IDriverComponent>> l = new List<Tuple<string, Interface, IDriverComponent>>();
                l.Add(new Tuple<string, Interface, IDriverComponent>("node", i, solution_component));
                i_list.Add(i.Id_interface, l);

                foreach (IList<Tuple<string, Interface, IDriverComponent>> ii in i_list.Values)
                    foreach (Tuple<string, Interface, IDriverComponent> iii in ii)
                        insertSliceInCatalog(solution_component.name, ic, iii, true, dbcon);

                placementOfComponents1_ic(ic, platform_placement_list, "%platform%", dbcon);

                this.updateResolutionTree(this.system_component, dbcon);

                dbtrans.Commit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                dbtrans.Rollback();
                throw e;
            }
          }

        private Interface insertNewSystemUnitInCatalog(string name, InnerComponent ic, string hash_component_uid, IDbConnection dbcon)
        {
            AbstractComponentFunctor acf_system = Backend.acfdao.retrieve(id_abstract_system, dbcon);
            string library_path_abstract = acf_system.Library_path;

            org.hpcshelf.database.Component c = Backend.cdao.listByAbstractComponent(id_abstract_system, dbcon)[0];
            string library_path_concrete = c.Library_path;

            int facet = platforms.IndexOf(name);
            Interface i = new Interface();
            i.Id_abstract = ic.Id_abstract_owner;
            i.Id_interface = "peer_" + facet;
            i.Facet = facet;
            i.Id_interface_super = "peer_" + facet;
            i.Class_name = library_path_abstract + ".IPeer" + facet;
            i.Assembly_string = i.Class_name + ", Culture=neutral, Version=0.0.0.0, PublicKey=" + hash_component_uid;
            i.URI_Source = "";
            i.Class_nargs = 0;
            i.SetId_interface_super_top("peer_" + facet);
            i.Order = 1;
            Backend.idao.insert(i,dbcon);

            Unit u = new Unit();
            u.Id_concrete = c.Id_concrete;
            u.Id_unit = "peer_" + facet;
            u.Id_interface = "peer_" + facet;
            u.Id_abstract = ic.Id_abstract_owner;
            u.Id_unit_super = "peer_" + facet;
            u.Class_name = library_path_concrete + ".IPeer" + facet + "Impl";
            u.Assembly_string = u.Class_name + ", Culture=neutral, Version=0.0.0.0, PublicKey=" + c.Hash_component_UID;
            u.URI_Source = "";
            u.Class_nargs = 0;
            u.Order = 1;
            Backend.udao.insert(u, dbcon);

            SourceCode sc = new SourceCode();
            sc.Type_owner = 'i';
            sc.Id_owner = "peer_" + facet;
            sc.Contents = "using org.hpcshelf.kinds;\n\nnamespace " + library_path_abstract + "\n{\n\tpublic interface IPeer" + facet + " : IApplicationKind\n\t{\n\t}\n}";
            sc.File_name = "IPeer" + facet + ".cs";
            sc.File_type = "user";
            sc.Language = "C# Language";
            sc.Id_owner_container = ic.Id_abstract_owner;
            sc.Order = 0;
            sc.Module_name = library_path_abstract + ".IPeer" + facet;
            sc.DeployType = "C# Wrapper";
            sc.Language = "C# Language";
            Backend.scdao.insert(sc,dbcon);

            sc.Type_owner = 'u';
            sc.Id_owner = "peer_" + facet;
            sc.Contents = "using System;\nusing org.hpcshelf.DGAC;\nusing org.hpcshelf.unit;\nusing org.hpcshelf.kinds;\nusing " + library_path_abstract + ";\n\nnamespace " + library_path_concrete + "\n{\n\tpublic class IPeer" + facet + "Impl : org.hpcshelf.kinds.Application, IPeer" + facet + "\n\t{\n\t\tprivate void Go(Object o)\n\t\t{\n\t\t\t((Activate)(o)).go();\n\t\t}\n\n\t\tpublic override void main()\n\t\t{\n\t\t}\n\t}\n}\n";
            sc.File_name = "IPeer" + facet + "Impl.cs";
            sc.File_type = "user";
            sc.Language = "C# Language";
            sc.Id_owner_container = c.Id_concrete;
            sc.Order = 0;
            sc.Module_name = library_path_concrete + ".IPeer" + facet + "Impl";
            sc.DeployType = "C# Wrapper";
            sc.Language = "C# Language";
            Backend.scdao.insert(sc, dbcon);

            return i;
        }

        private /*string*/ void insertConnectorInCatalog(IDriverComponent solution_component, IDbConnection dbcon)
        {
            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                InnerComponent ic = insertInnerComponentIntoCatalog((IDictionary<string, object>)mkContract(solution_component.library_path, /*new object[1] {*/ solution_component.context_argument, dbcon /*}*/), solution_component.name, false, dbcon);

                IDictionary<string, IList<Tuple<string, Interface, IDriverComponent>>> i_list = new ConcurrentDictionary<string, IList<Tuple<string, Interface, IDriverComponent>>>();

                fillListOfPlaces(mkPlacement(solution_component.placement), ic, i_list, dbcon);

                foreach (IList<Tuple<string, Interface, IDriverComponent>> ii in i_list.Values)
                    foreach (Tuple<string, Interface, IDriverComponent> iii in ii)
                    {
                        Slice s = insertSliceInCatalog(solution_component.name, ic, iii, false, dbcon);
                        insertFreePorts(solution_component.name, ic, s, dbcon);
                    }

                placementOfComponents1_ic(ic, platform_placement_list, "%platform%", dbcon);

                this.updateResolutionTree(this.system_component, dbcon);

                // IList<Tuple<string, int, int, string>> result = new List<Tuple<string, int, int, string>>();
                // fetchServicePorts(solution_component.library_path, solution_component.name, ref result, dbcon);
                // fetchActionPorts(solution_component.library_path, solution_component.name, ref result, dbcon);

                // string r = createDriverPortInfoOutput(result);

                dbtrans.Commit();

                // return r; 
            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                throw e;
            }

            //return null;
        }

        private string createDriverPortInfoOutput(IList<Tuple<string, int, int, string>> result)
        {
            IDriverPortInfo driver_port_info = new IDriverPortInfo();
            driver_port_info.port = new IDriverPortInfoElement[result.Count];
            for (int i = 0; i < result.Count; i++)
            {
                driver_port_info.port[i] = new IDriverPortInfoElement();
                driver_port_info.port[i].name = result[i].Item1;
                driver_port_info.port[i].index = result[i].Item2;
                driver_port_info.port[i].facet = result[i].Item3;
                driver_port_info.port[i].role = result[i].Item4;
            }

            return LoaderApp.serialize<IDriverPortInfo>(driver_port_info);
        }

        private void insertFreePorts(string name, InnerComponent ic, Slice s, IDbConnection dbcon)
        {
            IList<Slice> s0_list = Backend.sdao.listByInterface(ic.Id_abstract_inner, s.Id_interface_slice, dbcon);
            foreach (Slice s0 in s0_list)
            {
                if (Backend.icdao.retrieve(s0.Id_abstract, s0.Id_inner, dbcon).IsPublic)
                {
                    InnerComponent ic0 = Backend.icdao.retrieve(ic.Id_abstract_inner, s0.Id_inner, dbcon);

                    object o = new object();
                    int unique_id = o.GetHashCode();

                    if (!s0.Id_inner.StartsWith("platform"))
                    {
                        Slice s_ = new Slice();
                        s_.Id_abstract = s.Id_abstract;
                        s_.Id_inner = "$" + unique_id + "$" + s0.Id_inner;
                        s_.Id_interface = s.Id_interface;
                        s_.Id_interface_slice = s0.Id_interface_slice;
                        s_.Transitive = true;
                        s_.PortName = s_.Id_inner.Substring(0, 1).ToUpper() + s_.Id_inner.Substring(1); ;
                        //if (BackEnd.sdao.retrieve(s.Id_abstract, s.Id_inner, s.Id_interface_slice, s.Id_interface) == null)
                        Backend.sdao.insert(s_, dbcon);

                        InnerComponent ic0x = new InnerComponent();
                        ic0x.Id_abstract_inner = ic0.Id_abstract_inner;
                        ic0x.Id_abstract_owner = id_abstract_system;
                        ic0x.Id_functor_app = ic0.Id_functor_app;
                        ic0x.Id_inner = "$" + unique_id + "$" + ic0.Id_inner;
                        ic0x.IsPublic = false;
                        ic0x.Multiple = false;
                        ic0x.Parameter_top = "";
                        ic0x.Transitive = true;
                        Backend.icdao.insert(ic0x, dbcon);

                        InnerComponentExposed ice0x = new InnerComponentExposed();
                        ice0x.Id_abstract = id_abstract_system; // ic0x.Id_abstract_owner;
                        ice0x.Id_inner_owner = name;
                        ice0x.Id_inner = ic0.Id_inner;
                        ice0x.Id_inner_rename = "$" + unique_id + "$" + ic0.Id_inner; // ic0x.Id_inner;
                        Backend.icedao.insert(ice0x, dbcon);

                        SliceExposed se = new SliceExposed();
                        se.Id_abstract = s.Id_abstract;
                        se.Id_inner = "$" + unique_id + "$" + s0.Id_inner;
                        se.Id_inner_original = s0.Id_inner;
                        se.Id_inner_owner = s.Id_inner;
                        se.Id_interface_host = s.Id_interface;
                        se.Id_interface_slice = s0.Id_interface_slice;
                        se.Id_interface_slice_original = s0.Id_interface_slice;
                        se.Id_interface_slice_owner = s.Id_interface_slice;
                        Backend.sedao.insert(se, dbcon);
                    }
                }
            }
        }

        private /*string*/ void insertComputationOrDataSourceInCatalog(IDriverComponent solution_component, IDbConnection dbcon)
        {
            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                IDictionary<string, object> contract = (IDictionary<string, object>) mkContract(solution_component.library_path, solution_component.context_argument, dbcon);
                InnerComponent ic = insertInnerComponentIntoCatalog(contract, solution_component.name, false, dbcon);

                string id_platform = insertPlatformOfComputationOrDataSourceInCatalog(solution_component.name, solution_component.placement[0].virtual_platform[0], ic.Id_abstract_inner, dbcon);

                IDictionary<string, IList<Tuple<string, Interface, IDriverComponent>>> i_list = new ConcurrentDictionary<string, IList<Tuple<string, Interface, IDriverComponent>>>();

                fillListOfPlaces(mkPlacement(solution_component.placement), ic, i_list, dbcon);

                foreach (IList<Tuple<string, Interface, IDriverComponent>> ii in i_list.Values)
                    foreach (Tuple<string, Interface, IDriverComponent> iii in ii)
                    {
                        Slice s = insertSliceInCatalog(solution_component.name, ic, iii, false, dbcon);
                        insertFreePorts(solution_component.name, ic, s, dbcon);
                        if (!s.Transitive)
                            insertSlicedExposedOfPlatformOfSinglePlacedComponent(solution_component.name, solution_component.placement[0].virtual_platform[0], ic, s, id_platform, dbcon);
                    }

                placementOfComponents1_ic(ic, platform_placement_list, "%platform%" /*, solution_component.placement[0].virtual_platform[0]*/, dbcon);

                this.updateResolutionTree(this.system_component, dbcon);

//                IList<Tuple<string, int, int, string>> result = new List<Tuple<string, int, int, string>>();
//                fetchServicePorts(solution_component.library_path, solution_component.name, ref result, dbcon);
//                fetchActionPorts(solution_component.library_path, solution_component.name, ref result, dbcon);

//                string r = createDriverPortInfoOutput(result);

                dbtrans.Commit();

                //return r;
            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                throw e;
            }
        }

        private void insertSlicedExposedOfPlatformOfSinglePlacedComponent(string name, string place_name, InnerComponent ic, Slice s, string id_platform, IDbConnection dbcon)
        {
            SliceExposed se = new SliceExposed();
            se.Id_abstract = ic.Id_abstract_owner;
            se.Id_inner = place_name;
            se.Id_inner_original = id_platform;
            se.Id_inner_owner = name;
            se.Id_interface_host = s.Id_interface;
            se.Id_interface_slice = "node";
            se.Id_interface_slice_original = "node";
            se.Id_interface_slice_owner = s.Id_interface_slice;
            Backend.sedao.insert(se, dbcon);
        }

        private string insertPlatformOfComputationOrDataSourceInCatalog(string name, string place_name, int id_abstract, IDbConnection dbcon)
        {
            string id_platform = null;

            // Look for the name of the inner platform.
            IList<InnerComponent> ic_list = Backend.icdao.list(id_abstract, dbcon);
            foreach (InnerComponent icx in ic_list)
                if (icx.Id_inner.StartsWith("platform"))
                {
                    id_platform = icx.Id_inner;
                    break;
                }

            InnerComponentExposed ice = new InnerComponentExposed();
            ice.Id_abstract = id_abstract_system;
            ice.Id_inner_owner = name;
            ice.Id_inner = id_platform;
            ice.Id_inner_rename = place_name;
            Backend.icedao.insert(ice, dbcon);

            return id_platform;
        }

        public void insertParameter(string parameter_id, string contractual_restriction)
        {
            insertParameter(parameter_id, ParameterClass.application.ToString(), contractual_restriction);
        }

        public void insertParameter(string parameter_id, string parameter_class, string contractual_restriction)
        {
            IDbConnection dbcon = DBConnector.createConnection();
            dbcon.Open();

            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                if (!AbstractComponentFunctorParameter.parameterClass.ContainsKey(parameter_class))
                    throw new InvalidParameterClassException(parameter_class, system_component.ID);

                IDriverContract contract = LoaderApp.deserialize<IDriverContract>(contractual_restriction);
                object new_bound = mkContract(contract.library_path, contract.Items, dbcon);

                AbstractComponentFunctorParameter acfp = new AbstractComponentFunctorParameter();
                acfp.Id_abstract = id_abstract_system;
                acfp.Id_parameter = parameter_id;
                acfp.Bounds_of = Backend.createFunctorAppInner(id_abstract_system, new_bound, dbcon); ;
                acfp.Variance = acfp.Id_parameter.Equals("name") ? VarianceType.covariant : VarianceType.contravariant;
                acfp.ParameterClass = AbstractComponentFunctorParameter.parameterClass[parameter_class];
                Backend.acfpdao.insert(acfp, dbcon);

                dbtrans.Commit();
            }
            catch (HPCShelfException e)
            {
                dbtrans.Rollback();
                throw WebServicesException.raiseSoapException(e, "CoreServices.insertParameter");
            }
            catch (SoapException e)
            {
                dbtrans.Rollback();
                throw e;
            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.insertParameter", WebServicesException.FaultCode.Server);
            }

        }

        private void insertWorkflowActionPortInCatalog(string name, IDictionary<string, object> new_contract, IDbConnection dbcon)
        {
            string library_path = (string)new_contract["name"];
            AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath(library_path, dbcon);

            int id_functor_app = Backend.createFunctorAppInner(new_contract, dbcon);

            InnerComponent ic = new InnerComponent();
            ic.Id_abstract_owner = id_abstract_workflow; ;
            ic.Id_inner = name;
            ic.Id_functor_app = id_functor_app;
            ic.Id_abstract_inner = /*t.Item1*/ acf.Id_abstract;
            ic.IsPublic = true;
            ic.Transitive = false;
            ic.Parameter_top = "";
            Backend.icdao.insert(ic, dbcon);

            InnerComponent ic0 = new InnerComponent();
            ic0.Id_abstract_owner = id_abstract_system; ;
            ic0.Id_inner = name;
            ic0.Id_functor_app = id_functor_app;
            ic0.Id_abstract_inner = /*t.Item1*/ acf.Id_abstract;
            ic0.IsPublic = false;
            ic0.Transitive = false;
            ic0.Parameter_top = "";
            Backend.icdao.insert(ic0, dbcon);

            InnerComponentExposed ice0x = new InnerComponentExposed();
            ice0x.Id_abstract = id_abstract_system; ;
            ice0x.Id_inner_owner = "workflow";
            ice0x.Id_inner = name;
            ice0x.Id_inner_rename = name;
            Backend.icedao.insert(ice0x, dbcon);

            Slice s = new Slice();
            s.Id_abstract = ic.Id_abstract_owner;
            s.Id_inner = ic.Id_inner;
            s.Id_interface = "workflow";
            s.Id_interface_slice = "peer";
            s.PortName = name.Substring(0, 1).ToUpper() + name.Substring(1);
            s.Transitive = false;
            Backend.sdao.insert(s, dbcon);

            Slice s0 = new Slice();
            s0.Id_abstract = id_abstract_system; ;
            s0.Id_inner = name;
            s0.Id_interface = "peer_0";
            s0.Id_interface_slice = "peer";
            s0.PortName = name.Substring(0, 1).ToUpper() + name.Substring(1);
            s0.Transitive = true;
            Backend.sdao.insert(s0, dbcon);

            SliceExposed se0 = new SliceExposed();
            se0.Id_abstract = id_abstract_system;
            se0.Id_inner = ic.Id_inner;
            se0.Id_inner_original = ic.Id_inner;
            se0.Id_inner_owner = "workflow";
            se0.Id_interface_host = "peer_0";
            se0.Id_interface_slice = "peer";
            se0.Id_interface_slice_original = "peer";
            se0.Id_interface_slice_owner = "workflow";
            Backend.sedao.insert(se0, dbcon);

            updateResolutionTree(this.system_component, dbcon);
            updateResolutionTree(this.system_component.InnerComponents["workflow"][0].Item1, dbcon); // of workflow
        }

        private void insertApplicationServicePortInCatalog(string name, IDictionary<string, object> new_contract, string role, IDbConnection dbcon)
        {
            int id_functor_app = Backend.createFunctorAppInner(new_contract, dbcon);

            string library_path = (string)new_contract["name"];
            AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath(library_path, dbcon);
            
            InnerComponent ic = new InnerComponent();
            ic.Id_abstract_owner = id_abstract_application; ;
            ic.Id_inner = name;
            ic.Id_functor_app = id_functor_app;
            ic.Id_abstract_inner = acf.Id_abstract /* t.Item1*/;
            ic.IsPublic = true;
            ic.Transitive = false;
            ic.Parameter_top = "";
            Backend.icdao.insert(ic, dbcon);

            InnerComponent ic0 = new InnerComponent();
            ic0.Id_abstract_owner = id_abstract_system; ;
            ic0.Id_inner = name;
            ic0.Id_functor_app = id_functor_app;
            ic0.Id_abstract_inner = acf.Id_abstract /* t.Item1*/;
            ic0.IsPublic = false;
            ic0.Transitive = false;
            ic0.Parameter_top = "";
            Backend.icdao.insert(ic0, dbcon);

            InnerComponentExposed ice0x = new InnerComponentExposed();
            ice0x.Id_abstract = id_abstract_system; ;
            ice0x.Id_inner_owner = "application";
            ice0x.Id_inner = name;
            ice0x.Id_inner_rename = name;
            Backend.icedao.insert(ice0x, dbcon);

            Slice s = new Slice();
            s.Id_abstract = ic.Id_abstract_owner;
            s.Id_inner = ic.Id_inner;
            s.Id_interface = "application";
            s.Id_interface_slice = role;
            s.PortName = name.Substring(0, 1).ToUpper() + name.Substring(1);
            s.Transitive = false;
            Backend.sdao.insert(s, dbcon);

            Slice s0 = new Slice();
            s0.Id_abstract = id_abstract_system; ;
            s0.Id_inner = name;
            s0.Id_interface = "peer_0";
            s0.Id_interface_slice = role;
            s0.PortName = name.Substring(0, 1).ToUpper() + name.Substring(1);
            s0.Transitive = true;
            Backend.sdao.insert(s0, dbcon);

            SliceExposed se0 = new SliceExposed();
            se0.Id_abstract = id_abstract_system;
            se0.Id_inner = ic.Id_inner;
            se0.Id_inner_original = ic.Id_inner;
            se0.Id_inner_owner = "application";
            se0.Id_interface_host = "peer_0";
            se0.Id_interface_slice = role;
            se0.Id_interface_slice_original = role;
            se0.Id_interface_slice_owner = "application";
            Backend.sedao.insert(se0, dbcon);

            //DBConnector.closeConnection();

            updateResolutionTree(this.system_component, dbcon);
            updateResolutionTree(this.system_component.InnerComponents["application"][0].Item1, dbcon); // of application ...
        }

        private string library_path_concrete_system;

        private string insertConcreteSystemInCatalog(IDriverComponent solution_component, IDbConnection dbcon)
        {
            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                IDriverContextArgument[] contract = solution_component.context_argument;

                int id_functor_app_concrete = Backend.createFunctorAppInner((IDictionary<string, object>)mkContract(library_path_abstract_system, /*new object[1] {*/ contract /*}*/, dbcon), dbcon); // default contract;

                org.hpcshelf.database.Component c = new org.hpcshelf.database.Component();
                c.Id_concrete = DBConnector.nextKey("id_concrete", "component", dbcon); ;
                c.Library_path = library_path_concrete_system = solution_component.library_path != null ? solution_component.library_path + "Impl" : "temporary.system.impl.System" + c.Id_concrete + "Impl";
                c.Id_concrete_supertype = 0;
                c.Id_functor_app = id_functor_app_concrete;
                c.SetId_abstract(id_abstract_system) ;
                c.Hash_component_UID = FileUtil.create_snk_file(library_path_concrete_system + ".snk", library_path_concrete_system + ".pub");
                Backend.cdao.insert(c, dbcon);

                ComponentKey ck = new ComponentKey();
                ck.IDType = 'c';
                ck.ID = c.Id_concrete;
                ck.SNKContents = File.ReadAllBytes(library_path_concrete_system + ".snk");
                ck.PUBContents = File.ReadAllBytes(library_path_concrete_system + ".pub");
                Backend.ckdao.insert(ck, dbcon);

                Unit u = new Unit();
                u.Id_concrete = c.Id_concrete;
                u.Id_unit = "peer_0";
                u.Id_interface = "peer_0";
                u.Id_abstract = id_abstract_system; ;
                u.Id_unit_super = "peer_0";
                u.Class_name = this.library_path_concrete_system + ".IPeer0Impl";
                u.Assembly_string = u.Class_name + ", Culture=neutral, Version=0.0.0.0, PublicKey=" + c.Hash_component_UID;
                u.URI_Source = "";
                u.Class_nargs = 0;
                u.Order = 1;
                Backend.udao.insert(u, dbcon);

                SourceCode sc = new SourceCode();
                sc.Type_owner = 'u';
                sc.Id_owner = "peer_0";
                sc.Contents = "using System;\nusing org.hpcshelf.DGAC;\nusing org.hpcshelf.unit;\nusing org.hpcshelf.kinds;\nusing " + library_path_abstract_system + ";\n\nnamespace " + library_path_concrete_system + "\n{\n\tpublic class IPeer0Impl : org.hpcshelf.kinds.Application, IPeer0\n\t{\n\t\tprivate void Go(Object o)\n\t\t{\n\t\t\t((Activate)(o)).go();\n\t\t}\n\n\t\tpublic override void main()\n\t\t{\n\t\t}\n\t}\n}\n";
                sc.File_name = "IPeer0Impl.cs";
                sc.File_type = "user";
                sc.Language = "C# Language";
                sc.Id_owner_container = c.Id_concrete;
                sc.Order = 0;
                sc.Module_name = library_path_concrete_system + ".IPeer0Impl";
                sc.DeployType = "C# Wrapper";
                sc.Language = "C# Language";
                Backend.scdao.insert(sc, dbcon);

                Resolution.addToResolutionTree(c, dbcon);

                dbtrans.Commit();
            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                throw e;
            }

            return library_path_concrete_system;
        }

        private string library_path_abstract_system = null;
        private int id_abstract_system;

        private string insertAbstractSystemInCatalog(IDriverComponent solution_component, IDbConnection dbcon /* IDictionary<string, Tuple<string, object>> contextual_signature*/)
        {
            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                IDriverContextArgument[] contract = solution_component.context_argument;

                // CREATE component
                AbstractComponentFunctor acf = new AbstractComponentFunctor();
                acf.Id_abstract = id_abstract_system = DBConnector.nextKey("id_abstract", "abstractcomponentfunctor", dbcon);
                acf.Library_path = library_path_abstract_system = solution_component.library_path != null ? solution_component.library_path : "temporary.system.System" + acf.Id_abstract;
                acf.Kind = Constants.KIND_APPLICATION_NAME;
                acf.Id_functor_app_supertype = 0;
                string publicKey = FileUtil.create_snk_file(library_path_abstract_system + ".snk", library_path_abstract_system + ".pub");
                acf.Hash_component_UID = publicKey;
                Backend.acfdao.insert(acf, dbcon);

                ComponentKey ck = new ComponentKey();
                ck.IDType = 'a';
                ck.ID = acf.Id_abstract;
                ck.SNKContents = File.ReadAllBytes(library_path_abstract_system + ".snk");
                ck.PUBContents = File.ReadAllBytes(library_path_abstract_system + ".pub");
                Backend.ckdao.insert(ck, dbcon);

                // CREATE contextual signature
                if (contract != null)
                    foreach (IDriverContextArgument context_argument in contract /*KeyValuePair<string, Tuple<string, object>> context_parameter in contextual_signature*/)
                    {
                        AbstractComponentFunctorParameter acfp = new AbstractComponentFunctorParameter();
                        acfp.Id_abstract = acf.Id_abstract;
                        acfp.Id_parameter = context_argument.parameter_id;
                        acfp.Bounds_of = Backend.createFunctorAppInner(id_abstract_system, mkContract(context_argument.library_path, context_argument.Items, dbcon), dbcon);
                        acfp.Variance = acfp.Id_parameter.Equals("name") ? VarianceType.covariant : VarianceType.contravariant;
                        acfp.ParameterClass = ParameterClass.application;
                        Backend.acfpdao.insert(acfp, dbcon);
                    }

                // CREATE interface peer_0 (root)
                Interface i = new Interface();
                i.Id_abstract = acf.Id_abstract;
                i.Id_interface = "peer_0";
                i.Facet = 0;
                i.Id_interface_super = "peer_0";
                i.Class_name = this.library_path_abstract_system + ".IPeer0";
                i.Assembly_string = i.Class_name + ", Culture=neutral, Version=0.0.0.0, PublicKey=" + acf.Hash_component_UID;
                i.URI_Source = "";
                i.Class_nargs = 0;
                i.SetId_interface_super_top("peer_0");
                i.Order = 1;
                //i.Actions;
                //i.Conditions;
                //i.Protocol;
                i.Is_parallel = false;
                Backend.idao.insert(i, dbcon);

                SourceCode sc = new SourceCode();
                sc.Type_owner = 'i';
                sc.Id_owner = "peer_0";
                sc.Contents = "using org.hpcshelf.kinds;\n\nnamespace " + library_path_abstract_system + "\n{\n\tpublic interface IPeer0 : IApplicationKind\n\t{\n\t}\n}";
                sc.File_name = "IPeer0.cs";
                sc.File_type = "user";
                sc.Language = "C# Language";
                sc.Id_owner_container = acf.Id_abstract;
                sc.Order = 0;
                sc.Module_name = library_path_abstract_system + ".IPeer0";
                sc.DeployType = "C# Wrapper";
                sc.Language = "C# Language";
                Backend.scdao.insert(sc, dbcon);

                dbtrans.Commit();
            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                throw e;
            }

            return library_path_abstract_system;
        }


        private string library_path_concrete_workflow;

        private string insertConcreteWorkflowInCatalog(IDbConnection dbcon)
        {
            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                AbstractComponentFunctorApplication acfa = new AbstractComponentFunctorApplication();
                acfa.Id_abstract = id_abstract_workflow;
                acfa.Id_functor_app_next = 0;
                int id_functor_app_concrete = Backend.acfadao.insert(acfa, dbcon);

                org.hpcshelf.database.Component c = new org.hpcshelf.database.Component();
                c.Id_concrete = DBConnector.nextKey("id_concrete", "component", dbcon); ;
                c.Library_path = library_path_concrete_workflow = "temporary.application.impl.Workflow" + c.Id_concrete + "Impl";
                c.Id_concrete_supertype = 0;
                c.Id_functor_app = id_functor_app_concrete;
                c.SetId_abstract(id_abstract_workflow);
                c.Hash_component_UID = FileUtil.create_snk_file(library_path_concrete_workflow + ".snk", library_path_concrete_workflow + ".pub"); ;
                Backend.cdao.insert(c, dbcon);

                ComponentKey ck = new ComponentKey();
                ck.IDType = 'c';
                ck.ID = c.Id_concrete;
                ck.SNKContents = File.ReadAllBytes(library_path_concrete_workflow + ".snk");
                ck.PUBContents = File.ReadAllBytes(library_path_concrete_workflow + ".pub");
                Backend.ckdao.insert(ck, dbcon);

                Unit u = new Unit();
                u.Id_concrete = c.Id_concrete;
                u.Id_unit = "workflow";
                u.Id_interface = "workflow";
                u.Id_abstract = id_abstract_workflow;
                u.Id_unit_super = "workflow";
                u.Class_name = library_path_concrete_workflow + ".IWorkflowImpl";
                u.Assembly_string = u.Class_name + ", Culture=neutral, Version=0.0.0.0, PublicKey=" + c.Hash_component_UID;
                u.URI_Source = "";
                u.Class_nargs = 0;
                u.Order = 1;
                Backend.udao.insert(u, dbcon);

                SourceCode sc = new SourceCode();
                sc.Type_owner = 'u';
                sc.Id_owner = "workflow";
                sc.Contents = "using System;\nusing org.hpcshelf.DGAC;\nusing org.hpcshelf.unit;\nusing org.hpcshelf.kinds;\nusing " + library_path_abstract_workflow + ";\n\nnamespace " + library_path_concrete_workflow + "\n{\n\tpublic class IWorkflowImpl : org.hpcshelf.kinds.Workflow, IWorkflow\n\t{\n\t\tpublic override void main()\n\t\t{\n\t\t}\n\t}\n}\n";
                sc.File_name = "IWorkflowImpl.cs";
                sc.File_type = "user";
                sc.Language = "C# Language";
                sc.Id_owner_container = c.Id_concrete;
                sc.Order = 0;
                sc.Module_name = library_path_concrete_workflow + ".IWorkflowImpl";
                sc.DeployType = "C# Wrapper";
                sc.Language = "C# Language";
                Backend.scdao.insert(sc, dbcon);

                Resolution.addToResolutionTree(c, dbcon);

                dbtrans.Commit();
            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                throw e;
            }

            return library_path_concrete_workflow;
        }

        private int id_abstract_workflow;
        private string library_path_abstract_workflow;

        private string insertAbstractWorkflowInCatalog(IDbConnection dbcon)
        {
            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                // CREATE the component
                AbstractComponentFunctor acf = new AbstractComponentFunctor();
                acf.Id_abstract = id_abstract_workflow = org.hpcshelf.database.DBConnector.nextKey("id_abstract", "abstractcomponentfunctor", dbcon);
                acf.Library_path = library_path_abstract_workflow = "temporary.workflow.Workflow" + acf.Id_abstract;
                acf.Kind = Constants.KIND_COMPUTATION_NAME;
                acf.Id_functor_app_supertype = 0;
                string publicKey = FileUtil.create_snk_file(library_path_abstract_workflow + ".snk", library_path_abstract_workflow + ".pub");
                acf.Hash_component_UID = publicKey;
                Backend.acfdao.insert(acf, dbcon);

                //this.Contract["name"] = library_path_abstract_workflow;

                ComponentKey ck = new ComponentKey();
                ck.IDType = 'a';
                ck.ID = acf.Id_abstract;
                ck.SNKContents = File.ReadAllBytes(library_path_abstract_workflow + ".snk");
                ck.PUBContents = File.ReadAllBytes(library_path_abstract_workflow + ".pub");
                Backend.ckdao.insert(ck, dbcon);

                AbstractComponentFunctorApplication acfa = new AbstractComponentFunctorApplication();
                acfa.Id_abstract = acf.Id_abstract;
                acfa.Id_functor_app_next = 0;
                int id_functor_app_inner = Backend.acfadao.insert(acfa, dbcon);

                // CREATE THE INNER COMPONENT wokflow with repect to the system
                InnerComponent ic1 = new InnerComponent();
                ic1.Id_abstract_owner = id_abstract_system;
                ic1.Id_abstract_inner = acf.Id_abstract;
                ic1.Id_functor_app = id_functor_app_inner;
                ic1.Id_inner = Constants.WORKFLOW_COMPONENT_ID;
                ic1.IsPublic = false;
                ic1.Multiple = false;
                ic1.Parameter_top = "";
                ic1.Transitive = false;
                Backend.icdao.insert(ic1, dbcon);

                // CREATE THE INNER COMPONENT platform_SAFe with respect to the workflow
                InnerComponent ic2 = new InnerComponent();
                ic2.Id_abstract_owner = acf.Id_abstract;
                ic2.Id_abstract_inner = id_abstract_platform_SAFe;
                ic2.Id_functor_app = id_functor_app_platform_SAFe;
                ic2.Id_inner = Constants.PLATFORM_SAFE_ID;
                ic2.IsPublic = true;
                ic2.Multiple = false;
                ic2.Parameter_top = "";
                ic2.Transitive = false;
                Backend.icdao.insert(ic2, dbcon);

                // CREATE THE INNER COMPONENT EXPOSED for platform_SAFe
                InnerComponentExposed ice = new InnerComponentExposed();
                ice.Id_abstract = ic1.Id_abstract_owner;
                ice.Id_inner_owner = "workflow";
                ice.Id_inner = Constants.PLATFORM_SAFE_ID;
                ice.Id_inner_rename = Constants.PLATFORM_SAFE_ID;
                Backend.icedao.insert(ice, dbcon);

                // CREATE the interface
                Interface i = new Interface();
                i.Id_abstract = acf.Id_abstract;
                i.Id_interface = "workflow";
                i.Facet = 0;
                i.Id_interface_super = "";
                i.Class_name = this.library_path_abstract_workflow + ".IWorkflow";
                i.Assembly_string = i.Class_name + ", Culture=neutral, Version=0.0.0.0, PublicKey=" + acf.Hash_component_UID;
                i.URI_Source = "";
                i.Class_nargs = 0;
                i.SetId_interface_super_top ("workflow");
                i.Order = 1;
                //i.Actions;
                //i.Conditions;
                //i.Protocol;
                i.Is_parallel = false;
                Backend.idao.insert(i, dbcon);

                SourceCode sc = new SourceCode();
                sc.Type_owner = 'i';
                sc.Id_owner = "workflow";
                sc.Contents = "using org.hpcshelf.kinds;\n\nnamespace " + library_path_abstract_workflow + "\n{\n\tpublic interface IWorkflow : IWorkflowKind\n\t{\n\t}\n}";
                sc.File_name = "IWorkflow.cs";
                sc.File_type = "user";
                sc.Language = "C# Language";
                sc.Id_owner_container = acf.Id_abstract;
                sc.Order = 0;
                sc.Module_name = library_path_abstract_workflow + ".IWorkflow";
                sc.DeployType = "C# Wrapper";
                sc.Language = "C# Language";
                Backend.scdao.insert(sc, dbcon);

                // CREATE THE SLICE for workflow with respect to the system
                Slice s1 = new Slice();
                s1.Id_abstract = ic1.Id_abstract_owner;
                s1.Id_inner = "workflow";
                s1.Id_interface = "peer_0";
                s1.Id_interface_slice = "workflow";
                s1.Transitive = false;
                s1.PortName = "Workflow";
                Backend.sdao.insert(s1, dbcon);

                // CREATE  THE SLICE for platform_SAFe with respect to the workflow
                Slice s2 = new Slice();
                s2.Id_abstract = acf.Id_abstract;
                s2.Id_inner = Constants.PLATFORM_SAFE_ID;
                s2.Id_interface = "workflow";
                s2.Id_interface_slice = "node";
                s2.Transitive = false;
                s2.PortName = "Platform_SAFe";
                Backend.sdao.insert(s2, dbcon);

                // CREATE THE SICE EXPOSED for platform_SAFe with respect to the system component
                SliceExposed se = new SliceExposed();
                se.Id_abstract = ic1.Id_abstract_owner;
                se.Id_inner = Constants.PLATFORM_SAFE_ID;
                se.Id_inner_original = Constants.PLATFORM_SAFE_ID;
                se.Id_inner_owner = "workflow";
                se.Id_interface_host = "peer_0";
                se.Id_interface_slice = "node";
                se.Id_interface_slice_original = "node";
                se.Id_interface_slice_owner = "workflow";
                Backend.sedao.insert(se, dbcon);

                dbtrans.Commit();

            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                throw e;
            }

            return library_path_abstract_workflow;
        }

        private string library_path_concrete_application;

        private string insertConcreteApplicationInCatalog(IDbConnection dbcon)
        {
            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                AbstractComponentFunctorApplication acfa = new AbstractComponentFunctorApplication();
                acfa.Id_abstract = id_abstract_application; ;
                acfa.Id_functor_app_next = 0;
                int id_functor_app_concrete = Backend.acfadao.insert(acfa, dbcon);

                org.hpcshelf.database.Component c = new org.hpcshelf.database.Component();
                c.Id_concrete = DBConnector.nextKey("id_concrete", "component", dbcon); ;
                c.Library_path = library_path_concrete_application = "temporary.application.impl.Application" + c.Id_concrete + "Impl";
                c.Id_concrete_supertype = 0;
                c.Id_functor_app = id_functor_app_concrete;
                c.SetId_abstract(id_abstract_application);
                c.Hash_component_UID = FileUtil.create_snk_file(library_path_concrete_application + ".snk", library_path_concrete_application + ".pub"); ;
                Backend.cdao.insert(c, dbcon);

                ComponentKey ck = new ComponentKey();
                ck.IDType = 'c';
                ck.ID = c.Id_concrete;
                ck.SNKContents = File.ReadAllBytes(library_path_concrete_application + ".snk");
                ck.PUBContents = File.ReadAllBytes(library_path_concrete_application + ".pub");
                Backend.ckdao.insert(ck, dbcon);

                Unit u = new Unit();
                u.Id_concrete = c.Id_concrete;
                u.Id_unit = "application";
                u.Id_interface = "application";
                u.Id_abstract = id_abstract_application; ;
                u.Id_unit_super = "application";
                u.Class_name = this.library_path_concrete_application + ".IApplicationImpl";
                u.Assembly_string = u.Class_name + ", Culture=neutral, Version=0.0.0.0, PublicKey=" + c.Hash_component_UID;
                u.URI_Source = "";
                u.Class_nargs = 0;
                u.Order = 1;
                Backend.udao.insert(u, dbcon);

                SourceCode sc = new SourceCode();
                sc.Type_owner = 'u';
                sc.Id_owner = "application";
                sc.Contents = "using System;\nusing org.hpcshelf.DGAC;\nusing org.hpcshelf.unit;\nusing org.hpcshelf.kinds;\nusing " + library_path_abstract_application + ";\n\nnamespace " + library_path_concrete_application + "\n{\n\tpublic class IApplicationImpl : org.hpcshelf.kinds.Environment, IApplication\n\t{\n\t}\n}\n";
                sc.File_name = "IApplicationImpl.cs";
                sc.File_type = "user";
                sc.Language = "C# Language";
                sc.Id_owner_container = c.Id_concrete;
                sc.Order = 0;
                sc.Module_name = library_path_concrete_application + ".IApplicationImpl";
                sc.DeployType = "C# Wrapper";
                sc.Language = "C# Language";

                Backend.scdao.insert(sc, dbcon);

                Resolution.addToResolutionTree(c, dbcon);

                dbtrans.Commit();
            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                throw e;
            }

            return library_path_concrete_application;
        }

        private int id_abstract_application;
        private string library_path_abstract_application;

        private string insertAbstractApplicationInCatalog(IDbConnection dbcon)
        {
            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                // CREATE the component
                AbstractComponentFunctor acf = new AbstractComponentFunctor();
                acf.Id_abstract = id_abstract_application = DBConnector.nextKey("id_abstract", "abstractcomponentfunctor", dbcon);
                acf.Library_path = library_path_abstract_application = "temporary.application.Application" + acf.Id_abstract;
                acf.Kind = Constants.KIND_ENVIRONMENT_NAME;
                acf.Id_functor_app_supertype = 0;
                string publicKey = FileUtil.create_snk_file(library_path_abstract_application + ".snk", library_path_abstract_application + ".pub");
                acf.Hash_component_UID = publicKey;
                Backend.acfdao.insert(acf, dbcon);

                ComponentKey ck = new ComponentKey();
                ck.IDType = 'a';
                ck.ID = acf.Id_abstract;
                ck.SNKContents = File.ReadAllBytes(library_path_abstract_application + ".snk");
                ck.PUBContents = File.ReadAllBytes(library_path_abstract_application + ".pub");
                Backend.ckdao.insert(ck, dbcon);

                AbstractComponentFunctorApplication acfa = new AbstractComponentFunctorApplication();
                acfa.Id_abstract = acf.Id_abstract;
                acfa.Id_functor_app_next = 0;
                int id_functor_app_inner = Backend.acfadao.insert(acfa, dbcon);

                // CREATE THE INNER COMPONENT wokflow with repect to the system
                InnerComponent ic1 = new InnerComponent();
                ic1.Id_abstract_owner = id_abstract_system;
                ic1.Id_abstract_inner = acf.Id_abstract;
                ic1.Id_functor_app = id_functor_app_inner;
                ic1.Id_inner = Constants.APPLICATION_COMPONENT_ID;
                ic1.IsPublic = false;
                ic1.Multiple = false;
                ic1.Parameter_top = "";
                ic1.Transitive = false;
                Backend.icdao.insert(ic1, dbcon);

                // CREATE THE INNER COMPONENT platform_SAFe with respect to the workflow
                InnerComponent ic2 = new InnerComponent();
                ic2.Id_abstract_owner = acf.Id_abstract;
                ic2.Id_abstract_inner = id_abstract_platform_SAFe;
                ic2.Id_functor_app = id_functor_app_platform_SAFe;
                ic2.Id_inner = Constants.PLATFORM_SAFE_ID;
                ic2.IsPublic = true;
                ic2.Multiple = false;
                ic2.Parameter_top = "";
                ic2.Transitive = false;
                Backend.icdao.insert(ic2, dbcon);

                // CREATE THE INNER COMPONENT EXPOSED for platform_SAFe
                InnerComponentExposed ice = new InnerComponentExposed();
                ice.Id_abstract = ic1.Id_abstract_owner;
                ice.Id_inner_owner = "application";
                ice.Id_inner = Constants.PLATFORM_SAFE_ID;
                ice.Id_inner_rename = Constants.PLATFORM_SAFE_ID;
                Backend.icedao.insert(ice, dbcon);

                // CREATE the interface
                Interface i = new Interface();
                i.Id_abstract = acf.Id_abstract;
                i.Id_interface = "application";
                i.Facet = 0;
                i.Id_interface_super = "";
                i.Class_name = library_path_abstract_application + ".IApplication";
                i.Assembly_string = i.Class_name + ", Culture=neutral, Version=0.0.0.0, PublicKey=" + acf.Hash_component_UID;
                i.URI_Source = "";
                i.Class_nargs = 0;
                i.SetId_interface_super_top ("application");
                i.Order = 1;
                //i.Actions;
                //i.Conditions;
                //i.Protocol;
                i.Is_parallel = false;
                Backend.idao.insert(i, dbcon);

                SourceCode sc = new SourceCode();
                sc.Type_owner = 'i';
                sc.Id_owner = "application";
                sc.Contents = "using org.hpcshelf.kinds;\n\nnamespace " + library_path_abstract_application + "\n{\n\tpublic interface IApplication : IEnvironmentKind\n\t{\n\t}\n}";
                sc.File_name = "IApplication.cs";
                sc.File_type = "user";
                sc.Language = "C# Language";
                sc.Id_owner_container = acf.Id_abstract;
                sc.Order = 0;
                sc.Module_name = library_path_abstract_application + ".IApplication";
                sc.DeployType = "C# Wrapper";
                sc.Language = "C# Language";
                Backend.scdao.insert(sc, dbcon);

                // CREATE THE SLICE for workflow with respect to the system
                Slice s1 = new Slice();
                s1.Id_abstract = ic1.Id_abstract_owner;
                s1.Id_inner = "application";
                s1.Id_interface = "peer_0";
                s1.Id_interface_slice = "application";
                s1.Transitive = false;
                s1.PortName = "Application";
                Backend.sdao.insert(s1, dbcon);

                // CREATE  THE SLICE for platform_SAFe with respect to the workflow
                Slice s2 = new Slice();
                s2.Id_abstract = acf.Id_abstract;
                s2.Id_inner = Constants.PLATFORM_SAFE_ID;
                s2.Id_interface = "application";
                s2.Id_interface_slice = "node";
                s2.Transitive = false;
                s2.PortName = "Platform_SAFe";
                Backend.sdao.insert(s2, dbcon);

                // CREATE THE SICE EXPOSED for platform_SAFe with respect to the system component
                SliceExposed se = new SliceExposed();
                se.Id_abstract = ic1.Id_abstract_owner;
                se.Id_inner = Constants.PLATFORM_SAFE_ID;
                se.Id_inner_original = Constants.PLATFORM_SAFE_ID;
                se.Id_inner_owner = "application";
                se.Id_interface_host = "peer_0";
                se.Id_interface_slice = "node";
                se.Id_interface_slice_original = "node";
                se.Id_interface_slice_owner = "application";
                Backend.sedao.insert(se, dbcon);

                dbtrans.Commit();
            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                throw e;
            }


            return library_path_abstract_application;
        }

        private int id_abstract_platform_SAFe;
        private int id_functor_app_platform_SAFe;

        private int insertPlatformSAFeInCatalog(IDriverComponent solution_component, IDbConnection dbcon)
        {
            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                AbstractComponentFunctor acf1 = Backend.acfdao.retrieve_libraryPath("org.hpcshelf.platform.Platform", dbcon);
                AbstractComponentFunctor acf2 = Backend.acfdao.retrieve_libraryPath("org.hpcshelf.platform.maintainer.SAFeHost", dbcon);

                id_abstract_platform_SAFe = acf1.Id_abstract;

                AbstractComponentFunctorApplication acfa1 = new AbstractComponentFunctorApplication();
                acfa1.Id_abstract = acf1.Id_abstract;
                acfa1.Id_functor_app_next = -1;
                int id_functor_app_1 = id_functor_app_platform_SAFe = Backend.acfadao.insert(acfa1, dbcon);

                AbstractComponentFunctorApplication acfa2 = new AbstractComponentFunctorApplication();
                acfa2.Id_abstract = acf2.Id_abstract;
                acfa2.Id_functor_app_next = -1;
                int id_functor_app_2 = Backend.acfadao.insert(acfa2, dbcon);

                SupplyParameterComponent spc = new SupplyParameterComponent();
                spc.Id_functor_app = id_functor_app_1;
                spc.Id_functor_app_actual = id_functor_app_2;
                spc.Id_parameter = "maintainer";
                Backend.spdao.insert(spc, dbcon);

                // CREATE THE INNER COMPONENT of platform_SAFe 
                InnerComponent ic = new InnerComponent();
                ic.Id_abstract_owner = id_abstract_system;
                ic.Id_abstract_inner = acf1.Id_abstract;
                ic.Id_functor_app = id_functor_app_1;
                ic.Id_inner = Constants.PLATFORM_SAFE_ID;
                ic.IsPublic = false;
                ic.Multiple = false;
                ic.Parameter_top = "";
                ic.Transitive = true;
                Backend.icdao.insert(ic, dbcon);


                // CREATE THE SLICE of platform_SAFe 
                Slice s = new Slice();
                s.Id_abstract = id_abstract_system;
                s.Id_inner = Constants.PLATFORM_SAFE_ID;
                s.Id_interface = "peer_0";
                s.Id_interface_slice = "node";
                s.Transitive = true;
                s.PortName = "Platform_SAFe";
                Backend.sdao.insert(s, dbcon);

                dbtrans.Commit();
            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                throw e;
            }

            return id_functor_app_platform_SAFe;
        }


        private bool isPlacedIn(string name, string place)
        {
            Tuple<int, string>[] places;
            if (platform_placement_list.TryGetValue(name, out places))
                foreach (Tuple<int, string> current_place in places)
                    if (current_place.Item2.Equals(place))
                        return true;

            return false;
        }

        private void updatePlatforms(string name)
        {
            // UPDATE Platforms
            platforms.Add(name);

            // UPDATE FacetList
            int facet_instance_count = FacetInstanceCount;
            IList<int>[] system_facet_list = new IList<int>[facet_instance_count];
            system_component.FacetList.CopyTo(system_facet_list, 0);
            system_facet_list[facet_instance_count - 1] = new List<int>();
            system_facet_list[facet_instance_count - 1].Add(facet_instance_count - 1);
            system_component.FacetList = system_facet_list;

            // UPDATE UNIT MAPPING
            int facet = platforms.IndexOf(name);
            Instantiator.UnitMappingType new_unit_mapping = Backend.buildUnitMappingSystem("peer_" + facet, "peer_" + facet, facet, null);
            Instantiator.UnitMappingType[] old_unit_mapping_list = system_component.UnitMapping[0];
            Instantiator.UnitMappingType[] new_unit_mapping_list = new Instantiator.UnitMappingType[old_unit_mapping_list.Length + 1];
            old_unit_mapping_list.CopyTo(new_unit_mapping_list, 0);
            new_unit_mapping_list[old_unit_mapping_list.Length] = new_unit_mapping;
            system_component.UnitMapping[0] = new_unit_mapping_list;

            deployed_list[name] = new ConcurrentDictionary<string, ISet<int>>();
        }


        public void updateContract(string contract_xml)
        {
            IDbConnection dbcon = DBConnector.createConnection();
            dbcon.Open();

            IDbTransaction dbtrans = dbcon.BeginTransaction();

            try
            {
                IDriverContract driver_contract = LoaderApp.deserialize<IDriverContract>(contract_xml);
                if (driver_contract.library_path == null)
                    driver_contract.library_path = library_path_abstract_system;

                IDictionary<string, object> contract = (IDictionary<string, object>)mkContract(driver_contract.library_path, driver_contract.Items, dbcon);

                int id_functor_app = Backend.createFunctorAppInner(id_abstract_system, contract, dbcon); ;

                IDictionary<string, int> arguments = new ConcurrentDictionary<string, int>();
                IDictionary<string, int> arguments_top;
                Backend.determineArguments(arguments, arguments, id_functor_app, out arguments_top, dbcon);

                system_component.ArgumentsTop = arguments_top;
                system_component.Arguments = new ConcurrentDictionary<string, int>(arguments_top);

                dbtrans.Commit();
            }
            catch (HPCShelfException e)
            {
                dbtrans.Rollback();
                throw WebServicesException.raiseSoapException(e, "CoreServices.updateContract");
            }
            catch (SoapException e)
            {
                dbtrans.Rollback();
                throw e;
            }
            catch (Exception e)
            {
                dbtrans.Rollback();
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.updateContract", WebServicesException.FaultCode.Server);
            }
        }

        private IList<Tuple<string, int, int, string>> fetchServicePorts(string library_path, string name, ref IList<Tuple<string, int, int, string>> result, IDbConnection dbcon)
        {
            AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath(library_path, dbcon);
            int id_abstract = acf.Id_abstract;

            AbstractComponentFunctor acf_service_binding_base = Backend.acfdao.retrieve_libraryPath("org.hpcshelf.binding.environment.EnvironmentBindingBase", dbcon);

            IList<InnerComponent> ic_list = Backend.icdao.list(id_abstract, dbcon);

            foreach (InnerComponent ic in ic_list)
                if (ic.IsPublic)
                {
                    int id_abstract_inner = ic.Id_abstract_inner;
                    if (Backend.acfdao.isSubtype(id_abstract_inner, acf_service_binding_base.Id_abstract, dbcon))
                    {
                        IList<Slice> s_list = Backend.sdao.listByInner(id_abstract, ic.Id_inner, dbcon);
                        // The returned list must have a single slice, because the client and server slices cannot be slices of units 
                        // in the same public inner component.
                        //foreach (Slice s in s_list)
                        Slice s = s_list[0];
                        {
                            Interface i_s = Backend.idao.retrieve(id_abstract, s.Id_interface, dbcon);

                            // CALCULATE the numbers of units for s.id_interface
                            IList<Slice> s_host_list = Backend.sdao.listByInterfaceSlice(id_abstract_system, name, s.Id_interface, dbcon);

                            for (int i = 0; i < s_host_list.Count; i++)
                                result.Add(new Tuple<string, int, int, string>(ic.Id_inner, i, i_s.Facet, s.GetId_interface_slice_top(dbcon)));
                        }
                    }
                }

            return result;
        }

        private IList<Tuple<string, int, int, string>> fetchActionPorts(string library_path, string name, ref IList<Tuple<string, int, int, string>> result, IDbConnection dbcon)
        {
            AbstractComponentFunctor acf = Backend.acfdao.retrieve_libraryPath(library_path, dbcon);
            int id_abstract = acf.Id_abstract;

            AbstractComponentFunctor acf_action_binding_base = Backend.acfdao.retrieve_libraryPath("org.hpcshelf.binding.task.TaskBindingBase", dbcon);

            IList<InnerComponent> ic_list = Backend.icdao.list(id_abstract, dbcon);

            foreach (InnerComponent ic in ic_list)
                if (ic.IsPublic)
                {
                    int id_abstract_inner = ic.Id_abstract_inner;
                    if (Backend.acfdao.isSubtype(id_abstract_inner, acf_action_binding_base.Id_abstract, dbcon))
                    {
                        IList<Slice> s_list = Backend.sdao.listByInner(id_abstract, ic.Id_inner, dbcon);
                        // The returned list must have a single slice, because the client and server slices cannot be slices of units 
                        // in the same public inner component.
                        //foreach (Slice s in s_list)
                        Slice s = s_list[0];
                        Interface i_s = Backend.idao.retrieve(id_abstract, s.Id_interface, dbcon);

                        {
                            // CALCULATE the numbers of units for s.id_interface
                            IList<Slice> s_host_list = Backend.sdao.listByInterfaceSlice(id_abstract_system, name, s.Id_interface, dbcon);

                            for (int i = 0; i < s_host_list.Count; i++)
                                result.Add(new Tuple<string, int, int, string>(ic.Id_inner, i, i_s.Facet, "peer"));
                        }
                    }
                }

            return result;
        }


        private static IDictionary<string, Tuple<int, string>[]> placementOfComponents1(int id_abstract_system, IDbConnection dbcon)
        {
            IDictionary<string, Tuple<int, string>[]> result = new ConcurrentDictionary<string, Tuple<int, string>[]>();

            IList<InnerComponent> ic_list = Backend.icdao.list(id_abstract_system, dbcon);

            result.Add(Constants.APPLICATION_COMPONENT_ID, new Tuple<int, string>[1] { new Tuple<int, string>(0, Constants.PLATFORM_SAFE_ID) });
            result.Add(Constants.WORKFLOW_COMPONENT_ID, new Tuple<int, string>[1] { new Tuple<int, string>(0, Constants.PLATFORM_SAFE_ID) });
            result.Add(Constants.SOURCE_COMPONENT_ID, new Tuple<int, string>[1] { new Tuple<int, string>(0, Constants.PLATFORM_SOURCE_ID) });

            foreach (InnerComponent ic in ic_list)
                placementOfComponents1_ic(ic, result, "%platform%", dbcon);

            return result;
        }

        private static void placementOfComponents1_ic(InnerComponent ic, IDictionary<string, Tuple<int, string>[]> result, string id_platform, IDbConnection dbcon)
        {
            if (!ic.Transitive || isActionBinding(ic, dbcon) || isServiceBinding(ic, dbcon))
            {
                AbstractComponentFunctor acf_inner = Backend.acfdao.retrieve(ic.Id_abstract_inner, dbcon);
                int kind = Constants.kindInt[acf_inner.Kind];
                if (kind == Constants.KIND_SYNCHRONIZER || kind == Constants.KIND_COMPUTATION)
                {
                    IList<Tuple<int, string>> facet_placement_list = Backend.fetchPlacement(ic.Id_abstract_owner, ic.Id_inner, id_platform, dbcon);

                    if (!result.ContainsKey(ic.Id_inner))
                        result.Add(ic.Id_inner, facet_placement_list.ToArray());

                    foreach (Tuple<int, string> t in facet_placement_list)
                    {
                        Console.WriteLine("PLACEMENT {0} --- {1} {2} ", ic.Id_inner, t.Item1, t.Item2);
                    }
                }
            }
        }

        private static IDictionary<string, Tuple<int, string>[]> placementOfComponents2(int id_abstract_system, IDbConnection dbcon)
        {
            IDictionary<string, Tuple<int, string>[]> result = new ConcurrentDictionary<string, Tuple<int, string>[]>();

            IList<InnerComponent> ic_list = Backend.icdao.list(id_abstract_system, dbcon);

            result.Add(Constants.CORE_CERTIFICATION_CONTROLLER_COMPONENT_ID, new Tuple<int, string>[1] { new Tuple<int, string>(0, Constants.PLATFORM_SAFE_ID) });
            result.Add(Constants.CERTIFICATION_WORKFLOW_COMPONENT_ID, new Tuple<int, string>[1] { new Tuple<int, string>(0, Constants.PLATFORM_SAFE_ID) });

            foreach (InnerComponent ic in ic_list)
                if (!ic.Transitive || isActionBinding(ic, dbcon) || isServiceBinding(ic, dbcon))
                {
                    AbstractComponentFunctor acf_inner = Backend.acfdao.retrieve(ic.Id_abstract_inner, dbcon);
                    int kind = Constants.kindInt[acf_inner.Kind];
                    if (kind == Constants.KIND_SYNCHRONIZER || kind == Constants.KIND_COMPUTATION || kind == Constants.KIND_TACTICAL)
                    {
                        IList<Tuple<int, string>> facet_placement_list = Backend.fetchPlacement(id_abstract_system, ic.Id_inner, "%platform%", dbcon);

                        if (!result.ContainsKey(ic.Id_inner))
                            result.Add(ic.Id_inner, facet_placement_list.ToArray());

                        foreach (Tuple<int, string> t in facet_placement_list)
                            Console.WriteLine("PLACEMENT {0} --- {1} {2} ", ic.Id_inner, t.Item1, t.Item2);
                    }
                }


            return result;
        }


        private static string library_path_base_action_binding = "org.hpcshelf.binding.task.TaskBindingBase";
        private static int id_abstract_base_action_binding = -1;
        private static bool isServiceBinding(InnerComponent ic, IDbConnection dbcon)
        {
            if (id_abstract_base_action_binding < 0)
                id_abstract_base_action_binding = Backend.acfdao.retrieve_libraryPath(library_path_base_action_binding, dbcon).Id_abstract;

            int id_abstract = ic.Id_abstract_inner;
            while (id_abstract != id_abstract_base_action_binding)
            {
                int id_functor_app_super = Backend.acfdao.retrieve(id_abstract, dbcon).Id_functor_app_supertype;
                if (id_functor_app_super > 0)
                    id_abstract = Backend.acfadao.retrieve(id_functor_app_super, dbcon).Id_abstract;
                else
                    break;
            }

            return id_abstract == id_abstract_base_action_binding;
        }

        private static string library_path_base_service_binding = "org.hpcshelf.binding.environment.EnvironmentBindingBase";
        private static int id_abstract_base_service_binding = -1;
        private static bool isActionBinding(InnerComponent ic, IDbConnection dbcon)
        {
            if (id_abstract_base_service_binding < 0)
                id_abstract_base_service_binding = Backend.acfdao.retrieve_libraryPath(library_path_base_service_binding, dbcon).Id_abstract;

            int id_abstract = ic.Id_abstract_inner;
            while (id_abstract != id_abstract_base_service_binding)
            {
                int id_functor_app_super = Backend.acfdao.retrieve(id_abstract, dbcon).Id_functor_app_supertype;
                if (id_functor_app_super > 0)
                    id_abstract = Backend.acfadao.retrieve(id_functor_app_super, dbcon).Id_abstract;
                else
                    break;
            }

            return id_abstract == id_abstract_base_service_binding;
        }


        public void resolve(string c_ref)
        {
            IDbConnection dbcon = DBConnector.createConnection();
            dbcon.Open();

            try
            {
                string resolution_path = Path.Combine(Constants.PATH_CATALOG_FOLDER, system_component.ComponentChoice[0], c_ref + ".resolution");

                Backend.ResolveComponentHierarchy ctree;

                if (!system_component.InnerComponents.ContainsKey(c_ref))
                    throw new ComponentNotFoundException(c_ref, system_component.ID);

                if (!File.Exists(resolution_path))
                {
                    ctree = system_component.InnerComponents[c_ref][0].Item1;
                    ctree.clearSelection();
                    ctree.resolveAllComponentHierarchy(dbcon);
                    ConverterComponentHierachyToXML.addToCTreeCache(ctree);

                    if (c_ref.Equals(WORKFLOW_COMPONENT_ID))
                        isSAFeSWLWorkflow(ctree.ComponentChoice[0], out orchestration_code, dbcon);
                }
                else
                {
                    string ctree_str = File.ReadAllText(resolution_path);
                    XMLComponentHierarchy.ComponentHierarchyType ctree_xml = LoaderApp.deserialize<XMLComponentHierarchy.ComponentHierarchyType>(ctree_str);
                    ctree = ConverterComponentHierachyToXML.readComponentHierarchy(ctree_xml, c_ref);
                }

                registerResolution(c_ref, ctree);
            }
            catch(SoapException e)
            {
                throw e;
            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "CoreServices.resolve");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.resolve", WebServicesException.FaultCode.Server);
            }
            finally
            {
                dbcon.Close();
            }
        }

        public void resolve_system(string c_ref)
        {
            IDbConnection dbcon = DBConnector.createConnection();
            dbcon.Open();

            try
            {
                Backend.ResolveComponentHierarchy ctree_platform;

                if (!system_component.InnerComponents.ContainsKey(c_ref))
                    throw new ComponentNotFoundException(c_ref, system_component.ID);

                ctree_platform = system_component.InnerComponents[c_ref][0].Item1;

                IDictionary<string,Backend.ResolveComponentHierarchy> ctree_list = new ConcurrentDictionary<string,Backend.ResolveComponentHierarchy>();
                ctree_list[c_ref] = ctree_platform;

                Tuple<org.hpcshelf.database.Component, IDictionary<string, org.hpcshelf.database.Component>> selection = ctree_platform.resolveSystem(dbcon);

                ctree_platform.setSelection(selection.Item1);
                AbstractComponentFunctorApplication acfaRef_selected_platform = Backend.acfadao.retrieve(selection.Item1.Id_functor_app, dbcon);
                foreach (KeyValuePair<string, org.hpcshelf.database.Component> c in selection.Item2)
                {
                    Backend.ResolveComponentHierarchy ctree = system_component.InnerComponents[c.Key][0].Item1;
                    ctree.setSelection(c.Value);
                    try
                    {
                       // DBConnector.openConnection();
                        AbstractComponentFunctorApplication acfaRef_new = Backend.ResolveComponentHierarchySystemImpl.injectPlatformArguments(ctree.ACFARef0, acfaRef_selected_platform, dbcon);
                        ctree.ACFARef = acfaRef_new;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        if (e.InnerException != null)
                        {
                            Console.WriteLine(e.InnerException.Message);
                            Console.WriteLine(e.InnerException.StackTrace);
                        }
                    }
                    ctree_list[c.Key] = ctree;
                }

                foreach (KeyValuePair<string, Backend.ResolveComponentHierarchy> ctree in ctree_list)
                {
                    ctree.Value.resolveAllComponentHierarchy(dbcon); //TODO: comentado para o estudo de caso do artigo do Alite. Componentes BLAS não implementados ainda ...
                    ConverterComponentHierachyToXML.addToCTreeCache(ctree.Value);
                    registerResolution(ctree.Key, ctree.Value);
                }
            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "CoreServices.resolve_system");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.resolve_system", WebServicesException.FaultCode.Server);
            }
            finally
            {
                dbcon.Close();
            }
        }


        public string deploy(string arch_ref)
        {
            IDbConnection dbcon = DBConnector.createConnection();
            dbcon.Open();

            string result = null;
            try
            {
                //  DBConnector.openConnection();

                Console.WriteLine("DEPLOYING 0 --- " + arch_ref);

                if (!system_component.InnerComponents.ContainsKey(arch_ref))
                    throw new ComponentNotFoundException(arch_ref, system_component.ID);

                if (!workflow_resolution.ContainsKey(arch_ref))
                    throw new UnresolvedComponentException(arch_ref, system_component.ID);

                IDictionary<string, string> addresses = new ConcurrentDictionary<string, string>();

                string component_ref = null;
                Backend.ResolveComponentHierarchy resolution_tree = null;
                IDictionary<string, ISet<int>>[] dependency_list = new IDictionary<string, ISet<int>>[FacetInstanceCount];
                IList<string>[] dependency_order = new IList<string>[FacetInstanceCount];
                IDictionary<string, AbstractComponentFunctorApplication> component_contract = null;
                if (!arch_ref.Equals(PLATFORM_SAFE_ID))
                {
                    // The selected component for arch_ref in the previous resolve operation.
                    resolution_tree = workflow_resolution[arch_ref];
                    component_ref = resolution_tree.ComponentChoice[0];

                    // The dependencies of the selected component.
                    Backend.DependenciesCalculator dependencies_calculator = new Backend.DependenciesCalculator(FacetInstanceCount);
                    dependencies_calculator.visit(resolution_tree, dbcon);

                    component_contract = dependencies_calculator.ComponentContract;

                    for (int facet_instance = 0; facet_instance < dependencies_calculator.Dependencies.Length; facet_instance++)
                    {
                        dependency_list[facet_instance] = new ConcurrentDictionary<string, ISet<int>>(dependencies_calculator.Dependencies[facet_instance]);
                        dependency_order[facet_instance] = new List<string>(dependencies_calculator.DependenciesOrder[facet_instance]);
                    }
                }

                bool is_platform = platforms.Contains(arch_ref);

                if (is_platform)
                {
                    if (!arch_ref.Equals(PLATFORM_SAFE_ID))
                    {
                        org.hpcshelf.database.Component maintainer_ref = resolution_tree.InnerComponents["maintainer"][0].Item1.ComponentDB[0];

                        Tuple<string, int, int> platform_info = deployPlatform(arch_ref, component_ref, maintainer_ref, dependency_list[platforms.IndexOf(arch_ref)], dependency_order[platforms.IndexOf(arch_ref)], component_contract, dbcon);
                        result = platform_info.Item1;
                        int node_count = platform_info.Item2;
                        updateUnitMappingOfSystem(arch_ref/*, platform_access*/, node_count, system_component/*, system_dependencies[platforms.IndexOf(arch_ref)]*/);
                    }
                    else
                    {
                        platform_access_dict[Constants.PLATFORM_SAFE_ID] = new Tuple<IPlatformServices, string, string, string>(platform_SAFe, platform_SAFe_address, null, platform_SAFe_address);
                        result = platform_SAFe_address;
                        updateUnitMappingOfSystem(arch_ref/*, platform_SAFe*/, 1, system_component/*, system_dependencies[platforms.IndexOf(arch_ref)]*/);
                    }

                    string platform_address_ip = result.Substring(7, result.LastIndexOf(':') - 7);
                    if (!lock_platform_deploy_1.ContainsKey(platform_address_ip))
                        lock_platform_deploy_1[platform_address_ip] = new object();
                }
                else
                    result = deployComponent(arch_ref, dependency_list, dependency_order, component_contract, dbcon);

            }
            catch (HPCShelfException e)
            {
                if (deployed_list.ContainsKey(arch_ref)) deployed_list[arch_ref].Clear();
                throw WebServicesException.raiseSoapException(e, "CoreServices.deploy");
            }

            catch (SoapException e)
            {
                if (deployed_list.ContainsKey(arch_ref)) deployed_list[arch_ref].Clear();
                throw e;
            }
            catch (Exception e)
            {
                if (deployed_list.ContainsKey(arch_ref)) deployed_list[arch_ref].Clear();
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.deploy", WebServicesException.FaultCode.Server);
            }
            finally
            {
                dbcon.Close();
            }

            deployed_component[arch_ref] = result;

            return result;
        }

        private IDictionary<string, string> deployed_component = new ConcurrentDictionary<string, string>();

        private bool isSAFeSWLWorkflow(string component_ref, out string orchestration_code, IDbConnection dbcon)
        {
            bool result = false;

            org.hpcshelf.database.Component c = Backend.cdao.retrieve_libraryPath(component_ref, dbcon);

            IList<SourceCode> l = Backend.scdao.listByFileType('u', c.Id_concrete, "workflow", "orchestration", dbcon);

            if (l == null || l.Count == 0)
            {
                orchestration_code = null;
                result = false;
            }
            else
            {

                SourceCode s = l[0];
                orchestration_code = s.Contents;
                result = true;
            }

            return result;
        }

        private IDictionary<string, IList<Thread>> delayed_instantiations = new ConcurrentDictionary<string, IList<Thread>>();

        private object lock_object = new object();

        public string instantiate(string component_ref)
        {
            IDbConnection dbcon = DBConnector.createConnection();
            dbcon.Open();

            try
            {
                IDictionary<string, string> platform_address_list = new ConcurrentDictionary<string, string>();

                IList<Thread> bag_of_threads = new List<Thread>();

               // DBConnector.openConnection();

                if (!system_component.InnerComponents.ContainsKey(component_ref))
                    throw new ComponentNotFoundException(component_ref, system_component.ID);

                if (!workflow_resolution.ContainsKey(component_ref))
                    throw new UnresolvedComponentException(component_ref, system_component.ID);

                if (!deployed_component.ContainsKey(component_ref))
                    throw new UndeployedComponentException(component_ref, system_component.ID);

                if (instantiated_component.ContainsKey(component_ref))
                    throw new DuplicateComponentInstantiationException(component_ref, system_component.ID);

                Backend.ResolveComponentHierarchy ctree;

                if (system_component != null)
                {
                    ctree = workflow_resolution[component_ref];
                    ctree.setUnitMappingNodes(system_component, dbcon);
                }
                else
                    throw new Exception("The platform must be instantiated before the components placed on it");


                IList<string> channel_id_list = new List<string>();
                IList<int> channel_facet_instance_list = new List<int>();
                IList<string> channel_address_list_public = new List<string>();
                IList<string> channel_address_list_private = new List<string>();

                IList<string> place_platforms = new List<string>();
                IDictionary<string, string> is_local_binding = new ConcurrentDictionary<string, string>();

                bool is_platform = platforms.Contains(component_ref);

                if (!is_platform)
                {
                    Tuple<int, string>[] platforms_arch_ref = platform_placement_list[component_ref];
                    foreach (Tuple<int, string> p in platforms_arch_ref)
                    {
                        string platform_ref = p.Item2;
                        if (!place_platforms.Contains(platform_ref))
                        {
                            place_platforms.Add(platform_ref);

                            if (binding_list_dict.ContainsKey(component_ref))
                                foreach (Tuple<string, int> t in binding_list_dict[component_ref])
                                {
                                    string channel_id = t.Item1;
                                    int facet_count = t.Item2;
                                    int facet_instance = platforms.IndexOf(platform_ref);
                                    string binding_address = platform_access_dict[platform_ref].Item1.allocateBindingAddress(channel_id, facet_count*2);

                                    string[] b = binding_address.Split(new char[1] { ';' });
                                    string binding_address_private = b[0];
                                    string binding_address_public = b.Length > 1 ? b[1] : "";

                                    string ip_address_private = binding_address_private.Split(new char[1] { ':' })[0];

                                    string local_address;
                                    if (!is_local_binding.TryGetValue(channel_id, out local_address))
                                        is_local_binding[channel_id] = ip_address_private;
                                    else if (local_address != null && !local_address.Equals(ip_address_private))
                                        is_local_binding[channel_id] = null;

                                    channel_id_list.Add(channel_id);
                                    channel_facet_instance_list.Add(facet_instance);
                                    channel_address_list_private.Add(binding_address_private);
                                    channel_address_list_public.Add(binding_address_public);
                                }
                        }
                    }
                }
                else
                    place_platforms.Add(component_ref);
                //place_platforms = platforms;

                IList<string> channel_address_list = new List<string>();
                for (int i = 0; i < channel_address_list_private.Count; i++)
                    channel_address_list.Add(is_local_binding[channel_id_list[i]] != null ? channel_address_list_private[i] : channel_address_list_public[i]);

                string[] channel_id_array = new string[channel_id_list.Count];
                int[] channel_facet_instance_array = new int[channel_facet_instance_list.Count()];
                string[] channel_address_array = new string[channel_address_list.Count()];

                channel_id_list.CopyTo(channel_id_array, 0);
                channel_facet_instance_list.CopyTo(channel_facet_instance_array, 0);
                channel_address_list.CopyTo(channel_address_array, 0);

                IDictionary<string, Exception> failed = new ConcurrentDictionary<string, Exception>();

                foreach (string platform_ref in place_platforms)
                {
                    if (!platforms.Contains(component_ref) || component_ref.Equals(platform_ref))
                    {
                        Thread t = new Thread(new ThreadStart(delegate ()
                       {
                           try
                           {
                               IPlatformServices platform_access = platform_access_dict[platform_ref].Item1;

                               int facet_instance = platforms.IndexOf(platform_ref);

                               Console.WriteLine("BEGIN CORE INSTANTIATE {0} on {1}", component_ref, platform_ref);

                               string[] ctree_xml_str_list = new string[ctree.HostComponents.Count];

                               ConverterComponentHierachyToXML xml_converter = new ConverterComponentHierachyToXML();
                               XMLComponentHierarchy.ComponentHierarchyType ctree_xml = (XMLComponentHierarchy.ComponentHierarchyType)xml_converter.visit(ctree, dbcon);
                               ctree_xml_str_list[0] = LoaderApp.serialize(ctree_xml);

                               int j = 1;
                               for (int i = 0; i < ctree.HostComponents.Count; i++)
                               {
                                   Backend.ResolveComponentHierarchy ctree_host = ctree.HostComponents[i].Item2;
                                   if (ctree_host != system_component)
                                   {
                                       XMLComponentHierarchy.ComponentHierarchyType ctree_host_xml = (XMLComponentHierarchy.ComponentHierarchyType)xml_converter.visit(ctree_host, dbcon);
                                       ctree_xml_str_list[j++] = LoaderApp.serialize(ctree_host_xml);
                                   }
                               }

                               platform_access.instantiate(app_name + "." + component_ref, ctree_xml_str_list, facet_instance, channel_id_array, channel_facet_instance_array, channel_address_array);
                               Console.Write("SUCCESFULL");
                           }
                           catch (Exception e)
                           {
                               Console.Write("FAILED");
                               failed[platform_ref] = e;
                           }
                           finally
                           {
                               Console.WriteLine(": CORE INSTANTIATE {0} on {1}", component_ref, platform_ref);
                           }

                       }));

                        if (platform_access_dict.ContainsKey(platform_ref))
                        {
                            t.Start();
                            //t.Join();
                            bag_of_threads.Add(t);
                        }
                        else
                        {
                            if (!delayed_instantiations.ContainsKey(platform_ref))
                                delayed_instantiations[platform_ref] = new List<Thread>();
                            delayed_instantiations[platform_ref].Add(t);
                        }
                    }
                }

                // BARRIER (waiting completion)
                foreach (Thread t in bag_of_threads) t.Join();

                if (failed.Count > 0)
                    throw failed[failed.Keys.ToArray()[0]];

                if (is_platform && delayed_instantiations.ContainsKey(component_ref))
                    foreach (Thread t in delayed_instantiations[component_ref])
                    {
                        t.Start();
                        t.Join();
                    }

                instantiated_component[component_ref] = component_ref;
            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "CoreServices.instantiate");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.instantiate", WebServicesException.FaultCode.Server);
            }
            finally
            {
                dbcon.Close();
            }


            return null;
        }

        private IDictionary<string, string> instantiated_component = new ConcurrentDictionary<string, string>();


        private bool is_local_incremental(IList<string> channel_address_list, string binding_address)
        {
            string last_binding_address = channel_address_list.Last();

            string ip_1 = last_binding_address.Split(new char[1] { ':' })[0];
            string ip_2 = binding_address.Split(new char[1] { ':' })[0];

            return ip_1.Equals(ip_2);
        }

        public void run(string component_ref)
        {
            Console.WriteLine("CoreServices ---- RUN {0}", component_ref);

            try
            {
                if (!system_component.InnerComponents.ContainsKey(component_ref))
                    throw new ComponentNotFoundException(component_ref, system_component.ID);

                if (!workflow_resolution.ContainsKey(component_ref))
                    throw new UnresolvedComponentException(component_ref, system_component.ID);

                if (!deployed_component.ContainsKey(component_ref))
                    throw new UndeployedComponentException(component_ref, system_component.ID);

                if (!instantiated_component.ContainsKey(component_ref))
                    throw new UninstantiatedComponentException(component_ref, system_component.ID);

                IList<Thread> t_list = new List<Thread>();

                IList<string> platform_placement = new List<string>();

                IList<Exception> exception_run_list = new List<Exception>();

                foreach (Tuple<int, string> p in platform_placement_list[component_ref])
                {
                    string platform_ref = p.Item2;
                    if (!platform_placement.Contains(platform_ref))
                    {
                        platform_placement.Add(platform_ref);
                        Thread t = new Thread(new ParameterizedThreadStart(delegate (object o)
                        {
                            try { 
                                Console.WriteLine("RUN {0} on {1}", component_ref, platform_ref);
                                IPlatformServices platform_access = platform_access_dict[platform_ref].Item1;
                                platform_access.run(app_name + "." + component_ref);
                            }
                            catch (Exception exc)
                            {
                                exception_run_list.Add(exc);
                            }
                        }));
                        t.Start();
                        t_list.Add(t);
                    }
                }

               Console.WriteLine("RUN before join {0}", component_ref);

                foreach (Thread t in t_list)
                {
                    t.Join();
                    Console.WriteLine("RUN Finished {0}", component_ref);
                }

                if (exception_run_list.Count > 0)
                    throw new Exception(string.Format("RUN {0} failed !", component_ref), exception_run_list[0]);

                Console.WriteLine("RUN after join {0}", component_ref);
            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "CoreServices.run");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                                  "http://tempuri.org/HPCShelfWebServices",
                                                                  e.Message,
                                                                  ErrorCodes.GENERAL_ERROR.ToString(),
                                                                  "CoreServices.run", WebServicesException.FaultCode.Server);
            }
        }

        public void release(string component_ref)
        {
            try
            {
                if (!system_component.InnerComponents.ContainsKey(component_ref))
                    throw new ComponentNotFoundException(component_ref, system_component.ID);

                if (!workflow_resolution.ContainsKey(component_ref))
                    throw new UnresolvedComponentException(component_ref, system_component.ID);

                if (!deployed_component.ContainsKey(component_ref))
                    throw new UndeployedComponentException(component_ref, system_component.ID);

                if (!system_component.InnerComponents.ContainsKey(component_ref))
                    throw new UninstantiatedComponentException(component_ref, system_component.ID);

                bool is_platform = platforms.Contains(component_ref);
                if (is_platform)
                {
                    if (!component_ref.Equals(PLATFORM_SAFE_ID))
                    {
                        string platform_address = platform_access_dict[component_ref].Item4;
                        releasePlatform(component_ref, platform_address, platform_access_dict[component_ref].Item3);
                    }
                    else
                    {
                        platform_SAFe.release(app_name + "." + Constants.PLATFORM_SAFE_ID);
                        PlatformSAFe = null;
                    }
                }
                else
                {
                    releaseComponent(component_ref);
                }

                instantiated_component.Remove(component_ref);

            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "CoreServices.release");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.release", WebServicesException.FaultCode.Server);
            }
        }

        private void releaseComponent(string component_ref)
        {
            IList<Thread> t_list = new List<Thread>();

            IList<string> platform_placement = new List<string>();

            IList<Exception> exception_release_list = new List<Exception>();

            foreach (Tuple<int, string> p in platform_placement_list[component_ref])
            {
                string platform_ref = p.Item2;
                if (!platform_placement.Contains(platform_ref))
                {
                    platform_placement.Add(platform_ref);
                    Thread t = new Thread(new ParameterizedThreadStart(delegate (object o)
                    {
                        try
                        {
                            Console.WriteLine("RELEASE {0} from {1}", component_ref, platform_ref);
                            IPlatformServices platform_access = platform_access_dict[platform_ref].Item1;
                            platform_access.release(app_name + "." + component_ref);
                        }
                        catch (Exception exc)
                        {
                            exception_release_list.Add(exc);
                        }

                    }));
                    t.Start();
                    t_list.Add(t);
                }
            }

            foreach (Thread t in t_list)
                 t.Join();

            if (exception_release_list.Count > 0)
                throw new Exception(string.Format("RELEASE {0} failed !", component_ref), exception_release_list[0]);

        }

        private string releasePlatform(string platform_ref, string platform_address, string backend_address)
        {
            BackendServices.BackendServices backend_access = (BackendServices.BackendServices)Activator.CreateInstance(System.Type.GetType(BACKEND_SERVICES), new string[1] { backend_address });
            backend_access.Timeout = int.MaxValue;

            AutoResetEvent ev = new AutoResetEvent(false);

            string return_message = null;
            backend_access.releaseCompleted += (object sender, BackendServices.releaseCompletedEventArgs e) => { return_message = e.Result; ev.Set(); };
            backend_access.releaseAsync(platform_address);

            ev.WaitOne();

            platform_access_dict.Remove(platform_ref);
            deployed_list[platform_ref].Clear();

            return return_message;
        }

        private class CertificationProcess : HShelfCertificationSystem
        {
            private GoPort go_port;
            private CertificationPort certification_port;
            readonly CoreServices core_services;
            private BuilderService bsPort = null;
            private ComponentID host_cid = null;
            private ComponentID s_cid = null;

            public CertificationProcess(CoreServices core_services, string certifier_id, int id_concrete_certifiable)
            {
                this.certifier_id = certifier_id;
                this.id_concrete_certifiable = id_concrete_certifiable;
                this.core_services = core_services;
                this.system_component = core_services.system_component;
                create_certifier();
            }

            public CertificationPort CertificationPort { get { return certification_port;} set { certification_port = value; } }

            private Services appServices = null;
            private Backend.ResolveComponentHierarchy system_component;
            private string certifier_id;
            private int id_concrete_certifiable;

            private CertificationPort create_certifier()
            {
                // TODO: The Core acts as an application component for the parallel certification system. 
                //       So, it provides the necessary information for the certifier (workflow) through service bindings (provider).
                //       Finally, it takes the result of the certification through service bindings (user).
                //       The code below must connect to the service bindings.

                AbstractFramework frw_certification = new HShelfFramework(system_component, certifier_id, id_concrete_certifiable);

                appServices = /* core_services.Services; */ frw_certification.getServices(certifier_id, "HShelfCertificationSystem", frw_certification.createTypeMap());

                appServices.registerUsesPort(Constants.BUILDER_SERVICE_PORT_NAME, Constants.BUILDER_SERVICE_PORT_TYPE, new TypeMapImpl());
                host_cid = appServices.getComponentID();
                bsPort = (BuilderService)appServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);

                string workflowCRef = core_services.app_name + "." + certifier_id + "." + Constants.CERTIFICATION_WORKFLOW_COMPONENT_ID;
                s_cid = bsPort.getComponentID(workflowCRef);

                // CONNECT TO THE CERTIFICATION PORT OF THE CERTIFIER COMPONENT.
                string certification_port_name = workflowCRef + "_" + Constants.CERTIFICATION_PORT_NAME;
                appServices.registerUsesPort(certification_port_name, Constants.CERTIFICATION_PORT_TYPE, new TypeMapImpl());
                bsPort.connect(host_cid, certification_port_name, s_cid, Constants.CERTIFICATION_PORT_NAME);
                certification_port = (CertificationPort)appServices.getPort(certification_port_name);

                // CONNECT TO THE GO PORT OF THE CERTIFIER COMPONENT.
                string go_port_name = workflowCRef + "_" + Constants.GO_PORT_NAME;
                appServices.registerUsesPort(go_port_name, Constants.GO_PORT_TYPE, new TypeMapImpl());
                bsPort.connect(host_cid, go_port_name, s_cid, Constants.GO_PORT_NAME);
                Console.WriteLine("BEFORE GO PORT " + workflowCRef + " --- " + appServices.GetHashCode());
                go_port = (GoPort)appServices.getPort(go_port_name);
                Console.WriteLine("AFTER GO PORT " + workflowCRef + " --- " + appServices.GetHashCode());

                appServices.addProvidesPort(new CoreServicesFetchFilesPort(id_concrete_certifiable), Constants.FETCH_FILES_PORT_NAME, "", new TypeMapImpl());

                return certification_port;
            }

            internal void perform_certification(string certifiable_id)
            {
                ConnectionID conn = bsPort.connect(s_cid, Constants.FETCH_FILES_PORT_NAME, appServices.getComponentID(), Constants.FETCH_FILES_PORT_NAME);

                // Using the certifiable_id, the certifier may fetch the code of the certifiable component.
                certification_port.CertifiableID = certifiable_id;

                go_port.go();

                bsPort.disconnect(conn, 10000);
                //appServices.removeProvidesPort(certifiable_id + ".fetch_files");
            }

        }


        public void certify(string certifiable_id)
        {
            /* TODO: Test whether the chosen certifiable component is already certified by the certifier
             *       -- the contract of the verifier may be compared to the contract of the components already verified (default and contractual properties).
             *       -- if a contract match, the certification is not performed.
             */
            IDbConnection dbcon = DBConnector.createConnection();
            dbcon.Open();

            try
            {
             //   DBConnector.openConnection();

                if (!system_component.InnerComponents.ContainsKey(certifiable_id))
                    throw new ComponentNotFoundException(certifiable_id, system_component.ID);

                if (!workflow_resolution.ContainsKey(certifiable_id))
                    throw new UnresolvedComponentException(certifiable_id, system_component.ID);

                Backend.ResolveComponentHierarchy ctree = workflow_resolution[certifiable_id];
                Component c = Backend.cdao.retrieve_libraryPath(ctree.ComponentChoice[0], dbcon);
                int id_concrete_certifiable = c.Id_concrete;

                string certifier_id = fetchIdInnerCertifier(certifiable_id, dbcon);

                CertificationProcess certifier_process;
                if (!instantiated_certifiers.TryGetValue(certifier_id, out certifier_process))
                {
                    certifier_process = new CertificationProcess(this, certifier_id, id_concrete_certifiable);
                    instantiated_certifiers[certifier_id] = certifier_process;
                }

                //  Services.addProvidesPort(new CoreServicesFetchFilesPort(ctree.ComponentChoice[0]), certifiable_id + ".fetch_files", "", new TypeMapImpl());

                CertificationPort certification_port = certifier_process.CertificationPort;

                certifier_process.perform_certification(certifiable_id);

                switch (certification_port.Status)
                {
                    case CertificationStatus.Success:
                        //makeItCertified(id_concrete_certifiable, id_functor_app_certifier);
                        break;
                    case CertificationStatus.Fail:
                        throw new CertificationFailedException();
                        break;
                }
            }
            catch (AlreadyCertifiedException e)
            {
                // continue ...
            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "CoreServices.certify");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.certify", WebServicesException.FaultCode.Server);
            }
            finally
            {
                dbcon.Close();
            }
        }

        private IDictionary<string, CertificationProcess> instantiated_certifiers = new ConcurrentDictionary<string, CertificationProcess>();

        private string fetchIdInnerCertifier(string arch_ref, IDbConnection dbcon)
        {
            int id_abstract_system = system_component.ACFARef.Id_abstract;

            // THE (DEFAULT) NAME OF THE CERTIFER IN THE CERTIFIABLE COMPONENT IS "certifier".
            InnerComponentExposed ice = Backend.icedao.find_certifier_id(id_abstract_system, arch_ref, Constants.CERTIFIER_INNER_ID, dbcon);

            // RETURN THE NAME OF THE CERTIFIED IN THE SYSTEM COMPONENT.
            return ice.Id_inner_owner;
        }

        private void makeItCertified(int id_concrete, int id_functor_app, IDbConnection dbcon)
        {
            Certified ac = new Certified();
            ac.Id_concrete = id_concrete;
            ac.Id_functor_app = id_functor_app;
            Backend.certified_dao.insert(ac, dbcon);
        }

        private bool isCertified(int id_concrete_certifiable, int id_functor_app_certifier, IDbConnection dbcon)
        {
            IList<Certified> certified_list = Backend.certified_dao.list(id_concrete_certifiable, dbcon);

            foreach (Certified certified in certified_list)
            {
                int id_functor_app = certified.Id_functor_app;
                int res = Resolution.subType(Backend.acfadao.retrieve(id_functor_app_certifier, dbcon), Backend.acfadao.retrieve(id_functor_app, dbcon), dbcon);
                if (res > 0)
                    return true;
            }

            return false;
        }


        /* A operação deploy tem o comportamento dependente da espécie do componente que recebe. 
         * 1) Caso se trata de um componente plataforma, deve ler o arquivo SAFeSWL para gerar os componentes do sistema computacional. 
         *    Para isso, chama a operação LoaderSystem.deploySystemComponent, que já está prototipada.
         *    Para descobrir o endereço do Back-End, deve consultar o catálogo do componente, buscando o componente escolhido.
         *    Finalmente, invoca a operação deploy para implantar os componentes do sistema computacional na plataforma.
         * 2) Caso se trate de um componente de computação ou conector, deve ler o arquivo SAFeSWL para descobrir os endereços dos serviços Platform das plataformas
         *    nas quais as facetas do componente irão ser instaladas. Então, invoca a operaçao deploy de cada serviço Platform, passando como argumento os arquivos 
         *    ".hpe" dos componentes necessários para carregar o componente na instanciação. Inclusive, deve implantar as facetas dos bindings com a aplicação no serviço
         *    Platform do lado do SAFe. 
         */






        public void registerResolution(string c_ref, Backend.ResolveComponentHierarchy resolution_result)
        {
            if (workflow_resolution == null)
                workflow_resolution = new ConcurrentDictionary<string, Backend.ResolveComponentHierarchy>();

            if (workflow_resolution.ContainsKey(c_ref))
                workflow_resolution.Remove(c_ref);

            workflow_resolution[c_ref] = resolution_result;
        }

        /*   private static IDictionary<string, string> backend_services = new ConcurrentDictionary<string, string>
                                 {
                                    {"http://200.19.177.89:8001/backendservices/","LIABackendServices.BackEndService"},
                                    {"http://127.0.0.1:8079/BackendServices.asmx","BackendServices.BackendServices"},
                                    {"http://127.0.0.1:8080/BackendServices.asmx","BackendServicesLocal.BackendServices"},
                                    {"http://127.0.0.1:8081/BackendServices.asmx","BackendServices.BackendServices"},
                                    {"http://23.22.201.230:8081/BackendServices.asmx","BackendServices.BackendServices"}
                                 };
   */

        private const string BACKEND_SERVICES = "BackendServices.BackendServices";

        private IDictionary<string, Tuple<IPlatformServices, string, string, string>> platform_access_dict = new ConcurrentDictionary<string, Tuple<IPlatformServices, string, string, string>>();

        public Tuple<string, int, int> invokeBackEndDeploy(string arch_ref, string backend_address, string[] config, IDbConnection dbcon)
        {
            //ServiceUtils.BackendServices.BackendServices backend_access = new ServiceUtils.BackendServices.BackendServices (backend_address);
            //backend_access.Timeout = int.MaxValue;

            //string platform_address = backend_access.deploy (config);

            // ACESSANDO O BACKEND DO CLUSTER DO LIA ...
            //ServiceUtils.LIABackendServices.BackEndService backend_access = new ServiceUtils.LIABackendServices.BackEndService (backend_address);
            //backend_access.Timeout = int.MaxValue;

            Console.WriteLine("invokeBackEndDeploy -- {0} -- {1}", arch_ref, backend_address);

            //    if (!backend_services.ContainsKey(backend_address))
            //         Console.Write(true);

            Uri uri_backend_platform = new Uri(backend_address);
            string ip_backend_platform = uri_backend_platform.Host;
            if (ip_backend_platform.Equals(core_ip))
                backend_address = backend_address.Replace(ip_backend_platform, "127.0.0.1");

            BackendServices.BackendServices backend_access = (BackendServices.BackendServices)Activator.CreateInstance(System.Type.GetType(BACKEND_SERVICES/*backend_services[backend_address]*/), new string[1] { backend_address });
            backend_access.Timeout = int.MaxValue;

            IDictionary<string, string> context = getContext(arch_ref, dbcon);

            string[] platform_config_ = config[1].Split(';');
            string c0 = platform_config_[0].StartsWith("$") ? platform_config_[0].Substring(1) : platform_config_[0];
            string platform_config = (context.ContainsKey(c0) ? context[c0] : c0);

            for (int i = 1; i < platform_config_.Length; i++)
            {
                c0 = platform_config_[i].StartsWith("$") ? platform_config_[i].Substring(1) : platform_config_[i];
                platform_config += ";" + (context.ContainsKey(c0) ? context[c0] : c0);
            }

            //foreach (KeyValuePair<string, string> contextual_argument in context)
            //    platform_config = platform_config.Replace("$" + contextual_argument.Key, contextual_argument.Value);

            string platform_address = "";
            AutoResetEvent ev = new AutoResetEvent(false);

            Exception exception_deploy = null;

            backend_access.deployCompleted += (object sender, BackendServices.deployCompletedEventArgs e) =>
            {
                try
                {
                    platform_address = e.Result;
                }
                catch (Exception exc)
                {
                    Console.WriteLine("EXCEPTION (deploy thread): {0}\n{1}", exc.Message, exc.StackTrace);
                    if (exc.InnerException != null)
                        Console.WriteLine("INNER EXCEPTION (deploy thread): {0}\n{1}", exc.InnerException.Message, exc.InnerException.StackTrace);

                    platform_address = null;
                    exception_deploy = exc;
                }
                finally
                {
                    ev.Set();
                }
            };

            backend_access.deployAsync(platform_config);

            ev.WaitOne();

            if (platform_address == null)
            {
                Exception e = new Exception(string.Format("deploy error: cref={0}, backend_address={1}", arch_ref, backend_address), exception_deploy);
                throw e;
            }

            Console.WriteLine("PLATFORM ADDRESS (" + arch_ref + "): " + platform_address);

            string platform_address_global = platform_address;

            Uri uri_platform = new Uri(platform_address_global);
            string ip_platform = uri_platform.Host;
            if (ip_platform.Equals(core_ip))
                platform_address = platform_address_global.Replace(ip_platform, "127.0.0.1");

            CoreCachedServices.PlatformServices.PlatformServices platform_access = new CoreCachedServices.PlatformServices.PlatformServices(platform_address);
            platform_access.Timeout = int.MaxValue;
            int base_binding_address = -1;

            bool attempt_ok = false;
            while (!attempt_ok)
                try
                {
                    base_binding_address = platform_access.getBaseBindingPort();
                    attempt_ok = true;
                }
                catch (WebException e)
                {
                    Console.WriteLine("ERROR INVOKING PLATFORM {0}: {1}", arch_ref, e.Message);
                    Thread.Sleep(1000);
                }

            platform_access.setPlatformRef(app_name + "." + arch_ref);

            int nodes = platform_access.getNumberOfNodes();

            platform_access_dict[arch_ref] = new Tuple<IPlatformServices, string, string, string>(platform_access, platform_address, backend_address, platform_address_global);


            return new Tuple<string, int, int>(platform_address, base_binding_address, nodes);
        }

        private static IDictionary<string, object> lock_platform_deploy_1 = new ConcurrentDictionary<string, object>();

        private IDictionary<string, string> getContext(string arch_ref, IDbConnection dbcon)
        {
            IDictionary<string, string> result = new ConcurrentDictionary<string, string>();

            AbstractComponentFunctorApplication acfaRef = system_component.InnerComponents[arch_ref][0].Item1.ACFARef;

            int id_functor_app = acfaRef.Id_functor_app;

            ICollection<SupplyParameter> sp_list = Backend.spdao.list(id_functor_app, dbcon);
            foreach (SupplyParameter sp in sp_list)
            {
                string parameter_id = sp.Id_parameter;

                AbstractComponentFunctorParameter a = Backend.acfpdao.retrieve(acfaRef.Id_abstract, parameter_id, dbcon);
                if (a == null) continue;  // TODO: ...
                int b = a.Bounds_of;
                AbstractComponentFunctorApplication c = Backend.acfadao.retrieve(b, dbcon);
                int d = c.Id_abstract;
                AbstractComponentFunctor e = Backend.acfdao.retrieve(d, dbcon);

                string kind = e.Kind;

                if (kind.Equals(Constants.KIND_QUANTIFIER_NAME) || kind.Equals(Constants.KIND_QUALIFIER_NAME))
                {
                    int id_argument = default(int);
                    if (sp is SupplyParameterComponent)
                    {
                        SupplyParameterComponent spc = (SupplyParameterComponent)sp;
                        id_argument = spc.Id_functor_app_actual;
                    }
                    else if (sp is SupplyParameterParameter)
                    {
                        SupplyParameterParameter spp = (SupplyParameterParameter)sp;
                        id_argument = acfaRef.ParametersList[spp.Id_argument];
                    }

                    if (kind.Equals(Constants.KIND_QUANTIFIER_NAME))
                    {
                        AbstractComponentFunctorApplicationQuantifier acfaArgument = (AbstractComponentFunctorApplicationQuantifier)Backend.acfadao.retrieve(id_argument, dbcon);
                        string quantifier = acfaArgument.getQuantifierString(dbcon);
                        result[parameter_id] = quantifier;
                    }
                    else 
                    {
                        AbstractComponentFunctorApplication acfaArgument = (AbstractComponentFunctorApplication)Backend.acfadao.retrieve(id_argument, dbcon);
                        string qualifier = Backend.acfdao.retrieve(acfaArgument.Id_abstract, dbcon).Library_path;
                        result[parameter_id] = qualifier;
                    }
                }
            }

            return result;
        }

        private Tuple<string, int, int>
                       deployPlatform(string platform_ref,
                                      string component_ref,
                                      org.hpcshelf.database.Component maintainer,
                                      IDictionary<string, ISet<int>> dependency_list,
                                      IList<string> dependency_order,
                                      IDictionary<string, AbstractComponentFunctorApplication> component_contract, IDbConnection dbcon)
        {
            // descobrir o endereço do Back-End do mantenedor que ofereceu esse componente plataforma
            string[] config = CoreServicesUtils.fetchPlatformConfiguration(component_ref, dbcon);
            Console.WriteLine("component_ref = {0} config[0]={1} config[1]={2}", component_ref, config[0], config.Length == 1 ? "?" : config[1]);

            string backend_address = CoreServicesUtils.fetchBackendAddress(maintainer, config[0], dbcon);

            if (backend_address.Contains("127.0.0.1"))
            {
                Console.WriteLine("DEFAULT BACKEND ADDRESS is {0}", backend_address);
                Console.WriteLine("PlatformSAFE ADDRESS is {0}", platform_safe_address_ip);
                backend_address = backend_address.Replace("127.0.0.1", platform_safe_address_ip);
            }

            Console.WriteLine("CURRENT BACKEND ADDRESS is {0}", backend_address);

            IList<Tuple<DeployArgumentsType, IDictionary<string, byte[]>>> info_compile_list = new List<Tuple<DeployArgumentsType, IDictionary<string, byte[]>>>();

            buildInfoCompileList(platform_ref, dependency_list, dependency_order, component_contract, ref info_compile_list, dbcon);

            ISet<int> l = new HashSet<int>();
            l.Add(0);

            info_compile_list.Add(CoreServicesUtils.fetchDeployInfo(component_ref, l, component_contract, dbcon));

            Tuple<string, int, int> platform_address = null;
            /*Thread t = new Thread(new ThreadStart(() => {*/ platform_address = invokeBackEndDeploy(platform_ref, backend_address, config, dbcon); /*}));*/
            //t.Start();
            //t.Join();

            invokeDeployComponent(platform_address.Item1, platform_ref, platform_access_dict[platform_ref].Item1, info_compile_list);

            return new Tuple<string, int, int>(platform_address.Item1, platform_address.Item3, platform_address.Item2);
        }

        private void buildInfoCompileList(string platform_ref, 
                                          IDictionary<string, ISet<int>> dependency_list, 
                                          IList<string> dependency_order,
                                          IDictionary<string, AbstractComponentFunctorApplication> component_contract, 
                                          ref IList<Tuple<DeployArgumentsType, IDictionary<string, byte[]>>> info_compile_list, IDbConnection dbcon)
        {
            //foreach (KeyValuePair<string, ISet<int>> dependency_item in dependency_list)
            foreach (string key in dependency_order)
            {
                ISet<int> value = dependency_list[key];

                ISet<int> a = new HashSet<int>();
                if (!deployed_list[platform_ref].ContainsKey(key)
                    || value.Except(a = deployed_list[platform_ref][key]).Count() > 0)
                {
                    var deploy_info = CoreServicesUtils.fetchDeployInfo(key, value, component_contract, dbcon);
                    if (deploy_info != null)
                    {
                        info_compile_list.Add(deploy_info);
                        deployed_list[platform_ref][key] = new HashSet<int>(a.Union(value)); 
                    }
                    // else
                    //      abstract component with no sources
                }
            }
        }

        private void updateUnitMappingOfSystem(string platform_ref,
                                  //IPlatformServices platform_access,
                                  int node_count,
                                  Backend.ResolveComponentHierarchy ctree_system/*, IDictionary<string, ISet<int>> dependency_list*/)
        {
            int facet_instance = platforms.IndexOf(platform_ref);

            ctree_system.UnitMapping[0][facet_instance].node = new int[node_count];
            for (int i = 0; i < node_count; i++)
                ctree_system.UnitMapping[0][facet_instance].node[i] = i;

            //  IList<Tuple<DeployArguments.DeployArgumentsType, IDictionary<string, byte[]>>> info_compile_list = new List<Tuple<DeployArguments.DeployArgumentsType, IDictionary<string, byte[]>>>();

            //buildInfoCompileList(platform_ref, dependency_list, ref info_compile_list);

            //    CoreServicesUtils.invokeDeployComponent(platform_access, info_compile_list);
        }

        private IDictionary<string, Tuple<int, string>[]> platform_placement_list = null;

        private IDictionary<string, IDictionary<string, ISet<int>>> deployed_list = new ConcurrentDictionary<string, IDictionary<string, ISet<int>>>();

        private string deployComponent(string arch_ref, IDictionary<string, ISet<int>>[] dependency_list, IList<string>[] dependency_order, IDictionary<string, AbstractComponentFunctorApplication> component_contract, IDbConnection dbcon)
        {
            try
            {
                // determinar as plataformas onde cada componente da arquitetura deve instanciar suas facetas.
                // IDictionary<string, Tuple<int, string>[]> platform_placement_list = LoaderSystem.placementOfComponents(arch_desc);
                // determinar quais as plataformas para o componente identificado como arch_ref no código arquitetural.
                Tuple<int, string>[] platform_placement_of_component = platform_placement_list[arch_ref];

                IList<string> platforms_deploy = new List<string>();

                foreach (Tuple<int, string> platform_placement in platform_placement_of_component)
                {
                    string platform_ref = platform_placement.Item2;
                    if (!platforms_deploy.Contains(platform_ref))
                    {
                        platforms_deploy.Add(platform_ref);

                        int facet_instance = platforms.IndexOf(platform_ref);

                        if (dependency_list[facet_instance].Count == 0) continue;

                        IList<Tuple<DeployArgumentsType, IDictionary<string, byte[]>>> info_compile_list = new List<Tuple<DeployArguments.DeployArgumentsType, IDictionary<string, byte[]>>>();

                        buildInfoCompileList(platform_ref, dependency_list[facet_instance], dependency_order[facet_instance], component_contract, ref info_compile_list, dbcon);

                        invokeDeployComponent(platform_access_dict[platform_ref].Item2, platform_ref, platform_access_dict[platform_ref].Item1, info_compile_list);
                    }
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
                if (e.InnerException != null){
                    Console.Error.Write(e.InnerException.Message);
                    Console.Error.Write(e.InnerException.StackTrace);
                }
                throw e;
            }

            return "deployed";
        }

        public void invokeDeployComponent(string platform_address, string platform_ref, IPlatformServices platform_access, IList<Tuple<DeployArguments.DeployArgumentsType, IDictionary<string, byte[]>>> info_compile_list)
        {
            Console.WriteLine("info_compile_list.Count = {0}", info_compile_list.Count);

            string platform_address_ip = platform_address.Substring(7, platform_address.LastIndexOf(':') - 7);

            foreach (Tuple<DeployArguments.DeployArgumentsType, IDictionary<string, byte[]>> info_compile_list_item in info_compile_list)
                foreach (DeployArguments.DeployArgumentsUnitType info_compile_unit in info_compile_list_item.Item1.unit)
                {
                    string info_compile_string = LoaderApp.serialize<DeployArguments.DeployArgumentsUnitType>(info_compile_unit);
                    Console.WriteLine("platform_access.deploy -- {0}", info_compile_unit.library_path);
                    try
                    {
                        if (lock_platform_deploy_1.ContainsKey(platform_address_ip))
                        {
                            lock (lock_platform_deploy_1[platform_address_ip])
                            {
                                platform_access.deploy(info_compile_string, info_compile_list_item.Item2[info_compile_unit.moduleName]);
                            }
                        }
                        else
                            platform_access.deploy(info_compile_string, info_compile_list_item.Item2[info_compile_unit.moduleName]);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("ERROR DEPLOYING COMPONENT: {0} {1} {2}", info_compile_unit.library_path, e.Message, e.StackTrace);
                        deployed_list[platform_ref].Remove(info_compile_unit.library_path);
                        throw e;
                    }
                }
        }

        /*  private static string fetchComponentSource(string component_ref)
          {
              string folder_name = component_ref;
              string file_name = component_ref.Substring(component_ref.LastIndexOf('.') + 1) + ".hpe";

              string config_filename = Path.Combine(Constants.PATH_CATALOG_FOLDER, folder_name, file_name);
              string config_contents = File.ReadAllText(config_filename);

              return config_contents;
          }
  */

        #endregion

        // REGISTER COMPONENT IN THE CATALOG !


        public string register(string module_data, byte[] snk_contents)
        {
            IDbConnection dbcon = DBConnector.createConnection();
            dbcon.Open();

            int res;
            try
            {
                res = -1;
                ComponentType c = LoaderApp.deserialize<ComponentType>(module_data);
                if (c.header.baseType != null && c.header.baseType.extensionType.ItemElementName == ItemChoiceType.implements)
                    res = Backend.registerConcreteComponentTransaction(c, snk_contents, dbcon);
                else
                    res = Backend.registerAbstractComponentTransaction(c, snk_contents, dbcon);
            }
            catch (HPCShelfException e)
            {
                throw WebServicesException.raiseSoapException(e, "CoreServices.register");
            }
            catch (SoapException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message); Console.WriteLine(e.StackTrace);
                if (e.InnerException != null)
                {
                    Console.WriteLine(e.InnerException.Message); Console.WriteLine(e.InnerException.StackTrace);
                }

                throw WebServicesException.raiseSoapException("HPCShelfWebServices",
                                                              "http://tempuri.org/HPCShelfWebServices",
                                                              e.Message,
                                                              ErrorCodes.GENERAL_ERROR.ToString(),
                                                              "CoreServices.register", WebServicesException.FaultCode.Server);
            }
            finally
            {
                dbcon.Close();
            }


            return "deployed " + res;
        }

        #region PROVENANCE MANAGER




        // PROVENANCE DATA (LEVEL 2)

        #endregion PROVENANCE MANAGER
        private class LifeCycleManager : ICoreServices, Port
        {
            private CoreServices coreServices;

            public LifeCycleManager(CoreServices coreServices)
            {
                this.coreServices = coreServices;
            }

            public void certify(string arch_ref)
            {
                ((ICoreServices)coreServices).certify(arch_ref);
            }

            public string deploy(string arch_ref)
            {
                return ((ICoreServices)coreServices).deploy(arch_ref);
            }

            public string instantiate(string arch_ref)
            {
                return ((ICoreServices)coreServices).instantiate(arch_ref);
            }

            public void release(string arch_ref)
            {
                ((ICoreServices)coreServices).release(arch_ref);
            }

            public void resolve(string arch_ref)
            {
                ((ICoreServices)coreServices).resolve(arch_ref);
            }

            public void resolve_system(string arch_ref)
            {
                ((ICoreServices)coreServices).resolve_system(arch_ref);
            }
            
            public void run(string arch_ref)
            {
                ((ICoreServices)coreServices).run(arch_ref);
            }
        }
    }


}
