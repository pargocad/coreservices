﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Timers;
using System.Web.Services;
using org.hpcshelf.DGAC;

namespace org.hpcshelf
{
    public interface IProvenanceServices
    {
        void start(string handle, int level);
        string confirm(string handle);
        void cancel(string handle);
    }

    public interface ILifeCycleServices
    {
        void resolve(string handle, string arch_ref);
        string deploy(string handle, string arch_ref);
        string instantiate(string handle, string arch_ref);
        void run(string handle, string arch_ref);
        void release(string handle, string arch_ref);
        void certify(string handle, string arch_ref);
    }

    public interface ISessionServices
    {
        void open(string handle); // returns a handle
        void close(string handle);
        void heartbeat(string handle);
    }

    public interface IConfigurationServices
    {
        void   insertComponent (string handle, string solution_component_xml);
        void   insertParameter (string handle, string parameter_id, string parameter_class, string contractual_restriction);
        void   updateContract  (string handle, string contract);
        string fetchPorts      (string handle, string library_path, string name);
        string getVirtualPlatformAddress(string handle, string platform_ref);
    }


    public class CoreWebServices : WebService, ISessionServices, ILifeCycleServices, IProvenanceServices, IConfigurationServices
    {
        static IDictionary<string, CoreServices> core_services = new Dictionary<string, CoreServices>();

        [WebMethod]
        public string newCoreServices(string system_name, string platform_safe_address, int platfom_safe_port)
        {
            string handle = (new object()).GetHashCode().ToString();

            core_services[handle] = new CoreServices(system_name, platform_safe_address, platfom_safe_port);

            ServicePointManager.DefaultConnectionLimit = 1024;

            heartbeat(handle);

            return handle;
        }

        [WebMethod]
        public void connectToCoreServices(string system_name, string session_id, string platform_safe_address, int platfom_safe_port)
        {
            if (core_services.ContainsKey(session_id))
            {
                CoreServices this_core_services = core_services[session_id];
                this_core_services.setPlatformSAFeAddress(platform_safe_address, platfom_safe_port);
            }
            else
            {
                string errorMessage = string.Format("There is no session for {0} -- session_id={1}", system_name, session_id);

                throw exception.WebServicesException.raiseSoapException("HPCShelfWebServices",
                                              "http://tempuri.org/HPCShelfWebServices",
                                              errorMessage,
                                              exception.ErrorCodes.GENERAL_ERROR.ToString(),
                                              "CoreServices.create_system", exception.WebServicesException.FaultCode.Server);

            }
        }

        [WebMethod]
        public void open(string handle)
        {
            core_services[handle].open();
        }

        [WebMethod]
        public void open_contract(string handle, string system_contract)
        {
            core_services[handle].open(system_contract);
        }

        [WebMethod]
        public void close(string handle)
        {
            if (session_tm.ContainsKey(handle))
            {
                SessionTimer timer = session_tm[handle];
                timer.Stop();
                session_tm.Remove(handle);
            }
            core_services.Remove(handle);
            Console.WriteLine("CLOSING SESSION {0}", handle);
        }

        [WebMethod]
        public void certify(string handle, string arch_ref)
        {
            throw new NotImplementedException();
        }

        [WebMethod]
        public string deploy(string handle, string arch_ref)
        {
            return core_services[handle].deploy(arch_ref);
        }

        [WebMethod]
        public string instantiate(string handle, string arch_ref)
        {
            return core_services[handle].instantiate(arch_ref);
        }

        [WebMethod]
        public void release(string handle, string arch_ref)
        {
            core_services[handle].release(arch_ref);
        }

        [WebMethod]
        public void resolve(string handle, string arch_ref)
        {
            Console.WriteLine("HANDLE is null ??? {0}", handle == null);
            Console.WriteLine("HANDLE !!! {0} --- {1}", core_services.ContainsKey(handle), handle);

            core_services[handle].resolve(arch_ref);
        }

        [WebMethod]
        public void resolve_system(string handle, string arch_ref)
        {
            var sw = new Stopwatch();
            sw.Start();
            core_services[handle].resolve_system(arch_ref);
            sw.Stop();
            Console.WriteLine("Elapsed Time (resolve_system): {0} ms", sw.ElapsedMilliseconds);
        }

        [WebMethod]
        public void run(string handle, string arch_ref)
        {
            Console.WriteLine("WebCoreServices ---- RUN {0}", arch_ref);

            core_services[handle].run(arch_ref);
        }

        [WebMethod]
        public void start(string handle, int level)
        {
            core_services[handle].ProvenanceManager.start(level);
        }

        [WebMethod]
        public string confirm(string handle)
        {
            return core_services[handle].ProvenanceManager.confirm();
        }

        [WebMethod]
        public void cancel(string handle)
        {
            core_services[handle].ProvenanceManager.cancel();
        }

        [WebMethod]
        public void insertComponent(string handle, string solution_component_xml)
        {
            core_services[handle].insertComponent(solution_component_xml);
        }

        [WebMethod]
        public string fetchPorts(string handle, string library_path, string name)
        {
            return core_services[handle].fetchPorts(library_path, name);
        }


        //[WebMethod]
        //public void insertParameter(string handle, string parameter_id, string contractual_restriction)
        //{
        //    core_services[handle].insertParameter(parameter_id, contractual_restriction);
        //}

        [WebMethod]
        public void insertParameter(string handle, string parameter_id, string parameter_class, string contractual_restriction)
        {
            core_services[handle].insertParameter(parameter_id, parameter_class, contractual_restriction);
        }

        [WebMethod]
        public void updateContract(string handle, string contract)
        {
            core_services[handle].updateContract(contract);
        }

        [WebMethod]
        public string register(string handle, string module_data, byte[] snk_contents)
        {
            return core_services[handle].register(module_data, snk_contents);
        }


        private static IDictionary<string, SessionTimer> session_tm = new Dictionary<string, SessionTimer>();
        private static readonly object timer_lock = new object();

        /* This method must be invoked by the frontend application (SAFe) for each session. The Core Services will
         * save the last time it has been invoked and check whether it has not been invoked within the last
         * XXXX seconds. In such a case, the session is finished automatically */
        [WebMethod]
        public void heartbeat(string handle)
        {
            lock (timer_lock)
            {
                SessionTimer timer;
                if (session_tm.TryGetValue(handle, out timer))
                {
                    timer.Stop();
                    session_tm.Remove(handle);
                }

                timer = new SessionTimer(handle, 10 * 60 * 1000); // ten minutes
               // timer.AutoReset = true;
                timer.Elapsed += OnTimedEvent;
                timer.Start();
                session_tm[handle] = timer;
            }
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            lock (timer_lock)
            {
                SessionTimer session_timer = (SessionTimer)source;
                if (session_tm[session_timer.SessionID] == session_timer)
                {
                    string session_id = session_timer.SessionID;
                    close(session_id);
                }
            }
        }

        [WebMethod]
        public string getVirtualPlatformAddress(string handle, string platform_ref)
        {
            return core_services[handle].getVirtualPlatformAddress(platform_ref);
        }

        private class SessionTimer : Timer
        {
            private string session_id = null;
            public string SessionID { get { return session_id; } }

            public SessionTimer(string session_id, double interval) : base(interval)
            {
                this.session_id = session_id;
            }
        }
    }
}
